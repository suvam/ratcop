/**
 * Test Analysis
 * 
 * 1. Check the classes which get loaded.
 * 2. For each loaded class, print details of members (fields and methods)
 * 
 * Suvam Mukherjee, 2015
 */

package chord.analyses.suvamTests;


import chord.project.Chord;
import chord.project.ClassicProject;
import chord.project.analyses.JavaAnalysis;
import chord.program.Program;
import chord.util.IndexSet;
import joeq.Class.*;
import joeq.Compiler.*;
import joeq.Compiler.Quad.ControlFlowGraph; 

import chord.program.Program;
import joeq.Compiler.Quad.QuadVisitor;
import joeq.Class.jq_Method;
import joeq.Compiler.Quad.BasicBlock;
import joeq.Compiler.Quad.Quad;

import java.util.*;

@Chord(name = "jaiMa")

public class AnalysisTest1 extends JavaAnalysis {
	
	@Override public void run()
	{
		System.out.println("\nJai Ma!");
		String chordWorkDir = System.getProperty("chord.work.dir");
		System.out.println("\nChord working directory: " + chordWorkDir);
		System.out.println("\nTesting analysis and outputs...");
		
		Program program = Program.g();	// Denotes the program representation
		
		for(int i=0; i<40; i++)
			System.out.print("-");
		
		// Print the classes that may get loaded. 
		// Note that getClasses could return objects of type jq_Class or jq_Array, so we need a separate check for the classes
		System.out.println("\nPrinting list of classes which may get loaded: ");
		IndexSet<jq_Reference> references = program.getClasses();
		
		/**for(jq_Reference i: references)
		{
			if(i instanceof jq_Class && !(i.getName().contains("java.") || i.getName().contains("com.")))
			{
				i = (jq_Class) i;
				
				
				System.out.println("\nClass Name: " + i.getName());
			
				// For each class, print out the details: the signatures of the fields and the methods
			
				// Print details of static fields
				jq_StaticField[] staticFields = ((jq_Class) i).getDeclaredStaticFields();
				System.out.println("\nPrinting out static fields: ");
				
				for(jq_StaticField fld : staticFields)
				{
					System.out.println("\nField Name: " + fld.getName() + " with identifier " + fld.toString());
				}
				
				// Print details of instance fields
				jq_InstanceField[] instanceFields = ((jq_Class) i).getDeclaredInstanceFields();
				System.out.println("\nPrintout out instance fields: ");
				
				for(jq_InstanceField fld : instanceFields)
				{
					System.out.println("\nField Name: " + fld.getName() + " with identifier " + fld.toString());
				}
				
				// Print details of static methods
				jq_StaticMethod[] staticMethods = ((jq_Class) i).getDeclaredStaticMethods();
				System.out.println("\nPrinting out static methods: ");
				
				for(jq_StaticMethod mtd : staticMethods)
				{
					System.out.println("\nMethod Name: " + mtd.getName() + " with identifier " + mtd.toString());
				}
				
				
				// Print details of instance methods
				jq_InstanceMethod[] instanceMethods = ((jq_Class) i).getDeclaredInstanceMethods();
				System.out.println("\nPrinting out instance methods: ");
				
				for(jq_InstanceMethod mtd : instanceMethods)
				{
					System.out.println("\nMethod Name: " + mtd.getName() + " with identifier " + mtd.toString());
				} 
				
			}**/
		
		for(jq_Method m: program.getMethods())
		{
			if( !m.isAbstract() && !(m.getDeclaringClass().getName().contains("java.") || m.getDeclaringClass().getName().contains("com.") ))
			{
				ControlFlowGraph cfg = m.getCFG();
				List<BasicBlock> basicBlocks = cfg.reversePostOrder();
				Iterator<BasicBlock> it = basicBlocks.iterator();
				
				while(it.hasNext())
				{
					BasicBlock bb = it.next();
					
					for(int i=0; i<bb.size(); i++)
					{
						Quad q = bb.getQuad(i);
						System.out.println("Quad: " + q);
						
					}
					
				}
				
			}
			
		}
	}

}
