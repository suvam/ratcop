/**
 * Class implementing the semantics preserving transformation algorithm given in the paper.
 * 
 * For each class in the given program, extract each line.
 * If the line is an assignment, conditional or loop conditional, extract the variables, check which regions are read, and 
 * insert the appropriate region-based statement after it.
 * 
 * Suvam Mukherjee, 2015.
 * 
 * Input: Program P
 * Output: P' = TRANSFORM(P)
 */
package chord.analyses.ratcop;


/**
 * @author Suvam Mukherjee
 *
 */
import java.io.*;
import java.util.*;

import chord.project.Chord;
import chord.project.Chord;
import chord.project.ClassicProject;
import chord.project.analyses.JavaAnalysis;
import chord.program.Program;
import chord.util.IndexSet;
import joeq.Class.*;
import joeq.Compiler.*;

@Chord(name = "Transform")

public class TRANSFORM extends JavaAnalysis {

	/**
	 * Generic function to extract the variables from a given statement 
	 * 
	 * @param String s: input statement
	 * return variables[] : the set of variables in the statement s
	 */
	
	public static String[] extractVariables(String s)
	{
		String delimiters = "&|<>! =+-*/;\t\n\r\f";
		String sBackUp = s.substring(0, s.length());
		String[] variables; 
		int i = 0;
		
		StringTokenizer st = new StringTokenizer(sBackUp, delimiters);
		variables = new String[st.countTokens()];
		
		while(st.hasMoreTokens())
		{
			variables[i] = st.nextToken();
			i++;
		}
		
		// Get rid of any whitespaces before and after, just in case there is one
		for(i=0; i<variables.length; i++)
			variables[i] = variables[i].trim();
		
		return variables;
		
	}
	
	@SuppressWarnings("unchecked")
	@Override public void run() {
		// TODO Auto-generated method stub
		
		String[] variables;	// Holds the set of variables for each program statement
		String stmt = "";
		HashSet<String>[] regions = null;
		int numOfRegions;
		StringTokenizer st;	// StringTokenizer for general use
		
		// Code to read off each line of each class goes here
		
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		// Code to allow the user to specify the number of regions, and populate the regions
		try
		{
			System.out.println("Enter number of regions: ");
			numOfRegions = Integer.parseInt(br.readLine());
			regions = new HashSet[numOfRegions];
			
			for(int i=0; i<regions.length; i++)
			{
				regions[i] = new HashSet<String>();
			}
			
			System.out.println("\nEnter the variable names for each region: ");
			
			for(int i=0; i<regions.length; i++)
			{
				String choice = "y";
				String varName;
				
				System.out.println("\nPopulating Region "+ i + ": ");
				
				while(choice.equalsIgnoreCase("y"))
				{
					System.out.println("\nEnter variable name: ");
					varName = br.readLine();
					varName = varName.trim();
					regions[i].add(varName);
					
					System.out.println("\nEnter another? [y to continue]: ");
					choice = br.readLine();
					choice = choice.trim();
				}
			}
			
			
			System.out.println("\nEnter string: ");
			stmt = br.readLine();
			stmt = stmt.trim();
		}
		catch(IOException e)
		{
			System.out.println("\nSorry! An I/O Exception occurred!");
		}
		
		// Define the transformer for each kind of statement: assignment, conditional, loop, otherwise.
		
		// Case 1: Conditional or While Loop
		st = new StringTokenizer(stmt,"( \t\n\r\f");
		int conditionalFlag = 0;
		while(st.hasMoreTokens() && conditionalFlag==0)
		{
			String temp = st.nextToken();
			temp = temp.trim();
			if(temp.compareTo("if") == 0 || temp.compareTo("while")==0)
				conditionalFlag = 1;
		}
		if(conditionalFlag == 1)
		{
			// STEP 1: Compute the regions read
			
			// Extract everything beyond the first (
			st = new StringTokenizer(stmt, "(");
			int sizeOfConditional = st.countTokens();
			String s1 ="";
			
			// Drop the first token
			st.nextToken();
			// s1 is everything beyond the first (
			// s1 checked to be working correctly
			for (int i=2; i<=sizeOfConditional; i++)
				s1 += st.nextToken();
			
			// s2 is everything to the left of the last ) of s1
			st = new StringTokenizer(s1, ")");
			sizeOfConditional = st.countTokens();
			String s3 = "";
			
			for(int i=1; i<sizeOfConditional; i++)
				s3 += st.nextToken();
			
			String temp = st.nextToken();
			
			// If temp contains a {, it must be the code after the last )
			if(!(temp.contains("{")))
				s3 += temp;
			
			// s3 tested to be working correctly.
			// Check for the regions read
			
			variables = extractVariables(s3);
			//System.out.println("\nExtracted Variables: ");
			//for(int i=0; i<variables.length; i++)
			//	System.out.println(variables[i] + ", ");
			// Compute the regions read
			HashSet<Integer> conditionalRegions = new HashSet<Integer>();
			
			for(int i=0; i<variables.length; i++)
			{
				for(int j=0; j<regions.length; j++)
				{
					if(regions[j].contains(variables[i]))
					{
						conditionalRegions.add(j);
					}
				}
			}
			
			// STEP 2: Output the original conditional/loop, with the added r==r contraints
			String output = "";
			char[] condChars = stmt.toCharArray();
			int lastIndexOfBracket = 0;
			for(int i=condChars.length-1; i>=0; i--)
				if(condChars[i]==')')
				{
					lastIndexOfBracket = i-1;
					break;
				}
			
			output = stmt.substring(0, lastIndexOfBracket+1);
			
			Iterator it = conditionalRegions.iterator();
			
			while(it.hasNext())
			{
				int itNext = (Integer) it.next();
				output += " && (r" + itNext + "==" + "r" + itNext + ")";
			}
			for(int i=(lastIndexOfBracket+1); i<condChars.length; i++)
				output += condChars[i];
			
			System.out.println("\n******************************");
			System.out.println("Original statement: "+stmt);
			System.out.println("Added statement: " + output);
			
			/********** DEBUGGING CODE **********
			// Check if the regions are computed correctly
			System.out.println("Computed Regions: \n");
			Iterator it = conditionalRegions.iterator();
			while(it.hasNext())
				System.out.println(it.next() + " ");
			********** END DEBUG **********/
				
		}
		else
		{
			// Case 2: Assignments
			if(stmt.contains("="))	
			{
				variables = extractVariables(stmt);
				String lhs = variables[0];
				HashSet<Integer> lhsRegions = new HashSet<Integer>();
				HashSet<Integer> rhsRegions = new HashSet<Integer>();
				
				// Check the regions to which the lhs belongs. We need a fresh statement for each region to which lhs belongs.
				for(int i=0; i<regions.length; i++)
				{
					Iterator it = regions[i].iterator();
					
					while(it.hasNext())
					{
						String temp = (String) it.next();
						if(temp.compareTo(lhs)==0)
								lhsRegions.add(i);
					}
				}
				
				
				// Check all the regions that are read in the rhs.
				for(int i=1; i<variables.length; i++)
				{
					for(int j=0; j<regions.length; j++)
					{
						if(regions[j].contains(variables[i]))
						{
							rhsRegions.add(j);
						}
					}
				}
				
				/*********** DEBUGGING CODE **************	
				
				// Debugging: Has the regions for the lhs been correctly computed?
							System.out.println("\nLHS: " + lhs);
							System.out.println("LHS Regions: ");
							Iterator<Integer> it = lhsRegions.iterator();
							
							while(it.hasNext())
							{
								System.out.println(it.next() + ", ");
							}
							
				
				// Debugging: Has the regions for the rhs been correctly computed?
				System.out.println("\nRHS variables: ");
				
				for(int i=1; i<variables.length; i++)
				{
					System.out.println(variables[i]);
				}
				
				System.out.println("\nRHS Regions ");
				it = rhsRegions.iterator();
				
				while(it.hasNext())
				{
					System.out.println(it.next() + ", ");
				}
				
				********* END OF DEBUGGING CODE ***********/
				
				System.out.println("\n******************************");
				System.out.println("Original statement: "+stmt);
				System.out.println("Added statements: \n");
				
				// Compute the statement to output
				Iterator<Integer> lhsIterator = lhsRegions.iterator();
				
				while(lhsIterator.hasNext())
				{
					Iterator<Integer> rhsIterator = rhsRegions.iterator();
					
					String rhsExpression = "r" + rhsIterator.next();
					
					while(rhsIterator.hasNext())
					{
						rhsExpression += "+ r" + rhsIterator.next();
					}
					rhsExpression +=";";
					
					String output = "r" + lhsIterator.next() + " = " + rhsExpression;
					System.out.println(output + "\n");
				}
				
				
				
			}
			
		}
		
		
		

	}

}
