/**
 * 
 */
package myTests;

/**
 * @author Suvam Mukherjee
 *
 */
public class HelloWorld {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		HelloWorld a = new HelloWorld();
		HelloWorld b = new HelloWorld();
		
		a = b;
		b = new HelloWorld();
		
		System.out.println(a);
		System.out.println(b);

	}

}
