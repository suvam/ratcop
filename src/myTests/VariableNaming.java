/**
 * Test the variable naming pattern of Jimple. 
 * Test:
 * (a) how static variables are named
 * (b) how shared variables are named
 * (c) how local variablea are named
 * 
 * The shared variables should be named in similar fashion as local variables, as shared variables are passed as reference.
 */
package myTests;

/**
 * @author suvam
 *
 */

/**
 * A generic data class.
 * @author suvam
 *
 */
class D1 {
	private String s;
	private int x;
	
	public D1(String s) {
		this.s = s;
	}
	
	public void setS(String s) {
		this.s = s;
	}
	
	public String getS() {
		return s;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public int getX() {
		return x;
	}
}
public class VariableNaming {
	
	public static int value;
	
	public void foo1(D1 d1) {
		D1 d2 = new D1("hello");	// local variable
		d1.setS("Ma");	// access global variable
		System.out.println(d2.getS());
		System.out.println(d1.getS());
		value = 10;
		System.out.println(value);
		d1.setX(13);
		d2.setX(14);
		System.out.println(d1.getX());
		System.out.println(d2.getX());
	}
	
	public void foo2(D1 d1) {
		D1 d2 = new D1("hello");	// local variable
		d1.setS("Ma");	// access global variable
		System.out.println(d2.getS());
		System.out.println(d1.getS());
		value = 20;
		System.out.println(value);
		d1.setX(15);
		d2.setX(16);
		System.out.println(d1.getX());
		System.out.println(d2.getX());
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		D1 data = new D1("Puchki Pipi");
		VariableNaming vr = new VariableNaming();
		vr.foo1(data);
		vr.foo2(data);
		
		// Declare array variables
		int[] testArray = new int[10];
		
		for(int i=0; i<testArray.length; i++) {
			testArray[i] = 0;
		}
		
		int x = 10;
		int y = 20;
		int y1 = 50;
		int z = 0;
		testArray[1] = x + y;
		System.out.println(testArray[1]);
		
		z = x + y + y1;
		System.out.println(z);
		z = x - y;
		System.out.println(z);
		z = x / y;
		System.out.println(z);
		z = x*y;
		System.out.println(z);
		z = x % y;
		System.out.println(z);
		z = x + 30;
		System.out.println(z);
		z = x + (int)(25.5);
		System.out.println(z);
		z = 88 + 89;
		System.out.println(z);
	}

}
