/**
 * Test fixed point with join.
 */
package myTests;

class CT1_W1 extends Thread {
	
	public void run() {
		
		synchronized(CT1.lock) {
			CT1.x = 20;
		}
	}
}

class CT1_W2 extends Thread {
	
	public void run() {
		
		synchronized(CT1.lock) {
			
			if(!(CT1.x == 0))
				CT1.error = 1;
			
			if(!(CT1.x < 0))
				CT1.error = 1;
			
			if(!(CT1.x <= 20))
				CT1.error = 1;
		}
	}
}

/**
 * @author suvam
 *
 */
public class CT1 {
	
	public static int x;
	public static int error;
	public static Object lock;
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		CT1.lock = new Object();
		CT1.x = 0;
		CT1.error = 0;
		
		CT1_W1 t1 = new CT1_W1();
		CT1_W2 t2 = new CT1_W2();
		
		t1.start();
		t2.start();
		System.out.println("x: " + CT1.x);
		try {
			t1.join();
			t2.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
