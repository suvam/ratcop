/**
 * Testing operations on conditionals
 */
package myTests;

/**
 * @author Suvam Mukherjee.
 *
 * Indian Institute of Science, 2017.
 *
 */
class TestCond_T1 extends Thread {
	
	public void run() {
		
		while(true) {

			TestCond.a = 10;
			TestCond.b = TestCond.a + 1;

			if(TestCond.a < TestCond.b) {
				synchronized(TestCond.lock){
					TestCond.x++;
					TestCond.y++;
				}
			}
			else {
				synchronized(TestCond.lock) {
					TestCond.x = TestCond.x + 2;
					TestCond.y = TestCond.y + 2;
				}
			}
			
			synchronized(TestCond.lock) {
			if(!(TestCond.x >= 0))
				TestCond.error = 1;
			

			if(!(TestCond.y >= 0))
				TestCond.error = 1;
			
			if(!(TestCond.x == TestCond.y))
				TestCond.error = 1;
			}
		}
		
	}
}

class TestCond_T2 extends Thread {
	
	public void run() {
		System.out.println("\nStarting T2");
		TestCond.c = 30;
		
		synchronized(TestCond.lock) {
			TestCond.x++;
			TestCond.y++;
			
			if(!(TestCond.x == TestCond.y))
				TestCond.error = 1;
		}
	}
}
public class TestCond {

	public static int error;
	public static int a;
	public static int b;
	public static int c;
	public static int x;
	public static int y;
	public static Object lock;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		TestCond.error = 0;
		TestCond.a = 0;
		TestCond.b = 0;
		TestCond.c = 0;
		TestCond.x = 0;
		TestCond.y = 0;
		TestCond.lock = new Object();
		
		System.out.println("\nStarting 4 asserts...");
		
		TestCond_T1 t1 = new TestCond_T1();
		TestCond_T2 t2 = new TestCond_T2();
		t1.start();
		t2.start();
		
		try {
			t1.join();
			t2.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
