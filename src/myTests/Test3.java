/**
 * Compare a static variable with an instance variable
 */
package myTests;

import java.io.*;

/**
 * @author suvam
 *
 */
public class Test3 {
	public static int x = 0;
	
	public void foo() {
		int y = 0;
		Test3.x = 0;
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("\nEnter bound: ");
		int n = -1;
		try {
			n = Integer.parseInt(br.readLine().trim());
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		int i = n; 
		
		while(i >=0) {
			Test3.x--;
			y++;
			i--;
		}
		
		System.out.println("\nFinal x: " + x + ", Final y: " + y);
		
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Test3 obj = new Test3();
		obj.foo();

	}

}
