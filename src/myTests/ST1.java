/**
 * 
 */
package myTests;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author suvam
 *
 */
public class ST1 {

	public static int bound;
	public static int counter;
	public static int init;
	public static int error;
	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		ST1.error = 0;
		System.out.println("\nEnter bound: ");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		bound = Integer.parseInt(br.readLine());
		//		ST1.bound = 1000;
		
		if(ST1.bound >= 8) {
		
			ST1.init = 0;
			ST1.counter = 0;
			ST1.counter = ST1.init + ((ST1.bound / 4) - 1);
			
			if(!(ST1.init <= ST1.counter))
				ST1.error = 1;
		}
		
		System.out.println("\nDone");
	}

}
