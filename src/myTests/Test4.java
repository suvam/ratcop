/**
 * Test with static variables
 */
package myTests;

/**
 * @author suvam
 *
 */
public class Test4 {
	public static int x = 0;
	public static int y = 0;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Test4.x = 10;
		Test4.y = 20;
		int z= Test4.x + Test4.y;
		System.out.println(z);

	}

}
