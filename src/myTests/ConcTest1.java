/**
 * Simple concurrent program designed to test the sync-CFG construction
 */
package myTests;

/**
 * @author suvam
 *
 */

class Worker1 extends Thread {

	public Object lock;
	
	public void run() {
		synchronized(lock) {
			ConcTest1.x++;
		}
	}
}

class Worker2 extends Thread {
	
	public Object lock;
	
	public void run() {
		synchronized(lock) {
			ConcTest1.x++;
			
			if(!(ConcTest1.x >=0))
				ConcTest1.error = 1;
			
			if(!(ConcTest1.x <= 3))
				ConcTest1.error = 1;
		}
	}
}

public class ConcTest1{
	
	public static int x;
	public static int error;
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Worker1 t1 = new Worker1();
		Worker2 t2 = new Worker2();
		
		Object lock = new Object();
		
		ConcTest1.x = 0;
		ConcTest1.error = 0;
		
		
		t1.lock = lock;
		t2.lock = lock;
		
		t1.start();
		t2.start();
		
		try {
			t1.join();
			t2.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			System.err.println("\nAn Interrupted Exception occurred");
		}
		System.out.println(ConcTest1.x);
	}

}
