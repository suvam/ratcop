/**
 * 
 */
package myTests;

/**
 * @author suvam
 *
 */
public class ReferenceTest {
	
	public static void alterString(String s) {
		s = "newString";
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String s = "JaiMa";
		System.out.println("Pre String: " + s);
		alterString(s);
		System.out.println("Post String: " + s);
	}

}
