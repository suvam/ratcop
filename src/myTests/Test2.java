/**
 * Sequential test with loop without an a priori known bound.
 */
package myTests;

import java.io.*;

/**
 * @author suvam
 *
 */
public class Test2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("\nEnter bound: ");
		int n = Integer.parseInt(br.readLine().trim());
		int x = -1;
		int y = 0;
		int i = 0;
		
		while(i<=n){
			x--;
			y++;
			i++;
		}
//		System.out.println(y);
		System.out.println("\nFinal x: " + x +", Final y: " + y);
		
		

	}

}
