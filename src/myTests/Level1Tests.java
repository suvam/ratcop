/**
 * 
 */
package myTests;

import java.util.Arrays;

import gmp.*;
import apron.*;

/**
 * @author suvam
 *
 * Checking and Understanding Level 1 Primitives
 */
public class Level1Tests {

	
	// Abstract Domain Testing
	public static void testDomain(Manager man) throws ApronException
	{
		// Level 1
		String[] intNames = {"ia", "ib"};
		String[] intNames2 = {"ic", "id"};
		String[] realNames = {"ra", "rb"};
		
		Environment env = new Environment(intNames, realNames );
		Environment env2 = new Environment(intNames2, realNames);
		
		Linterm1[] xterms = 
			{
				new Linterm1("ia", new MpqScalar(-5)),
				new Linterm1("ib", new Interval(0.1, 0.6)),
				new Linterm1("ra", new MpfrScalar(0.1, Mpfr.RNDU)),
				new Linterm1("rb", new DoubleScalar(2.8))
			};
		Linexpr1 xLinearExpr = new Linexpr1(env, xterms, new MpfrScalar(0.2, Mpfr.RNDU));
		
		// Testing Level 1 Abstract Elements
		Abstract1 xfull = new Abstract1(man, env);	// new universal abstract element
		Abstract1 xempty = new Abstract1(man, env, true); // new univeral empty abstract element
		
		String[] varNames = {"x", "y", "z"};
		Linterm1[] terms =
			{
				new Linterm1("x", new MpqScalar(1)),
				new Linterm1("y", new MpqScalar(1))
			};
		// Create the expression x + y + 0
		Environment varEnv = new Environment(varNames, realNames);	// For weird reasons, can't create an environment with only ints. Add reals, and then remove them
		varEnv = varEnv.remove(realNames);
		Linexpr1 expr = new Linexpr1(varEnv, terms, new MpqScalar(0));
		System.out.println("Expression: " + expr);
		
		Lincons1 constraint = new Lincons1(Lincons1.SUPEQ, expr);	// constraint: x + y >= 0
		System.out.println("\nConstraint: " + constraint);
		
		// Abstract element takes an array of constraints as input
		Lincons1[] cons = {constraint};
		Abstract1 a = new Abstract1(man, cons);
		
		// Perform z = x + y
		// Create the tree expression for x + y
		Texpr1Node texpr = 
				new Texpr1BinNode(Texpr1BinNode.OP_ADD,
							new Texpr1VarNode("x"),
							new Texpr1VarNode("y")
						);
		Texpr1Intern addExpr = new Texpr1Intern(varEnv, texpr);
		
		// Apply the transformer for the assignment operator
		a.assign(man, "z", addExpr, null);
		System.out.println("\nResult: " + a ); 	// Perform z = x + y, results in z - x - y = 0, x + y >= 0
		
		// What if we now want to project out x and y to only obtain z >= 0?
		String z[] = {"z"};
		Environment zEnv = new Environment(z, realNames);
		zEnv = zEnv.remove(realNames);
		Abstract1 a2 = a.changeEnvironmentCopy(man, zEnv, true);
		System.out.println("\nProjected Environment: " + a2);
		
		
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		
		System.out.println("\nEnvironments: ");
		Environment env1 = new Environment();
		
		String n1[] = {"ia", "ib", "ic"};	// Integer Valued Variables
		String n2[] = {"ra", "rb", "rc"};	// Real valued variables
		
		String n3[] = {"rd", "re"};	
		
		//sEnvironment env2 = new Environment(n1, n2);
		env1 = env1.add(n1, null);	// add does not modify this
		env1 = env1.add(n3,  null);
		env1 = env1.remove(n3);	// remove does not modify this
		
		// Empty out env1
		env1 = env1.remove(n1);
		System.out.println("\nEmpty env1: " + env1);
		
		// Add stuff back to the environment
		env1 = env1.add(n1, n2);
		System.out.println("\nEnv1: " + env1);
		
		// Print all the variables out in lexicographic order
		System.out.println("Env1: " + Arrays.toString(env1.getVars()));
		// Print out the integers in lex order
		System.out.println("\nEnv1 Integers: " + Arrays.toString(env1.getIntVars()));
		// Print out reals in lex order
		System.out.println("\nEnv1 Reals: " + Arrays.toString(env1.getRealVars()));
		
		// Print dimension: prints number of integer variables and number of real variables in the environment
		System.out.println("\nEnv1 Dimension: " + env1.getDimension());
		
		// Check if a variable is present in an environment
		System.out.println("\nEnv1 status of ia: " + env1.hasVar("ia") + " ** status of id: " + env1.hasVar("id"));
		
		// Dimension of a variable and variable of a dimension
		System.out.println("\nDimension of ia: " + env1.dimOfVar("ia"));
		// dimension is the dimension of the environment vector
		System.out.println("\nEnv1 variable at dimension 3: " + env1.varOfDim(3));
		
		Dimperm[] dp = new Dimperm[1];
		Environment env2 = env1.clone();
		
		System.out.println("\nAdd: " + env2.addPerm(null, n3, dp));
		System.out.println("\nPermutation: " + dp[0]);
		
		Environment env3 = env2.add(null, n3);
		// env2 = n1, n2   env3 = n1, <n2, n3>
		System.out.println("\nEnv3 with n3 removed: " + env3.remove(n3));
		
		// Comparisons
		// n2 == n3?
		System.out.println("\nEnv3 == Env2? " + env3.isEqual(env2));
		env3 = env3.remove(n3);
		System.out.println("\nEnv3 == Env2? " + env3.isEqual(env2));
		
		env3.add(null, n3);
		System.out.println("\nEnv2 subset of Env3? " + env2.isIncluded(env3));
	
		Environment env4 = new Environment(n1, n3);
		// LCE: Lease Common Environment is essentially a Union of the environments of this and the operand
		System.out.println("\nLCE test: " + env2.lce(env4));
		
		Environment[] envs = {env1, env2, env4};
		System.out.println("\nUnion (LCE) of Env1, Env2 and Env4: " + Environment.lce(envs));
		
		String[] intRen = {"ia1", "ib1"};
		String[] originalInt = {"ia", "ib"};
		String[] realRen = {"ra1", "rb1", "rc1"};
		System.out.println("\nRenaming full environment: " + env2.rename(originalInt, intRen));
		System.out.println("\nRenaming full environment: " + env2.rename(originalInt, intRen, dp));
		System.out.println("\nPermutation: " + dp[0]);
		
		// Not sure what these two functions do
		System.out.println("\nDimension change: " + env2.dimchange(env3));
		System.out.println("dimchange2: " + Arrays.toString(env2.dimchange2(env4)));
		
		// LINEAR EXPRESSION TEST
		System.out.println("\n******** LINEAR EXPRESSION TEST ********");
		System.out.println("Env2: " + env2);
		Linexpr1 ll1 = new Linexpr1(env2);
		Linexpr1 ll2 = new Linexpr1(env2, 3);	// specify size of the linear expression
		ll1.setCst(new MpqScalar(1,2));
		ll1.setCoeff("ia", new MpfrScalar(1.23, Mpfr.RNDU));	// RNDU rounds towards + inf
		ll1.setCoeff("ib", new DoubleScalar(1.52));
		ll1.setCoeff("ic", new Interval(1.2, 2.3));
		ll1.setCoeff("ra", new Interval(new Mpq(4,5), new Mpq(6,7)));
		ll1.setCoeff("rb", new Interval(new MpfrScalar(1.34, Mpfr.RNDU), new MpfrScalar(2.37, Mpfr.RNDU)));
		ll1.setCoeff("rc", new DoubleScalar(-2));
		
		System.out.println("LL1: " + ll1);
		ll1.minimize();
		System.out.println("Minimized LL1: " + ll1);
		
		// Get terms: integers before reals, everything is returned in lexicographic order
		System.out.println("\nLL1 terms: " + Arrays.toString(ll1.getLinterms()));
		
		// Test Convex Polyhedra
		testDomain(new Polka(true));
		
	}

}
