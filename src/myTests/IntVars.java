/**
 * Simple program with variables of different data-types.
 * Used to test if the PrintVars analysis is correctly able to identify the integers.
 */
package myTests;

/**
 * @author suvam
 *
 */
public class IntVars {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int x = 10;
		double y = 20;
		int x2 = 30;
		
		double z = 2*y;
		System.out.println(z);
		
		x = 2*x + x2;
		System.out.println(x);

	}

}
