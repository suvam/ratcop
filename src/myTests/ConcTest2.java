/**
 * Testing two threads manipulating a disjoint set of variables.
 */
package myTests;

import java.io.*;
/**
 * @author suvam
 *
 */
class ConcTest2_Worker1 extends Thread {
	
	
	public void run() {
		int i = ConcTest2.n1;
		
		while(i >= 0) {
			ConcTest2.x1--;
			ConcTest2.y1++;
			i--;
		}
		
	}
}

class ConcTest2_Worker2 extends Thread {
	
	
	public void run() {
		int i = ConcTest2.n2;
		
		while(i >= 0) {
			ConcTest2.x2++;
			ConcTest2.y1--;
			i--;
		}
		
	}
}

public class ConcTest2 {
	public static int n1, n2;
	public static int x1, y1, x2, y2;
	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("\nEnter n1: ");
		ConcTest2.n1 = Integer.parseInt(br.readLine().trim());
		System.out.println("\nEnter n2: ");
		ConcTest2.n2 = Integer.parseInt(br.readLine().trim());
		
		ConcTest2.x1 = 0;
		ConcTest2.x2 = 0;
		ConcTest2.y1 = 0;
		ConcTest2.y2 = 0;
		
		ConcTest2_Worker1 t1 = new ConcTest2_Worker1();
		ConcTest2_Worker2 t2 = new ConcTest2_Worker2();
		
		t1.start();
		t2.start();
		
		try {
			t1.join();
			t2.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			System.err.println("\nError");
		}
		
		System.out.println("Test done");
		
	}

}
