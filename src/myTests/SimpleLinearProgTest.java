/**
 * Testing a basic program for inferred invariants with region decomposition:
 * 
 * u = x;
 * v = y;
 * 
 * u = u + 1;
 * v = v + 1;
 * 
 * x = u;
 * y = v;
 * 
 * No loops. No threads.
 * r1 = {x,y}
 * r2 = {u,v}
 */
package myTests;

/**
 * @author suvam
 * macbook-pro
 *
 */
import java.util.Arrays;

import gmp.*;
import apron.*;

public class SimpleLinearProgTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws ApronException {
		// TODO Auto-generated method stub
		
		Manager man = new Polka(true);	// Allow convex polyhedra with strict inequalities
		
		String[] r1Vars = {"x", "y"};
		String[] r2Vars = {"u", "v"};
		
		String[] realVars = {"dummy"};
		
		// Create the regions, which are environments in Apron terms
		Environment r1 = new Environment(r1Vars, realVars);
		r1.remove(realVars);
		Environment r2 = new Environment(r2Vars, realVars);
		r2.remove(realVars);
		
		// Print the variables
		Var[] integerVars = r1.getIntVars();
		for(int i=0; i<integerVars.length; i++) {
			System.out.println(integerVars[i]);
		}
		
		// Test creation of a bottom element
		Lincons1[] emptyCons = new Lincons1[1];
		emptyCons[0] = new Lincons1(r1, false);
		Abstract1 testBot = new Abstract1(man, emptyCons);
		System.out.println("\ntestBot is: " + testBot.isBottom(man));
		
		// Init: x = y = 1, u = v = 0
		
		// Region 1 Initial Constraints
		
		// terms for the constraint x - y = 0
		Linterm1[] r1terms1 =
			{
				new Linterm1("x", new MpqScalar(1)),
				new Linterm1("y", new MpqScalar(-1))
			};
		
		// terms for the constraint x = 1
		Linterm1[] r1term2 = 
			{
				new Linterm1("x", new MpqScalar(1))
			};
		
		Linexpr1 r1expr1 = new Linexpr1(r1.lce(r2), r1terms1, new MpqScalar(0));
		Linexpr1 r1expr2 = new Linexpr1(r1.lce(r2), r1term2, new MpqScalar(-1));
		
		
		// Region 2 Initial Constraints
		
		// terms for u - v = 0
		Linterm1[] r2terms1 = 
			{
				new Linterm1("u", new MpqScalar(1)),
				new Linterm1("v", new MpqScalar(-1))
			};
		
		Linterm1[] r2terms2 = 
			{
				new Linterm1("u", new MpqScalar(1))
			};
		
		Linexpr1 r2expr1 = new Linexpr1(r2.lce(r1), r2terms1, new MpqScalar(0));
		Linexpr1 r2expr2 = new Linexpr1(r2.lce(r1), r2terms2, new MpqScalar(0));
		
		
		// Initial constraints
		Lincons1[] initCons =
			{
				new Lincons1(Lincons1.EQ, r1expr1),
				new Lincons1(Lincons1.EQ, r1expr2),
				new Lincons1(Lincons1.EQ, r2expr1),
				new Lincons1(Lincons1.EQ, r2expr2)
			};
		
		System.out.println("\nPP1: " + Arrays.toString(initCons));
		

		// Statement 1: u = x
		Texpr1Node s = new Texpr1BinNode
				(
					Texpr1BinNode.OP_ADD,
					new Texpr1VarNode("x"),
					new Texpr1CstNode(new MpqScalar(0))
				);
		// The footprint of this operation is r1 and r2, hence we use lce to union them
		Texpr1Intern stmt = new Texpr1Intern(r1.lce(r2), s);

		Abstract1 a = new Abstract1(man, initCons);
		
		a.assign(man, "u", stmt, null);	// perform u = x
		
		// Project the computed facts onto the respective regions
		Lincons1[] r1cons = a.changeEnvironmentCopy(man, r1, true).toLincons(man);
		Lincons1[] r2cons = a.changeEnvironmentCopy(man, r2, true).toLincons(man);
		
		System.out.println("**************** PP2 ********************");
		System.out.println("\nR1 cons: " + Arrays.toString(r1cons));
		System.out.println("\nR2 cons: " + Arrays.toString(r2cons));
		a = null;
		
		// v = y
		s = new Texpr1BinNode
				(
					Texpr1BinNode.OP_ADD,
					new Texpr1VarNode("y"),
					new Texpr1CstNode(new MpqScalar(0))
				);
		
		Lincons1[] cons = new Lincons1[r1cons.length + r2cons.length];
		int i = 0;
		for(Lincons1 r1consTemp: r1cons)
		{
			cons[i] = r1consTemp.extendEnvironmentCopy(r1.lce(r2));
			i++;
		}
		for(Lincons1 r2consTemp: r2cons)
		{
			cons[i] = r2consTemp.extendEnvironmentCopy(r2.lce(r1));
			i++;
		}
		stmt = new Texpr1Intern(r1.lce(r2), s);
		a = new Abstract1(man, cons);
		
		a.assign(man, "v", stmt, null);
		
		r1cons = a.changeEnvironmentCopy(man, r1, true).toLincons(man);
		r2cons = a.changeEnvironmentCopy(man, r2, true).toLincons(man);
		
		System.out.println("**************** PP3 ********************");
		System.out.println("\nR1 cons: " + Arrays.toString(r1cons));
		System.out.println("\nR2 cons: " + Arrays.toString(r2cons));
		a = null;
		
		// u = u + 1
		s = new Texpr1BinNode
				(
					Texpr1BinNode.OP_ADD,
					new Texpr1VarNode("u"),
					new Texpr1CstNode(new MpqScalar(1))
				);
		stmt = new Texpr1Intern(r2, s);
		a = new Abstract1(man, r2cons);
		a.assign(man, "u", stmt, null);
		
		r2cons = a.changeEnvironmentCopy(man, r2, true).toLincons(man);
		
		System.out.println("**************** PP4 ********************");
		System.out.println("\nR1 cons: " + Arrays.toString(r1cons));
		System.out.println("\nR2 cons: " + Arrays.toString(r2cons));
		a = null;
		
		// v = v + 1
		s = new Texpr1BinNode
				(
					Texpr1BinNode.OP_ADD,
					new Texpr1VarNode("v"),
					new Texpr1CstNode(new MpqScalar(1))
				);
		stmt = new Texpr1Intern(r2, s);
		a = new Abstract1(man, r2cons);
		a.assign(man, "v", stmt, null);
		
		r2cons = a.changeEnvironmentCopy(man, r2, true).toLincons(man);
		
		System.out.println("**************** PP4 ********************");
		System.out.println("\nR1 cons: " + Arrays.toString(r1cons));
		System.out.println("\nR2 cons: " + Arrays.toString(r2cons));
		a = null;
		
		// x = u
		s = new Texpr1BinNode
				(
					Texpr1BinNode.OP_ADD,
					new Texpr1VarNode("u"),
					new Texpr1CstNode(new MpqScalar(0))
				);
		
		cons = new Lincons1[r1cons.length + r2cons.length];
		i = 0;
		for(Lincons1 r1consTemp: r1cons)
		{
			cons[i] = r1consTemp.extendEnvironmentCopy(r1.lce(r2));
			i++;
		}
		for(Lincons1 r2consTemp: r2cons)
		{
			cons[i] = r2consTemp.extendEnvironmentCopy(r2.lce(r1));
			i++;
		}
		stmt = new Texpr1Intern(r1.lce(r2), s);
		a = new Abstract1(man, cons);
		
		a.assign(man, "x", stmt, null);
		
		r1cons = a.changeEnvironmentCopy(man, r1, true).toLincons(man);
		r2cons = a.changeEnvironmentCopy(man, r2, true).toLincons(man);
		
		System.out.println("**************** PP5 ********************");
		System.out.println("\nR1 cons: " + Arrays.toString(r1cons));
		System.out.println("\nR2 cons: " + Arrays.toString(r2cons));
		a = null;
		
		// y = v
	
		s = new Texpr1BinNode
				(
						Texpr1BinNode.OP_ADD,
						new Texpr1VarNode("v"),
						new Texpr1CstNode(new MpqScalar(0))
						);

		cons = new Lincons1[r1cons.length + r2cons.length];
		i = 0;
		for(Lincons1 r1consTemp: r1cons)
		{
			cons[i] = r1consTemp.extendEnvironmentCopy(r1.lce(r2));
			i++;
		}
		for(Lincons1 r2consTemp: r2cons)
		{
			cons[i] = r2consTemp.extendEnvironmentCopy(r2.lce(r1));
			i++;
		}
		stmt = new Texpr1Intern(r1.lce(r2), s);
		a = new Abstract1(man, cons);

		a.assign(man, "y", stmt, null);

		r1cons = a.changeEnvironmentCopy(man, r1, true).toLincons(man);
		r2cons = a.changeEnvironmentCopy(man, r2, true).toLincons(man);

		System.out.println("**************** PP6 ********************");
		System.out.println("\nR1 cons: " + Arrays.toString(r1cons));
		System.out.println("\nR2 cons: " + Arrays.toString(r2cons));
		//a = null;

		// Test if a join is possible between abstract elements with different environments: TESTED NOT POSSIBLE
//		Abstract1 a1 = new Abstract1(man, r1cons);
//		Abstract1 a2 = new Abstract1(man, r2cons);
//		a1.join(man, a2);
//		System.out.println(a1);
		
		// Create a sub-environment
		Abstract1 r1Env = a.changeEnvironmentCopy(man, r1, true); 
		String[] xVar = new String[1];
		xVar[0] = "x";
		Environment xEnv = new Environment(xVar, realVars);
		xEnv.remove(realVars);
		//Get the constraints in-terms of x only
		Lincons1[] xCons = a.changeEnvironmentCopy(man, xEnv, true).toLincons(man);
		System.out.println("\nConstraints on x: " + Arrays.toString(xCons));
		//Create abstract element with sub-environment
		Abstract1 xAbs = new Abstract1(man, xCons);
		xAbs.changeEnvironment(man, r1, true);
		System.out.println("\nNew environment: " + xAbs.getEnvironment());
		System.out.println("\nJoined: " + Arrays.toString(r1Env.toLincons(man)));
		r1Env.join(man, xAbs);
		System.out.println("\nJoined: " + Arrays.toString(r1Env.toLincons(man)));
		

	}

}
