/**
 * Testing sequential conditionals
 */
package myTests;

import java.util.Random;

/**
 * @author Suvam Mukherjee.
 *
 * Indian Institute of Science, 2017.
 *
 */
public class SeqCondTest {

	public static int error;
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Random rand = new Random();
		int a = 0;
		int b = 0;
		int x = 0;
		SeqCondTest.error = 0;
		System.out.println("\nTesting...");
		while(true) {
			a = rand.nextInt();
			b = a + 1;
			if(a<b)
				x++;
			else
				x = x+2;
	
			if(!(x >= 0))
				SeqCondTest.error = 1;
		}
	}

}
