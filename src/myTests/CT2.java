/**
 * 
 */
package myTests;

/**
 * @author suvam
 *
 */

class W1_CT2 extends Thread {
	
	public void run() {
		
		synchronized(CT2.lock) {
			CT2.x++;
			
			if(!(CT2.x >= 0))
				CT2.error = 1;
				
		}
	}
}

class W2_CT2 extends Thread {
	
	public void run() {
		
		synchronized(CT2.lock) {
			CT2.x++;
		}
	}
}

public class CT2 {

	public static int x;
	public static Object lock;
	public static int error;
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		CT2.x = 0;
		CT2.error = 0;
		lock = new Object();
		
		W1_CT2 t1 = new W1_CT2();
		W2_CT2 t2 = new W2_CT2();
		
		t1.start();
		t2.start();
		
		try {
			t1.join();
			t2.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println(CT2.x);
	}

}
