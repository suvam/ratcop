/**
 * Checking soundness of inter-thread join
 */
package myTests;

/**
 * @author suvam
 *
 */

class ConcTest3_T1 extends Thread {
	
	public Object lock;
	
	public void run() {
		
		synchronized(lock) {
			ConcTest3.x++;
			ConcTest3.y++;
			
			if(!(ConcTest3.x >= 0))
				ConcTest3.error = 1;
			
			if(!(ConcTest3.y >= 0))
				ConcTest3.error = 1;
			
//			if(!(ConcTest3.x <= 4))
//				ConcTest3.error = 1;
			
			if(! (ConcTest3.x == ConcTest3.y))
				ConcTest3.error = 1;
		}
	}
	
}

class ConcTest3_T2 extends Thread {
	
	public Object lock;
	
	public void run() {
		
		synchronized(lock) {
			ConcTest3.x++;
			ConcTest3.y++;
			
//			if(!(ConcTest3.x <= 4))
//				ConcTest3.error = 1;
			
			if(!(ConcTest3.y >= 0))
				ConcTest3.error = 1;
			
			if(! (ConcTest3.x == ConcTest3.y))
				ConcTest3.error = 1;
		}
	}
	
}

public class ConcTest3 {
	
	public static int x;
	public static int y;
	public static int error = 0;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ConcTest3_T1 t1 = new ConcTest3_T1();
		ConcTest3_T2 t2 = new ConcTest3_T2();
		Object lock = new Object();
		ConcTest3.x = 0;
		ConcTest3.y = 0;
		
		t1.lock = lock;
		t2.lock = lock;
		
		t1.start();
		t2.start();
		
		try {
			t1.join();
			t2.join();
		}
		catch(InterruptedException e) {
			System.err.println("\nAn Interrupted Exception occurred");
		}
		System.out.println("\nFin x: " + ConcTest3.x + " fin y: " + ConcTest3.y);
	}

}
