// Auto-generated stress test 
// Suvam Mukherjee, 2017 
 
package stress.fixedthr; 

class W0_SOS_3_4 extends Thread { 
	 public void run() {
	 	 while(true) { 
			 SOS_3_4.x0=0; 
			SOS_3_4.x1 =SOS_3_4.x0 + 1;
			SOS_3_4.x2 =SOS_3_4.x1 + 1;
		 }
	 }
}
class W1_SOS_3_4 extends Thread { 
	 public void run() {
	 	 while(true) { 
			 SOS_3_4.x3=3; 
			SOS_3_4.x4 =SOS_3_4.x3 + 1;
			SOS_3_4.x5 =SOS_3_4.x4 + 1;
		 }
	 }
}
class W2_SOS_3_4 extends Thread { 
	 public void run() {
	 	 while(true) { 
			 SOS_3_4.x6=6; 
			SOS_3_4.x7 =SOS_3_4.x6 + 1;
			SOS_3_4.x8 =SOS_3_4.x7 + 1;
		 }
	 }
}
class W3_SOS_3_4 extends Thread { 
	 public void run() {
	 	 while(true) { 
			 SOS_3_4.x9=9; 
			SOS_3_4.x10 =SOS_3_4.x9 + 1;
			SOS_3_4.x11 =SOS_3_4.x10 + 1;
		 }
	 }
}
public class SOS_3_4 { 
public static int x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11;

	 public static void main(String[] args) throws InterruptedException { 
		 SOS_3_4.x0=0; 
 		 SOS_3_4.x1=0; 
 		 SOS_3_4.x2=0; 
 		 SOS_3_4.x3=0; 
 		 SOS_3_4.x4=0; 
 		 SOS_3_4.x5=0; 
 		 SOS_3_4.x6=0; 
 		 SOS_3_4.x7=0; 
 		 SOS_3_4.x8=0; 
 		 SOS_3_4.x9=0; 
 		 SOS_3_4.x10=0; 
 		 SOS_3_4.x11=0;

		 W0_SOS_3_4 t0 = new W0_SOS_3_4(); 
		 W1_SOS_3_4 t1 = new W1_SOS_3_4(); 
		 W2_SOS_3_4 t2 = new W2_SOS_3_4(); 
		 W3_SOS_3_4 t3 = new W3_SOS_3_4(); 
		 t0.start(); 
		 t1.start(); 
		 t2.start(); 
		 t3.start(); 
		 t0.join(); 
		 t1.join(); 
		 t2.join(); 
		 t3.join(); 
	 }
}
