// Auto-generated stress test 
// Suvam Mukherjee, 2017 
 
package stress.fixedthr; 

class W0_SOS_8_4 extends Thread { 
	 public void run() {
	 	 while(true) { 
			 SOS_8_4.x0=0; 
			SOS_8_4.x1 =SOS_8_4.x0 + 1;
			SOS_8_4.x2 =SOS_8_4.x1 + 1;
			SOS_8_4.x3 =SOS_8_4.x2 + 1;
			SOS_8_4.x4 =SOS_8_4.x3 + 1;
			SOS_8_4.x5 =SOS_8_4.x4 + 1;
			SOS_8_4.x6 =SOS_8_4.x5 + 1;
			SOS_8_4.x7 =SOS_8_4.x6 + 1;
		 }
	 }
}
class W1_SOS_8_4 extends Thread { 
	 public void run() {
	 	 while(true) { 
			 SOS_8_4.x8=8; 
			SOS_8_4.x9 =SOS_8_4.x8 + 1;
			SOS_8_4.x10 =SOS_8_4.x9 + 1;
			SOS_8_4.x11 =SOS_8_4.x10 + 1;
			SOS_8_4.x12 =SOS_8_4.x11 + 1;
			SOS_8_4.x13 =SOS_8_4.x12 + 1;
			SOS_8_4.x14 =SOS_8_4.x13 + 1;
			SOS_8_4.x15 =SOS_8_4.x14 + 1;
		 }
	 }
}
class W2_SOS_8_4 extends Thread { 
	 public void run() {
	 	 while(true) { 
			 SOS_8_4.x16=16; 
			SOS_8_4.x17 =SOS_8_4.x16 + 1;
			SOS_8_4.x18 =SOS_8_4.x17 + 1;
			SOS_8_4.x19 =SOS_8_4.x18 + 1;
			SOS_8_4.x20 =SOS_8_4.x19 + 1;
			SOS_8_4.x21 =SOS_8_4.x20 + 1;
			SOS_8_4.x22 =SOS_8_4.x21 + 1;
			SOS_8_4.x23 =SOS_8_4.x22 + 1;
		 }
	 }
}
class W3_SOS_8_4 extends Thread { 
	 public void run() {
	 	 while(true) { 
			 SOS_8_4.x24=24; 
			SOS_8_4.x25 =SOS_8_4.x24 + 1;
			SOS_8_4.x26 =SOS_8_4.x25 + 1;
			SOS_8_4.x27 =SOS_8_4.x26 + 1;
			SOS_8_4.x28 =SOS_8_4.x27 + 1;
			SOS_8_4.x29 =SOS_8_4.x28 + 1;
			SOS_8_4.x30 =SOS_8_4.x29 + 1;
			SOS_8_4.x31 =SOS_8_4.x30 + 1;
		 }
	 }
}
public class SOS_8_4 { 
public static int x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31;

	 public static void main(String[] args) throws InterruptedException { 
		 SOS_8_4.x0=0; 
 		 SOS_8_4.x1=0; 
 		 SOS_8_4.x2=0; 
 		 SOS_8_4.x3=0; 
 		 SOS_8_4.x4=0; 
 		 SOS_8_4.x5=0; 
 		 SOS_8_4.x6=0; 
 		 SOS_8_4.x7=0; 
 		 SOS_8_4.x8=0; 
 		 SOS_8_4.x9=0; 
 		 SOS_8_4.x10=0; 
 		 SOS_8_4.x11=0; 
 		 SOS_8_4.x12=0; 
 		 SOS_8_4.x13=0; 
 		 SOS_8_4.x14=0; 
 		 SOS_8_4.x15=0; 
 		 SOS_8_4.x16=0; 
 		 SOS_8_4.x17=0; 
 		 SOS_8_4.x18=0; 
 		 SOS_8_4.x19=0; 
 		 SOS_8_4.x20=0; 
 		 SOS_8_4.x21=0; 
 		 SOS_8_4.x22=0; 
 		 SOS_8_4.x23=0; 
 		 SOS_8_4.x24=0; 
 		 SOS_8_4.x25=0; 
 		 SOS_8_4.x26=0; 
 		 SOS_8_4.x27=0; 
 		 SOS_8_4.x28=0; 
 		 SOS_8_4.x29=0; 
 		 SOS_8_4.x30=0; 
 		 SOS_8_4.x31=0;

		 W0_SOS_8_4 t0 = new W0_SOS_8_4(); 
		 W1_SOS_8_4 t1 = new W1_SOS_8_4(); 
		 W2_SOS_8_4 t2 = new W2_SOS_8_4(); 
		 W3_SOS_8_4 t3 = new W3_SOS_8_4(); 
		 t0.start(); 
		 t1.start(); 
		 t2.start(); 
		 t3.start(); 
		 t0.join(); 
		 t1.join(); 
		 t2.join(); 
		 t3.join(); 
	 }
}
