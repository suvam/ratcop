// Auto-generated stress test 
// Suvam Mukherjee, 2017 
 
package stress.fixedthr; 

class W0_SOS_5_4 extends Thread { 
	 public void run() {
	 	 while(true) { 
			 SOS_5_4.x0=0; 
			SOS_5_4.x1 =SOS_5_4.x0 + 1;
			SOS_5_4.x2 =SOS_5_4.x1 + 1;
			SOS_5_4.x3 =SOS_5_4.x2 + 1;
			SOS_5_4.x4 =SOS_5_4.x3 + 1;
		 }
	 }
}
class W1_SOS_5_4 extends Thread { 
	 public void run() {
	 	 while(true) { 
			 SOS_5_4.x5=5; 
			SOS_5_4.x6 =SOS_5_4.x5 + 1;
			SOS_5_4.x7 =SOS_5_4.x6 + 1;
			SOS_5_4.x8 =SOS_5_4.x7 + 1;
			SOS_5_4.x9 =SOS_5_4.x8 + 1;
		 }
	 }
}
class W2_SOS_5_4 extends Thread { 
	 public void run() {
	 	 while(true) { 
			 SOS_5_4.x10=10; 
			SOS_5_4.x11 =SOS_5_4.x10 + 1;
			SOS_5_4.x12 =SOS_5_4.x11 + 1;
			SOS_5_4.x13 =SOS_5_4.x12 + 1;
			SOS_5_4.x14 =SOS_5_4.x13 + 1;
		 }
	 }
}
class W3_SOS_5_4 extends Thread { 
	 public void run() {
	 	 while(true) { 
			 SOS_5_4.x15=15; 
			SOS_5_4.x16 =SOS_5_4.x15 + 1;
			SOS_5_4.x17 =SOS_5_4.x16 + 1;
			SOS_5_4.x18 =SOS_5_4.x17 + 1;
			SOS_5_4.x19 =SOS_5_4.x18 + 1;
		 }
	 }
}
public class SOS_5_4 { 
public static int x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19;

	 public static void main(String[] args) throws InterruptedException { 
		 SOS_5_4.x0=0; 
 		 SOS_5_4.x1=0; 
 		 SOS_5_4.x2=0; 
 		 SOS_5_4.x3=0; 
 		 SOS_5_4.x4=0; 
 		 SOS_5_4.x5=0; 
 		 SOS_5_4.x6=0; 
 		 SOS_5_4.x7=0; 
 		 SOS_5_4.x8=0; 
 		 SOS_5_4.x9=0; 
 		 SOS_5_4.x10=0; 
 		 SOS_5_4.x11=0; 
 		 SOS_5_4.x12=0; 
 		 SOS_5_4.x13=0; 
 		 SOS_5_4.x14=0; 
 		 SOS_5_4.x15=0; 
 		 SOS_5_4.x16=0; 
 		 SOS_5_4.x17=0; 
 		 SOS_5_4.x18=0; 
 		 SOS_5_4.x19=0;

		 W0_SOS_5_4 t0 = new W0_SOS_5_4(); 
		 W1_SOS_5_4 t1 = new W1_SOS_5_4(); 
		 W2_SOS_5_4 t2 = new W2_SOS_5_4(); 
		 W3_SOS_5_4 t3 = new W3_SOS_5_4(); 
		 t0.start(); 
		 t1.start(); 
		 t2.start(); 
		 t3.start(); 
		 t0.join(); 
		 t1.join(); 
		 t2.join(); 
		 t3.join(); 
	 }
}
