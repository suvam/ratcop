// Auto-generated stress test 
// Suvam Mukherjee, 2017 
 
package stress.fixedthr; 

class W0_SOS_4_4 extends Thread { 
	 public void run() {
	 	 while(true) { 
			 SOS_4_4.x0=0; 
			SOS_4_4.x1 =SOS_4_4.x0 + 1;
			SOS_4_4.x2 =SOS_4_4.x1 + 1;
			SOS_4_4.x3 =SOS_4_4.x2 + 1;
		 }
	 }
}
class W1_SOS_4_4 extends Thread { 
	 public void run() {
	 	 while(true) { 
			 SOS_4_4.x4=4; 
			SOS_4_4.x5 =SOS_4_4.x4 + 1;
			SOS_4_4.x6 =SOS_4_4.x5 + 1;
			SOS_4_4.x7 =SOS_4_4.x6 + 1;
		 }
	 }
}
class W2_SOS_4_4 extends Thread { 
	 public void run() {
	 	 while(true) { 
			 SOS_4_4.x8=8; 
			SOS_4_4.x9 =SOS_4_4.x8 + 1;
			SOS_4_4.x10 =SOS_4_4.x9 + 1;
			SOS_4_4.x11 =SOS_4_4.x10 + 1;
		 }
	 }
}
class W3_SOS_4_4 extends Thread { 
	 public void run() {
	 	 while(true) { 
			 SOS_4_4.x12=12; 
			SOS_4_4.x13 =SOS_4_4.x12 + 1;
			SOS_4_4.x14 =SOS_4_4.x13 + 1;
			SOS_4_4.x15 =SOS_4_4.x14 + 1;
		 }
	 }
}
public class SOS_4_4 { 
public static int x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15;

	 public static void main(String[] args) throws InterruptedException { 
		 SOS_4_4.x0=0; 
 		 SOS_4_4.x1=0; 
 		 SOS_4_4.x2=0; 
 		 SOS_4_4.x3=0; 
 		 SOS_4_4.x4=0; 
 		 SOS_4_4.x5=0; 
 		 SOS_4_4.x6=0; 
 		 SOS_4_4.x7=0; 
 		 SOS_4_4.x8=0; 
 		 SOS_4_4.x9=0; 
 		 SOS_4_4.x10=0; 
 		 SOS_4_4.x11=0; 
 		 SOS_4_4.x12=0; 
 		 SOS_4_4.x13=0; 
 		 SOS_4_4.x14=0; 
 		 SOS_4_4.x15=0;

		 W0_SOS_4_4 t0 = new W0_SOS_4_4(); 
		 W1_SOS_4_4 t1 = new W1_SOS_4_4(); 
		 W2_SOS_4_4 t2 = new W2_SOS_4_4(); 
		 W3_SOS_4_4 t3 = new W3_SOS_4_4(); 
		 t0.start(); 
		 t1.start(); 
		 t2.start(); 
		 t3.start(); 
		 t0.join(); 
		 t1.join(); 
		 t2.join(); 
		 t3.join(); 
	 }
}
