// Auto-generated stress test 
// Suvam Mukherjee, 2017 
 
package stress.fixedvar; 

class W0_SOS_6_2 extends Thread { 
	 public void run() {
	 	 while(true) { 
			 SOS_6_2.x0=0; 
			SOS_6_2.x1 =SOS_6_2.x0 + 1;
			SOS_6_2.x2 =SOS_6_2.x1 + 1;
			SOS_6_2.x3 =SOS_6_2.x2 + 1;
			SOS_6_2.x4 =SOS_6_2.x3 + 1;
			SOS_6_2.x5 =SOS_6_2.x4 + 1;
		 }
	 }
}
class W1_SOS_6_2 extends Thread { 
	 public void run() {
	 	 while(true) { 
			 SOS_6_2.x6=6; 
			SOS_6_2.x7 =SOS_6_2.x6 + 1;
			SOS_6_2.x8 =SOS_6_2.x7 + 1;
			SOS_6_2.x9 =SOS_6_2.x8 + 1;
			SOS_6_2.x10 =SOS_6_2.x9 + 1;
			SOS_6_2.x11 =SOS_6_2.x10 + 1;
		 }
	 }
}
public class SOS_6_2 { 
public static int x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11;

	 public static void main(String[] args) throws InterruptedException { 
//		 System.out.println("Ma");
		 SOS_6_2.x0=0; 
 		 SOS_6_2.x1=0; 
 		 SOS_6_2.x2=0; 
 		 SOS_6_2.x3=0; 
 		 SOS_6_2.x4=0; 
 		 SOS_6_2.x5=0; 
 		 SOS_6_2.x6=0; 
 		 SOS_6_2.x7=0; 
 		 SOS_6_2.x8=0; 
 		 SOS_6_2.x9=0; 
 		 SOS_6_2.x10=0; 
 		 SOS_6_2.x11=0;

		 W0_SOS_6_2 t0 = new W0_SOS_6_2(); 
		 W1_SOS_6_2 t1 = new W1_SOS_6_2(); 
		 t0.start(); 
		 t1.start(); 
		 t0.join(); 
		 t1.join(); 
	 }
}
