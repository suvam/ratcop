// Auto-generated stress test 
// Suvam Mukherjee, 2017 
 
package stress.fixedvar; 

class W0_SOS_6_7 extends Thread { 
	 public void run() {
	 	 while(true) { 
			 SOS_6_7.x0=0; 
			SOS_6_7.x1 =SOS_6_7.x0 + 1;
			SOS_6_7.x2 =SOS_6_7.x1 + 1;
			SOS_6_7.x3 =SOS_6_7.x2 + 1;
			SOS_6_7.x4 =SOS_6_7.x3 + 1;
			SOS_6_7.x5 =SOS_6_7.x4 + 1;
		 }
	 }
}
class W1_SOS_6_7 extends Thread { 
	 public void run() {
	 	 while(true) { 
			 SOS_6_7.x6=6; 
			SOS_6_7.x7 =SOS_6_7.x6 + 1;
			SOS_6_7.x8 =SOS_6_7.x7 + 1;
			SOS_6_7.x9 =SOS_6_7.x8 + 1;
			SOS_6_7.x10 =SOS_6_7.x9 + 1;
			SOS_6_7.x11 =SOS_6_7.x10 + 1;
		 }
	 }
}
class W2_SOS_6_7 extends Thread { 
	 public void run() {
	 	 while(true) { 
			 SOS_6_7.x12=12; 
			SOS_6_7.x13 =SOS_6_7.x12 + 1;
			SOS_6_7.x14 =SOS_6_7.x13 + 1;
			SOS_6_7.x15 =SOS_6_7.x14 + 1;
			SOS_6_7.x16 =SOS_6_7.x15 + 1;
			SOS_6_7.x17 =SOS_6_7.x16 + 1;
		 }
	 }
}
class W3_SOS_6_7 extends Thread { 
	 public void run() {
	 	 while(true) { 
			 SOS_6_7.x18=18; 
			SOS_6_7.x19 =SOS_6_7.x18 + 1;
			SOS_6_7.x20 =SOS_6_7.x19 + 1;
			SOS_6_7.x21 =SOS_6_7.x20 + 1;
			SOS_6_7.x22 =SOS_6_7.x21 + 1;
			SOS_6_7.x23 =SOS_6_7.x22 + 1;
		 }
	 }
}
class W4_SOS_6_7 extends Thread { 
	 public void run() {
	 	 while(true) { 
			 SOS_6_7.x24=24; 
			SOS_6_7.x25 =SOS_6_7.x24 + 1;
			SOS_6_7.x26 =SOS_6_7.x25 + 1;
			SOS_6_7.x27 =SOS_6_7.x26 + 1;
			SOS_6_7.x28 =SOS_6_7.x27 + 1;
			SOS_6_7.x29 =SOS_6_7.x28 + 1;
		 }
	 }
}
class W5_SOS_6_7 extends Thread { 
	 public void run() {
	 	 while(true) { 
			 SOS_6_7.x30=30; 
			SOS_6_7.x31 =SOS_6_7.x30 + 1;
			SOS_6_7.x32 =SOS_6_7.x31 + 1;
			SOS_6_7.x33 =SOS_6_7.x32 + 1;
			SOS_6_7.x34 =SOS_6_7.x33 + 1;
			SOS_6_7.x35 =SOS_6_7.x34 + 1;
		 }
	 }
}
class W6_SOS_6_7 extends Thread { 
	 public void run() {
	 	 while(true) { 
			 SOS_6_7.x36=36; 
			SOS_6_7.x37 =SOS_6_7.x36 + 1;
			SOS_6_7.x38 =SOS_6_7.x37 + 1;
			SOS_6_7.x39 =SOS_6_7.x38 + 1;
			SOS_6_7.x40 =SOS_6_7.x39 + 1;
			SOS_6_7.x41 =SOS_6_7.x40 + 1;
		 }
	 }
}
public class SOS_6_7 { 
public static int x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33,x34,x35,x36,x37,x38,x39,x40,x41;

	 public static void main(String[] args) throws InterruptedException { 
		 SOS_6_7.x0=0; 
 		 SOS_6_7.x1=0; 
 		 SOS_6_7.x2=0; 
 		 SOS_6_7.x3=0; 
 		 SOS_6_7.x4=0; 
 		 SOS_6_7.x5=0; 
 		 SOS_6_7.x6=0; 
 		 SOS_6_7.x7=0; 
 		 SOS_6_7.x8=0; 
 		 SOS_6_7.x9=0; 
 		 SOS_6_7.x10=0; 
 		 SOS_6_7.x11=0; 
 		 SOS_6_7.x12=0; 
 		 SOS_6_7.x13=0; 
 		 SOS_6_7.x14=0; 
 		 SOS_6_7.x15=0; 
 		 SOS_6_7.x16=0; 
 		 SOS_6_7.x17=0; 
 		 SOS_6_7.x18=0; 
 		 SOS_6_7.x19=0; 
 		 SOS_6_7.x20=0; 
 		 SOS_6_7.x21=0; 
 		 SOS_6_7.x22=0; 
 		 SOS_6_7.x23=0; 
 		 SOS_6_7.x24=0; 
 		 SOS_6_7.x25=0; 
 		 SOS_6_7.x26=0; 
 		 SOS_6_7.x27=0; 
 		 SOS_6_7.x28=0; 
 		 SOS_6_7.x29=0; 
 		 SOS_6_7.x30=0; 
 		 SOS_6_7.x31=0; 
 		 SOS_6_7.x32=0; 
 		 SOS_6_7.x33=0; 
 		 SOS_6_7.x34=0; 
 		 SOS_6_7.x35=0; 
 		 SOS_6_7.x36=0; 
 		 SOS_6_7.x37=0; 
 		 SOS_6_7.x38=0; 
 		 SOS_6_7.x39=0; 
 		 SOS_6_7.x40=0; 
 		 SOS_6_7.x41=0;

		 W0_SOS_6_7 t0 = new W0_SOS_6_7(); 
		 W1_SOS_6_7 t1 = new W1_SOS_6_7(); 
		 W2_SOS_6_7 t2 = new W2_SOS_6_7(); 
		 W3_SOS_6_7 t3 = new W3_SOS_6_7(); 
		 W4_SOS_6_7 t4 = new W4_SOS_6_7(); 
		 W5_SOS_6_7 t5 = new W5_SOS_6_7(); 
		 W6_SOS_6_7 t6 = new W6_SOS_6_7(); 
		 t0.start(); 
		 t1.start(); 
		 t2.start(); 
		 t3.start(); 
		 t4.start(); 
		 t5.start(); 
		 t6.start(); 
		 t0.join(); 
		 t1.join(); 
		 t2.join(); 
		 t3.join(); 
		 t4.join(); 
		 t5.join(); 
		 t6.join(); 
	 }
}
