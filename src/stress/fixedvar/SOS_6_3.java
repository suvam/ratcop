// Auto-generated stress test 
// Suvam Mukherjee, 2017 
 
package stress.fixedvar; 

class W0_SOS_6_3 extends Thread { 
	 public void run() {
	 	 while(true) { 
			 SOS_6_3.x0=0; 
			SOS_6_3.x1 =SOS_6_3.x0 + 1;
			SOS_6_3.x2 =SOS_6_3.x1 + 1;
			SOS_6_3.x3 =SOS_6_3.x2 + 1;
			SOS_6_3.x4 =SOS_6_3.x3 + 1;
			SOS_6_3.x5 =SOS_6_3.x4 + 1;
		 }
	 }
}
class W1_SOS_6_3 extends Thread { 
	 public void run() {
	 	 while(true) { 
			 SOS_6_3.x6=6; 
			SOS_6_3.x7 =SOS_6_3.x6 + 1;
			SOS_6_3.x8 =SOS_6_3.x7 + 1;
			SOS_6_3.x9 =SOS_6_3.x8 + 1;
			SOS_6_3.x10 =SOS_6_3.x9 + 1;
			SOS_6_3.x11 =SOS_6_3.x10 + 1;
		 }
	 }
}
class W2_SOS_6_3 extends Thread { 
	 public void run() {
	 	 while(true) { 
			 SOS_6_3.x12=12; 
			SOS_6_3.x13 =SOS_6_3.x12 + 1;
			SOS_6_3.x14 =SOS_6_3.x13 + 1;
			SOS_6_3.x15 =SOS_6_3.x14 + 1;
			SOS_6_3.x16 =SOS_6_3.x15 + 1;
			SOS_6_3.x17 =SOS_6_3.x16 + 1;
		 }
	 }
}
public class SOS_6_3 { 
public static int x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17;

	 public static void main(String[] args) throws InterruptedException { 
		 SOS_6_3.x0=0; 
 		 SOS_6_3.x1=0; 
 		 SOS_6_3.x2=0; 
 		 SOS_6_3.x3=0; 
 		 SOS_6_3.x4=0; 
 		 SOS_6_3.x5=0; 
 		 SOS_6_3.x6=0; 
 		 SOS_6_3.x7=0; 
 		 SOS_6_3.x8=0; 
 		 SOS_6_3.x9=0; 
 		 SOS_6_3.x10=0; 
 		 SOS_6_3.x11=0; 
 		 SOS_6_3.x12=0; 
 		 SOS_6_3.x13=0; 
 		 SOS_6_3.x14=0; 
 		 SOS_6_3.x15=0; 
 		 SOS_6_3.x16=0; 
 		 SOS_6_3.x17=0;

		 W0_SOS_6_3 t0 = new W0_SOS_6_3(); 
		 W1_SOS_6_3 t1 = new W1_SOS_6_3(); 
		 W2_SOS_6_3 t2 = new W2_SOS_6_3(); 
		 t0.start(); 
		 t1.start(); 
		 t2.start(); 
		 t0.join(); 
		 t1.join(); 
		 t2.join(); 
	 }
}
