/**
 * Simple test of the interval analysis on a sequential program.
 */
package summerSchool;

/**
 * @author Suvam Mukherjee
 *
 */
public class IntervalTest {

	/**
	 * @param args
	 */
	public static int error;
	
	public static void main(String[] args) {
		int i = 0;
		IntervalTest.error = 0;
		
		System.out.println("\nStarting test...");
		
		while(true) {
			i++;
			
			if(!(i >= 0)) {		// assert (i >= 0);
				IntervalTest.error = 1;
			}
		}
	}
}
