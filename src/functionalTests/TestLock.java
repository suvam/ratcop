/**
 * Testing the Jimple code generated when java.util.concurrent.* is used.
 */
package functionalTests;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Suvam Mukherjee
 *
 */
class T1_TestLock extends Thread {
	
	public void run() {
		TestLock.l.lock();
		TestLock.x = 10;
		TestLock.l.unlock();
	}
}

class T2_TestLock extends Thread {
	
	public void run() {
		TestLock.l.lock();
		TestLock.x = 20;
		TestLock.l.unlock();
	}
}
public class TestLock {
	
	public static Lock l;
	public static int x;

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		TestLock.l = new ReentrantLock();
		TestLock.x = 0;
		System.out.println("\nTesting Locks");
		T1_TestLock t1 = new T1_TestLock();
		T2_TestLock t2 = new T2_TestLock();
		
		t1.start();
		t2.start();
		
		t1.join();
		t2.join();

	}

}
