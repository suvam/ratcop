/**
 * 
 */
package functionalTests;

/**
 * @author Suvam Mukherjee
 *
 */
class T1_BEx extends Thread {
	public void run() {
		synchronized(BEx.lock) {
			BEx.x++;
		}
	}
}

class T2_BEx extends Thread {
	public void run() {
		synchronized(BEx.lock) {
			if(!(BEx.x <= 1))
				BEx.error = 1;
		}
	}
}

public class BEx {
	
	public static int x;
	public static int error;
	public static Object lock;

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		BEx.x = 0;
		BEx.error = 0;
		BEx.lock = new Object();
		
		T1_BEx t1 = new T1_BEx();
		T2_BEx t2 = new T2_BEx();
		
		t1.start();
		t2.start();
		
		t1.join();
		t2.join();

	}

}
