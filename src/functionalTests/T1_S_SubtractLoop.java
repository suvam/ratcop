/**
 * Tests whether we are handling subtraction correctly.
 */
package functionalTests;

/**
 * @author Suvam Mukherjee.
 *
 * Indian Institute of Science, 2017.
 *
 */
public class T1_S_SubtractLoop {

	public static int error; 
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("\nStarting... ");
		int n = Integer.parseInt(args[0]);
		int i = n;
		T1_S_SubtractLoop.error = 0;
		
		while(i>=0)
			i--;
		
		if(!(i >= 0))	// A1
			T1_S_SubtractLoop.error = 1;
		
		i = n;
		if(n >= 0) {
			while(i > 0)
				i = i - 1;
			
			if(!(i>=0))	// A2
				T1_S_SubtractLoop.error = 1;
		}
	}

}
