/**
 * Test the behavior of the subtract operator in a compound statement
 */
package functionalTests;

/**
 * @author Suvam Mukherjee.
 *
 * Indian Institute of Science, 2017.
 *
 */
public class T2_S_SubtractCompound {

	public static int error;
	public static int n;
	public static int x;
	public static int y;
	public static int z;
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		T2_S_SubtractCompound.z = -1;
		T2_S_SubtractCompound.error = 0;
		T2_S_SubtractCompound.x = 10;
		
		System.out.println("\nT2_S_SubtractCompound");
		T2_S_SubtractCompound.n = 20;
		
		T2_S_SubtractCompound.y = T2_S_SubtractCompound.x +  ((T2_S_SubtractCompound.n/4) - 1);
		
		if(!(T2_S_SubtractCompound.y == 14))
			T2_S_SubtractCompound.error = 1;
	}

}
