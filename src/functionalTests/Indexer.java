/**
 * 
 */
package functionalTests;

/**
 * @author Suvam Mukherjee
 *
 */

class W1_Indexer extends Thread {
	
	public void run() {
		int m = 0;
		int w = 0;
		int h = 0, rv = 0;
		
		while(m < Indexer.MAX) {
			w = (++m) * 12;
			h = (w*7) % Indexer.SIZE;
			
			if(!(h>=0))
				Indexer.error = 1;
			
			rv = 0;
			h = rv + 1;
//			if(!(rv == 0))
//				Indexer.error = 1;
			
			while(rv == 0) {
				synchronized(Indexer.lock) {
					if(Indexer.array == 0) {
						Indexer.array = h;
						rv = 1;
					}
					else
						h = (h + 1) % Indexer.SIZE;
				}
			}
		}
	}
}

class W2_Indexer extends Thread {
	
	public void run() {
		int m = 0;
		int w = 0;
		int h = 0, rv = 0;
		
		while(m < Indexer.MAX) {
			w = (++m) * 12;
			h = (w*7) % Indexer.SIZE;
			
			if(!(h>=0))
				Indexer.error = 1;
			
			rv = 0;
			h = rv + 1;
//			if(!(rv == 0))
//				Indexer.error = 1;
			
			while(rv == 0) {
				synchronized(Indexer.lock) {
					if(Indexer.array == 0) {
						Indexer.array = h;
						rv = 1;
					}
					else
						h = (h + 1) % Indexer.SIZE;
				}
			}
		}
	}
}

public class Indexer {
	
	public static int error;
	public static int SIZE;
	public static int MAX;
	public static Object lock;
	public static int array;	// abstraction of array

	/**
	 * @param args
	 */
	public static void main(String[] args) throws InterruptedException {
		System.out.println("Indexer");
		Indexer.SIZE = 128;
		Indexer.MAX = 4;
		Indexer.error = 0;
		Indexer.array = 0;
		Indexer.lock = new Object();
		
		W1_Indexer t1 = new W1_Indexer();
		W2_Indexer t2 = new W2_Indexer();
		t1.start();
		t2.start();
		t1.join();
		t2.join();
	}

}
