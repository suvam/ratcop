/**
 * Contains example from the paper.
 */
package functionalTests;

/**
 * @author Suvam Mukherjee
 *
 */
class T1_PEx extends Thread {
	public void run() {
		
		synchronized(PEx.lock) {
			PEx.x = PEx.y;
			PEx.x++;
			PEx.y++;
			
			if(!(PEx.x == PEx.y)) 
				PEx.error = 1;
		}
	}
}

class T2_PEx extends Thread {
	public void run() {
		
		PEx.z = 1;
		
		if(!(PEx.z == 1))
			PEx.error = 1;
		
		synchronized(PEx.lock) {
		if(!(PEx.x == PEx.y))
			PEx.error = 1;
		}
	}
}
public class PEx {
	
	public static int x,y,z;
	public static int error;
	public static Object lock;

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		PEx.x = 0;
		PEx.y = 0;
		PEx.z = 0;
		PEx.error = 0;
		PEx.lock = new Object();
		
		T1_PEx t1 = new T1_PEx();
		T2_PEx t2 = new T2_PEx();
		
		t1.start();
		t2.start();
		
		t1.join();
		t2.join();
	}

}
