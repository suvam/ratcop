/**
 * 
 */
package functionalTests;

/**
 * @author Suvam Mukherjee.
 *
 * Indian Institute of Science, 2017.
 *
 */

//class W1_RW extends Thread {
//	
//	public void run() {
//		
//		synchronized(RWConcTest.lock) {
//			RWConcTest.x++;
//		}
//	}
//}

class W2_RW extends Thread {
	
	public void run() {
		
		synchronized(RWConcTest.lock) {
			int t = RWConcTest.x;
			System.out.println(t);
			
			if(!(RWConcTest.x <= 1))
				RWConcTest.error = 1;
		}
	}
}

//class W3_RW extends Thread {
//	
//	public void run() {
//		
//		synchronized(RWConcTest.lock) {
//			int t = RWConcTest.x;
//			System.out.println(t);
//			
//			if(!(RWConcTest.x <= 2))
//				RWConcTest.error = 1;
//		}
//	}
//}

public class RWConcTest {

	public static int x;
	public static Object lock;
	public static int error;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		RWConcTest.x = 0;
		RWConcTest.lock = new Object();
		RWConcTest.error = 0;
		
		//W1_RW t1 = new W1_RW();
		W2_RW t2 = new W2_RW();
		
		t2.start();
		synchronized(RWConcTest.lock) {
			RWConcTest.x++;
		}
		
//		W3_RW t3 = new W3_RW();
//		t3.start();
//		synchronized(RWConcTest.lock) {
//			RWConcTest.x++;
//		}
		try {
		//	t1.join();
			t2.join();
//			t3.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
