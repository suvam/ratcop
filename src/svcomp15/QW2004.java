/**
 * 
 */
package svcomp15;

/**
 * @author Suvam Mukherjee, 2017.
 *
 */
class T1_QW2004 extends Thread {
	
	public void run() {
		// BCSP_PnpStop
		int pending;
		synchronized(QW2004.m) {
		  QW2004.pendingIo--;
		  pending = QW2004.pendingIo;
		  if (pending == 0) {
		    QW2004.stoppingEvent = 1;
		    QW2004.stopped = 0;
		  }
		  else
		    QW2004.stopped--;
		  
		  if(!(QW2004.pendingIo == QW2004.stopped))
		    	QW2004.error = 1;
		}

	}
}

public class QW2004 {
	
	public static int pendingIo, stoppingEvent, stopped, status;
	public static Object m;
	public static int error, conjunct_flag;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		 int status, pending;
		 QW2004.pendingIo = 1;
		 QW2004.stoppingEvent = 0;
		 QW2004.stopped = 1;
		 QW2004.error = 0;
		 
		 QW2004.status = 0;
		 
		 QW2004.m = new Object();
		 T1_QW2004 t1 = new T1_QW2004();
		 t1.start();
		  
		 synchronized(QW2004.m) {
			  if (QW2004.stopped==0)
			    QW2004.status = -1;
			  else {
			    QW2004.pendingIo++;
			    QW2004.stopped++;
			    QW2004.status = 0;
			  }
			  if(!(QW2004.pendingIo == QW2004.stopped))
			    	QW2004.error = 1;
//			  if(!(QW2004.status < QW2004.pendingIo))
//				  QW2004.error = 1;
//			  if(!(0 <= QW2004.pendingIo))
//				  QW2004.error = 1;
			  
		 }
		 synchronized(QW2004.m) {
		  if (QW2004.status == 0)
		    if(!(QW2004.pendingIo == QW2004.stopped))
		    	QW2004.error = 1;
		 }
		 synchronized(QW2004.m) {
			  if (QW2004.pendingIo > 0) {
			    QW2004.pendingIo--;
			    QW2004.stopped--;
			  }
			  pending = QW2004.pendingIo;
			  if (pending == 0)
			    QW2004.stoppingEvent = 1;
			  
			  if(!(QW2004.pendingIo == QW2004.stopped))
			    	QW2004.error = 1;
		 }

	}

}
