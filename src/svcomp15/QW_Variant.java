/**
 * 
 */
package svcomp15;


/**
 * @author Suvam Mukherjee, 2017.
 *
 */

class T1_QW_Variant extends Thread {
	
	public void run() {
		int pending;
		synchronized(QW_Variant.m) {
		  if (QW_Variant.pendingIo > 0)
			  QW_Variant.pendingIo--;
		  pending = QW_Variant.pendingIo;
		  if (pending == 0) {
			  QW_Variant.stoppingEvent = 1;
			  QW_Variant.stopped = 0;
		  }
		  else
			  QW_Variant.stopped--;
		}
	}
}

class T2_QW_Variant extends Thread {
	
	public void run() {
		int pending;
		synchronized(QW_Variant.m) {
			if (QW_Variant.stopped==0)
				QW_Variant.status = -1;
		  else {
			  QW_Variant.pendingIo++;
			  QW_Variant.stopped++;
			  QW_Variant.status = 0;
		  }
		}
		synchronized(QW_Variant.m) {
		  if (QW_Variant.status == 0)
			  if(!(QW_Variant.pendingIo == QW_Variant.stopped))
				  QW_Variant.error = 1;
		}
		synchronized(QW_Variant.m) {
		  if (QW_Variant.pendingIo > 1) {
			  QW_Variant.pendingIo--;
			  QW_Variant.stopped--;
		  }
		  pending = QW_Variant.pendingIo;
		  if (pending == 0)
			  QW_Variant.stoppingEvent = 1;
		}
	}
}

class T3_QW_Variant extends Thread {
	
	public void run() {
		int pending;
		synchronized(QW_Variant.m) {
			if (QW_Variant.stopped==0)
				QW_Variant.status = -1;
		  else {
			  QW_Variant.pendingIo++;
			  QW_Variant.stopped++;
			  QW_Variant.status = 0;
		  }
		}
		synchronized(QW_Variant.m) {
		  if (QW_Variant.status == 0)
			  if(!(QW_Variant.pendingIo == QW_Variant.stopped))
				  QW_Variant.error = 1;
		}
		synchronized(QW_Variant.m) {
		  if (QW_Variant.pendingIo > 1) {
			  QW_Variant.pendingIo--;
			  QW_Variant.stopped--;
		  }
		  pending = QW_Variant.pendingIo;
		  if (pending == 0)
			  QW_Variant.stoppingEvent = 1;
		}
	}
}

class T4_QW_Variant extends Thread {
	
	public void run() {
		int pending;
		synchronized(QW_Variant.m) {
			if (QW_Variant.stopped==0)
				QW_Variant.status = -1;
		  else {
			  QW_Variant.pendingIo++;
			  QW_Variant.stopped++;
			  QW_Variant.status = 0;
		  }
		}
		synchronized(QW_Variant.m) {
		  if (QW_Variant.status == 0)
			  if(!(QW_Variant.pendingIo == QW_Variant.stopped))
				  QW_Variant.error = 1;
		}
		synchronized(QW_Variant.m) {
		  if (QW_Variant.pendingIo > 1) {
			  QW_Variant.pendingIo--;
			  QW_Variant.stopped--;
		  }
		  pending = QW_Variant.pendingIo;
		  if (pending == 0)
			  QW_Variant.stoppingEvent = 1;
		}
	}
}

class T5_QW_Variant extends Thread {
	
	public void run() {
		int pending;
		synchronized(QW_Variant.m) {
			if (QW_Variant.stopped==0)
				QW_Variant.status = -1;
		  else {
			  QW_Variant.pendingIo++;
			  QW_Variant.stopped++;
			  QW_Variant.status = 0;
		  }
		}
		synchronized(QW_Variant.m) {
		  if (QW_Variant.status == 0)
			  if(!(QW_Variant.pendingIo == QW_Variant.stopped))
				  QW_Variant.error = 1;
		}
		synchronized(QW_Variant.m) {
		  if (QW_Variant.pendingIo > 1) {
			  QW_Variant.pendingIo--;
			  QW_Variant.stopped--;
		  }
		  pending = QW_Variant.pendingIo;
		  if (pending == 0)
			  QW_Variant.stoppingEvent = 1;
		}
	}
}

class T6_QW_Variant extends Thread {
	
	public void run() {
		int pending;
		synchronized(QW_Variant.m) {
			if (QW_Variant.stopped==0)
				QW_Variant.status = -1;
		  else {
			  QW_Variant.pendingIo++;
			  QW_Variant.stopped++;
			  QW_Variant.status = 0;
		  }
		}
		synchronized(QW_Variant.m) {
		  if (QW_Variant.status == 0)
			  if(!(QW_Variant.pendingIo == QW_Variant.stopped))
				  QW_Variant.error = 1;
		}
		synchronized(QW_Variant.m) {
		  if (QW_Variant.pendingIo > 1) {
			  QW_Variant.pendingIo--;
			  QW_Variant.stopped--;
		  }
		  pending = QW_Variant.pendingIo;
		  if (pending == 0)
			  QW_Variant.stoppingEvent = 1;
		}
	}
}

class T7_QW_Variant extends Thread {
	
	public void run() {
		int pending;
		synchronized(QW_Variant.m) {
			if (QW_Variant.stopped==0)
				QW_Variant.status = -1;
		  else {
			  QW_Variant.pendingIo++;
			  QW_Variant.stopped++;
			  QW_Variant.status = 0;
		  }
		}
		synchronized(QW_Variant.m) {
		  if (QW_Variant.status == 0)
			  if(!(QW_Variant.pendingIo == QW_Variant.stopped))
				  QW_Variant.error = 1;
		}
		synchronized(QW_Variant.m) {
		  if (QW_Variant.pendingIo > 1) {
			  QW_Variant.pendingIo--;
			  QW_Variant.stopped--;
		  }
		  pending = QW_Variant.pendingIo;
		  if (pending == 0)
			  QW_Variant.stoppingEvent = 1;
		}
	}
}

class T8_QW_Variant extends Thread {
	
	public void run() {
		int pending;
		synchronized(QW_Variant.m) {
			if (QW_Variant.stopped==0)
				QW_Variant.status = -1;
		  else {
			  QW_Variant.pendingIo++;
			  QW_Variant.stopped++;
			  QW_Variant.status = 0;
		  }
		}
		synchronized(QW_Variant.m) {
		  if (QW_Variant.status == 0)
			  if(!(QW_Variant.pendingIo == QW_Variant.stopped))
				  QW_Variant.error = 1;
		}
		synchronized(QW_Variant.m) {
		  if (QW_Variant.pendingIo > 1) {
			  QW_Variant.pendingIo--;
			  QW_Variant.stopped--;
		  }
		  pending = QW_Variant.pendingIo;
		  if (pending == 0)
			  QW_Variant.stoppingEvent = 1;
		}
	}
}

class T9_QW_Variant extends Thread {
	
	public void run() {
		int pending;
		synchronized(QW_Variant.m) {
			if (QW_Variant.stopped==0)
				QW_Variant.status = -1;
		  else {
			  QW_Variant.pendingIo++;
			  QW_Variant.stopped++;
			  QW_Variant.status = 0;
		  }
		}
		synchronized(QW_Variant.m) {
		  if (QW_Variant.status == 0)
			  if(!(QW_Variant.pendingIo == QW_Variant.stopped))
				  QW_Variant.error = 1;
		}
		synchronized(QW_Variant.m) {
		  if (QW_Variant.pendingIo > 1) {
			  QW_Variant.pendingIo--;
			  QW_Variant.stopped--;
		  }
		  pending = QW_Variant.pendingIo;
		  if (pending == 0)
			  QW_Variant.stoppingEvent = 1;
		}
	}
}

class T10_QW_Variant extends Thread {
	
	public void run() {
		int pending;
		synchronized(QW_Variant.m) {
			if (QW_Variant.stopped==0)
				QW_Variant.status = -1;
		  else {
			  QW_Variant.pendingIo++;
			  QW_Variant.stopped++;
			  QW_Variant.status = 0;
		  }
		}
		synchronized(QW_Variant.m) {
		  if (QW_Variant.status == 0)
			  if(!(QW_Variant.pendingIo == QW_Variant.stopped))
				  QW_Variant.error = 1;
		}
		synchronized(QW_Variant.m) {
		  if (QW_Variant.pendingIo > 1) {
			  QW_Variant.pendingIo--;
			  QW_Variant.stopped--;
		  }
		  pending = QW_Variant.pendingIo;
		  if (pending == 0)
			  QW_Variant.stoppingEvent = 1;
		}
	}
}

class T11_QW_Variant extends Thread {
	
	public void run() {
		int pending;
		synchronized(QW_Variant.m) {
			if (QW_Variant.stopped==0)
				QW_Variant.status = -1;
		  else {
			  QW_Variant.pendingIo++;
			  QW_Variant.stopped++;
			  QW_Variant.status = 0;
		  }
		}
		synchronized(QW_Variant.m) {
		  if (QW_Variant.status == 0)
			  if(!(QW_Variant.pendingIo == QW_Variant.stopped))
				  QW_Variant.error = 1;
		}
		synchronized(QW_Variant.m) {
		  if (QW_Variant.pendingIo > 1) {
			  QW_Variant.pendingIo--;
			  QW_Variant.stopped--;
		  }
		  pending = QW_Variant.pendingIo;
		  if (pending == 0)
			  QW_Variant.stoppingEvent = 1;
		}
	}
}

public class QW_Variant {
	public static int pendingIo, stoppingEvent, stopped, status, pending;
	public static Object m;
	public static int error;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		QW_Variant.pendingIo = 1;
		QW_Variant.stopped = 1;
		QW_Variant.stoppingEvent = 0;
		QW_Variant.status = 0;
		QW_Variant.error = 0;
		QW_Variant.m = new Object();
		
		T1_QW_Variant t1 = new T1_QW_Variant();
		T2_QW_Variant t2 = new T2_QW_Variant();
		T3_QW_Variant t3 = new T3_QW_Variant();
		T4_QW_Variant t4 = new T4_QW_Variant();
		T5_QW_Variant t5 = new T5_QW_Variant();
		T6_QW_Variant t6 = new T6_QW_Variant();
		T7_QW_Variant t7 = new T7_QW_Variant();
		T8_QW_Variant t8 = new T8_QW_Variant();
		T9_QW_Variant t9 = new T9_QW_Variant();
		T10_QW_Variant t10 = new T10_QW_Variant();
		T11_QW_Variant t11 = new T11_QW_Variant();
		
		t1.start();
		t2.start();
		t3.start();
		t4.start();
		t5.start();
		t6.start();
		t7.start();
		t8.start();
		t9.start();
		t10.start();
		t11.start();

	}

}
