/**
 * Port from SVCOMP'15.
 */
package svcomp15;

/**
 * @author Suvam Mukherjee.
 *
 * Indian Institute of Science, 2017.
 *
 */
class T1_SL5 extends Thread {
	
	public void run() {
		while(true) {
			synchronized(SimpleLoop5.lock) {
				if(!(SimpleLoop5.a != SimpleLoop5.b))
					SimpleLoop5.error = 1;
			}
		}
	}
}

class T2_SL5 extends Thread {
	
	public void run() {
		while(true) {
			synchronized(SimpleLoop5.lock) {
				int temp = SimpleLoop5.a;
				SimpleLoop5.a = SimpleLoop5.b;
				SimpleLoop5.b = SimpleLoop5.c;
				SimpleLoop5.c = temp;
			}
		}
	}
}

class T3_SL5 extends Thread {
	
	public void run() {
		while(true) {
			synchronized(SimpleLoop5.lock) {
				int temp = SimpleLoop5.a;
				SimpleLoop5.a = SimpleLoop5.b;
				SimpleLoop5.b = SimpleLoop5.c;
				SimpleLoop5.c = temp;
			}
		}
	}
}

public class SimpleLoop5 {
	
	public static int a, b, c, error;
	public static Object lock;

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		SimpleLoop5.a = 1;
		SimpleLoop5.b = 2;
		SimpleLoop5.c = 3;
		SimpleLoop5.error = 0;
		SimpleLoop5.lock = new Object();
		
		System.out.println("\nSL5");
		
		T1_SL5 t1 = new T1_SL5();
		T2_SL5 t2 = new T2_SL5();
		T3_SL5 t3 = new T3_SL5();
		
		t1.start();
		t2.start();
		t3.start();
		t1.join();
		t2.join();
		t3.join();

	}

}
