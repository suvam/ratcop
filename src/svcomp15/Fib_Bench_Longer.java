/**
 * 
 */
package svcomp15;

/**
 * @author Suvam Mukherjee, 2017.
 *
 */
class W1_Fib_Bench_Longer_False_Unreach_Call extends Thread {
	
	public void run() {
		for(int k = 0; k < Fib_Bench.NUM; k++) {
			synchronized(Fib_Bench.lock) {
				Fib_Bench.i += Fib_Bench.j;
			}
		}
	}
}

class W2_Fib_Bench_Longer_False_Unreach_Call extends Thread {
	
	public void run() {
		
		for(int k = 0; k < Fib_Bench.NUM; k++) {
			synchronized(Fib_Bench.lock) {
				Fib_Bench.j += Fib_Bench.i;
			}
		}
	}
}

public class Fib_Bench_Longer {

	public static int i;
	public static int j;
	public static int error;
	public static Object lock;
	public static int NUM;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Fib_Bench.i = 1;
		Fib_Bench.j = 1;
		Fib_Bench.error = 0;
		Fib_Bench.NUM = 6;
		Fib_Bench.lock = new Object();
		
		W1_Fib_Bench_False_Unreach_Call w1 = new W1_Fib_Bench_False_Unreach_Call();
		W2_Fib_Bench_False_Unreach_Call w2 = new W2_Fib_Bench_False_Unreach_Call();
		
		w1.start();
		w2.start();
		
		try {
			w1.join();
			w2.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println(Fib_Bench.i);
		System.out.println(Fib_Bench.j ); 
		
		if(!(Fib_Bench.i <= 377))
			Fib_Bench.error = 1;
		
		if(!(Fib_Bench.j <= 377))
			Fib_Bench.error = 1;

	}
}
