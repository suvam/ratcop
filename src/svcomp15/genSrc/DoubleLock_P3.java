/**
 * Port from SVCOMP'15.
 */
package svcomp15.genSrc;
/**
 * @author Suvam Mukherjee.
 *
 * Indian Institute of Science, 2017.
 *
 */
class T1_DLP3 extends Thread {
	
	public void run() {
		synchronized(DoubleLock_P3.lock) {
			DoubleLock_P3.count++;
			DoubleLock_P3.count--;
		}
		
		synchronized(DoubleLock_P3.lock) {
			DoubleLock_P3.count--;
			DoubleLock_P3.count++;
		}
	}
}

class T2_DLP3 extends Thread {
	
	public void run() {
		
		synchronized(DoubleLock_P3.lock) {
			if(!(DoubleLock_P3.count >= -1) && (Region.r0==Region.r0))
				DoubleLock_P3.error = 1;
Region.r0 = 0;
		}
	}
}
public class DoubleLock_P3 {

	public static int count, error;
	public static Object lock;
	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		DoubleLock_P3.count = 0;
Region.r0 = 0;
		DoubleLock_P3.error = 0;
Region.r0 = 0;
		DoubleLock_P3.lock = new Object();
		
		System.out.println("\nDoubleLock_P3");
		
		T1_DLP3 t1 = new T1_DLP3();
		T2_DLP3 t2 = new T2_DLP3();
		
		t1.start();
		t2.start();
		t1.join();
		t2.join();
		

	}

}
