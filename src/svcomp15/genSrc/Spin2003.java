/**
 * 
 */

/**
 * @author Suvam Mukherjee.
 *
 * Indian Institute of Science, 2017.
 *
 */
package svcomp15.genSrc;
class T1_Spin extends Thread {
	
	public void run() {
		
		synchronized(Spin2003.lock) {
			Spin2003.x = 0;
Region.r0 = 0;
			Spin2003.x = 1;
Region.r0 = 0;
			
			if(!(Spin2003.x >= 1) && (Region.r0==Region.r0))
				Spin2003.error = 1;
		}
	}
}
class T2_Spin extends Thread {
	
	public void run() {
		
		synchronized(Spin2003.lock) {
			Spin2003.x = 0;
Region.r0 = 0;
			Spin2003.x = 1;
Region.r0 = 0;
			
			if(!(Spin2003.x >= 1) && (Region.r0==Region.r0))
				Spin2003.error = 1;
		}
	}
}

public class Spin2003 {

	public static int x, error;
	public static Object lock;
	
	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.out.println("Spin2003");
		Spin2003.x = 1;
Region.r0 = 0;
		Spin2003.error = 0;
		Spin2003.lock = new Object();
Region.r0 = 0;
		
		T1_Spin t1 = new T1_Spin();
		T2_Spin t2 = new T2_Spin();
		
		t1.start();
		t2.start();
		t1.join();
		t2.join();

	}

}
