/**
 * Port from SVCOMP15 benchmark.
 */

/**
 * @author Suvam Mukherjee.
 *
 * Indian Institute of Science, 2017.
 *
 */
package svcomp15.genSrc;

class iSet_1 extends Thread {
	
	public void run() {
		
		synchronized(Reorder_2.al) {
			Reorder_2.a = 1;
Region.r0 = 0;
		}
		synchronized(Reorder_2.bl) {
			Reorder_2.b = -1;
Region.r1 = 0;
		}
	}
}

class iSet_2 extends Thread {
	
	public void run() {
		
		synchronized(Reorder_2.al) {
			Reorder_2.a = 1;
Region.r0 = 0;
		}
		synchronized(Reorder_2.bl) {
			Reorder_2.b = -1;
Region.r1 = 0;
		}
	}
}

class iCheck_1 extends Thread {
	
	public void run() {
		synchronized(Reorder_2.al) {
			synchronized(Reorder_2.bl) {
				if(!(Reorder_2.a + Reorder_2.b == 0) && (Region.r0==Region.r0) && (Region.r1==Region.r1))
					Reorder_2.error = 1;
Region.r2 = 0;
			}
		}
	}
}

class iCheck_2 extends Thread {
	
	public void run() {
		synchronized(Reorder_2.al) {
			synchronized(Reorder_2.bl) {
				if(!(Reorder_2.a + Reorder_2.b == 0) && (Region.r0==Region.r0) && (Region.r1==Region.r1))
					Reorder_2.error = 1;
Region.r2 = 0;
			}
		}
	}
}

public class Reorder_2 {

	public static Object al;	// lock for a
	public static Object bl;	// lock for b
	public static int a;
	public static int b;
	public static int error;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Reorder_2.a = 0;
Region.r0 = 0;
		Reorder_2.b = 0;
Region.r1 = 0;
		Reorder_2.al = new Object();
		Reorder_2.bl = new Object();
		Reorder_2.error = 0;
Region.r2 = 0;
		
		System.out.println("\nReorder_2_False_Unreach_Call");
		
		iSet_1 t1 = new iSet_1();
		iSet_2 t2 = new iSet_2();
		iCheck_1 t3 = new iCheck_1();
		iCheck_2 t4 = new iCheck_2();
		
		t1.start();
		t2.start();
		t3.start();
		t4.start();
		
		try {
			t1.join();
			t2.join();
			t3.join();
			t4.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}
