/**
 * Port from SVCOMP.
 * Array data is not modeled.
 */

/**
 * @author Suvam Mukherjee.
 *
 * Indian Institute of Science, 2017.
 *
 */
package svcomp15.genSrc;
class T1_SSSC12 extends Thread {
	
	public void run() {
		int c, end, temp;
		c = 0;
		end = 0;
		
		synchronized(SSSC12.lock) {
			temp = SSSC12.len;
			
			if(SSSC12.next + 10 <= SSSC12.len && (Region.r0==Region.r0)) {
				c = SSSC12.next;
				end = SSSC12.next + 10;
				SSSC12.next = SSSC12.next + 10;
Region.r0 = Region.r0;
			}
		}
		
		while(c < end) {
			if(!(c >= 0))
				synchronized(SSSC12.error_lock) {
					SSSC12.error = 1;
Region.r1 = 0;
				}
			if(!(c <= temp))
				synchronized(SSSC12.error_lock) {
					SSSC12.error = 1;
Region.r1 = 0;
				}
			c = c +1;
		}
	}
}

class T2_SSSC12 extends Thread {
	
	public void run() {
		int c, end, temp;
		c = 0;
		end = 0;
		
		synchronized(SSSC12.lock) {
			temp = SSSC12.len;
			
			if(SSSC12.next + 10 <= SSSC12.len && (Region.r0==Region.r0)) {
				c = SSSC12.next;
				end = SSSC12.next + 10;
				SSSC12.next = SSSC12.next + 10;
Region.r0 = Region.r0;
			}
		}
		
		while(c < end) {
			if(!(c >= 0))
				synchronized(SSSC12.error_lock) {
					SSSC12.error = 1;
Region.r1 = 0;
				}
			if(!(c <= temp))
				synchronized(SSSC12.error_lock) {
					SSSC12.error = 1;
Region.r1 = 0;
				}
			c = c +1;
		}
	}
}

public class SSSC12 {
	
	public static int len;
	public static int next;
	public static int error;
	public static Object lock;
	public static Object error_lock;

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		System.out.println("\nSSSC12");
		SSSC12.len = Integer.parseInt(args[0]);
Region.r0 = 0;
		SSSC12.lock = new Object();
		SSSC12.error_lock = new Object();
		SSSC12.error = 0;
Region.r1 = 0;
		
		if(SSSC12.len > 0 && (Region.r0==Region.r0)) {
			SSSC12.next = 0;
Region.r0 = 0;
			
			T1_SSSC12 t1 = new T1_SSSC12();
			T2_SSSC12 t2 = new T2_SSSC12();
			t1.start();
			t2.start();
			t1.join();
			t2.join();
		}

	}

}
