/**
 * Port from SVCOMP'15.
 */

/**
 * @author Suvam Mukherjee.
 *
 * Indian Institute of Science, 2017.
 *
 */
package svcomp15.genSrc;
class T1_SL extends Thread {
	
	public void run() {
		int i=0, j=0, k=0;
		
		for(i=0; i< SimpleLoop.x && Region.r0==Region.r0; i++) {
		    for(j=i+1; j<SimpleLoop.y && Region.r0==Region.r0; j++) {
		      for(k = j; k < SimpleLoop.z && Region.r0 == Region.r0; k++) {
		    	  if(!(k > i))
		    		  synchronized(SimpleLoop.lock) {
		    			  SimpleLoop.error = 1;
Region.r1 = 0;
		    		  }
		      }
		     }
		}

		// Unsure about the following assertion
//		if(!(i==SimpleLoop.x && (SimpleLoop.x==0 || j == SimpleLoop.y || SimpleLoop.y <= SimpleLoop.x+1) && (SimpleLoop.x == 0 || SimpleLoop.y <= SimpleLoop.x+1 || k == SimpleLoop.z || SimpleLoop.z < SimpleLoop.y)) && (Region.r0==Region.r0))
//				SimpleLoop.error = 1;

	}
}

class T2_SL extends Thread {
	
	public void run() {
		int i, j, k;
		
		for(i=0; i< SimpleLoop.x && Region.r0==Region.r0; i++) {
		    for(j=i+1; j<SimpleLoop.y && Region.r0==Region.r0; j++) {
		      for(k = j; k < SimpleLoop.z && Region.r0==Region.r0; k++) {
		    	  if(!(k > i))
		    		  synchronized(SimpleLoop.lock) {
		    			  SimpleLoop.error = 1;
Region.r1 = 0;
		    		  }
		      }
		     }
		}
		
		
	}
}

public class SimpleLoop {
	public static int x,y,z, error;
	public static Object lock;

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.out.println("Simple Loop");
		SimpleLoop.x = Integer.parseInt(args[0]);
Region.r0 = 0;
		SimpleLoop.y = Integer.parseInt(args[1]);
Region.r0 = 0;
		SimpleLoop.z = Integer.parseInt(args[2]);
Region.r0 = 0;
		SimpleLoop.error = 0;
Region.r1 = 0;
		SimpleLoop.lock = new Object();
		
		T1_SL t1 = new T1_SL();
		T2_SL t2 = new T2_SL();
		
		t1.start();
		t2.start();
		t1.join();
		t2.join();

	}

}
