/**
 * Port from the SVCOMP15 benchmark.
 */
package svcomp15;

/**
 * @author Suvam Mukherjee.
 *
 * Indian Institute of Science, 2017.
 *
 */
class W1 extends Thread {
	
	public void run() {
		
		synchronized(Sigma.lock) {
			if(!(Sigma.array_index <= 4))
				Sigma.error = 1;
			
			Sigma.array[Sigma.array_index] = 1;
		}
	}
}

class W2 extends Thread {
	
	public void run() {
		synchronized(Sigma.lock) {
			if(!(Sigma.array_index <= 4))
				Sigma.error = 1;
			Sigma.array[Sigma.array_index] = 1;
		}
	}
}

class W3 extends Thread {
	
	public void run() {
		synchronized(Sigma.lock) {
			if(!(Sigma.array_index <= 4))
				Sigma.error = 1;
			Sigma.array[Sigma.array_index] = 1;
		}
	}
}

class W4 extends Thread {
	
	public void run() {
		synchronized(Sigma.lock) {
			if(!(Sigma.array_index <= 4))
				Sigma.error = 1;
			Sigma.array[Sigma.array_index] = 1;
		}
	}
}

public class Sigma {
	
	public static int[] array;
	public static int array_index;
	public static int error;
	public static Object lock;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int sum = 0;
		Sigma.error = 0;
		Sigma.lock = new Object();
		Sigma.array = new int[4];
		Sigma.array_index = 0;
		
		for(int i=0; i<4; i++)
			Sigma.array[i] = 0;
		
		W1 w1 = new W1();
		w1.start();
		synchronized(Sigma.lock){
			Sigma.array_index++;
		}
		W2 w2 = new W2();
		w2.start();
		synchronized(Sigma.lock){
			Sigma.array_index++;
		}
		W3 w3 = new W3();
		w3.start();
		synchronized(Sigma.lock){
			Sigma.array_index++;
		}
		W4 w4 = new W4();
		w4.start();
		synchronized(Sigma.lock){
			Sigma.array_index++;
		}

		try {
			w1.join();
			w2.join();
			w3.join();
			w4.join();

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for(int i=0; i<4; i++)
			sum += Sigma.array[i];
		
		//if(!(sum <= 4))
		//	Sigma.error = 1;
		System.out.println("\nSum: " + sum);
	}

}
