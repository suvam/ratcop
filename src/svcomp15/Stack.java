/**
 * 
 */
package svcomp15;

/**
 * @author Suvam Mukherjee, 2017.
 *
 */

class T1_Stack extends Thread {
	
	public void run() {
	 int i;
	 int j;
	 synchronized(Stack.m) {
		 j = Stack.SIZE;
	 }
	  for(i=0; i<j; i++) {
	    synchronized(Stack.m) {
		    // tmp = push(arr,tmp)
	    	if(!(Stack.top < Stack.SIZE))
	    		Stack.error = 1;
	    	
		    if (Stack.top < Stack.SIZE) {
		    	Stack.top++;
		    }
	    	//Stack.top++;
		    Stack.flag = 1;
	    }
	  }
	}
}

class T2_Stack extends Thread {
	
	public void run() {
	  int i;
	  int j;
	  synchronized(Stack.m) {
			 j = Stack.SIZE;
		 }
	  for(i=0; i<j; i++) {
	    synchronized(Stack.m) {
		    if (Stack.flag == 1) {
		      // pop(arr)
		    //	if(!(Stack.top != 0))
		   // 		Stack.error = 1;
		    	if (Stack.top != 0)
		    		Stack.top--;
//		    	Stack.top--;
		    }
	    }
	  }
	}
}

public class Stack {
	
	public static int SIZE;
	public static int OVERFLOW;
	public static int top;
	public static int[] arr;
	public static int flag;
	public static Object m;
	public static int error;

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		Stack.SIZE = 2;
		Stack.OVERFLOW  = -1;
		Stack.top = 0;
		Stack.arr = new int[SIZE];
		Stack.flag = 0;
		Stack.m = new Object();
		Stack.error = 0;
		
		T1_Stack t1 = new T1_Stack();
		T2_Stack t2 = new T2_Stack();
		
		t1.start();
		t2.start();
		
		t1.join();
		t2.join();
		

	}

}
