/**
 * 
 */
package svcomp15;

/**
 * @author ratcop-vm
 *
 */

class T1_Sync01 extends Thread {
	
	public void run() {
		synchronized(Sync01.m) {
			while(Sync01.num > 0) 
				Sync01.num++;
		}
	}
}

class T2_Sync01 extends Thread {
	
	public void run() {
		synchronized(Sync01.m) {
			while(Sync01.num == 0) 
				Sync01.num--;
		}
	}
}

public class Sync01 {
	
	public static int num;
	public static int error;
	public static Object m;

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		
		Sync01.num = 1;
		Sync01.error = 0;
		Sync01.m = new Object();

		T1_Sync01 t1 = new T1_Sync01();
		T2_Sync01 t2 = new T2_Sync01();
		
		t1.start();
		t2.start();
		
		t1.join();
		t2.join();
		
		if(!(Sync01.num >= 0))
			Sync01.error = 1;
		
		if(!(Sync01.num <= 1))
			Sync01.error = 1;
		
	
	}

}
