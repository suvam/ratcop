/**
 * 
 */
package svcomp15;

/**
 * @author Suvam Mukherjee, 2017.
 *
 */

class T1_Stack_Longest extends Thread {
	
	public void run() {
	 int i;
	  for(i=0; i<Stack_Longest.SIZE; i++) {
	    synchronized(Stack_Longest.m) {
		    // tmp = push(arr,tmp)
	    	if(!(Stack_Longest.top != Stack_Longest.SIZE))
	    		Stack_Longest.error = 1;
	    	
		    if (Stack_Longest.top != Stack_Longest.SIZE) {
		    	Stack_Longest.top++;
		    }
	    	//Stack.top++;
		    Stack_Longest.flag = 1;
	    }
	  }
	}
}

class T2_Stack_Longest extends Thread {
	
	public void run() {
	  int i;
	  for(i=0; i<Stack_Longest.SIZE; i++) {
	    synchronized(Stack_Longest.m) {
		    if (Stack_Longest.flag == 1) {
		      // pop(arr)
		    	if(!(Stack_Longest.top != 0))
		    		Stack_Longest.error = 1;
		    	if (Stack_Longest.top != 0)
		    		Stack_Longest.top--;
//		    	Stack.top--;
		    }
	    }
	  }
	}
}

public class Stack_Longest {
	
	public static int SIZE;
	public static int OVERFLOW;
	public static int top;
	public static int[] arr;
	public static int flag;
	public static Object m;
	public static int error;

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		Stack_Longest.SIZE = 800;
		Stack_Longest.OVERFLOW  = -1;
		Stack_Longest.top = 0;
		Stack_Longest.arr = new int[SIZE];
		Stack_Longest.flag = 0;
		Stack_Longest.m = new Object();
		Stack_Longest.error = 0;
		
		T1_Stack_Longest t1 = new T1_Stack_Longest();
		T2_Stack_Longest t2 = new T2_Stack_Longest();
		
		t1.start();
		t2.start();
		
		t1.join();
		t2.join();
		

	}

}
