/**
 * Port from SVCOMP15 benchmarks.
 */
package svcomp15;

/**
 * @author Suvam Mukherjee.
 *
 * Indian Institute of Science, 2017.
 * Added example.
 *
 */

class T1_CVS extends Thread {

	public void run() {
		int k = 0;
		
		synchronized(Conditionals_VS.lock) {
			if(Conditionals_VS.x == Conditionals_VS.y)
			{
				k = 0;
			} else {
				k = 1;
			}
	
			if(k == 0)
			{
				if(!(Conditionals_VS.x == Conditionals_VS.y))
					Conditionals_VS.error = 1;
			} else {
				if(!(Conditionals_VS.x != Conditionals_VS.y))
					Conditionals_VS.error = 1;
			}
		}
	}
}

class T2_CVS extends Thread {

	public void run() {
		int k  = 0;
		
		synchronized(Conditionals_VS.lock) {
			if(Conditionals_VS.x == Conditionals_VS.y)
			{
				k = 0;
			} else {
				k = 1;
			}
	
			if(k == 0)
			{
				if(!(Conditionals_VS.x == Conditionals_VS.y))
					Conditionals_VS.error = 1;
			} else {
				if(!(Conditionals_VS.x != Conditionals_VS.y))
					Conditionals_VS.error = 1;
			}
		}
	}
}



public class Conditionals_VS {

	public static int x;
	public static int y;
	public static int error;
	public static Object lock;
	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		System.out.println("Conditionals_VS");
		Conditionals_VS.x = 0;
		Conditionals_VS.y = 0;
		Conditionals_VS.error = 0;
		Conditionals_VS.lock = new Object();
		
		T1_CVS t1 = new T1_CVS();
		T2_CVS t2 = new T2_CVS();
		
		t1.start();
		t2.start();
		t1.join();
		t2.join();
		
		
	}

}
