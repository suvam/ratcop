/**
 * Port from SVCOMP15.
 * Symmetry-Aware Predicate Abstraction for Shared-Variable Concurrent Programs (Extended Technical Report). CoRR abs/1102.2330 (2011)
 */
package svcomp15;

/**
 * @author Suvam Mukherjee.
 *
 * Indian Institute of Science, 2017.
 *
 */

class T1_Unverif extends Thread {
	
	public void run() {
		
		synchronized(Unverif.lock) {
			Unverif.r = Unverif.r + 1;
		}
		
		int l = 0;
		
		synchronized(Unverif.lock) {
			if(Unverif.r == 1) {
				Unverif.s = Unverif.s + 1;
				l = l + 1;
				if(!(Unverif.s == l))
					Unverif.error = 1;
			}
		}
	}
}

class T2_Unverif extends Thread {
	
	public void run() {
		
		synchronized(Unverif.lock) {
			Unverif.r = Unverif.r + 1;
		}
		
		int l = 0;
		
		synchronized(Unverif.lock) {
			if(Unverif.r == 1) {
				Unverif.s = Unverif.s + 1;
				l = l + 1;
				if(!(Unverif.s == l))
					Unverif.error = 1;
			}
		}
	}
}
public class Unverif {
	
	public static int r, s, error;
	public static Object lock;

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		System.out.println("Unverif");
		
		Unverif.r = 0;
		Unverif.s = 0;
		Unverif.error = 0;
		Unverif.lock = new Object();
		
		T1_Unverif t1 = new T1_Unverif();
		T2_Unverif t2 = new T2_Unverif();
			
		t1.start();
		t2.start();
		t1.join();
		t2.join();

	}

}
