/**
 * Port from SVCOMP15 benchmarks.
 */
package svcomp15;

/**
 * @author Suvam Mukherjee.
 *
 * Indian Institute of Science, 2017.
 *
 */
class W1_Lazy01 extends Thread {
	
	public void run() {
		synchronized(Lazy01.lock) {
			Lazy01.data++;
		}
	}
}

class W2_Lazy01 extends Thread {
	
	public void run() {
		synchronized(Lazy01.lock){
			Lazy01.data += 2;
		}
	}
}

class W3_Lazy01 extends Thread {
	
	public void run() {
		synchronized(Lazy01.lock) {
			if(!(Lazy01.data <= 3))
				Lazy01.error = 1;
		}
	}
}
public class Lazy01 {

	public static int data;
	public static int error;
	public static Object lock;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Lazy01_False_Unreach_Call");
		Lazy01.data = 0;
		Lazy01.error = 0;
		Lazy01.lock = new Object();
		
		W1_Lazy01 w1 = new W1_Lazy01();
		W2_Lazy01 w2 = new W2_Lazy01();
		W3_Lazy01 w3 = new W3_Lazy01();
		
		w1.start();
		w2.start();
		w3.start();
		
		try {
			w1.join();
			w2.join();
			w3.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}
