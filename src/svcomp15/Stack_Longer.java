/**
 * 
 */
package svcomp15;

/**
 * @author Suvam Mukherjee, 2017.
 *
 */

class T1_Stack_Longer extends Thread {
	
	public void run() {
	 int i;
	  for(i=0; i<Stack_Longer.SIZE; i++) {
	    synchronized(Stack_Longer.m) {
		    // tmp = push(arr,tmp)
	    	if(!(Stack_Longer.top != Stack_Longer.SIZE))
	    		Stack_Longer.error = 1;
	    	
		    if (Stack_Longer.top != Stack_Longer.SIZE) {
		    	Stack_Longer.top++;
		    }
	    	//Stack.top++;
		    Stack_Longer.flag = 1;
	    }
	  }
	}
}

class T2_Stack_Longer extends Thread {
	
	public void run() {
	  int i;
	  for(i=0; i<Stack_Longer.SIZE; i++) {
	    synchronized(Stack_Longer.m) {
		    if (Stack_Longer.flag == 1) {
		      // pop(arr)
		    	if(!(Stack_Longer.top != 0))
		    		Stack_Longer.error = 1;
		    	if (Stack_Longer.top != 0)
		    		Stack_Longer.top--;
//		    	Stack.top--;
		    }
	    }
	  }
	}
}

public class Stack_Longer {
	
	public static int SIZE;
	public static int OVERFLOW;
	public static int top;
	public static int[] arr;
	public static int flag;
	public static Object m;
	public static int error;

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		Stack_Longer.SIZE = 400;
		Stack_Longer.OVERFLOW  = -1;
		Stack_Longer.top = 0;
		Stack_Longer.arr = new int[SIZE];
		Stack_Longer.flag = 0;
		Stack_Longer.m = new Object();
		Stack_Longer.error = 0;
		
		T1_Stack_Longer t1 = new T1_Stack_Longer();
		T2_Stack_Longer t2 = new T2_Stack_Longer();
		
		t1.start();
		t2.start();
		
		t1.join();
		t2.join();
		

	}

}
