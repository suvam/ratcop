/**
 * Port from SVCOMP15 benchmarks.
 */
package svcomp15;

/**
 * @author Suvam Mukherjee.
 *
 * Indian Institute of Science, 2017.
 * Added example.
 *
 */
class iTThread_1 extends Thread {
	
	public void run() {
		synchronized(Twostage_3.lock) {
			Twostage_3.data1value = 1;
		}
		
		synchronized(Twostage_3.lock) {
			Twostage_3.data2value = Twostage_3.data1value + 1;
		}
	}
}

class iTThread_2 extends Thread {
	
	public void run() {
		synchronized(Twostage_3.lock) {
			Twostage_3.data1value = 1;
		}
		
		synchronized(Twostage_3.lock) {
			Twostage_3.data2value = Twostage_3.data1value + 1;
		}
	}
}

class iRThread_1 extends Thread {
	
	public void run() {
		
		int t1 = -1;
		int t2 = -1;
		
		synchronized(Twostage_3.lock) {
			t1 = Twostage_3.data1value;
			t2 = Twostage_3.data2value;
		}
		
		if(t1 != 0) {
			if(!(t2 != (t1+1)))
				Twostage_3.error = 1;
			if(!(t2 == (t1+1)))
				Twostage_3.error = 1;
		}
		
		
	}
}
public class Twostage_3 {

	public static int data1value;
	public static int data2value;
	public static int error;
	public static Object lock;
	
	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {

		System.out.println("\nTwostage_3_False_Unreach_Call");
		Twostage_3.data1value = 0;
		Twostage_3.data2value = 0;
		Twostage_3.error = 0;
		Twostage_3.lock = new Object();

		iTThread_1 t1 = new iTThread_1();
		iTThread_2 t2 = new iTThread_2();
		iRThread_1 t3 = new iRThread_1();
		
		t1.start();
		t2.start();
		t3.start();
		
		t1.join();
		t2.join();
		t3.join();
	}

}
