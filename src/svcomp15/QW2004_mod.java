/**
 * 
 */
package svcomp15;

/**
 * @author Suvam Mukherjee, 2017.
 *
 */
class T1_QW2004_mod extends Thread {
	
	public void run() {
		// BCSP_PnpStop
		 synchronized(QW2004_mod.m) {
		  if (QW2004_mod.pendingIo > 0)
			  QW2004_mod.pendingIo--;
		  QW2004_mod.pending = QW2004_mod.pendingIo;
		  if (QW2004_mod.pending == 0) {
			  QW2004_mod.stoppingEvent = 1;
			  QW2004_mod.stopped = 0;
		  }
		  else
			  QW2004_mod.stopped--;
		 }

	}
}

public class QW2004_mod {
	
	public static int pending, pendingIo, stoppingEvent, stopped, status;
	public static Object m;
	public static int error, conjunct_flag;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		QW2004_mod.pending = 0;
		QW2004_mod.pendingIo = 1;
		QW2004_mod.stoppingEvent = 0;
		QW2004_mod.stopped = 1;
		QW2004_mod.error = 0;
		QW2004_mod.status = 0;
		QW2004_mod.m = new Object();
		 
		T1_QW2004_mod t1 = new T1_QW2004_mod();
		t1.start();
		
		synchronized(QW2004_mod.m) {
			  if (QW2004_mod.stopped==0)
				  QW2004_mod.status = -1;
			  else {
				  QW2004_mod.pendingIo++;
				  QW2004_mod.stopped++;
				  QW2004_mod.status = 0;
			  }
		}
		synchronized(QW2004_mod.m) {
		  if (QW2004_mod.status == 0)
			  if(!(QW2004_mod.pendingIo == QW2004_mod.stopped))
				  QW2004_mod.error = 1;
		}
		synchronized(QW2004_mod.m) {
		  if (QW2004_mod.pendingIo > 1) {
			  QW2004_mod.pendingIo--;
			  QW2004_mod.stopped--;
		  }
		  QW2004_mod.pending = QW2004_mod.pendingIo;
		  if (QW2004_mod.pending == 0)
			  QW2004_mod.stoppingEvent = 1;
		}

	}

}
