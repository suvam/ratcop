/**
 * Modified from standard SVCOMP to talk about integers rather than characters.
 */
package svcomp15;

/**
 * @author Suvam Mukherjee, 2017.
 *
 */
class T1_Singleton extends Thread {
	
	public void run() {
		synchronized(Singleton.lock) {
			Singleton.x = 3;
		}
	}
	
}

class T2_Singleton extends Thread {
	
	public void run() {
		synchronized(Singleton.lock) {
			Singleton.x = 5;
		}
	}
	
}

public class Singleton {
	
	public static int x;
	public static int error;
	public static Object lock;

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		Singleton.x = 0;
		Singleton.error = 0;
		Singleton.lock = new Object();
		
		T1_Singleton t1 = new T1_Singleton();
		T2_Singleton t2 = new T2_Singleton();
		
		t1.start();
		t2.start();
		
		t1.join();
		t2.join();
		
		if(!(Singleton.x == 3))
			Singleton.error = 1;

	}

}
