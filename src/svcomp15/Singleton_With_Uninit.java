/**
 * pthread. Modified to deal with integers rather than characters.
 */
package svcomp15;

/**
 * @author Suvam Mukherjee, 2017.
 *
 */
class T1_Singleton_With_Uninit extends Thread {
	
	public void run() {
		synchronized(Singleton_With_Uninit.lock) {
			Singleton_With_Uninit.x = 3;
		}
	}
	
}

class T2_Singleton_With_Uninit extends Thread {
	
	public void run() {
		synchronized(Singleton_With_Uninit.lock) {
			Singleton_With_Uninit.x = 5;
		}
	}
	
}

public class Singleton_With_Uninit {
	
	public static int x;
	public static int error;
	public static Object lock;

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		Singleton_With_Uninit.x = 0;
		Singleton_With_Uninit.error = 0;
		Singleton_With_Uninit.lock = new Object();
		
		T1_Singleton_With_Uninit t1 = new T1_Singleton_With_Uninit();
		T2_Singleton_With_Uninit t2 = new T2_Singleton_With_Uninit();
		
		t1.start();
		t2.start();
		
		t1.join();
		t2.join();
		
		if(!(Singleton_With_Uninit.x <= 5))
			Singleton_With_Uninit.error = 1;

	}

}
