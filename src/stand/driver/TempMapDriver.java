package stand.driver;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import soot.Body;
import soot.Local;
import soot.Pack;
import soot.PackManager;
import soot.Scene;
import soot.SceneTransformer;
import soot.SootMethod;
import soot.Transform;
import stand.analysis.util.Lvalue;
import stand.analysis.util.TempMap;
import stand.graph.SyncCG;
import stand.util.NonSparkPTA;

public class TempMapDriver extends SceneTransformer {
	
	private List<Body> bl = new LinkedList<Body>();
	private TempMap tmap;
	
	public TempMapDriver() {
		
	}
	
	public void testGetLvalue() {
		
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Pack wjtp = PackManager.v().getPack("wjtp");        
        wjtp.add(new Transform("wjtp.slnpa", new TempMapDriver()));      
        
        System.out.println("Starting analysis");
        soot.Main.main(args);
		

	}

	@SuppressWarnings("unchecked")
	@Override
	protected void internalTransform(String arg0, Map arg1) {
		try {
			SyncCG scg = new SyncCG(Scene.v());
			for(SootMethod sm : scg.getMethods()) {
				bl.add(scg.getBody(sm));
			}
			tmap = new TempMap(bl);
			
			for(SootMethod sm : scg.getMethods()) {
				System.out.println("\n" + sm.getSignature() + ":::");
				for(Local l : scg.getBody(sm).getLocals()) {
					Set<Lvalue> lvals = tmap.getLvalues(l);
					if(lvals != null) {				
						System.out.print("Temp: " + l + " -> ");
						for (Lvalue lv : lvals) {
							System.out.print(lv + " | ");					
						}
						System.out.println();
					}
				}
			}
		} catch (NonSparkPTA e) {
			
			e.printStackTrace();
			
		}
		
	}

}
