package stand.driver;

import java.util.ArrayList;
import java.util.Map;

import soot.Body;
import soot.Local;
import soot.Pack;
import soot.PackManager;
import soot.Scene;
import soot.SceneTransformer;
import soot.SootClass;
import soot.SootField;
import soot.SootMethod;
import soot.Transform;
import soot.Unit;
import soot.Value;
import soot.jimple.EnterMonitorStmt;
import soot.jimple.ExitMonitorStmt;
import soot.jimple.FieldRef;
import soot.jimple.InstanceFieldRef;
import soot.jimple.InstanceInvokeExpr;
import soot.jimple.InvokeExpr;
import soot.jimple.MonitorStmt;
import soot.jimple.Stmt;
import soot.jimple.toolkits.callgraph.ReachableMethods;
import soot.jimple.toolkits.pointer.StrongLocalMustAliasAnalysis;
import soot.toolkits.graph.UnitGraph;
import soot.toolkits.graph.pdg.EnhancedUnitGraph;
import soot.util.Chain;
import stand.analysis.npa.SyncLockNPA;
import stand.analysis.util.AliasAnalysis;
import stand.analysis.util.Lvalue;
import stand.analysis.util.LvalueSet;
import stand.graph.SyncCG;
import stand.util.NonSparkPTA;

public class SyncLockNPADriver extends SceneTransformer{
	
	@SuppressWarnings("unchecked")
	@Override
	protected void internalTransform(String arg0, Map arg1) {
		System.out.println("****\nPerforming Lock-aware value-set Analysis****");
		System.out.println(Scene.v().getApplicationClasses());
		SyncCG scg = null;
		SyncLockNPA snpa = null;
		long size = 0;
		long time = -1; 
		try {
			long start = System.currentTimeMillis();
			scg = new SyncCG(Scene.v());
			size = scg.getCodeSize();
			//scg.printSyncCG();
			AliasAnalysis aa = new AliasAnalysis(Scene.v().getPointsToAnalysis());
			snpa = new SyncLockNPA(scg, aa);
			long end = System.currentTimeMillis();
			time = end - start;
			System.out.println("\nTime taken by analysis: " + (end - start) + "ms");
			
		} catch (NonSparkPTA e) {			
			e.printStackTrace();
			System.exit(-1);
		}
		
		int totalref = 0;
		int safe = 0;
		
		System.out.println("\n**************UNSAFE STATEMENTS************\n");
		for(SootMethod sm : scg.getMethods()) {
			//System.out.println("Method: " + sm.getSignature());
			Body b = sm.getActiveBody();
			for(Unit u : b.getUnits()) {
				
				if(u instanceof EnterMonitorStmt || u instanceof ExitMonitorStmt)
					continue;
				
				Stmt s = (Stmt) u;
				
				if(s.containsFieldRef()) {
					
					FieldRef fr = s.getFieldRef();
					if(fr instanceof InstanceFieldRef) {						
						Value v = ((InstanceFieldRef) fr).getBase();
						if(v instanceof Local) {         
							totalref++;
							Lvalue lv = new Lvalue(((Local) v), new ArrayList<SootField>());
							LvalueSet set = snpa.getAggrResult(u);
							//System.out.println("\nInstanceFieldRef on LValue: " + lv);
							//System.out.println(set);
							if(set.isFull() || set.contains(lv)){
								safe++;
								//System.out.println("Safe");
							}
							else	{						
								System.out.println("Method: <"+ sm + "> Instr: " + s);
								System.out.println("\nAssociated fact: " + snpa.getLocalResultWithPrinting(u));
							}
						}
					}
				}
				else if(s.containsInvokeExpr()) {
					InvokeExpr ie = s.getInvokeExpr();
					if(ie instanceof InstanceInvokeExpr) {
						
						Value v = ((InstanceInvokeExpr) ie).getBase();
						if(v instanceof Local) {
							totalref++;
							Lvalue lv = new Lvalue(((Local) v), new ArrayList<SootField>());
							LvalueSet set = snpa.getAggrResult(u);	
							//System.out.println(set);
							//System.out.println("\n********************");
							snpa.getLocalResultWithPrinting(u);
							//System.out.println("\nInstanceInvokeExpr on LValue: " + lv);
							//System.out.println(set);
							if(set.isFull() || set.contains(lv)){
								safe++;
								//System.out.println("Safe");
							}
							else {							
								System.out.println("Method: <"+ sm + "> Instr: " + s);
								System.out.println("\nAssociated fact: " + snpa.getLocalResultWithPrinting(u));
							}
						}
					}
				}
				else if(s instanceof MonitorStmt) {
					Value v  = ((MonitorStmt) s).getOp();
					if(v instanceof Local) {
						totalref++;
						Lvalue lv = new Lvalue(((Local) v), new ArrayList<SootField>());
						LvalueSet set = snpa.getAggrResult(u);
						//System.out.println(set);
						//System.out.println("\nMonitorStmt on LValue: " + lv);
						//System.out.println(set);
						if(set.isFull() || set.contains(lv)){
							safe++;
							//System.out.println("Safe");
						}
						else {							
							System.out.println("Method: <"+ sm + "> Instr: " + s);
							System.out.println("\nAssociated fact: " + snpa.getLocalResultWithPrinting(u));
						}
					}
				
				}
//				System.out.println("\n----------------------------------------");
//				System.out.println("IN: " + snpa.getLocalResult(u).toString());
//				System.out.println("Unit: " + u);		
//				System.out.println("----------------------------------------");	

			}
		}
		System.out.println("\nTotal: " + totalref);
		System.out.println("Safe: " + safe);
		System.out.println("\nTime taken by analysis: " + time + "ms");
		System.out.println("Size: " + size);
		/*
		while (uIt.hasNext()) {
			Unit u = (Unit) uIt.next();

			System.out.println("IN: " + snpa.getResult().get(u).toString());
			System.out.println("Unit: " + u);			
			
		}*/
				
	}
		
		public static void main(String[] args) {
			
			String[] soot_args = new String[]{
					//"-v",		// Increase verbosity
					"-w",
					"-p",
					"cg.spark",	// Explicitly run the SPARK phase 
					"enabled:true",
					"-p",
					"jb",
					"use-original-names:true",
					"-cp",
					args[0], 
					//"-allow-phantom-refs",
					"-app",
					args[1],
					//"-i"
					//"java.",	 
					"-f",
					"jimple",
					//"-x",
					//"jdk",
					//"-no-bodies-for-excluded"
					
			};
			Pack wjtp = PackManager.v().getPack("wjtp");     
	        wjtp.add(new Transform("wjtp.slnpa", new SyncLockNPADriver()));
	        
	        
	        System.out.println("Starting analysis");
	        soot.Main.main(soot_args);
		}
		

}
