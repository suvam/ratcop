package stand.driver;

/*
 * Original author: Arnab De
 * 
 * Major fixes, additions: Suvam Mukherjee, 2016
 */

import java.util.ArrayList;
import java.util.Map;

import soot.Body;
import soot.Local;
import soot.Pack;
import soot.PackManager;
import soot.Scene;
import soot.SceneTransformer;
import soot.SootField;
import soot.SootMethod;
import soot.Transform;
import soot.Unit;
import soot.Value;
import soot.jimple.FieldRef;
import soot.jimple.InstanceFieldRef;
import soot.jimple.InstanceInvokeExpr;
import soot.jimple.InvokeExpr;
import soot.jimple.MonitorStmt;
import soot.jimple.Stmt;
import stand.analysis.npa.SyncNPA;
import stand.analysis.util.AliasAnalysis;
import stand.analysis.util.Lvalue;
import stand.analysis.util.LvalueSet;
import stand.graph.SyncCG;
import stand.util.NonSparkPTA;

public class SyncNPADriver extends SceneTransformer {
	
	@SuppressWarnings("unchecked")
	@Override
	protected void internalTransform(String arg0, Map arg1) {
		System.out.println("****\nPerforming sync-CFG Analysis****");
		System.out.println(Scene.v().getApplicationClasses());
		SyncCG scg = null;
		SyncNPA snpa = null;
		long size = 0;
		long time = -1; 
		try {
			long start = System.currentTimeMillis();
			scg = new SyncCG(Scene.v());
			size = scg.getCodeSize();
			//scg.printSyncCG();
			AliasAnalysis aa = new AliasAnalysis(Scene.v().getPointsToAnalysis());
			snpa = new SyncNPA(scg, aa);
			long end = System.currentTimeMillis();
			time = end - start;
			//System.out.println("\nTime taken by analysis: " + (end - start) + "ms");
			
		} catch (NonSparkPTA e) {			
			e.printStackTrace();
			System.exit(-1);
		}
		
		int totalref = 0;
		int safe = 0;
		
		System.out.println("\n**************UNSAFE STATEMENTS************\n");
		for(SootMethod sm : scg.getMethods()) {
			//System.out.println();
			//System.out.println("Method: " + sm.getSignature());
			Body b = sm.getActiveBody();
			for(Unit u : b.getUnits()) {
				Stmt s = (Stmt) u;
				
//				System.out.println("\n----------------------------------------");
//				System.out.println("IN: " + snpa.getLocalResult(u).toString());
//				System.out.println("Unit: " + u);		
//				System.out.println("----------------------------------------");	
				
				if(s.containsFieldRef()) {
					FieldRef fr = s.getFieldRef();
					if(fr instanceof InstanceFieldRef) {						
						Value v = ((InstanceFieldRef) fr).getBase();
						if(v instanceof Local) {         
							totalref++;
							Lvalue lv = new Lvalue(((Local) v), new ArrayList<SootField>());
							LvalueSet set = snpa.getAggrResult(u);		
							//System.out.println("\nInstanceFieldRef on LValue: " + lv);
							//System.out.println(set);
							if(set.isFull() || set.contains(lv)){
								safe++;
								//System.out.println("Safe");
							}
							else
								System.out.println("Method: <"+ sm + "> Instr: " + s);
						}
					}
				}
				else if(s.containsInvokeExpr()) {
					InvokeExpr ie = s.getInvokeExpr();
					if(ie instanceof InstanceInvokeExpr) {
						
						Value v = ((InstanceInvokeExpr) ie).getBase();
						if(v instanceof Local) {
							totalref++;
							Lvalue lv = new Lvalue(((Local) v), new ArrayList<SootField>());
							//System.out.println("Unit: " + s + " : " +snpa.getAggrResult(u).toString());
							LvalueSet set = snpa.getAggrResult(u);	
							//System.out.println("\nInstanceInvokeExpr on LValue: " + lv);
							//System.out.println(set);
							if(set.isFull() || set.contains(lv)){
								safe++;
								//System.out.println("Safe");
							}
							else							
								System.out.println("Method: <"+ sm + "> Instr: " + s);
						}
					}
				}
				else if(s instanceof MonitorStmt) {
					Value v  = ((MonitorStmt) s).getOp();
					if(v instanceof Local) {
						totalref++;
						Lvalue lv = new Lvalue(((Local) v), new ArrayList<SootField>());
						LvalueSet set = snpa.getAggrResult(u);	
						//System.out.println("\nMonitorStmt on LValue: " + lv);
						//System.out.println(set);
						if(set.isFull() || set.contains(lv)){
							safe++;
							//System.out.println("Safe");
						}
						else
							System.out.println("Method: <"+ sm + "> Instr: " + s);
					}
				
				}
				
				

			}
		}
		System.out.println("\nTotal: " + totalref);
		System.out.println("Safe: " + safe);
		System.out.println("\nTime taken by analysis: " + time + "ms");
		System.out.println("Size: " + size);
		/*
		while (uIt.hasNext()) {
			Unit u = (Unit) uIt.next();

			System.out.println("IN: " + snpa.getResult().get(u).toString());
			System.out.println("Unit: " + u);			
			
		}*/

	}
	
	public static void main(String[] args) {
		
		String[] soot_args = new String[]{
				//"-v",		// Increase verbosity
				"-w",
				"-p",
				"cg.spark",	// Explicitly run the SPARK phase 
				"enabled:true",
				"-cp",
				args[0], 
				//"-allow-phantom-refs",
				"-app",
				args[1],
				//"-i"
				//"java.",	 
				"-f",
				"jimple",
				//"-x",
				//"jdk",
				//"-no-bodies-for-excluded"
				
		};
		Pack wjtp = PackManager.v().getPack("wjtp");     
        wjtp.add(new Transform("wjtp.snpa", new SyncNPADriver()));
        
        
        System.out.println("Starting analysis");
        soot.Main.main(soot_args);
	}

}
