package stand.driver;

import java.util.Map;

import soot.PackManager;
import soot.Scene;
import soot.SceneTransformer;
import soot.Transform;

import stand.graph.SyncCG;
import stand.util.NonSparkPTA;

public class SyncCGDriver extends SceneTransformer {
	
	@SuppressWarnings("unchecked")
	@Override
	protected void internalTransform(String phase, Map options) {
		
		try {
			SyncCG scg = new SyncCG(Scene.v());
			scg.printSyncCG();
		} catch (NonSparkPTA e1) {			
			e1.printStackTrace();
		}
		
	} 

	public static void main(String[] args) {
		PackManager.v().getPack("wjtp").add(new Transform ("wjtp.synccg", new SyncCGDriver()));
		
		String[] soot_args = new String[]{
				//"-v",		// Increase verbosity
				"-w",
				"-p",
				"cg.spark",	// Explicitly run the SPARK phase 
				"enabled:true",
				"-cp",
				args[0], 
				//"-allow-phantom-refs",
				"-app",
				args[1],
				//"-i"
				//"java.",	 
				"-f",
				"jimple",
				//"-x",
				//"jdk",
				//"-no-bodies-for-excluded"
				
		};
		soot.Main.main(soot_args);
		
	}

}

