/**
 * 
 */
package stand.driver;

import java.util.HashMap;
import java.util.Map;


/*
 * Experimental class to check if we are able to detect aliasing of the monitor variables within a method correctly.
 */



import soot.jimple.CaughtExceptionRef;
import soot.jimple.EnterMonitorStmt;
import soot.jimple.ExitMonitorStmt;
import soot.jimple.IdentityStmt;
import soot.jimple.IfStmt;
import soot.jimple.ReturnStmt;
import soot.jimple.ReturnVoidStmt;
import soot.jimple.Stmt;
import soot.jimple.toolkits.callgraph.CallGraph;
import soot.jimple.toolkits.callgraph.Edge;
import soot.jimple.toolkits.callgraph.ReachableMethods;
import soot.jimple.toolkits.pointer.LocalMustAliasAnalysis;
import soot.jimple.toolkits.pointer.StrongLocalMustAliasAnalysis;
import soot.toolkits.graph.BriefUnitGraph;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.toolkits.graph.UnitGraph;
import soot.toolkits.graph.pdg.EnhancedUnitGraph;
import soot.util.Chain;
import soot.Body;
import soot.Local;
import soot.PackManager;
import soot.Scene;
import soot.SceneTransformer;
import soot.SootClass;
import soot.SootMethod;
import soot.Transform;
import soot.Unit;
import soot.Value;
/**
 * @author suvam
 *
 */
public class AliasTestDriver extends SceneTransformer {
	
	//private SyncCG scg;
	private Map<SootMethod, UnitGraph> methToCfg;	// maps methods to cfg
	private Scene appscene;
	private soot.jimple.toolkits.callgraph.CallGraph cg;
	
	protected void internalTransform(String phase, Map options)
	{
		appscene = Scene.v();
		cg = appscene.getCallGraph();
		Chain<SootClass> appClasses = appscene.getApplicationClasses();
		ReachableMethods rm = appscene.getReachableMethods();
		rm.update();
		methToCfg = new HashMap<SootMethod, UnitGraph>();
		
		Map<Unit, Value> enterlocks = new HashMap<Unit, Value>();
		Map<Unit, Value> exitlocks = new HashMap<Unit, Value>();
		Map<Value, Value> exitLocksToEnterLocks = new HashMap<Value, Value>();	// maps the exit lock vars to entry lock vars
		
		// Iterate over each reachable method of each application class
		for(SootClass currClass: appClasses)
		{
			for(SootMethod currMethod: currClass.getMethods())
			{
				if(rm.contains(currMethod))
				{
					System.out.println("\nAnalysing: Class: " + currClass + " Method: " + currMethod);
					if(currMethod.toString().contains("<init>"))
						continue;
					
					Body b = currMethod.getActiveBody();
					UnitGraph g = new ExceptionalUnitGraph(b);
					
					StrongLocalMustAliasAnalysis mustAlias = new StrongLocalMustAliasAnalysis(g);
					Value lock;
					if(currMethod.isSynchronized()) {		// Method is synchronized		
						if(currMethod.isStatic())
							//throw new NotYetImplementedException("static sync method");
							continue;
						else {
							lock = b.getThisLocal();
							for(Unit u : g.getHeads()) {
								enterlocks.put(u, lock);
							}
							for(Unit u : g.getTails()) {
								exitlocks.put(u, lock);
							}
						}
					}
					for(Unit currunit : b.getUnits()) {
						if (currunit instanceof EnterMonitorStmt) {
							lock = ((EnterMonitorStmt) currunit).getOp();
							enterlocks.put(currunit, lock);
						}
						else if (currunit instanceof ExitMonitorStmt) {
							lock = ((ExitMonitorStmt) currunit).getOp();
							exitlocks.put(currunit, lock);
						}
					}
					
					System.out.println("\nSet of exit locks: ");
					// Print out list of exit locks
					for(Unit unlockStmt : exitlocks.keySet())
						System.out.println(unlockStmt + " " + exitlocks.get(unlockStmt));
					
					System.out.println("\nSet of entry locks: ");
					// Print out list of entry locks
					for(Unit lockStmt : enterlocks.keySet())
						System.out.println(lockStmt + " " + enterlocks.get(lockStmt));
					
					for(Unit unlockStmt: exitlocks.keySet())
					{
						for(Unit lockStmt: enterlocks.keySet())
						{
							Value unlockOp = exitlocks.get(unlockStmt);
							Value lockOp = enterlocks.get(lockStmt);
							
							//System.out.println("\nCheck if mustAlias is null: " + mustAlias);
							try {
								if(mustAlias.mustAlias((Local)unlockOp, (Stmt) unlockStmt, (Local)lockOp, (Stmt) lockStmt))
								{
									exitLocksToEnterLocks.put(unlockOp, lockOp);
									//System.out.println("\nExit lock " + unlockOp + " aliased with Entry lock " + lockOp);
								}
							}
							catch(NullPointerException e)
							{
								//System.out.println("\nNull pointer exception while processing exitlock " + unlockOp + " entry lock " + lockOp);
							}
							
						}
					}
					
					System.out.println("\nPrinting Lock Correlations: ");
					for(Value unlockOp : exitLocksToEnterLocks.keySet())
					{
						System.out.println(unlockOp + " <--> " + exitLocksToEnterLocks.get(unlockOp));
					}
					
					System.out.println("\nFinished analysing: Class: " + currClass + " Method: " + currMethod);
				}
				
			}
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PackManager.v().getPack("wjtp").add(new Transform ("wjtp.aliasTestDriver", new AliasTestDriver()));
		
		String[] soot_args = new String[]{
				//"-v",		// Increase verbosity
				"-w",
				"-p",
				"cg.spark",	// Explicitly run the SPARK phase 
				"enabled:true",
				"-cp",
				args[0], 
				//"-allow-phantom-refs",
				"-app",
				args[1],
				//"-i"
				//"java.",	 
				"-f",
				"jimple",
				//"-x",
				//"jdk",
				//"-no-bodies-for-excluded"
				
		};
		soot.Main.main(soot_args);
		
	}

}
