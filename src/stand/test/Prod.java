package stand.test;

public class Prod extends Thread {
	public Buff buff;
		
	public Prod(Buff b) {
		buff = b;
	}

	@Override
	public void run() {		
		int oldv;
		
		while(true) {
			// unsafe update to buff
			/*
			synchronized(buff) {				
				oldv = buff.data.intValue();
				buff.data = null;
			}
			
				int newv = oldv + 1;
			
			synchronized(buff) {
				buff.data = new Integer(newv);						
			}
			*/
			
			// safe update to buff
			synchronized(buff) {				
				oldv = buff.data.intValue();
				buff.data = null;
			
			
				int newv = oldv + 1;
			
			
				buff.data = new Integer(newv);						
			}
		}
	}
	

}