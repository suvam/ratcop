package stand.test;

public class SimpleNPATest {
	
	private Object o;
	
	public SimpleNPATest() {
		o = new Object();
	}
	
	public Object bar() {return null;}
	
	public Object foo(Object o) {
		return foo1(o);
	}
	
	public Object foo1(Object o) {
		return foo2(o);
		//return o;
	}
	
	public Object foo2(Object o) {
		return o;
	}
	
	public static void main(String[] args) {
		SimpleNPATest st = new SimpleNPATest();
		//SimpleNPATest st1 = st;				
		
		Object o = st.foo(st.o);
		
		Object o1 = st.foo(null);
		
		o.hashCode();
		o1.hashCode();	
	}

}
