package stand.test;

public class PCMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Integer i = new Integer(0);
		Buff b = new Buff(i);
		b.data = new Integer(0);
		
		Prod p = new Prod(b);		
		Cons c = new Cons(b);
		
		p.start();
		c.start();	

	}

}
