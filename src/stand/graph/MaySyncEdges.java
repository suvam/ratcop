package stand.graph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import stand.util.NonSparkPTA;
import stand.util.NotYetImplementedException;

import soot.Body;
import soot.Local;
import soot.PointsToAnalysis;
import soot.Scene;
import soot.Unit;
import soot.SootMethod;
import soot.Value;
import soot.jimple.EnterMonitorStmt;
import soot.jimple.ExitMonitorStmt;
import soot.jimple.Stmt;
import soot.jimple.spark.pag.PAG;
import soot.jimple.toolkits.thread.mhp.MhpTester;
import soot.jimple.toolkits.thread.mhp.MhpTransformer;
import soot.jimple.toolkits.thread.mhp.StartJoinFinder;
import soot.toolkits.graph.UnitGraph;

public class MaySyncEdges {
	private Map<Unit, Set<Unit>> monedges = new HashMap<Unit, Set<Unit>>();
	private Map<Unit, Set<Unit>> startedges = new HashMap<Unit, Set<Unit>>();
	private Map<SootMethod, UnitGraph> methtocfg;
	private PointsToAnalysis pta;
	private StartJoinFinder sjf;
	private MhpTester mhp;
	
	public MaySyncEdges(Scene s, Map<SootMethod, UnitGraph> mg) throws NonSparkPTA, NotYetImplementedException {
		this.methtocfg = mg;
		pta = Scene.v().getPointsToAnalysis();
		/*
		 * Suvam: The following exception doesn't need to be thrown. getPointsToAnalysis only returns DumbPointerAnalysis if SPARK is not
		 * run prior to executing the transform. We explicitly add the SPARK phase in the command line arguments in the SyncNPADriver.
		 */
		/*
		if(!(pta instanceof PAG))
			throw new NonSparkPTA();
		*/
		sjf = new StartJoinFinder(Scene.v().getCallGraph(), (PAG)pta);
		mhp = MhpTransformer.v().getMhpTester();

		startRunEdges();
		monitorEdges();
		/* volatileEdges(); 
		endJoinEdges(); */
		
	}

	private void monitorEdges() throws NotYetImplementedException {
		// find lock statements
		Map<Unit, Value> enterlocks = new HashMap<Unit, Value>();
		Map<Unit, Value> exitlocks = new HashMap<Unit, Value>();
		Map<Unit, SootMethod> monstmttometh = new HashMap<Unit, SootMethod>();
		
		for(SootMethod currsm : methtocfg.keySet()) {
			Body currbody = currsm.getActiveBody();
			Value lock;
			if(currsm.isSynchronized()) {		// Method is synchronized		
				if(currsm.isStatic())
					//throw new NotYetImplementedException("static sync method");
					continue;
				else {
					lock = currbody.getThisLocal();
					for(Unit u : methtocfg.get(currsm).getHeads()) {
						enterlocks.put(u, lock);
						monstmttometh.put(u, currsm);
					}
					for(Unit u : methtocfg.get(currsm).getTails()) {
						exitlocks.put(u, lock);
						monstmttometh.put(u, currsm);
					}
				}
			}
			for(Unit currunit : currbody.getUnits()) {
				if (currunit instanceof EnterMonitorStmt) {
					lock = ((EnterMonitorStmt) currunit).getOp();
					enterlocks.put(currunit, lock);
					monstmttometh.put(currunit, currsm);
				}
				else if (currunit instanceof ExitMonitorStmt) {
					lock = ((ExitMonitorStmt) currunit).getOp();
					exitlocks.put(currunit, lock);
					monstmttometh.put(currunit, currsm);
				}
			}
		}
		
		for(Unit unlockstmt : exitlocks.keySet()) {
			for(Unit lockstmt : enterlocks.keySet()) {
				Value lock1 = exitlocks.get(unlockstmt);
				Value lock2 = enterlocks.get(lockstmt);
				Boolean isalias = pta.reachingObjects((Local) lock1).hasNonEmptyIntersection(pta.reachingObjects((Local) lock2));
				if(isalias) {
					if(mhp.mayHappenInParallel(monstmttometh.get(unlockstmt), /*unlockstmt,*/ monstmttometh.get(lockstmt)/*, lockstmt*/)) {
						Set<Unit> targetset = monedges.get(unlockstmt);
						if (targetset == null) {
							targetset = new HashSet<Unit>();
							monedges.put(unlockstmt, targetset);
						}
						targetset.add(lockstmt);
						
					}
				}
			}
		}
		
		/*for(Unit u : enterlocks.keySet()) {
			System.out.println(u + " --> " + enterlocks.get(u));
		}
		for(Unit u : exitlocks.keySet()) {
			System.out.println(u + " --> " + exitlocks.get(u));
		}*/
	}

//	private void endJoinEdges() {
//		Map<Stmt, Stmt> stoj = sjf.getStartToJoin();
//		for(Stmt s : stoj.keySet()) {
//			System.out.println(s + " --> " + stoj.get(s));
//		}
//		
//	}

	private void startRunEdges() {		
		Map<Stmt, List<SootMethod>> sttorun =  sjf.getStartToRunMethods();
		for(Stmt currstmt : sttorun.keySet()) {
			List<SootMethod> runmeths = sttorun.get(currstmt);
			for(SootMethod currmeth : runmeths) {
				UnitGraph currcfg = methtocfg.get(currmeth);
				if(currcfg != null) {
					List<Unit> runheads = currcfg.getHeads();
					Set<Unit> targetset = startedges.get(currstmt);
					if (targetset == null) {
						targetset = new HashSet<Unit>();
						startedges.put(currstmt, targetset);
					}
					targetset.addAll(runheads);
						
				}
					
			}
		}
		
	}
	
	public Set<Unit> getMonitorSucc(Unit u) {		
		return monedges.get(u);
	}
	
	public Set<Unit> getStartSucc(Unit u) {		
		return startedges.get(u);
	}
	
	public void printEdges() {
		
		for(Unit currunit : startedges.keySet()) {
			System.out.println(currunit + " -->");
			Set<Unit> sttargets = startedges.get(currunit);
			if(sttargets != null) {				
				for(Unit u : sttargets) {
					System.out.println("\t" + u);
				}
			}
			Set<Unit> montargets = monedges.get(currunit);
			if(montargets != null) {				
				for(Unit u : montargets) {
					System.out.println("\t" + u);
				}
			}
		}
	}
}
