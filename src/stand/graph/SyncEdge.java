package stand.graph;

import soot.Unit;

public class SyncEdge {
	private EdgeKind kind;
	private Unit source;
	private Unit target;
	
	public SyncEdge(Unit su, Unit tu, EdgeKind k) {
		kind = k;
		source = su;
		target = tu;
	}
	
	public Unit getSource() {return source;}
	public Unit getTarget() {return target;}
	public EdgeKind getKind() {return kind;} 
	
	@Override
	public boolean equals(Object other) {
		if(this == other) return true;
		
		if(other == null) return false;
		if (this.getClass() != other.getClass()) return false; 
		
		SyncEdge oe = (SyncEdge) other;
		return ((kind.equals(oe.kind)) && (source.equals(oe.source)) && (target.equals(oe.target)));
	}
	
	@Override
	public int hashCode() {
//		if(kind==null) {
//			System.out.println("KIND is NULL");
//		}
//		if(source==null) {
//			System.out.println("SOURCE is NULL");
//		}
//		if(target==null) {
//			System.out.println("TARGET is NULL");
//		}
		return kind.hashCode() + source.hashCode() + target.hashCode();
	}

}

