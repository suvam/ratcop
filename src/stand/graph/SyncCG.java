package stand.graph;

/*
 * Suvam: Comments added.
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import soot.jimple.CaughtExceptionRef;
import soot.jimple.IdentityStmt;
import soot.jimple.IfStmt;
import soot.jimple.ReturnStmt;
import soot.jimple.ReturnVoidStmt;
import soot.jimple.toolkits.callgraph.CallGraph;
import soot.jimple.toolkits.callgraph.Edge;
import soot.jimple.toolkits.callgraph.ReachableMethods;
import soot.toolkits.graph.BriefUnitGraph;
import soot.toolkits.graph.UnitGraph;
import soot.util.Chain;
import soot.Body;
import soot.Local;
import soot.Scene;
import soot.SootClass;
import soot.SootMethod;
import soot.Unit;

import stand.util.NonSparkPTA;
import stand.util.NotYetImplementedException;

class RetInfo {
	public RetInfo(Unit c, Unit t) {
		caller = c;
		tgt = t;
	}
	Unit caller;
	Unit tgt;
}

public class SyncCG implements Iterable<Unit> {
	private Scene sc;
	private CallGraph cg;
	private MaySyncEdges se;
	private Map<SootMethod, UnitGraph> methtocfg;	// maps methods to its cfg
	private Set<Unit> nodes;
	private Map<Unit, SootMethod> unittometh;	// maps units to the method containing it
	private Map<Unit, Set<RetInfo>> retedges;	
	private Map<Local, SootMethod> localtometh;	// maps local vars to the method containing it
	
	public SyncCG(Scene appscene) throws NonSparkPTA {
		sc = appscene;
		cg = appscene.getCallGraph();
		ReachableMethods rm = Scene.v().getReachableMethods();
		rm.update();
		methtocfg = new HashMap<SootMethod, UnitGraph>();
		nodes = new HashSet<Unit>();
		unittometh = new HashMap<Unit, SootMethod>();
		localtometh = new HashMap<Local, SootMethod>();
				
		Chain<SootClass> scc = Scene.v().getApplicationClasses();
		
		// construct method to cfg and unit to method
		for(SootClass currsc : scc) {			
			for(SootMethod currsm : currsc.getMethods()) {	
				
				// build cfg and reverse map from units to method and locals to method
				if(rm.contains(currsm)) {				
					//System.out.println("\nSyncCG.java: Current analyzing: " + currsc + " " + currsm);
					Body b = currsm.retrieveActiveBody();
					UnitGraph cfg = new BriefUnitGraph(b);
					methtocfg.put(currsm, cfg);
					nodes.addAll(b.getUnits());
					for(Unit u : b.getUnits()) {
						unittometh.put(u, currsm);
					}
					
					for(Local l : b.getLocals()) {
						localtometh.put(l, currsm);
					}
				}
					
			}							
		}		
		
		// sync edges
		try {
			//System.out.println("\nAttempting to create se");
			se = new MaySyncEdges(Scene.v(), methtocfg);
			//System.out.println("\nValue of se: " + se);
		} catch (stand.util.NonSparkPTA e1) {
			
			e1.printStackTrace();
		} catch (NotYetImplementedException e1) {

			e1.printStackTrace();
		}
		
		// return edges
		retedges = new HashMap<Unit, Set<RetInfo>>();
		for(Unit currunit : nodes) {
			SootMethod srcmeth = unittometh.get(currunit);
			UnitGraph srccfg = methtocfg.get(srcmeth);
			Iterator<Edge> cgit = cg.edgesOutOf(currunit);
			while(cgit.hasNext()) {
				Edge e = cgit.next();						
				SootMethod m = e.getTgt().method();				
				UnitGraph mcfg = methtocfg.get(m);
				if(mcfg == null) continue;
				
				List<Unit> tails = mcfg.getTails();							
				if(e.isExplicit() || e.isInstance() || e.isStatic()) {
					for(Unit t : tails) {
						if(!((t instanceof ReturnStmt) || (t instanceof ReturnVoidStmt))) continue;
						Set<RetInfo> rettgt = retedges.get(t);
						if(rettgt == null) {
							rettgt = new HashSet<RetInfo>();
							retedges.put(t, rettgt);
						}
						for(Unit su : srccfg.getSuccsOf(currunit)) {
							RetInfo ri = new RetInfo(currunit, su);
							rettgt.add(ri);
						}
						//rettgt.addAll(srccfg.getSuccsOf(currunit));
						
					}
				}
			}
		}
	}
	
	
	public HashMap<SootMethod, UnitGraph> getMethToCFG() {
		return (HashMap<SootMethod, UnitGraph>) methtocfg;
	}
	
	public List<Unit> getHeads() {
		SootMethod main = sc.getMainMethod();
		return methtocfg.get(main).getHeads();
	}
	
	public List<SyncEdge> edgesOutOf(Unit u) {
		SootMethod sm = unittometh.get(u);
		if(sm == null)
			return null;
		
		List<SyncEdge> res = new ArrayList<SyncEdge>();
		boolean cgflag = false;
		// get the callgraph edges
		Iterator<Edge> cgit = cg.edgesOutOf(u);
		while(cgit.hasNext()) {
			Edge e = cgit.next();						
			SootMethod m = e.getTgt().method();
			//System.out.println("HERE   " + m.getSignature() + "\n\n");
			UnitGraph mcfg = methtocfg.get(m);
			if(mcfg == null) continue;
			
			//TODO: bad hack
			if(e.isClinit()) continue;
			
			List<Unit> heads = mcfg.getHeads();
			for(Unit h : heads) {
				if((h instanceof IdentityStmt) && (((IdentityStmt) h).getRightOp() instanceof CaughtExceptionRef)) continue;
				if(e.isClinit()) {
					cgflag = true;
					res.add(new SyncEdge(u, h, EdgeKind.CLINIT));
				}			
				else if(e.isExplicit()) {
					cgflag = true;
					res.add(new SyncEdge(u, h, EdgeKind.EXPLICIT));
				}
				else if(e.isInstance()) {
					cgflag = true;
					res.add(new SyncEdge(u, h, EdgeKind.INSTANCE));
				}
				else if(e.isStatic()) {
					cgflag = true;
					res.add(new SyncEdge(u, h, EdgeKind.STATIC));
				}					
			}
		}
		
		if (!cgflag) {
			// get the cfg edges		
			UnitGraph cfg = methtocfg.get(sm);
			
			/*List<UnitBox> ubl = u.getUnitBoxes();
			Set<Unit> branchset = new HashSet<Unit>();
			for(UnitBox ub : ubl)
				branchset.add(ub.getUnit());*/
			Boolean isbranch = (u instanceof IfStmt);
			Unit target = null;
			if(isbranch)
				target = ((IfStmt) u).getTarget();
			List<Unit> ul = cfg.getSuccsOf(u);
			for(Unit currunit : ul) {
				//Boolean isbranch = branchset.contains(currunit);
				SyncEdge s;
				if(isbranch && currunit.equals(target))
					s = new SyncEdge(u, currunit, EdgeKind.BRANCH);
				else if(isbranch)
					s = new SyncEdge(u, currunit, EdgeKind.BRANCH_FALL_THROUGH);
				else
					s = new SyncEdge(u, currunit, EdgeKind.FALL_THROUGH);
				res.add(s);
			}
		}
		
		
		// return edges
		Set<RetInfo> rets = retedges.get(u);
		if(rets != null) {
			for(RetInfo ri : rets) {
				res.add(new SyncEdge(u, ri.tgt, EdgeKind.RETURN));
			}
		}
		
		// get the sync edges
		// Suvam: added the se != null constraint
		if(se != null) {
			Set<Unit> startsuccs = se.getStartSucc(u);
			if(startsuccs != null) {
				for(Unit ss : startsuccs)  {
					if((ss instanceof IdentityStmt) && (((IdentityStmt) ss).getRightOp() instanceof CaughtExceptionRef)) continue;
					res.add(new SyncEdge(u, ss, EdgeKind.START));
				}
			}
		}
		
		// Suvam: added the se != null constraint
		if(se != null) {
			Set<Unit> monsuccs = se.getMonitorSucc(u);
			if(monsuccs != null) {
				for(Unit ss : monsuccs) {
					if((ss instanceof IdentityStmt) && (((IdentityStmt) ss).getRightOp() instanceof CaughtExceptionRef)) continue;
					res.add(new SyncEdge(u, ss, EdgeKind.MON));
				}
			}
		}
	
		
		return res;
		
	}
	
	public void printSyncCG() {
		for(SootMethod currsm : methtocfg.keySet()) {
			System.out.println();
			System.out.println("Method: " + currsm.getSignature());			
			for(Unit currunit : methtocfg.get(currsm).getBody().getUnits()) {
				System.out.println(currunit + " -->");
				List<SyncEdge> succs = edgesOutOf(currunit);
				if(succs != null)
					for(SyncEdge succ : succs) {
						System.out.print("\t" + succ.getTarget() + " " + succ.getKind());
						
						// Testing assertion detection
//						if(succ.getTarget().toString().contains("java.lang.AssertionError") && currunit.toString().contains("if ") && currunit.toString().contains("goto")) {
//							System.out.println("\nUnit " + currunit + " is an assert condition!");
//						}
						/*
						if (succ.getKind().equals(EdgeKind.RETURN)) {
							System.out.print("\t" + getCallers(succ));
						}*/
						System.out.println();
				}
			}
		}
	}

	public Iterator<Unit> iterator() {
		return nodes.iterator();
	}
	
	public Set<SootMethod> getMethods() { return methtocfg.keySet(); }



	public Set<Unit> getNodes() {
		return nodes;
	}
	
	public Body getBody(SootMethod sm) {
		UnitGraph cfg = methtocfg.get(sm);
		if(cfg != null)
			return cfg.getBody();
		return null;
	}

	public SootMethod getMethodFromLocal(Local l) {		
		return localtometh.get(l);
	}

	public SootMethod getMethodFromUnit(Unit u) {		
		return unittometh.get(u);
	}
	
	public List<Unit> getCallers(SyncEdge retedge) {
		List<Unit> res = new LinkedList<Unit>();
		if(retedge.getKind().equals(EdgeKind.RETURN)) {
			Unit src = retedge.getSource();
			Unit tgt = retedge.getTarget();
			Set<RetInfo> ris = retedges.get(src);
			if(ris != null) {				
				for(RetInfo ri : ris) {
					if(ri.tgt.equals(tgt)) {
						res.add(ri.caller);
					}
				}
			}
		}
		return res;
	}



	public long getCodeSize() {
		
		return nodes.size();
	}

}

