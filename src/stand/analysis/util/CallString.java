package stand.analysis.util;

import java.util.LinkedList;

import soot.Unit;


public class CallString {
	
	class CSElem {
		Unit u;		
		
		CSElem(Unit u) {
			this.u = u;
		}
		
		CSElem() {
			this.u = null;
		}
		
		protected boolean isAll() {
			return (u == null);
		}
		
		protected Unit getUnit() {
			return u;
		}
		
		@Override
		public  boolean equals(Object other) {
			if(this == other) return true;
			if(other == null) return false;
			if(!other.getClass().equals(this.getClass())) return false;
			CSElem ocs = (CSElem) other;
			if(this.u != null) return this.u.equals(ocs.u);
			return (ocs.u == null);
		}
		
		@Override
		public int hashCode() {
			if(this.u == null) return 42;
			return this.u.hashCode();
		}
	}
	
	private LinkedList<CSElem> cs; 
	// max length of the call string
	public static final int maxlen = 3;
	
	public CallString() {
		cs = new LinkedList<CSElem>();;
	}
	
	public CallString(CallString ocs) {
		cs = new LinkedList<CSElem>(ocs.cs);
	}
	
	public void append(Unit u) {
		cs.addLast(new CSElem(u));
		if(cs.size() > maxlen)
			cs.removeFirst();
	}
	
	public Unit popLastElem() {	
		if(cs.size() > 0) {
			CSElem last = cs.removeLast();
			if(cs.size() == maxlen - 1) {
				cs.addFirst(new CSElem());
			}
			return last.getUnit();
		}
		return null;
	}
	
	@Override
	public boolean equals(Object other) {
		if(this == other) return true;
		if(other == null) return false;
		if(!other.getClass().equals(this.getClass())) return false;
		CallString ocs = (CallString) other;
		return ocs.cs.equals(this.cs);
	}
	
	@Override
	public int hashCode() {
		return cs.hashCode();
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer("[");
		for(CSElem celem : cs) {
			if(celem.isAll())
				sb.append("*");
			else
				sb.append(celem.getUnit());
			sb.append(";");
		}
		sb.append("]");
		return sb.toString();		
	}

}
