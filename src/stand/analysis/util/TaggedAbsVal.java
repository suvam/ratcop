package stand.analysis.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import stand.analysis.util.CallString;

public class TaggedAbsVal<A> {
	private Map<CallString, A> cstoval;
	
	public TaggedAbsVal (CallString cs, A av) {
		cstoval = new HashMap<CallString, A>();
		cstoval.put(cs, av);
	}
	
	public TaggedAbsVal() {
		cstoval = new HashMap<CallString, A>();
	}	
	
	public TaggedAbsVal(TaggedAbsVal<A> tav) {
		cstoval = new HashMap<CallString, A>();
		
		for(CallString cs: tav.getCallStrings()) {
			A absSet = tav.getAbsVal(cs);
			CallString cs1 = new CallString(cs);
			cstoval.put(cs1, absSet);
			
		}
	}

	public Set<CallString> getCallStrings() {
		return cstoval.keySet();
	}
	
	public A getAbsVal(CallString cs) {
		return cstoval.get(cs);
	}
	
	public A putAbsVal(CallString cs, A a) {
		return cstoval.put(cs, a);
	}
	
	@Override
	public String toString() {
		String outString = "";
		for(CallString cs: cstoval.keySet()) {
			outString += "CS: " + cs.toString() + " --> " + cstoval.get(cs).toString();
		}
		return outString;
	}
}
