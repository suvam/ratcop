/**
 * Holds a set of locks. Representative of the set of locks that must be held at a program point.
 * In Jimple, each lock is a Value object.
 */
package stand.analysis.util;

import java.util.HashSet;
import java.util.Set;

import soot.Value;

/**
 * @author suvam
 *
 */
public class LockSet {
	
	protected HashSet<Value> lockSet;
	private boolean isFull = false;
	
	public LockSet(LockSet lset)
	{
		lockSet = new HashSet<Value>(lset.lockSet);
		isFull = lset.isFull;
	}
	
	public LockSet(Set<Value> lset)
	{
		lockSet = new HashSet<Value>(lset);
	}
	
	public LockSet()
	{
		lockSet = new HashSet<Value>();
	}
	
	public LockSet(boolean isfull) {
		this();
		isFull = isfull;
	}
	
	public HashSet<Value> getLockSet()
	{
		return this.lockSet;
	}
	
	public boolean isFull() {
		return isFull;
	}
	/**
	 * Join of two locksets is the intersection of the two, since this is a must analysis
	 */
	public void intersect(LockSet lockSet1)
	{
		HashSet<Value> tempLset = new HashSet<Value>(lockSet1.lockSet);
		if(this.isFull) {
			this.lockSet.addAll(tempLset); // Set.addAll(other_set)
			this.isFull = lockSet1.isFull;
			return;
		}
		if(lockSet1.isFull)
			return;
		this.lockSet.retainAll(tempLset);
	}
	
	/** 
	 * Compares this lockset with another lockset. This lockset is "safer" if it is contained in the other (this is a must analysis)
	 */
	public boolean isSafer(LockSet other)
	{
		if(this.isFull) return other.isFull;
		if(other.isFull) return false;
		return other.lockSet.containsAll(this.lockSet);
	}
	
	/**
	 * Compares two locksets to check if they are equal. Logic is basic set equality.
	 */
	public boolean isEqual(LockSet other)
	{
		return (this.lockSet.containsAll(other.lockSet) && other.lockSet.containsAll(this.lockSet));
	}
	
	/**
	*Delete contents of the lockset based on another lockset
	*/
	public void removeAll(LockSet other)
	{
		if(other.lockSet.size() > 0)
			this.isFull = false;
		lockSet.removeAll(other.lockSet);
	}
	
	/**
	*Add contents to the lockset based on another lockset
	*/
	public void addAll(LockSet other) {
		HashSet<Value> lset = new HashSet<Value>(other.lockSet);
		this.lockSet.addAll(lset);
	}
	
	public boolean contains(Value lk) {
		if(this.isFull) return true;
		return lockSet.contains(lk);
	}
	
	@Override
	public String toString() {
		if(this.isFull) return "T";
		else return lockSet.toString();
	}

}
