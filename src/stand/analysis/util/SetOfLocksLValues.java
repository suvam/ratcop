/**
 * Represents a data-structure containing a set of pairs of LockSets and LValueSets
 */
package stand.analysis.util;

import java.util.HashSet;

import soot.Value;
import soot.toolkits.scalar.Pair;

/**
 * @author suvam
 *
 */
public class SetOfLocksLValues {
	
	public HashSet<Pair<LockSet, LvalueSet>> fact;	// a fact is a set of pairs, where each pair contains a set of locks (must), and a set of lvalues safe to dereference (must)
	
	public SetOfLocksLValues(SetOfLocksLValues otherFact) {
		this.fact = otherFact.fact;
	}
	
	public SetOfLocksLValues(HashSet<Pair<LockSet, LvalueSet>> otherFact) {
		this.fact = new HashSet<Pair<LockSet, LvalueSet>>();
		for(Pair<LockSet, LvalueSet> p : otherFact) {
			HashSet<Value> lockHSet = new HashSet<>();
			lockHSet.addAll(p.getO1().getLockSet());
			LockSet lockSet = new LockSet(lockHSet);
			HashSet<Lvalue> lValueHSet = new HashSet<>();
			lValueHSet.addAll(p.getO2().getLvalues());
			LvalueSet lValueSet = new LvalueSet(lValueHSet);
			
			Pair p_i = new Pair<LockSet, LvalueSet>(lockSet, lValueSet);
			fact.add(p_i);
		}
	}
	
	
	public SetOfLocksLValues(Pair<LockSet, LvalueSet> pair)
	{
		LockSet lset = new LockSet(pair.getO1());
		LvalueSet lvset = new LvalueSet(pair.getO2());
		Pair<LockSet, LvalueSet> p = new Pair<LockSet, LvalueSet>(lset, lvset);
		this.fact = new HashSet<Pair<LockSet, LvalueSet>>();
		fact.add(pair);
	}
	
	public SetOfLocksLValues(LockSet lset, LvalueSet lvset) {
		Pair<LockSet, LvalueSet> pair = new Pair<LockSet, LvalueSet>(lset, lvset); 
		fact = new HashSet<Pair<LockSet, LvalueSet>>();
		fact.add(pair);
	}
	
	public SetOfLocksLValues() {
		LockSet ls = new LockSet();
		LvalueSet lvs = new LvalueSet();
		Pair<LockSet, LvalueSet> p = new Pair<LockSet, LvalueSet>(ls, lvs);
		fact = new HashSet<Pair<LockSet, LvalueSet>>();
		fact.add(p);
		
	}
	
	/*
	 * If empty is true, create a completely empty SetOfLocksLValues. Otherwise add a empty pair to the set.
	 */
	public SetOfLocksLValues(boolean empty) {
		if(empty) {
			fact = new HashSet<Pair<LockSet, LvalueSet>>();
		}
		else {
			LockSet ls = new LockSet();
			LvalueSet lvs = new LvalueSet();
			Pair<LockSet, LvalueSet> p = new Pair<LockSet, LvalueSet>(ls, lvs);
			fact = new HashSet<Pair<LockSet, LvalueSet>>();
			fact.add(p);
		}
	}
	
	// Add a pair to the existing set
	public void addPair(Pair<LockSet, LvalueSet> p)
	{
		LockSet lset = new LockSet(p.getO1());
		LvalueSet lvset = new LvalueSet(p.getO2());
		Pair<LockSet, LvalueSet> pair = new Pair<LockSet, LvalueSet>(lset, lvset);
		fact.add(pair);
	}
	
	// Remove a pair from the existing set
	public void removePair(Pair<LockSet, LvalueSet> p)
	{
		fact.remove(p);
	}
	
	/*
	 *	Clone the fact set 
	 */
	
	public HashSet<Pair<LockSet, LvalueSet>> cloneFact()
	{
		HashSet<Pair<LockSet, LvalueSet>> clonedFact = new HashSet<Pair<LockSet, LvalueSet>>();
		
		for(Pair<LockSet, LvalueSet> p: fact) {
			LockSet lset = new LockSet(p.getO1());
			LvalueSet lvset = new LvalueSet(p.getO2());
			Pair<LockSet, LvalueSet> pair = new Pair<LockSet, LvalueSet>(lset, lvset);
			clonedFact.add(pair);
		}
		
		return clonedFact;
	}
	
	// return the fact
	public HashSet<Pair<LockSet, LvalueSet>> getPairs()
	{
		return cloneFact();
	}
	
	/*
	 * Clone this SetOfLocksLvalues
	 */
	public SetOfLocksLValues clone()
	{
		HashSet<Pair<LockSet, LvalueSet>> fset = new HashSet<Pair<LockSet, LvalueSet>>();
		
		fset = cloneFact();
		SetOfLocksLValues cloned = new SetOfLocksLValues(fset);
		return cloned;
	}
	
	@Override
	public String toString()
	{
		String outString = "[";
		for(Pair<LockSet, LvalueSet> p :fact) 
			outString += p.getO1().toString() + ", " + p.getO2();
		
		outString += " ]";
		return outString;
	}
	
}
