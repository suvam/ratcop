package stand.analysis.util;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import soot.Local;
import soot.SootField;
import soot.SootMethod;
import soot.Type;
import soot.Value;
import soot.jimple.InstanceFieldRef;

public class Lvalue {
	final private Local base;
	final private List<SootField> flist;
	final private Type type;
	private Value val = null; 
	
	public Lvalue(Local l, List<SootField> ap) {
		base = l;
		if(ap == null)
			flist = new ArrayList<SootField>();
		else
			flist = new ArrayList<SootField>(ap);
		
		if(flist.isEmpty())
			type = base.getType();
		else
			type = flist.get(ap.size() - 1).getType();
	}
	
	public Lvalue(Local l) {
		this(l, null);
	}
	
	@Override
	public boolean equals(Object other) {
		if(!(other instanceof Lvalue))
			return false;
		
		Lvalue olval = (Lvalue) other;
		Local obase = olval.base;
		List<SootField> oflist = olval.flist;
		return ((this.base.equals(obase)) && (this.flist.equals(oflist)));
		
	}
	
	@Override
	public int hashCode() {
		int hash = base.hashCode();
		for (SootField sf : flist) {
			hash += sf.hashCode();
		}
		return hash;
	}
	
	@Override
	public String toString() {
		StringBuffer ls = new StringBuffer();
		ls.append(base);
		for(SootField sf : flist) {
			ls.append(".");
			ls.append(sf.getName());
		}
		
		return ls.toString();
	}
	
	public SootField getLast() {
		if(flist.size() < 1) return null;
		return flist.get(flist.size() - 1);
	}
	
	public Lvalue pruneLast() {
		if(this.flist.size() < 1) return null;
		List<SootField> sfl = new LinkedList<SootField>(this.flist);
		sfl.remove(sfl.size() - 1);
		return new Lvalue(this.base, sfl);
	}
	
	public Local getBase() {		
		return base;
	}

	public List<SootField> getAccessPath() {		
		return new ArrayList<SootField>(flist);
	}

	public Type getType() {		
		return type;
	}
	
	public boolean isDep(Lvalue other) {
		if(!(this.base.equals(other.base)) || (this.flist.size() >= other.flist.size()))
			return false;
				
		List<SootField> thispath = this.flist;
		List<SootField> otherpath = other.flist;
		
		for(int i = 0; i < thispath.size(); i++) {
			if(!(thispath.get(i).equals(otherpath.get(i))))
				return false;
		}
		return true;
	}
	
	
	public List<SootField> getReachingPath(Lvalue other) {
		if(this.equals(other))
			return new ArrayList<SootField>();
		if(!this.isDep(other)) 
			return null;
		return new ArrayList<SootField>(other.flist.subList(this.flist.size(), other.flist.size()));
	}

	public boolean isLocal() {		
		return (flist.size() < 1);
	}
	
	public Lvalue substituteBase(Local l) {
		return new Lvalue(l, this.getAccessPath());
	}
	
	public boolean isInScope(SootMethod sm) {
		if(!(sm.hasActiveBody())) return false;
		return (sm.getActiveBody().getLocals().contains(this.base));
	}
	
	public static Lvalue makeLvalue(Value v) {
		if(v instanceof Local)
			return new Lvalue((Local) v);
		if(v instanceof InstanceFieldRef) {
			InstanceFieldRef ifr = (InstanceFieldRef) v; 
			List<SootField> sfl = new ArrayList<SootField>();
			sfl.add(ifr.getField());
			return new Lvalue((Local) ifr.getBase(), sfl);
		}
		return null;
	}
	

}

