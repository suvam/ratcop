package stand.analysis.util;

import java.util.HashSet;
import java.util.Set;

import soot.Local;
import soot.PointsToAnalysis;
import soot.PointsToSet;
import soot.SootField;
import soot.jimple.spark.sets.EqualsSupportingPointsToSet;

public class AliasAnalysis {
	private PointsToAnalysis pta;
	
	public AliasAnalysis(PointsToAnalysis p) {
		pta = p;
	}
	
	public PointsToSet reachingObjects(Lvalue lv) {
		Local base = lv.getBase();
		PointsToSet lvpts = pta.reachingObjects(base);
		for(SootField sf : lv.getAccessPath()) {
			lvpts = pta.reachingObjects(lvpts, sf);
		}
		return lvpts;
	}
	
	public Set<Lvalue> getAliases(Lvalue lv, Set<Lvalue> lvset) {
		
		Set<Lvalue> res = new HashSet<Lvalue>();
		SootField lastfld= lv.getLast();
		if(lastfld == null) return res;
				
		Lvalue deplv = lv.pruneLast();
		
		PointsToSet lvpts = reachingObjects(deplv);				
		
		for(Lvalue currlv : lvset) {
			SootField currlastfld = currlv.getLast();
			if(!lastfld.equals(currlastfld)) continue;
			
			
			PointsToSet currset = reachingObjects(currlv.pruneLast());
			if(lvpts.hasNonEmptyIntersection(currset))
				res.add(currlv);
		}
		return res;
		
	}
	
	public boolean isMustAlias(Lvalue l1, Lvalue l2) {		
		
		if(l1.isLocal() || l2.isLocal()) return false;
		if(!(l1.getLast().equals(l2.getLast()))) return false;
		Lvalue nl1 = l1.pruneLast();
		Lvalue nl2 = l2.pruneLast();
		PointsToSet p1 = reachingObjects(nl1);
		PointsToSet p2 = reachingObjects(nl2);
		return ((EqualsSupportingPointsToSet) p1).pointsToSetEquals(p2);
	}

}
