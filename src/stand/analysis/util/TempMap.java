package stand.analysis.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import soot.Body;
import soot.Local;
import soot.RefLikeType;
import soot.SootField;
import soot.Unit;
import soot.Value;
import soot.jimple.AssignStmt;
import soot.jimple.CastExpr;
import soot.jimple.InstanceFieldRef;
import soot.util.Chain;

public class TempMap {

	private Map<Local, Set<Lvalue>> alltempmap = new HashMap<Local, Set<Lvalue>>();

	public TempMap(List<Body> mbodies) {
		Local lhs;
		Local lbase;
		SootField sf;
		List<SootField> sfl;

		for (Body b : mbodies) {
			Map<Local, Set<Lvalue>> methtempmap = new HashMap<Local, Set<Lvalue>>();

			Chain<Unit> stmtl = b.getUnits();
			Iterator<Unit> stmtiter = stmtl.iterator();

			while (stmtiter.hasNext()) {
				Unit stmt = stmtiter.next();
				if (stmt instanceof AssignStmt) {
					AssignStmt astmt = (AssignStmt) stmt;
					Value lhsv = astmt.getLeftOp();
					if (/*(isTemp(lhsv))*/ (lhsv instanceof Local)
							 && (lhsv.getType() instanceof RefLikeType)) {
						lhs = (Local) lhsv;
						Value rhsv = astmt.getRightOp();
						if (rhsv instanceof CastExpr)
							rhsv = ((CastExpr) rhsv).getOp();
						if (rhsv instanceof InstanceFieldRef) {
							InstanceFieldRef ifr = (InstanceFieldRef) rhsv;
							Value base = ifr.getBase();
							if (base instanceof Local)
								lbase = (Local) base;
							else
								continue;

							sf = ifr.getField();
							sfl = new ArrayList<SootField>();
							sfl.add(sf);
							Lvalue lval = new Lvalue(lbase, sfl);
							Set<Lvalue> lset = new HashSet<Lvalue>();
							lset.add(lval);
							if(methtempmap.get(lhs) == null)
								methtempmap.put(lhs, lset);
							else
								methtempmap.remove(lhs);
						}
						else if (rhsv instanceof Local) {
							Lvalue lval = new Lvalue((Local) rhsv, new ArrayList<SootField>());
							Set<Lvalue> lset = new HashSet<Lvalue>();
							lset.add(lval);
							if(methtempmap.get(lhs) == null)
								methtempmap.put(lhs, lset);
							else
								methtempmap.remove(lhs);
						}
					}

				}
			}

			collapse(methtempmap);
			alltempmap.putAll(methtempmap);
		}

	}

	private void collapse(Map<Local, Set<Lvalue>> methtempmap) {
		
		List<Local> tlocals = new ArrayList<Local>(methtempmap.keySet());
		Collections.sort(tlocals, new Comparator<Local>() {
			@Override
			public int compare(Local arg0, Local arg1) {
				return arg0.getName().compareTo(arg1.getName());
			}
		});

		for (Local l : tlocals) {
			Set<Lvalue> lvals = methtempmap.get(l);
			for (Lvalue lv : lvals) {
				expandMap(methtempmap, l, lv);
			}
		}

		/*
		 * Map<Local, Boolean> changemap = new HashMap<Local, Boolean>();
		 * 
		 * for (Local l : tempmap.keySet()) { changemap.put(l, Boolean.TRUE); }
		 * 
		 * 
		 * boolean change; do { change = false; for (Local l : tempmap.keySet())
		 * { changemap.put(l, Boolean.FALSE);
		 * 
		 * Set<Lvalue> lvals = new HashSet<Lvalue>(tempmap.get(l)); for (Lvalue
		 * lv : lvals) { Local base = lv.getBase();
		 * if((changemap.containsKey(base)) &&
		 * (changemap.get(base).equals(Boolean.TRUE))) { expandMap(l, lv);
		 * changemap.put(l, Boolean.TRUE); change = true; } }
		 * 
		 * } } while (change);
		 */

	}

	private void expandMap(Map<Local, Set<Lvalue>> methtempmap, Local l, Lvalue lv) {

		Local base = lv.getBase();
		List<SootField> ap = lv.getAccessPath();
		if (methtempmap.containsKey(base)) {
			for (Lvalue bl : methtempmap.get(base)) {
				Local newbase = bl.getBase();
				List<SootField> newap = new ArrayList<SootField>();
				newap.addAll(bl.getAccessPath());
				newap.addAll(ap);
				Lvalue newlval = new Lvalue(newbase, newap);
				methtempmap.get(l).add(newlval);
			}
		}

	}

	public Set<Lvalue> getLvalues(Local l) {
		return alltempmap.get(l);
	}

	public static boolean isTemp(Value lhsv) {
		if (lhsv instanceof Local) {
			Local l = (Local) lhsv;
			if (l.getName().startsWith("$"))
				return true;
		}
		return false;
	}
}

