package stand.analysis.util;

import java.util.HashSet;
import java.util.Set;

import soot.jimple.toolkits.pointer.StrongLocalMustAliasAnalysis;


public class LvalueSet {
	public HashSet<Lvalue> lset;
	private boolean isFull = false;
	
	public LvalueSet(LvalueSet olset) {
		lset = new HashSet<Lvalue>(olset.lset);
		isFull = olset.isFull;
	}
	
	public LvalueSet(Set<Lvalue> olset) {
		lset = new HashSet<Lvalue>(olset);
	}	
	
	public LvalueSet() {
		lset = new HashSet<Lvalue>();
	}
	
	public LvalueSet(boolean isfull) {
		this();
		isFull = isfull;
	}
	


	public Set<Lvalue> getLvalues() {
		return lset;
	}
	
	public boolean isFull() {
		return isFull;
	}
	
	/*public LvalueSet merge(LvalueSet other) {
		if(this.isFull)
			return new LvalueSet(other);
		if(other.isFull)
			return new LvalueSet(this);
		Set<Lvalue> newset = new HashSet<Lvalue>(this.lset);
		newset.retainAll(other.lset);
		LvalueSet newlvset = new LvalueSet();
		newlvset.addSet(newset);
		return newlvset;
	}*/
	
//	public void intersect(LvalueSet other) {
//		if(this.isFull) {
//			this.lset.addAll(other.lset);
//			this.isFull = other.isFull;
//			return;
//		}
//		if(other.isFull)
//			return;
//		this.lset.retainAll(other.lset);
//	}

	public void intersect(LvalueSet other) {
		HashSet<Lvalue> tempLset = new HashSet<Lvalue>(other.lset);
		if(this.isFull) {
			this.lset.addAll(tempLset); // Set.addAll(other_set)
			this.isFull = other.isFull;
			return;
		}
		if(other.isFull)
			return;
		this.lset.retainAll(tempLset);
	}
	
	public boolean isSafer(LvalueSet other) {
		if(this.isFull) return other.isFull;
		if(other.isFull) return false;
		return other.lset.containsAll(this.lset);
	}
	
	@Override
	public String toString() {
		if(this.isFull) return "T";
		else return lset.toString();
	}


	
	public void addAll(Set<Lvalue> set) {
		if(this.isFull) return;
		HashSet<Lvalue> tempLset = new HashSet<Lvalue>(set);
		lset.addAll(tempLset);
		
	}

	public void removeAll(Set<Lvalue> delset) {
		if(delset.size() > 0)
			this.isFull = false;
		lset.removeAll(delset);
		
	}

	public boolean contains(Lvalue lv) {
		if(this.isFull) return true;
		return lset.contains(lv);
	}

}
