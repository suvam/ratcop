package stand.analysis;

/**
 * Implementation of the worklist algorithm.
 */

/**
 * @author suvam
 *
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import apron.Abstract1;
import soot.Unit;
import stand.analysis.util.TaggedAbsVal;
import stand.graph.EdgeKind;
import stand.graph.SyncCG;
import stand.graph.SyncEdge;


public abstract class SyncAnalysis<A> {
	
	public long start;
	public long end;
	public HashMap<Unit, Integer> unit2CountMap;
	public int interThreadProps = 0;	// count the number of inter-thread comms
	public int intraThreadProps = 0;	// count the number of intra-thread comms
	
	protected class WorkListElem {
		//private Unit u;
		private SyncEdge edge;
		private A wlval;	
		
		protected WorkListElem(SyncEdge e, A absval) {
			//this.u = u;
			this.wlval = absval;
			this.edge = e;
		}
		
		protected Unit getUnit() {
			return edge.getTarget();
		}
		protected A getAbsVal() {
			return wlval;
		}
		
		@Override
		public boolean equals(Object other) {
			if (this == other) return true;
		    if (other == null) return false; 
		    if (this.getClass() != other.getClass()) return false; 
			
			WorkListElem wother = (WorkListElem) other;
			return ((wother.edge.equals(this.edge)) && wother.wlval.equals(this.wlval));
		}
		
		public int hashCode() {
			if(edge==null) {
				System.out.println("\nHashCode received NULL edge");
			}
			if(wlval==null) {
				System.out.println("\nHashCode received NULL wlval");
			}
			return (edge.hashCode() + wlval.hashCode());
		}

		public SyncEdge getEdge() {			
			return edge;
		}
	}
	
	protected SyncCG g;
	
	// Set-up widening parameters
	protected boolean useWidening = false;
	@SuppressWarnings("unused")
	protected int wideningThreshold;	// if widening used, specify threshold value
	
	private Map<Unit, A> result = new HashMap<Unit, A>();
	
	public SyncAnalysis(stand.graph.SyncCG g2) {
		this.g = g2;
	}
	
	public SyncAnalysis(stand.graph.SyncCG g, boolean useWidening) {
		this.g = g;
		this.useWidening = useWidening;
	}
	
	
	protected void doAnalysis() {
		start = System.currentTimeMillis();
		
	//	System.out.println("\nExecuting doAnalysis()");
		int no = 0;
		List<WorkListElem> worklist = new LinkedList<WorkListElem>();
		
		// initialize result pool
		Iterator<Unit> uit = g.iterator();
		while(uit.hasNext()) {
			Unit u = uit.next();
			result.put(u, newInitialFlow());
		}
		
		
		// initialize work list
		List<Unit> heads = g.getHeads();
		for(Unit head : heads) {
			WorkListElem initwle = new WorkListElem(new SyncEdge(null, head, EdgeKind.FALL_THROUGH), entryInitialFlow());
			worklist.add(initwle);
		}
		
//		if(worklist.isEmpty())
//			System.out.println("\nWarning: Worklist is empty!");
//		else
//			System.out.println("\nWorklist generated with size: " + worklist.size());
		int count = worklist.size();
		
		// unit2CountMap tracks the number of times each unit is processed by the worklist algorithm.
		// Extremely useful to compare the runs of two different algorithms using this worklist implementation.
		// The relevant code to update the map occurs immediately before the flowThrough call
		unit2CountMap = new HashMap();
		
		// fixpoint analysis
		while(!(worklist.isEmpty())) {
			// System.out.println("Here!" + no++);
			// get an element from work list
			WorkListElem currwle = worklist.remove(0);			
			Unit currunit = currwle.getUnit();
			SyncEdge curredge = currwle.getEdge();
			A currval = currwle.getAbsVal();
			
			// The following tracks the number of times each unit is processed. Useful to compare the execution runs of two different algorithms.
			// Also used to apply the widening.
			if(unit2CountMap.containsKey(currunit))
				unit2CountMap.put(currunit, (unit2CountMap.get(currunit)+1));
			else
				unit2CountMap.put(currunit, 1);
			
			// get old result from pool
			A val = result.get(currunit);
			
			// Debugging a specific instruction
//			if(currunit.toString().contains("r2 = $r5.<testSuite.Data: java.lang.String x>")) {
//				System.out.println("\nAccessing buggy dereference");
//				System.out.println("In: " + currval.toString());
//				System.out.println("Old: " + val.toString());
//			}
			
			// are we ready to process this element?
			if(!readyToProcess(currwle, val)) {
				worklist.add(currwle);
			//	System.out.println("\ndoAnalysis: Not ready to process " + currwle.toString() + " " + val.toString());
				continue;
			}
			// is old result already safe? 
			if(isSafeApprox(val, currval)) {
			//	System.out.println("\ndoAnalysis: Unit " + currunit + " Old value Safe approx: " + val.toString() + " " + currval.toString());
				continue;
			}
			else {
			//	System.out.println("\ndoAnalysis SAFETY CHECK: Old value NOT SAFE for: " + currunit + " old: " + val + " new: " + currval);
			}
			
			// merge the incoming abstract value with old pool value
			//A newval = merge(curredge, currval, oldval);
//			if(currunit.toString().contains("entermonitor $r3")) {
//				System.out.println("\nEXPLICIT FACTS FOR $r3");
//			}
			if(useWidening) {
			//	System.out.println(currunit + " iteration count: "+ unit2CountMap.get(currunit));
				merge(curredge, currval, val, unit2CountMap, currunit);				
			}
				
			else {
				if(unit2CountMap.get(currunit) != null) {
				//	System.out.println(currunit + " iteration count: "+ unit2CountMap.get(currunit));
				}
				merge(curredge, currval, val);
			}
				
			//System.out.println("VAL1: " + val.toString());
			//merge(curredge, currval, oldval, newval);
			//result.put(currunit, newval);
			
			// propagate the value					
			List<SyncEdge> succlist = g.edgesOutOf(currunit);
			List<A> proplist = new ArrayList<A>(succlist.size());
			/*for(int i = 0; i < succlist.size(); i++) {
				proplist.add(i, new HashSet<A>());				
			}*/
			//proplist is null-initialized
			
		
			//System.out.println("VAL2: " + val.toString());
			flowthrough(currunit, val, succlist, proplist);
			
			/*
			List<Set<A>> proplist = new ArrayList<Set<A>>(succset.size());
			List<EdgeKind> ekl = new ArrayList<EdgeKind>(succset.size());
			for(int i = 0; i < succset.size(); i++) {
				proplist.add(i, new HashSet<A>());
				ekl.add(i, EdgeKind.FALL_THROUGH);
			}
			for(UnitBox ub : branchboxes) {
				int index = succlist.indexOf(ub.getUnit());
				ekl.set(index, EdgeKind.BRANCH);
			}
			flowThrough(currunit, newval, succlist, ekl, proplist);
			*/
			
			// put the propagated values in the worklist			
			for(int i = 0; i < succlist.size(); i++) {
				WorkListElem newelem = new WorkListElem(succlist.get(i), proplist.get(i));
				worklist.add(newelem);
			}
		}
		end = System.currentTimeMillis();
		
//		System.out.println("Output Map \n");
//		for(Unit u: unit2CountMap.keySet()) {
//			System.out.println(u + ": " + unit2CountMap.get(u));
//		}
	}

	public abstract boolean isSafeApprox(A oldval, A currval);

	public abstract boolean readyToProcess(WorkListElem currwle, A oldval);

	public abstract void flowthrough(Unit u, A in, List<SyncEdge> succlist, List<A> proplist);
	
	//public abstract void flowThrough(Unit currunit, Set<A> in, List<Unit> succlist, List<EdgeKind> ekl, List<Set<A>> proplist);

	//public abstract A merge(SyncEdge edge, A currval, A oldval);
	public abstract void merge(SyncEdge edge, A currval, A val);
	
	public abstract void merge(SyncEdge edge, A currval, A val, HashMap<Unit, Integer> unit2CountMap, Unit currunit);
	
	//public abstract A mergeCopy(SyncEdge edge, A currval, A val, HashMap<Unit, Integer> unit2CountMap, Unit currunit);

	public abstract A newInitialFlow();

	public abstract A entryInitialFlow();
	
	public A getResult(Unit u) {
		return result.get(u);
	}	

}
