package stand.analysis.npa;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import soot.Body;
import soot.Local;
import soot.RefLikeType;
import soot.SootField;
import soot.SootMethod;
import soot.Unit;
import soot.Value;
import soot.ValueBox;
import soot.jimple.ArrayRef;
import soot.jimple.AssignStmt;
import soot.jimple.BinopExpr;
import soot.jimple.CastExpr;
import soot.jimple.CaughtExceptionRef;
import soot.jimple.IdentityStmt;
import soot.jimple.IfStmt;
import soot.jimple.InstanceFieldRef;
import soot.jimple.InstanceInvokeExpr;
import soot.jimple.InvokeExpr;
import soot.jimple.LengthExpr;
import soot.jimple.MonitorStmt;
import soot.jimple.NeExpr;
import soot.jimple.NewArrayExpr;
import soot.jimple.NewExpr;
import soot.jimple.NewMultiArrayExpr;
import soot.jimple.NullConstant;
import soot.jimple.ReturnStmt;
import soot.jimple.Stmt;
import soot.jimple.ThisRef;
import soot.jimple.ThrowStmt;
import soot.util.Chain;
import stand.analysis.SyncAnalysis;
import stand.analysis.util.AliasAnalysis;
import stand.analysis.util.CallString;
import stand.analysis.util.Lvalue;
import stand.analysis.util.LvalueSet;
import stand.analysis.util.TaggedAbsVal;
import stand.analysis.util.TempMap;
import stand.graph.EdgeKind;
import stand.graph.SyncCG;
import stand.graph.SyncEdge;

public class SyncNPA extends SyncAnalysis<TaggedAbsVal<LvalueSet>> {
	
	private Map<Unit, Set<Lvalue>> unittogen = new HashMap<Unit, Set<Lvalue>>();
	private TempMap tmap;

	private AliasAnalysis aa;

	public SyncNPA(SyncCG g, AliasAnalysis aa) {
		super(g);
		
		this.aa = aa;
		
		// create the temp map
		List<Body> bl = new LinkedList<Body>();
		for (SootMethod sm : g.getMethods())
			bl.add(g.getBody(sm));
		tmap = new TempMap(bl);
		
		for (SootMethod sm : g.getMethods()) {
			Body b = g.getBody(sm);
			
			// compute the unconditional gen sets for each statement
			Chain<Unit> uc = b.getUnits();
			for (Unit u : uc) {
				unittogen.put(u, new HashSet<Lvalue>());				

				List<ValueBox> subl = u.getUseBoxes();				
				subl.addAll(u.getDefBoxes());
				for (ValueBox rvb : subl) {
					Value rv = rvb.getValue();
					Value base = null;

					if (rv instanceof InstanceFieldRef) {
						base = ((InstanceFieldRef) rv).getBase();
					} else if (rv instanceof ArrayRef) {
						base = ((ArrayRef) (rv)).getBase();
					} else if (rv instanceof InstanceInvokeExpr) {
						base = ((InstanceInvokeExpr) rv).getBase();
					} else if (rv instanceof LengthExpr) {
						base = ((LengthExpr) rv).getOp();
					} else if (u instanceof ThrowStmt) {
						base = ((ThrowStmt) u).getOp();
					} else if (u instanceof MonitorStmt) {
						base = ((MonitorStmt) u).getOp();
					}

					if (base != null && base.getType() instanceof RefLikeType
							&& base instanceof Local)
						addGen(u, (Local) base);
				}
			}
		}

		doAnalysis();
		
	}
	
	private void addGen(Unit u, Local base) {
		Set<Lvalue> lset = unittogen.get(u);
		Lvalue lv = new Lvalue(base, new ArrayList<SootField>());
		lset.add(lv);
		if(tmap.getLvalues(base) != null)
			lset.addAll(tmap.getLvalues(base));
	}

	@Override
	public TaggedAbsVal<LvalueSet> entryInitialFlow() {
		TaggedAbsVal<LvalueSet> tav = new TaggedAbsVal<LvalueSet>(new CallString(), new LvalueSet()); // empty call string with empty set
		return tav;
	}

	@Override
	public void flowthrough(Unit u, TaggedAbsVal<LvalueSet> in, List<SyncEdge> succlist,
			List<TaggedAbsVal<LvalueSet>> proplist) {
		// TODO Auto-generated method stub
		
		System.out.println("\n**** Flow Through Invocation ***");
		System.out.println("\nObtained fact: " + in.toString());
		System.out.println("Processing statement: " + u);
		
		// create a new tagged set
		TaggedAbsVal<LvalueSet> newtav = new TaggedAbsVal<LvalueSet>();
		
		// copy the in set
		for(CallString cs : in.getCallStrings()) {
			LvalueSet newset = new LvalueSet(in.getAbsVal(cs));
			newtav.putAbsVal(cs, newset);
		}
		
		if(u instanceof IdentityStmt) {
			handleIdentityStmt((IdentityStmt) u, newtav);
		}
		else if(u instanceof AssignStmt) {
			handleAssignStmt((AssignStmt) u, newtav);
		}
		
		// add the default gen set
		Set<Lvalue> genset = unittogen.get(u);
		for(CallString cs : newtav.getCallStrings()) {
			LvalueSet lset = newtav.getAbsVal(cs);
			lset.addAll(genset);			
		}
		System.out.println("Output Fact: " + newtav.toString());
		
		this.propValue(newtav, succlist, proplist); // path sensitive part handled here

	}
	
	private void handleAssignStmt(AssignStmt u, TaggedAbsVal<LvalueSet> newtav) {
				
		Value lo = u.getLeftOp();
		Value ro = u.getRightOp();
		
		// lhs is not an object
		if(!(lo.getType() instanceof RefLikeType)) {			
			return;
		}
		
		Lvalue lhs = Lvalue.makeLvalue(lo);		
		
		// lhs is not an lvalue that we can handle
		if(lhs == null) return;
		
		// rhs calls a function, it will be handled later
		if(ro instanceof InvokeExpr) return;
		
		for(CallString cs : newtav.getCallStrings()) {
			LvalueSet currset = newtav.getAbsVal(cs);
			basicAssignStmtHandler(u, currset);
		}
		
	}

	private void basicAssignStmtHandler(AssignStmt u, LvalueSet currset) {
		Value lo = u.getLeftOp();
		Value ro = u.getRightOp();
		
		Lvalue lhs = Lvalue.makeLvalue(lo);
		
		// set of lvalues to be removed
		Set<Lvalue> delset = new HashSet<Lvalue>();
		
		// remove the lvalues dependent on the lhs		
		for (Lvalue lv : currset.getLvalues()) {			
			if (lhs.isDep(lv))
				delset.add(lv);																
		}
		currset.removeAll(delset);
		
		if (ro instanceof CastExpr)
			ro = ((CastExpr) ro).getOp();

		Lvalue rhs = Lvalue.makeLvalue(ro);
		
		// set of lvalues to add
		Set<Lvalue> addset = new HashSet<Lvalue>();
		
		// if lhs is of the form l and rhs.g is in currset, add l.g and l's map.g
		if(rhs != null) {
			for(Lvalue lv : currset.getLvalues()) {
				if(rhs.isDep(lv)) {
					Local base = lhs.getBase();
					List<SootField> newac = new ArrayList<SootField>(lhs.getAccessPath());
					List<SootField> reachingpath = rhs.getReachingPath(lv);					
					newac.addAll(reachingpath);
					Lvalue newlval = new Lvalue(base, newac);
					addset.add(newlval);
					
					if(lhs.isLocal()) {
						Set<Lvalue> mlvals = tmap.getLvalues(lhs.getBase());
						if(mlvals != null) {
							for(Lvalue mlval : mlvals) {
								base = mlval.getBase();
								newac = new ArrayList<SootField>(mlval.getAccessPath());					
								newac.addAll(reachingpath);
								newlval = new Lvalue(base, newac);
								addset.add(newlval);
							}
						}
					}
				}
			}
		}
			
		
		
		if(currset.contains(rhs)) { // rhs is non-null
			// add lhs
			addset.add(lhs);
						
			// if lhs is of the form l.f, add l's map.f (f can be empty)			
			Set<Lvalue> mlvals = tmap.getLvalues(lhs.getBase());
			if(mlvals != null) {
				for(Lvalue mlval : mlvals) {
					Local base = mlval.getBase();
					List<SootField> sfl = new ArrayList<SootField>(mlval.getAccessPath());
					sfl.addAll(lhs.getAccessPath());
					Lvalue newlval = new Lvalue(base, sfl);
					addset.add(newlval);
				}
			}											
		}
		// rhs is a new expression
		else if (ro instanceof NewExpr || ro instanceof NewArrayExpr
				|| ro instanceof NewMultiArrayExpr || ro instanceof ThisRef
				|| ro instanceof CaughtExceptionRef) { 
			
			addset.add(lhs);
		}
		else { // rhs is may null
			Set<Lvalue> aliasset = aa.getAliases(lhs, currset.getLvalues());
			currset.removeAll(aliasset);
		}
		
		currset.addAll(addset);
		
	}

	private void handleIdentityStmt(IdentityStmt u,	TaggedAbsVal<LvalueSet> newtav) {
		
		Set<Lvalue> newset = new HashSet<Lvalue>();
		if(u.getRightOp() instanceof ThisRef) {
			Lvalue newlval = Lvalue.makeLvalue(u.getLeftOp());
			if(newlval != null)
				newset.add(newlval);
		}
		for(CallString cs : newtav.getCallStrings()) {
			LvalueSet set = newtav.getAbsVal(cs);
			set.addAll(newset);
		}
		
	}

	private void propValue(TaggedAbsVal<LvalueSet> newtav, List<SyncEdge> succlist,
			List<TaggedAbsVal<LvalueSet>> proplist) {
		for(int i = 0; i < succlist.size(); i++) {
			SyncEdge curredge = succlist.get(i);
			if(curredge.getKind() == EdgeKind.FALL_THROUGH) {
				proplist.add(i, newtav);
			}
			else if((curredge.getKind() == EdgeKind.BRANCH) || (curredge.getKind() == EdgeKind.BRANCH_FALL_THROUGH)) {
				TaggedAbsVal<LvalueSet> proptav = handleBranchStmt(curredge, newtav);
				proplist.add(i, proptav);
			}
			/*else if(curredge.getKind() == EdgeKind.BRANCH_FALL_THROUGH) {
				TaggedAbsVal<LvalueSet> proptav = handleBranchStmt((IfStmt) curredge.getSource(), newtav, false);
				proplist.add(i, proptav);
			}*/
			else if(curredge.getKind() == EdgeKind.EXPLICIT) {
				TaggedAbsVal<LvalueSet> proptav = handleMethodCall(curredge, newtav);				
				proplist.add(i, proptav);
			}
			else if(curredge.getKind() == EdgeKind.RETURN) {
				TaggedAbsVal<LvalueSet> proptav = handleReturn(curredge, newtav);								
				proplist.add(i, proptav);
			}
			else if(curredge.getKind() == EdgeKind.MON) {
				TaggedAbsVal<LvalueSet> proptav = handleSync(curredge, newtav);								
				proplist.add(i, proptav);
			}
			else if(curredge.getKind() == EdgeKind.START) {
				TaggedAbsVal<LvalueSet> proptav = handleStart(curredge, newtav);								
				proplist.add(i, proptav);
			}
			else {
				System.out.println("Un-handled edge type: " + curredge.getKind());
				System.exit(-1);
			}
		}
		
	}

	private TaggedAbsVal<LvalueSet> handleStart(SyncEdge edge,
			TaggedAbsVal<LvalueSet> tav) {
		CallString propcs = new CallString();
		LvalueSet propset = new LvalueSet(true);
		for(CallString cs : tav.getCallStrings()) {
			propset.intersect(tav.getAbsVal(cs));
		}
		
		Unit u = edge.getSource();
		InvokeExpr ie = ((Stmt) u).getInvokeExpr();
		SootMethod sm = g.getMethodFromUnit(edge.getTarget());
		Set<Lvalue> addset = new HashSet<Lvalue>();
		
		if(ie instanceof InstanceInvokeExpr) {
			Value base = ((InstanceInvokeExpr) ie).getBase();			
			Local l = g.getBody(sm).getThisLocal();
			addset.addAll(substitute((Local) base, l, propset));			
		}
		propset.addAll(addset);
		
		return new TaggedAbsVal<LvalueSet>(propcs, propset);
	}

	private TaggedAbsVal<LvalueSet> handleSync(SyncEdge curredge,
			TaggedAbsVal<LvalueSet> tav) {
		CallString propcs = new CallString();
		LvalueSet propset = new LvalueSet(true);
		for(CallString cs : tav.getCallStrings()) {
			propset.intersect(tav.getAbsVal(cs));
		}
		return new TaggedAbsVal<LvalueSet>(propcs, propset);
	}

	private TaggedAbsVal<LvalueSet> handleReturn(SyncEdge edge,	TaggedAbsVal<LvalueSet> tav) {
		
		Unit u = edge.getSource();
		SootMethod sm = g.getMethodFromUnit(u);
		
		TaggedAbsVal<LvalueSet> proptav = new TaggedAbsVal<LvalueSet>();
		
		for(CallString cs : tav.getCallStrings()) {
			CallString propcs = new CallString(cs);
			Unit caller = propcs.popLastElem();
			
			if((caller == null) || (g.getCallers(edge).contains(caller))) {
				LvalueSet propset = basicReturnHandler(edge, sm, tav.getAbsVal(cs));
				proptav.putAbsVal(propcs, propset);
			}						
		}
		return proptav;
	}

	private LvalueSet basicReturnHandler(SyncEdge edge, SootMethod sm, LvalueSet absval) {
		LvalueSet propset = new LvalueSet(absval);
		Set<Lvalue> addset = new HashSet<Lvalue>();
		
		Unit u = edge.getSource();
		
		List<Unit> callers = g.getCallers(edge);
		for(Unit caller : callers) {
			if ((caller instanceof AssignStmt) && ((AssignStmt) caller).getLeftOp().getType() instanceof RefLikeType) {
				if((((ReturnStmt)u).getOp() != null) && (((ReturnStmt)u).getOp() instanceof Local)) {
					addset.addAll(substitute((Local)((ReturnStmt) u).getOp(), (Local) ((AssignStmt) caller).getLeftOp(), absval));
				}
			}
			InvokeExpr ie = ((Stmt) caller).getInvokeExpr();
			if(ie instanceof InstanceInvokeExpr) {
				Value base = ((InstanceInvokeExpr) ie).getBase();
			
				Local l = g.getBody(g.getMethodFromUnit(u)).getThisLocal();
				addset.addAll(substitute(l, (Local) base, absval));
				/*
				Set<Lvalue> mlvals = tmap.getLvalues((Local) base);
				if(mlvals != null) {
					for(Lvalue mlval : mlvals) {
						base = mlval.getBase();
						newac = new ArrayList<SootField>(mlval.getAccessPath());					
						newac.addAll(reachingpath);
						newlval = new Lvalue(base, newac);
						addset.add(newlval);
					}
				}*/
			}
			for(int j = 0; j < ie.getArgCount(); j++) {
				Value currarg =  ie.getArg(j);
				Local param = g.getBody(sm).getParameterLocal(j);
				if(currarg instanceof Local)
					addset.addAll(substitute(param, (Local)currarg, absval));									
			}			
		}
		
		// remove lvalues local to returning method
		Set<Lvalue> delset = new HashSet<Lvalue>();
		for(Lvalue lv : absval.getLvalues()) {
			if(lv.isInScope(sm))
				delset.add(lv);
		}
		
		propset.addAll(addset);
		propset.removeAll(delset);
		return propset;
	}

	private TaggedAbsVal<LvalueSet> handleMethodCall(SyncEdge edge, TaggedAbsVal<LvalueSet> tav) {
		Unit u = edge.getSource();		
		SootMethod sm = g.getMethodFromUnit(edge.getTarget());
		
		TaggedAbsVal<LvalueSet> proptav = new TaggedAbsVal<LvalueSet>();
		
		for(CallString cs : tav.getCallStrings()) {
			CallString propcs = new CallString(cs);
			propcs.append(u);
			LvalueSet propset = basicMethodCallHandler(u, sm, tav.getAbsVal(cs));
			proptav.putAbsVal(propcs, propset);			
		}
		
		return proptav;
	}

	private LvalueSet basicMethodCallHandler(Unit u, SootMethod sm, LvalueSet absval) {
		
		LvalueSet propset = new LvalueSet(absval);
		Set<Lvalue> addset = new HashSet<Lvalue>();
		Set<Lvalue> delset = new HashSet<Lvalue>();
		
		InvokeExpr ie = ((Stmt) u).getInvokeExpr();
		
		// if instance invoke, add thislocal.f 
		if(ie instanceof InstanceInvokeExpr) {
			Value base = ((InstanceInvokeExpr) ie).getBase();			
			Local l = g.getBody(sm).getThisLocal();
			addset.addAll(substitute((Local) base, l, absval));			
		}
		
		// add or delete parameters
		for(int i = 0; i < ie.getArgCount(); i++) {
			Value currarg =  ie.getArg(i);
			Local param = g.getBody(sm).getParameterLocal(i);
			if(currarg instanceof Local)
				addset.addAll(substitute((Local) currarg, param, absval));
			else 
				delset.add(Lvalue.makeLvalue(param));
		}
		
		propset.addAll(addset);
		propset.removeAll(delset);
		return propset;
	}
	
	
	private Set<Lvalue> substitute(Local oldbase, Local newbase, LvalueSet lvset) {
		Set<Lvalue> res = new HashSet<Lvalue>();
		Set<Lvalue> mlvals = tmap.getLvalues(newbase);
		for(Lvalue currlv : lvset.getLvalues()) {
			if(oldbase.equals(currlv.getBase())) {
				res.add(currlv.substituteBase(newbase));
				if(mlvals != null) {
					for(Lvalue mlval : mlvals) {
						Local base = mlval.getBase();
						List<SootField> newac = new LinkedList<SootField>(mlval.getAccessPath());
						newac.addAll(currlv.getAccessPath());
						Lvalue lv = new Lvalue(base, newac);
						res.add(lv);
					}
				}
			}
		}
		return res;
	}

	private TaggedAbsVal<LvalueSet> handleBranchStmt(SyncEdge edge, TaggedAbsVal<LvalueSet> newtav) {
		
		IfStmt u = (IfStmt)(edge.getSource());
		boolean isbranch = edge.getKind().equals(EdgeKind.BRANCH);
		
		Value cond = u.getCondition();
		Value op1 = ((BinopExpr) cond).getOp1();
		Value op2 = ((BinopExpr) cond).getOp2();
		boolean isNeg = cond instanceof NeExpr;
		Value toGen = null;
		
		// case 1: opN is a local and opM is NullConstant
		// => opN nonnull on ne branch.
		if (op1 instanceof Local && op2 instanceof NullConstant)
			toGen = op1;

		if (op2 instanceof Local && op1 instanceof NullConstant)
			toGen = op2;
		
		if(toGen == null) return newtav;
		if((isNeg && !isbranch) || (!isNeg && isbranch)) return newtav;
		
		Lvalue toadd = Lvalue.makeLvalue(toGen);
		if(toadd == null) return newtav;
		 		
		Set<Lvalue> addset = new HashSet<Lvalue>();
		addset.add(toadd);
		if(toadd.isLocal()) {
			Set<Lvalue> tset = tmap.getLvalues(toadd.getBase());
			if(tset != null)
				addset.addAll(tset);
		}
		
		TaggedAbsVal<LvalueSet> propval = new TaggedAbsVal<LvalueSet>();
		for(CallString cs : newtav.getCallStrings()) {
			LvalueSet lvset = new LvalueSet(newtav.getAbsVal(cs));
			lvset.addAll(addset);
			propval.putAbsVal(cs, lvset);
		}
		
		return propval;
	}

	@Override
	public void merge(SyncEdge edge, TaggedAbsVal<LvalueSet> currval, TaggedAbsVal<LvalueSet> val) {
		if(edge.getKind() == EdgeKind.MON) {
			System.out.println("\n***MONITOR STATEMENT MERGE***");
			System.out.println("Incoming fact: " + currval);
			System.out.println("Existing fact: " + val);
			LvalueSet currlv = currval.getAbsVal(new CallString()); //sync edge has a summary value with empty call string
			for(CallString cs : val.getCallStrings()) {				
				syncMerge(currlv, val.getAbsVal(cs));
			}
			System.out.println("Propagated fact: " + val);
		}
		else {
			for(CallString cs : currval.getCallStrings()) {
				LvalueSet currset = currval.getAbsVal(cs);
				if(val.getAbsVal(cs) == null) {
					val.putAbsVal(cs, new LvalueSet(currset));
				}
				else {
					val.getAbsVal(cs).intersect(currset);
				}
			}
		}
		
		
	}

	private void syncMerge(LvalueSet synclvset, LvalueSet oldlvset) {
		if(oldlvset.isFull()) {
			System.out.println("OOPS! Can't handle full sets for sync merge!");
			System.exit(-1);
		}
		Set<Lvalue> retainset = new HashSet<Lvalue>();
		for(Lvalue lv : oldlvset.getLvalues()) {
			if((lv.isLocal()) || isThreadLocal(lv)) {
				retainset.add(lv);
				continue;
			}
			for(Lvalue slv : synclvset.getLvalues()) {
				if(aa.isMustAlias(lv, slv))
					retainset.add(lv);
			}
		}
		
		LvalueSet retainlvset = new LvalueSet();
		retainlvset.addAll(retainset);
		oldlvset.intersect(retainlvset);
	}

	private boolean isThreadLocal(Lvalue lv) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public TaggedAbsVal<LvalueSet> newInitialFlow() {		
		return new TaggedAbsVal<LvalueSet>(); // empty map denotes top
	}

	@Override
	public boolean readyToProcess(
			WorkListElem currwle, TaggedAbsVal<LvalueSet> oldval) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isSafeApprox(TaggedAbsVal<LvalueSet> oldval, TaggedAbsVal<LvalueSet> currval) {
		for(CallString cs : currval.getCallStrings()) {
			if(oldval.getAbsVal(cs) == null) return false;
			if(!oldval.getAbsVal(cs).isSafer(currval.getAbsVal(cs))) return false;			
		}
		return true;
	}
	
	public TaggedAbsVal<LvalueSet> getLocalResult(Unit u) {
		TaggedAbsVal<LvalueSet> taggedres = this.getResult(u);
		SootMethod sm = g.getMethodFromUnit(u);
		Set<Lvalue> delset = new HashSet<Lvalue>();
		for(CallString cs : taggedres.getCallStrings()) {
			delset.clear();
			Set<Lvalue> lset = taggedres.getAbsVal(cs).getLvalues();
			for(Lvalue lv : lset) {
				if(!lv.isInScope(sm)) {
					delset.add(lv);
				}
			}
			lset.removeAll(delset);
		}
		return taggedres;
	}
		
	public LvalueSet getAggrResult(Unit u) {
		TaggedAbsVal<LvalueSet> taggedres = this.getLocalResult(u);
		LvalueSet res = new LvalueSet(true);
		for(CallString cs : taggedres.getCallStrings()) {
			res.intersect(taggedres.getAbsVal(cs));
		}
		return res;
	}

	/**
	 * @Suvam: Used with widening. For sync-CFG based analysis, widening does not seem to be necessary.
	 */
	public void merge(SyncEdge edge, TaggedAbsVal<LvalueSet> currval, TaggedAbsVal<LvalueSet> val,
			HashMap<Unit, Integer> unit2CountMap, Unit currunit) {
		if(edge.getKind() == EdgeKind.MON) {
			System.out.println("\n***MONITOR STATEMENT MERGE***");
			System.out.println("Incoming fact: " + currval);
			System.out.println("Existing fact: " + val);
			LvalueSet currlv = currval.getAbsVal(new CallString()); //sync edge has a summary value with empty call string
			for(CallString cs : val.getCallStrings()) {				
				syncMerge(currlv, val.getAbsVal(cs));
			}
			System.out.println("Propagated fact: " + val);
		}
		else {
			for(CallString cs : currval.getCallStrings()) {
				LvalueSet currset = currval.getAbsVal(cs);
				if(val.getAbsVal(cs) == null) {
					val.putAbsVal(cs, new LvalueSet(currset));
				}
				else {
					val.getAbsVal(cs).intersect(currset);
				}
			}
		}
	}

	

	
	

}
