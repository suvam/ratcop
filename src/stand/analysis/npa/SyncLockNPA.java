/**
 * Sets up the NPA Analysis over the sync-CFG, where the dataflow facts are enriched with the set of locks held.
 */
package stand.analysis.npa;

import stand.analysis.SyncAnalysis;
import stand.analysis.util.SetOfLocksLValues;
import stand.analysis.util.TaggedAbsVal;
import stand.analysis.util.LockSet;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import soot.Body;
import soot.Local;
import soot.RefLikeType;
import soot.Scene;
import soot.SootClass;
import soot.SootField;
import soot.SootMethod;
import soot.Unit;
import soot.Value;
import soot.ValueBox;
import soot.jimple.ArrayRef;
import soot.jimple.AssignStmt;
import soot.jimple.BinopExpr;
import soot.jimple.CastExpr;
import soot.jimple.CaughtExceptionRef;
import soot.jimple.DefinitionStmt;
import soot.jimple.EnterMonitorStmt;
import soot.jimple.ExitMonitorStmt;
import soot.jimple.IdentityStmt;
import soot.jimple.IfStmt;
import soot.jimple.InstanceFieldRef;
import soot.jimple.InstanceInvokeExpr;
import soot.jimple.InvokeExpr;
import soot.jimple.LengthExpr;
import soot.jimple.MonitorStmt;
import soot.jimple.NeExpr;
import soot.jimple.NewArrayExpr;
import soot.jimple.NewExpr;
import soot.jimple.NewMultiArrayExpr;
import soot.jimple.NullConstant;
import soot.jimple.ReturnStmt;
import soot.jimple.Stmt;
import soot.jimple.ThisRef;
import soot.jimple.ThrowStmt;
import soot.jimple.internal.JEnterMonitorStmt;
import soot.jimple.internal.JExitMonitorStmt;
import soot.jimple.toolkits.callgraph.CallGraph;
import soot.jimple.toolkits.callgraph.ReachableMethods;
import soot.jimple.toolkits.pointer.StrongLocalMustAliasAnalysis;
import soot.toolkits.graph.BriefUnitGraph;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.toolkits.graph.UnitGraph;
import soot.toolkits.graph.pdg.EnhancedUnitGraph;
import soot.toolkits.scalar.Pair;
import soot.util.Chain;
import stand.analysis.SyncAnalysis;
import stand.analysis.util.AliasAnalysis;
import stand.analysis.util.CallString;
import stand.analysis.util.Lvalue;
import stand.analysis.util.LvalueSet;
import stand.analysis.util.TaggedAbsVal;
import stand.analysis.util.TempMap;
import stand.graph.EdgeKind;
import stand.graph.SyncCG;
import stand.graph.SyncEdge;

/**
 * @author suvam
 *
 */
public class SyncLockNPA extends SyncAnalysis<TaggedAbsVal<SetOfLocksLValues>> {
	
	private Map<Unit, Set<Lvalue>> unittogen = new HashMap<Unit, Set<Lvalue>>();
	private TempMap tmap;
	protected StrongLocalMustAliasAnalysis mustAlias;
	Map<Unit, Value> enterlocks = new HashMap<Unit, Value>();
	Map<Unit, Value> exitlocks = new HashMap<Unit, Value>();
	Map<Value, Value> exitLocksToEnterLocks = new HashMap<Value, Value>();	// maps each exit lock to its corresponding entry lock
	Map<Value, Value> enterLocksToExitLocks = new HashMap<Value, Value>();	// maps each entry lock to its corresponding exit lock

	private AliasAnalysis aa;

	public SyncLockNPA(SyncCG g, AliasAnalysis aa) {
		super(g);
		
		this.aa = aa;
		
		// create the temp map
		List<Body> bl = new LinkedList<Body>();
		for (SootMethod sm : g.getMethods())
			bl.add(g.getBody(sm));
		tmap = new TempMap(bl);
		
		for (SootMethod sm : g.getMethods()) {
			Body b = g.getBody(sm);
			
			// compute the unconditional gen sets for each statement
			Chain<Unit> uc = b.getUnits();
			for (Unit u : uc) {
				unittogen.put(u, new HashSet<Lvalue>());				

				List<ValueBox> subl = u.getUseBoxes();				
				subl.addAll(u.getDefBoxes());
				for (ValueBox rvb : subl) {
					Value rv = rvb.getValue();
					Value base = null;

					if (rv instanceof InstanceFieldRef) {
						base = ((InstanceFieldRef) rv).getBase();
					} else if (rv instanceof ArrayRef) {
						base = ((ArrayRef) (rv)).getBase();
					} else if (rv instanceof InstanceInvokeExpr) {
						base = ((InstanceInvokeExpr) rv).getBase();
					} else if (rv instanceof LengthExpr) {
						base = ((LengthExpr) rv).getOp();
					} else if (u instanceof ThrowStmt) {
						base = ((ThrowStmt) u).getOp();
					} else if (u instanceof MonitorStmt) {
						base = ((MonitorStmt) u).getOp();
					}

					if (base != null && base.getType() instanceof RefLikeType
							&& base instanceof Local)
						addGen(u, (Local) base);
				}
			}
		}
		populateLockCorrelations();
		System.out.println("\nLock correlations computed. Doing analysis...");
		doAnalysis();
		
	}
	
	// Correlate each exit lock with the corresponding entry lock and vice versa
	private void populateLockCorrelations() {
		Scene appscene = Scene.v();
		Chain<SootClass> appClasses = appscene.getApplicationClasses();
		ReachableMethods rm = appscene.getReachableMethods();
		rm.update();
		BufferedWriter br = null;
		
		try {
		br = new BufferedWriter(new FileWriter(new File("/Users/suvam/workspace/stand/logs/logOut1.txt")));
		}
		catch(IOException e)
		{
			System.out.println("\nI/O Exception occurred");
		}
		// Iterate over each reachable method of each application class
		for(SootClass currClass: appClasses)
		{
			for(SootMethod currMethod: currClass.getMethods())
			{
				if(rm.contains(currMethod))
				{
					System.out.println("\nAnalysing: Class: " + currClass + " Method: " + currMethod);
					if(currMethod.toString().contains("<init>"))
						continue;
					
					Body b = currMethod.getActiveBody();
					UnitGraph g = new EnhancedUnitGraph(b);
					
					// Ensure that g has a single head node
					if(g.getHeads().size() > 1)
					{
						System.out.println("\nHead size exceeds 1!");
						
					}
					
					mustAlias = new StrongLocalMustAliasAnalysis(g);
					Value lock;
					if(currMethod.isSynchronized()) {		// Method is synchronized		
						if(currMethod.isStatic())
							//throw new NotYetImplementedException("static sync method");
							continue;
						else {
							lock = b.getThisLocal();
							for(Unit u : g.getHeads()) {
								enterlocks.put(u, lock);
							}
							for(Unit u : g.getTails()) {
								exitlocks.put(u, lock);
							}
						}
					}
					for(Unit currunit : b.getUnits()) {
						if (currunit instanceof EnterMonitorStmt) {
							lock = ((EnterMonitorStmt) currunit).getOp();
							enterlocks.put(currunit, lock);
						}
						else if (currunit instanceof ExitMonitorStmt) {
							lock = ((ExitMonitorStmt) currunit).getOp();
							exitlocks.put(currunit, lock);
						}
					}
					
					System.out.println("\nSet of exit locks: ");
					// Print out list of exit locks
					for(Unit unlockStmt : exitlocks.keySet()) {
						System.out.println(unlockStmt + " " + exitlocks.get(unlockStmt));
					}
					
					System.out.println("\nSet of entry locks: ");
					// Print out list of entry locks
					for(Unit lockStmt : enterlocks.keySet())
						System.out.println(lockStmt + " " + enterlocks.get(lockStmt));
					
					for(Unit unlockStmt: exitlocks.keySet())
					{
						for(Unit lockStmt: enterlocks.keySet())
						{
							Value unlockOp = exitlocks.get(unlockStmt);
							Value lockOp = enterlocks.get(lockStmt);
							
							//System.out.println("\nCheck if mustAlias is null: " + mustAlias);
							try {
								if(mustAlias.mustAlias((Local)unlockOp, (Stmt) unlockStmt, (Local)lockOp, (Stmt) lockStmt))
								{
									try {
										br.write("\nEntermonitor: " + lockOp);
										br.write("Exit monitor: " + unlockOp);
										br.write("Checking lock equality: " + lockOp.equals(unlockOp));
									}
									catch(IOException e) {
										System.err.println("\nAn I/O Exception occurred!");
									}
									
									System.out.println("Exitmonitor: " + unlockOp);
									System.out.println("EquivTo result: " + lockOp.equivTo(unlockOp));
									exitLocksToEnterLocks.put(unlockOp, lockOp);
									enterLocksToExitLocks.put(lockOp, unlockOp);
									//System.out.println("\nExit lock " + unlockOp + " aliased with Entry lock " + lockOp);
								}
							}
							catch(NullPointerException e)
							{
								//System.out.println("\nNull pointer exception while processing exitlock " + unlockOp + " entry lock " + lockOp);
							}
							
						}
					}
					
					System.out.println("\nPrinting Lock Correlations: ");
					try
					{
						br.write("\nWriting lock correlations: ");
					}
					catch(IOException e)
					{
						System.err.println("\nError while writing to log");
					}
					for(Value unlockOp : exitLocksToEnterLocks.keySet())
					{
						System.out.println(unlockOp + " <--> " + exitLocksToEnterLocks.get(unlockOp));
						
						try
						{
							br.write(unlockOp + " <--> " + exitLocksToEnterLocks.get(unlockOp));
						}
						catch(IOException e)
						{
							System.err.println("\nError while writing to log");
						}
						
					}
					/*
					System.out.println("\nFinished analysing: Class: " + currClass + " Method: " + currMethod);
					*/
				}
				
			}
		}
		try {
			br.close();
		}
		catch(IOException e) {
			System.err.println("Error while closing log");
		}
	}
	
	private void addGen(Unit u, Local base) {
		Set<Lvalue> lset = unittogen.get(u);
		Lvalue lv = new Lvalue(base, new ArrayList<SootField>());
		lset.add(lv);
		if(tmap.getLvalues(base) != null)
			lset.addAll(tmap.getLvalues(base));
	}

	@Override
	public TaggedAbsVal<SetOfLocksLValues> entryInitialFlow() {
		System.out.println("\nInside entryInitialFlow");
		TaggedAbsVal<SetOfLocksLValues> tav = new TaggedAbsVal<SetOfLocksLValues>(new CallString(), new SetOfLocksLValues()); // empty call string with empty set
		for(CallString cs : tav.getCallStrings())
			System.out.println("Initial fact: " + tav.toString());
		return tav;
	}

	@Override
	public void flowthrough(Unit u, TaggedAbsVal<SetOfLocksLValues> in, List<SyncEdge> succlist,
			List<TaggedAbsVal<SetOfLocksLValues>> proplist) {
		// TODO Auto-generated method stub
		System.out.println("\n**** Flow Through Invocation ***");
		System.out.println("\nObtained fact: " + in.toString());
		System.out.println("Processing statement: " + u);
		// create a new tagged set
		TaggedAbsVal<SetOfLocksLValues> newtav = new TaggedAbsVal<SetOfLocksLValues>();
		
		// copy the in set
		for(CallString cs : in.getCallStrings()) {
			SetOfLocksLValues newset = new SetOfLocksLValues(in.getAbsVal(cs));
			newtav.putAbsVal(cs, newset);
		}
		
		// Transfer functions for each type of statement
		
		if(u instanceof IdentityStmt) {
			handleIdentityStmt((IdentityStmt) u, newtav);
		}
		else if(u instanceof AssignStmt) {
			handleAssignStmt((AssignStmt) u, newtav);
		}
		
		// From the incoming fact, remove all facts which has this lock in its lock set
		// Enter monitor
		else if(u instanceof EnterMonitorStmt) {
			newtav = new TaggedAbsVal<SetOfLocksLValues>(handleEnterMonitorStmt((EnterMonitorStmt) u, newtav));
		}
		
		// From the incoming fact, remove the lock (must-aliased) from the lock set
		// Exit monitor
		else if(u instanceof ExitMonitorStmt) {
			newtav = new TaggedAbsVal<SetOfLocksLValues>(handleExitMonitorStmt((ExitMonitorStmt) u, newtav));
		}
		
		// add the default gen set
		Set<Lvalue> genset = unittogen.get(u);
		TaggedAbsVal<SetOfLocksLValues> outTaggedSet = new TaggedAbsVal<SetOfLocksLValues>(newtav);
		for(CallString cs1 : newtav.getCallStrings()) {
			SetOfLocksLValues tavSet = newtav.getAbsVal(cs1);
			SetOfLocksLValues genTavSet = new SetOfLocksLValues(true);	// create a fully empty set
			for(Pair<LockSet, LvalueSet> p : tavSet.getPairs()) {
				LockSet tempLset = p.getO1();
				LvalueSet lvset = p.getO2();
				lvset.addAll(genset);
				Pair pNew = new Pair(tempLset, lvset);
				genTavSet.addPair(pNew);
			}
			outTaggedSet.putAbsVal(cs1, genTavSet);
		}
		newtav = new TaggedAbsVal<SetOfLocksLValues>(outTaggedSet);
		System.out.println("Output fact: " + newtav.toString());
		this.propValue(u, newtav, succlist, proplist); // path sensitive part handled here

	}
	
	// From the incoming flow fact, remove any pair which has this lock in its lockset
	private TaggedAbsVal<SetOfLocksLValues> handleEnterMonitorStmt(EnterMonitorStmt u, TaggedAbsVal<SetOfLocksLValues> tav) {
		// Get lock associated with current entermonitor
		System.out.println("\n***************** ENTER MONITOR **************");
		System.out.println("Entry fact: " + tav);
		Value lock = u.getOp();
		LockSet lset = new LockSet();
		lset.getLockSet().add(lock);
		TaggedAbsVal<SetOfLocksLValues> outTaggedSet = new TaggedAbsVal<SetOfLocksLValues>();
		for(CallString cs: tav.getCallStrings()) {
			SetOfLocksLValues tavSet = tav.getAbsVal(cs);
			SetOfLocksLValues genTavSet = new SetOfLocksLValues(true);	// create a fully empty set
			for(Pair<LockSet, LvalueSet> p : tavSet.getPairs()) {
				if(p.getO1().getLockSet().contains(lock))
					continue;
				else {
					LockSet tempLset = p.getO1();
					tempLset.addAll(lset);
					LvalueSet lvset = p.getO2();
					Pair pNew = new Pair(tempLset, lvset);
					genTavSet.addPair(pNew);
				}
			}
			outTaggedSet.putAbsVal(cs, genTavSet);
			
		}
		tav = new TaggedAbsVal<SetOfLocksLValues>(outTaggedSet);
		System.out.println("Exit fact: " + tav);
		return tav;
	}
	
	// From the incoming fact, remove this lock from each lock set
	private TaggedAbsVal<SetOfLocksLValues> handleExitMonitorStmt(ExitMonitorStmt u, TaggedAbsVal<SetOfLocksLValues> tav) {
		// Get enter lock associated with current exitmonitor lock using exitLocksToEnterLocks map
		Value lock = exitLocksToEnterLocks.get(u.getOp());
		LockSet lset = new LockSet();
		lset.getLockSet().add(lock);
		TaggedAbsVal<SetOfLocksLValues> outTaggedSet = new TaggedAbsVal<SetOfLocksLValues>();
		
		for(CallString cs: tav.getCallStrings()) {
			SetOfLocksLValues tavSet = tav.getAbsVal(cs);
			SetOfLocksLValues genTavSet = new SetOfLocksLValues(true);	// create a fully empty set
			
			for(Pair<LockSet, LvalueSet> p: tavSet.getPairs()) {
				LockSet tempLset = p.getO1();
				tempLset.removeAll(lset);	// Remove the lock from the lock-set
				LvalueSet lvset = p.getO2();
				Pair pNew = new Pair(tempLset, lvset);
				genTavSet.addPair(pNew);
			}
			outTaggedSet.putAbsVal(cs, genTavSet);
		}
		tav = new TaggedAbsVal<SetOfLocksLValues>(outTaggedSet);
		return tav;
	}
	
	// Update the LvalueSet associated with each Pair for each call-string
	private void handleAssignStmt(AssignStmt u, TaggedAbsVal<SetOfLocksLValues> newtav) {
				 
		System.out.println("newtav inside handleAssignStmt: "+newtav);
		Value lo = u.getLeftOp();
		Value ro = u.getRightOp();
			
		// lhs is not an object
		if(!(lo.getType() instanceof RefLikeType)) {			
			return;
		}
		
		Lvalue lhs = Lvalue.makeLvalue(lo);		
		
		// lhs is not an lvalue that we can handle
		if(lhs == null) return;
		
		// rhs calls a function, it will be handled later
		if(ro instanceof InvokeExpr) return;
		
		// update the LvalueSet of each pair associated with each call-string
		System.out.println("update the LvalueSet of each pair associated with each call-string");
		for(CallString cs : newtav.getCallStrings()) {
			for(Pair<LockSet, LvalueSet> p : newtav.getAbsVal(cs).fact) {
				//LvalueSet currset = p.getO2();
				basicAssignStmtHandler(u, (LvalueSet)p.getO2());
				System.out.println("Pairs: "+p);
			}
			
		}
		
	}

	private void basicAssignStmtHandler(AssignStmt u, LvalueSet currset) {
		Value lo = u.getLeftOp();
		Value ro = u.getRightOp();
		
		Lvalue lhs = Lvalue.makeLvalue(lo);
		
		// set of lvalues to be removed
		Set<Lvalue> delset = new HashSet<Lvalue>();
		
		// remove the lvalues dependent on the lhs		
		for (Lvalue lv : currset.getLvalues()) {			
			if (lhs.isDep(lv))
				delset.add(lv);																
		}
		currset.removeAll(delset); // IN - KILL 
		
		if (ro instanceof CastExpr)
			ro = ((CastExpr) ro).getOp();

		Lvalue rhs = Lvalue.makeLvalue(ro);
		
		// set of lvalues to add
		Set<Lvalue> addset = new HashSet<Lvalue>();
		
		// if lhs is of the form l and rhs.g is in currset, add l.g and l's map.g
		if(rhs != null) {
			for(Lvalue lv : currset.getLvalues()) {
				if(rhs.isDep(lv)) {
					Local base = lhs.getBase();
					List<SootField> newac = new ArrayList<SootField>(lhs.getAccessPath());
					List<SootField> reachingpath = rhs.getReachingPath(lv);					
					newac.addAll(reachingpath);
					Lvalue newlval = new Lvalue(base, newac);
					addset.add(newlval);
					
					if(lhs.isLocal()) {
						Set<Lvalue> mlvals = tmap.getLvalues(lhs.getBase());
						if(mlvals != null) {
							for(Lvalue mlval : mlvals) {
								base = mlval.getBase();
								newac = new ArrayList<SootField>(mlval.getAccessPath());					
								newac.addAll(reachingpath);
								newlval = new Lvalue(base, newac);
								addset.add(newlval);
							}
						}
					}
				}
			}
		}
			
		
		
		if(currset.contains(rhs)) { // rhs is non-null
			// add lhs
			addset.add(lhs);
						
			// if lhs is of the form l.f, add l's map.f (f can be empty)			
			Set<Lvalue> mlvals = tmap.getLvalues(lhs.getBase());
			if(mlvals != null) {
				for(Lvalue mlval : mlvals) {
					Local base = mlval.getBase();
					List<SootField> sfl = new ArrayList<SootField>(mlval.getAccessPath());
					sfl.addAll(lhs.getAccessPath());
					Lvalue newlval = new Lvalue(base, sfl);
					addset.add(newlval);
				}
			}											
		}
		// rhs is a new expression
		else if (ro instanceof NewExpr || ro instanceof NewArrayExpr
				|| ro instanceof NewMultiArrayExpr || ro instanceof ThisRef
				|| ro instanceof CaughtExceptionRef) { 
			
			addset.add(lhs);
		}
		else { // rhs is may null
			Set<Lvalue> aliasset = aa.getAliases(lhs, currset.getLvalues());
			currset.removeAll(aliasset);
		}
		
		currset.addAll(addset); //(IN - KILL) + GEN 
		
	}

	// Function to update each LvalueSet associated with some Pair tagged with some call-string.
	// The set of locks associated with each pair remains unaltered.
	private void handleIdentityStmt(IdentityStmt u,	TaggedAbsVal<SetOfLocksLValues> newtav) {
		
		Set<Lvalue> newset = new HashSet<Lvalue>();
		if(u.getRightOp() instanceof ThisRef) {
			Lvalue newlval = Lvalue.makeLvalue(u.getLeftOp());
			if(newlval != null)
				newset.add(newlval);
		}
		// for each callstring
		for(CallString cs : newtav.getCallStrings()) {
			// obtain the set of facts (where each fact is a pair)
			SetOfLocksLValues setOfFacts = newtav.getAbsVal(cs);
			for(Pair<LockSet, LvalueSet> p: setOfFacts.fact) {
				p.getO2().addAll(newset);
				// LvalueSet lvalues =  p.getO2();
				// lvalues.addAll(newset);
			}
			
		}
		
	}

	// Propagate the updated value of a node to its successor node, depending on the type of the successor edge.
	private void propValue(Unit u, TaggedAbsVal<SetOfLocksLValues> newtav, List<SyncEdge> succlist,
			List<TaggedAbsVal<SetOfLocksLValues>> proplist) {
		for(int i = 0; i < succlist.size(); i++) {
			SyncEdge curredge = succlist.get(i);
			if(curredge.getKind() == EdgeKind.FALL_THROUGH) {
				if(u.toString().contains("entermonitor")) {
					System.out.println("\n****Enter Monitor: " + u);
					System.out.println("\nFact propagated: " + newtav);
					System.out.println("*****");
				}
				proplist.add(i, newtav);
			}
			else if((curredge.getKind() == EdgeKind.BRANCH) || (curredge.getKind() == EdgeKind.BRANCH_FALL_THROUGH)) {
				TaggedAbsVal<SetOfLocksLValues> proptav = new TaggedAbsVal<SetOfLocksLValues>(handleBranchStmt(curredge, newtav));
				proplist.add(i, proptav);
			}
			/*else if(curredge.getKind() == EdgeKind.BRANCH_FALL_THROUGH) {
				TaggedAbsVal<LvalueSet> proptav = handleBranchStmt((IfStmt) curredge.getSource(), newtav, false);
				proplist.add(i, proptav);
			}*/
			else if(curredge.getKind() == EdgeKind.EXPLICIT) {
				TaggedAbsVal<SetOfLocksLValues> proptav = new TaggedAbsVal<SetOfLocksLValues>(handleMethodCall(u, curredge, newtav));				
				proplist.add(i, proptav);
			}
			else if(curredge.getKind() == EdgeKind.RETURN) {
				TaggedAbsVal<SetOfLocksLValues> proptav = new TaggedAbsVal<SetOfLocksLValues>(handleReturn(curredge, newtav));								
				proplist.add(i, proptav);
			}
			else if(curredge.getKind() == EdgeKind.MON) {
				TaggedAbsVal<SetOfLocksLValues> proptav = new TaggedAbsVal<SetOfLocksLValues>(handleSync(curredge, newtav));	
				System.out.println("\n****Monitor Stmt: " + u);
				System.out.println("Fact propagated: " + proptav);
				System.out.println("*****");
				proplist.add(i, proptav);
			}
			else if(curredge.getKind() == EdgeKind.START) {
				TaggedAbsVal<SetOfLocksLValues> proptav = new TaggedAbsVal<SetOfLocksLValues>(handleStart(curredge, newtav));								
				proplist.add(i, proptav);
			}
			else {
				System.out.println("Un-handled edge type: " + curredge.getKind());
				System.exit(-1);
			}
		}
		
	}

	private TaggedAbsVal<SetOfLocksLValues> handleStart(SyncEdge edge,
			TaggedAbsVal<SetOfLocksLValues> tav) {
		CallString propcs = new CallString();
		LvalueSet propset = new LvalueSet(true);
		LockSet lset = new LockSet();
		for(CallString cs : tav.getCallStrings()) {
			for(Pair<LockSet, LvalueSet> p : tav.getAbsVal(cs).getPairs()) {
				LvalueSet lvset = p.getO2();
				propset.intersect(lvset);
				lset = p.getO1();
			}
			
		}
		
		Unit u = edge.getSource();
		InvokeExpr ie = ((Stmt) u).getInvokeExpr();
		SootMethod sm = g.getMethodFromUnit(edge.getTarget());
		Set<Lvalue> addset = new HashSet<Lvalue>();
		
		if(ie instanceof InstanceInvokeExpr) {
			Value base = ((InstanceInvokeExpr) ie).getBase();			
			Local l = g.getBody(sm).getThisLocal();
			addset.addAll(substitute((Local) base, l, propset));			
		}
		propset.addAll(addset);
		HashSet<Pair<LockSet, LvalueSet>> tempMap = new HashSet<Pair<LockSet, LvalueSet>>();
		tempMap.add(new Pair<LockSet, LvalueSet>(lset, propset));
		
		
		return new TaggedAbsVal<SetOfLocksLValues>(propcs, new SetOfLocksLValues(tempMap));
	}

	private TaggedAbsVal<SetOfLocksLValues> handleSync(SyncEdge curredge,
			TaggedAbsVal<SetOfLocksLValues> tav) {
		CallString propcs = new CallString();
		LvalueSet propset = new LvalueSet(true);
		LockSet lset = new LockSet(true);
		for(CallString cs : tav.getCallStrings()) {
			for(Pair<LockSet, LvalueSet> p : tav.getAbsVal(cs).getPairs()) {
				propset.intersect((LvalueSet)p.getO2()); // Join is intersection
				lset.intersect((LockSet)p.getO1());    // Join is intersection
			}
		}
		HashSet<Pair<LockSet, LvalueSet>> tempMap = new HashSet<Pair<LockSet, LvalueSet>>();
		tempMap.add(new Pair<LockSet, LvalueSet>(lset, propset));
		
		return new TaggedAbsVal<SetOfLocksLValues>(propcs, new SetOfLocksLValues(tempMap));
														
	}

	private TaggedAbsVal<SetOfLocksLValues> handleReturn(SyncEdge edge,	TaggedAbsVal<SetOfLocksLValues> tav) {
		
		Unit u = edge.getSource();
		SootMethod sm = g.getMethodFromUnit(u);
		
		TaggedAbsVal<SetOfLocksLValues> proptav = new TaggedAbsVal<SetOfLocksLValues>();
		
		for(CallString cs : tav.getCallStrings()) {
			CallString propcs = new CallString(cs);
			Unit caller = propcs.popLastElem();
			HashSet<Pair<LockSet, LvalueSet>> tempSet = new HashSet<Pair<LockSet, LvalueSet>>();
			if((caller == null) || (g.getCallers(edge).contains(caller))) {
				SetOfLocksLValues tempLockLvalueSet = new SetOfLocksLValues(tav.getAbsVal(cs));
				for(Pair<LockSet, LvalueSet> p : tempLockLvalueSet.getPairs()) {
					LvalueSet propset = p.getO2();
					propset = basicReturnHandler(edge, sm, propset);
					LockSet lset = p.getO1();
					Pair p1 = new Pair(lset, propset);
					tempSet.add(p1);
			
				}	
				proptav.putAbsVal(propcs, new SetOfLocksLValues(tempSet));
			}	
			
		}
		return proptav;
	}

	private LvalueSet basicReturnHandler(SyncEdge edge, SootMethod sm, LvalueSet absval) {
		LvalueSet propset = new LvalueSet(absval);
		Set<Lvalue> addset = new HashSet<Lvalue>();
		
		Unit u = edge.getSource();
		
		List<Unit> callers = g.getCallers(edge);
		for(Unit caller : callers) {
			if ((caller instanceof AssignStmt) && ((AssignStmt) caller).getLeftOp().getType() instanceof RefLikeType) {
				if((((ReturnStmt)u).getOp() != null) && (((ReturnStmt)u).getOp() instanceof Local)) {
					addset.addAll(substitute((Local)((ReturnStmt) u).getOp(), (Local) ((AssignStmt) caller).getLeftOp(), absval));
				}
			}
			InvokeExpr ie = ((Stmt) caller).getInvokeExpr();
			if(ie instanceof InstanceInvokeExpr) {
				Value base = ((InstanceInvokeExpr) ie).getBase();
			
				Local l = g.getBody(g.getMethodFromUnit(u)).getThisLocal();
				addset.addAll(substitute(l, (Local) base, absval));
				/*
				Set<Lvalue> mlvals = tmap.getLvalues((Local) base);
				if(mlvals != null) {
					for(Lvalue mlval : mlvals) {
						base = mlval.getBase();
						newac = new ArrayList<SootField>(mlval.getAccessPath());					
						newac.addAll(reachingpath);
						newlval = new Lvalue(base, newac);
						addset.add(newlval);
					}
				}*/
			}
			for(int j = 0; j < ie.getArgCount(); j++) {
				Value currarg =  ie.getArg(j);
				Local param = g.getBody(sm).getParameterLocal(j);
				if(currarg instanceof Local)
					addset.addAll(substitute(param, (Local)currarg, absval));									
			}			
		}
		
		// remove lvalues local to returning method
		Set<Lvalue> delset = new HashSet<Lvalue>();
		for(Lvalue lv : absval.getLvalues()) {
			if(lv.isInScope(sm))
				delset.add(lv);
		}
		
		propset.addAll(addset);
		propset.removeAll(delset);
		return propset;
	}

	private TaggedAbsVal<SetOfLocksLValues> handleMethodCall(Unit stmt, SyncEdge edge, TaggedAbsVal<SetOfLocksLValues> tav) {
		Unit u = edge.getSource();		
		SootMethod sm = g.getMethodFromUnit(edge.getTarget());
		
		TaggedAbsVal<SetOfLocksLValues> proptav = new TaggedAbsVal<SetOfLocksLValues>();
		
		for(CallString cs : tav.getCallStrings()) {
			CallString propcs = new CallString(cs);
			propcs.append(u);
			HashSet<Pair<LockSet, LvalueSet>> tempSet = new HashSet<Pair<LockSet, LvalueSet>>();
			SetOfLocksLValues setlklvalue = new SetOfLocksLValues(tav.getAbsVal(cs));
			for(Pair<LockSet, LvalueSet> p: setlklvalue.getPairs()) {
				LvalueSet propset = (LvalueSet) p.getO2();
				LockSet lkset = (LockSet) p.getO1();
				propset = new LvalueSet(basicMethodCallHandler(u, sm, propset));
				tempSet.add(new Pair<LockSet, LvalueSet>(lkset, propset));
			}
			proptav.putAbsVal(propcs, new SetOfLocksLValues(tempSet));
		}
		
		return proptav;
	}

	private LvalueSet basicMethodCallHandler(Unit u, SootMethod sm, LvalueSet absval) {
		
		LvalueSet propset = new LvalueSet(absval);
		Set<Lvalue> addset = new HashSet<Lvalue>();
		Set<Lvalue> delset = new HashSet<Lvalue>();
		
		InvokeExpr ie = ((Stmt) u).getInvokeExpr();
		
		// if instance invoke, add thislocal.f 
		if(ie instanceof InstanceInvokeExpr) {
			Value base = ((InstanceInvokeExpr) ie).getBase();			
			Local l = g.getBody(sm).getThisLocal();
			addset.addAll(substitute((Local) base, l, absval));			
		}
		
		// add or delete parameters
		for(int i = 0; i < ie.getArgCount(); i++) {
			Value currarg =  ie.getArg(i);
			Local param = g.getBody(sm).getParameterLocal(i);
			if(currarg instanceof Local)
				addset.addAll(substitute((Local) currarg, param, absval));
			else 
				delset.add(Lvalue.makeLvalue(param));
		}
		
		propset.addAll(addset);
		propset.removeAll(delset);
		
		// the method returned a non-zero number of definitely non-null lvalues. 
		// thus, the lhs must also be non-null
		System.out.println("\nADDSET: " + addset.toString());
		System.out.println("DELSET: " + delset.toString());

		return propset;
	}
	
	
	private Set<Lvalue> substitute(Local oldbase, Local newbase, LvalueSet lvset) {
		Set<Lvalue> res = new HashSet<Lvalue>();
		Set<Lvalue> mlvals = tmap.getLvalues(newbase);
		for(Lvalue currlv : lvset.getLvalues()) {
			if(oldbase.equals(currlv.getBase())) {
				res.add(currlv.substituteBase(newbase));
				if(mlvals != null) {
					for(Lvalue mlval : mlvals) {
						Local base = mlval.getBase();
						List<SootField> newac = new LinkedList<SootField>(mlval.getAccessPath());
						newac.addAll(currlv.getAccessPath());
						Lvalue lv = new Lvalue(base, newac);
						res.add(lv);
					}
				}
			}
		}
		return res;
	}

	private TaggedAbsVal<SetOfLocksLValues> handleBranchStmt(SyncEdge edge, TaggedAbsVal<SetOfLocksLValues> newtav) {
		
		IfStmt u = (IfStmt)(edge.getSource());
		boolean isbranch = edge.getKind().equals(EdgeKind.BRANCH);
		
		Value cond = u.getCondition();
		Value op1 = ((BinopExpr) cond).getOp1();
		Value op2 = ((BinopExpr) cond).getOp2();
		boolean isNeg = cond instanceof NeExpr;
		Value toGen = null;
		
		// case 1: opN is a local and opM is NullConstant
		// => opN nonnull on ne branch.
		if (op1 instanceof Local && op2 instanceof NullConstant)
			toGen = op1;

		if (op2 instanceof Local && op1 instanceof NullConstant)
			toGen = op2;
		
		if(toGen == null) return newtav;
		if((isNeg && !isbranch) || (!isNeg && isbranch)) return newtav;
		
		Lvalue toadd = Lvalue.makeLvalue(toGen);
		if(toadd == null) return newtav;
		 		
		Set<Lvalue> addset = new HashSet<Lvalue>();
		addset.add(toadd);
		if(toadd.isLocal()) {
			Set<Lvalue> tset = tmap.getLvalues(toadd.getBase());
			if(tset != null)
				addset.addAll(tset);
		}
		
		TaggedAbsVal<SetOfLocksLValues> propval = new TaggedAbsVal<SetOfLocksLValues>();
		for(CallString cs : newtav.getCallStrings()) {
			HashSet<Pair<LockSet, LvalueSet>> tempSet = new HashSet<Pair<LockSet, LvalueSet>>();
			for(Pair<LockSet, LvalueSet> p : newtav.getAbsVal(cs).getPairs()) {
				LvalueSet lvset = p.getO2();
				lvset.addAll(addset);
				tempSet.add(new Pair<LockSet, LvalueSet>(p.getO1(), lvset));
			}
			SetOfLocksLValues generatedSet = new SetOfLocksLValues(tempSet);
			propval.putAbsVal(cs, generatedSet);
		}
		
		return propval;
	}

	public void merge(SyncEdge edge, TaggedAbsVal<SetOfLocksLValues> currval, TaggedAbsVal<SetOfLocksLValues> val) {
		if(edge.getKind() == EdgeKind.MON) {
			SetOfLocksLValues fact = currval.getAbsVal(new CallString()); //sync edge has a summary value with empty call string
			System.out.println("\n***MONITOR MERGE***");
			System.out.println("Incoming: " + currval);
			System.out.println("Old: " + val);
			
//			for(CallString cs: val.getCallStrings()) {
//				basicSyncMerge(fact, val.getAbsVal(cs));
				
			for(CallString cs: val.getCallStrings()) {
				SetOfLocksLValues valLockLValueSet = new SetOfLocksLValues(true);
				for(Pair<LockSet, LvalueSet> p1: fact.getPairs()) {		// for each pair in the incoming monitor edge
					
					boolean matched = false;
					for(Pair<LockSet, LvalueSet> p2: val.getAbsVal(cs).getPairs()) {
						if(p1.getO1().toString().equals(p2.getO1().toString())) {	// if the locksets of the pair match, perform basicLValueMerge on the locksets
							matched = true;
							syncMergeLvalueSet((LvalueSet)p1.getO2(), (LvalueSet)p2.getO2());
							valLockLValueSet.addPair(new Pair<LockSet, LvalueSet>(p2.getO1(), p2.getO2()));
							break;
						}
					}
					if(matched==false) {	// lockset of p1 doesn't match up with any lockset in p2
						valLockLValueSet.addPair(p1);
					}
					
				}
				val.putAbsVal(cs, valLockLValueSet);
			}
			System.out.println("Val before MON return: " + val.toString());
			}
	
		else {
			
			for(CallString cs : currval.getCallStrings()) {
			
				SetOfLocksLValues currLockLValueSet = currval.getAbsVal(cs);
				if(val.getAbsVal(cs) == null) {
					val.putAbsVal(cs, new SetOfLocksLValues(currLockLValueSet));
					System.out.println("Val in merge [no call string]: "+val.getAbsVal(cs));
				}
//				else if(val.getAbsVal(cs).getPairs() == null) {
//						val.putAbsVal(cs, new SetOfLocksLValues(currval.getAbsVal(cs)));
//				}
				else {
					SetOfLocksLValues valLockLValueSet = new SetOfLocksLValues(true);	// moved above for
					for(Pair<LockSet, LvalueSet> p1: currLockLValueSet.getPairs()) {
						
						System.out.println("Val after initialize: " + valLockLValueSet.toString());
						boolean matched = false;
						for(Pair<LockSet, LvalueSet> p2: val.getAbsVal(cs).getPairs()) {
							System.out.println("!!! MATCHING LOCKS: " + p1.getO1().toString() + " " + p2.getO1().toString() );
							if(p1.getO1().toString().equals(p2.getO1().toString())) {	// Lock sets match, intersect lvalueset
								System.out.println("Old Values: " + p2.getO2().toString());
								System.out.println("Incoming Values: " + p1.getO2().toString());
								matched = true;
								((LvalueSet)p2.getO2()).intersect(p1.getO2());
								System.out.println("Intersected values: " + p2.getO2().toString());
								
								valLockLValueSet.addPair(new Pair<LockSet, LvalueSet>(p2.getO1(),p2.getO2()));
								System.out.println("Val after intersect: " + valLockLValueSet.toString());
								break;
							}
						}
						if(matched == false){
							valLockLValueSet.addPair(p1);
							System.out.println("Val in merge[lock mismatch]: "+val.getAbsVal(cs));
						}
						
					}
					val.putAbsVal(cs, valLockLValueSet); // moved below }
				}
			}
			System.out.println("Val before NON-MON return: " + val.toString());
		}
	}
	
	
	private void basicSyncMerge(SetOfLocksLValues s1, SetOfLocksLValues s2) {
		boolean match = false;	// check if a pair in s1 has matched up with a pair in s2
		
		for(Pair<LockSet, LvalueSet> p1: s1.getPairs()) {
			match = false;
			for(Pair<LockSet, LvalueSet> p2: s2.getPairs()) {
				if(p1.getO1().isEqual(p2.getO1())) {	// Locksets match, perform standard join of lvaluesets
					syncMergeLvalueSet(p1.getO2(), p2.getO2());
					match = true;
				}			
			}
			if(match == false)	// p1's lockset is distinct for the lockset of every p2, add p1 to s2
				s2.addPair(p1);
		}
	}

	private void syncMergeLvalueSet(LvalueSet synclvset, LvalueSet oldlvset) {
		if(oldlvset.isFull()) {
			System.out.println("OOPS! Can't handle full sets for sync merge!");
			System.exit(-1);
		}
		Set<Lvalue> retainset = new HashSet<Lvalue>();
		for(Lvalue lv : oldlvset.getLvalues()) {
			if((lv.isLocal()) || isThreadLocal(lv)) {
				retainset.add(lv);
				continue;
			}
			for(Lvalue slv : synclvset.getLvalues()) {
				if(aa.isMustAlias(lv, slv))
					retainset.add(lv);
			}
		}
		
		LvalueSet retainlvset = new LvalueSet();
		retainlvset.addAll(retainset);
		oldlvset.intersect(retainlvset);
	}

	private boolean isThreadLocal(Lvalue lv) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public TaggedAbsVal<SetOfLocksLValues> newInitialFlow() {		
		return new TaggedAbsVal<SetOfLocksLValues>(); // empty map denotes top
	}

	@Override
	public boolean readyToProcess(
			WorkListElem currwle, TaggedAbsVal<SetOfLocksLValues> oldval) {
		// TODO Auto-generated method stub
		return true;
	}
	
	@Override public boolean isSafeApprox(TaggedAbsVal<SetOfLocksLValues> oldval, TaggedAbsVal<SetOfLocksLValues> currval) {
		for(CallString cs: currval.getCallStrings()) {
			if(oldval.getAbsVal(cs) == null)	return false;
			
			SetOfLocksLValues oldset = oldval.getAbsVal(cs);
			SetOfLocksLValues currset = currval.getAbsVal(cs);
			
			boolean match = false;	// checks if each pair in f1 is matched up with some pair in f2
			
			if(oldset.getPairs() == null)	return false;
			
			for(Pair<LockSet, LvalueSet> p1 : oldset.getPairs()) {
				
				match = false;
				for(Pair<LockSet, LvalueSet> p2: currset.getPairs()) {
					if(p1.getO1().isSafer(p2.getO1())) 
						if(p1.getO2().isSafer(p2.getO2()))
							match = true;
				}
				if(match == false)
					return false;
			}
		}
		return true;
	}


//	public boolean LValueSetisSafeApprox(TaggedAbsVal<SetOfLo oldval, TaggedAbsVal<SetOfLocksLValues> currval) {
//		for(CallString cs : currval.getCallStrings()) {
//			if(oldval.getAbsVal(cs) == null) return false;
//			if(!oldval.getAbsVal(cs).isSafer(currval.getAbsVal(cs))) return false;			
//		}
//		return true;
//	}
	
	public TaggedAbsVal<SetOfLocksLValues> getLocalResult(Unit u) {
		TaggedAbsVal<SetOfLocksLValues> taggedres = this.getResult(u);
		SootMethod sm = g.getMethodFromUnit(u);
		Set<Lvalue> delset = new HashSet<Lvalue>();
		for(CallString cs : taggedres.getCallStrings()) {
			delset.clear();
			for(Pair<LockSet, LvalueSet> p: taggedres.getAbsVal(cs).getPairs()) {
				HashSet<Lvalue> lset = (HashSet<Lvalue>) p.getO2().getLvalues();
				for(Lvalue lv : lset) {
					if(!lv.isInScope(sm)) {
						delset.add(lv);
					}
				}
				lset.removeAll(delset);
			}
		}
		return taggedres;
	}
	
	public String getLocalResultWithPrinting(Unit u) {
		String outString = "";
		TaggedAbsVal<SetOfLocksLValues> taggedres = this.getResult(u);
		SootMethod sm = g.getMethodFromUnit(u);
		Set<Lvalue> delset = new HashSet<Lvalue>();
		if(taggedres == null)
		{
			System.out.println("Unit " + u + "returned no taggedres");
			return "";
		}
		for(CallString cs : taggedres.getCallStrings()) {
			delset.clear();
			for(Pair<LockSet, LvalueSet> p: taggedres.getAbsVal(cs).getPairs()) {
				HashSet<Lvalue> lset = (HashSet<Lvalue>) p.getO2().getLvalues();
				for(Lvalue lv : lset) {
					if(!lv.isInScope(sm)) {
						delset.add(lv);
					}
				}
				lset.removeAll(delset);
			}
		}
		//System.out.println("Printing local result at " + u);
		for(CallString cs: taggedres.getCallStrings()) {
			outString += cs.toString() + "-->" + taggedres.getAbsVal(cs).toString();
		}
		return outString;
	}
		
	public LvalueSet getAggrResult(Unit u) {
		TaggedAbsVal<SetOfLocksLValues> taggedres = this.getLocalResult(u);
		LvalueSet res = new LvalueSet(true);
		for(CallString cs : taggedres.getCallStrings()) {
			for(Pair<LockSet, LvalueSet> p: taggedres.getAbsVal(cs).getPairs()) {
				res.intersect(p.getO2());
			}
		}
		return res;
	}
	

}
