/**
 * 
 */
package experiments.AntoineHabilitation.genSrc;
/**
 * @author Suvam Mukherjee.
 *
 * Indian Institute of Science, 2017.
 *
 */

class W1_3_11 extends Thread {
	
	public void run() {
		
		while(true) {
			synchronized(Fig3_11.lock) {


				if(!(Fig3_11.X == Fig3_11.Y) && (Region.r0==Region.r0))
					Fig3_11.error = 1;
Region.r0 = 0;

				if(Fig3_11.X > 0 && (Region.r0==Region.r0)) {
					Fig3_11.X = Fig3_11.X - 1;
Region.r0 = Region.r0;
					Fig3_11.Y = Fig3_11.Y - 1;
Region.r0 = Region.r0;
				}
			}
		}
	}
}

class W2_3_11 extends Thread {
	
	public void run() {
		
		while(true) {
			synchronized(Fig3_11.lock) {

				if(!(Fig3_11.X == Fig3_11.Y) && (Region.r0==Region.r0))
					Fig3_11.error = 1;
Region.r0 = 0;

				if(Fig3_11.X < 10 && (Region.r0==Region.r0)) {
					Fig3_11.X = Fig3_11.X + 1;
Region.r0 = Region.r0;
					Fig3_11.Y = Fig3_11.Y + 1;
Region.r0 = Region.r0;
				}
			}
		}
	}
}

public class Fig3_11 {
	
	public static int X;
	public static int Y;
	public static Object lock;
	public static int error;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		System.out.println("\nStarting Fig3.11 ...");
		Fig3_11.X = 0;
Region.r0 = 0;
		Fig3_11.Y = 0;
Region.r0 = 0;
		Fig3_11.lock = new Object();
		Fig3_11.error = 0;
Region.r0 = 0;
		
		W1_3_11 t1 = new W1_3_11();
		W2_3_11 t2 = new W2_3_11();
		
		t1.start();
		t2.start();
		
		try {
			t1.join();
			t2.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("\nEnd of program");
		

	}

}
