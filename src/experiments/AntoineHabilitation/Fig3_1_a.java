/**
 * 
 */
package experiments.AntoineHabilitation;

/**
 * @author Suvam Mukherjee.
 *
 * Indian Institute of Science, 2017.
 *
 */
class W1_3_1 extends Thread {
	
	public void run() {
		
		while(true) {
			synchronized(Fig3_1_a.lock) {
				
				if(!(Fig3_1_a.X <= Fig3_1_a.Y))
					Fig3_1_a.error = 1;
				
				if(!(Fig3_1_a.X >= 0))
					Fig3_1_a.error = 1;
				
				if(!(Fig3_1_a.X <= 10))
					Fig3_1_a.error = 1;
				
				if(!(Fig3_1_a.Y <= 10))
					Fig3_1_a.error = 1;
				
				if(Fig3_1_a.X < Fig3_1_a.Y) {
					
					if(!(Fig3_1_a.X <= Fig3_1_a.Y - 1))
						Fig3_1_a.error = 1;
					
					Fig3_1_a.X = Fig3_1_a.X + 1;
				}
			}
		}
	}
}

class W2_3_1 extends Thread {
	
	public void run() {
		
		while(true) {
			synchronized(Fig3_1_a.lock) {
				
				if(!(Fig3_1_a.X <= Fig3_1_a.Y))
					Fig3_1_a.error = 1;
				
				if(!(Fig3_1_a.X >= 0))
					Fig3_1_a.error = 1;
				
				if(!(Fig3_1_a.X <= 10))
					Fig3_1_a.error = 1;
				
				if(!(Fig3_1_a.Y <= 10))
					Fig3_1_a.error = 1;
				
				if(Fig3_1_a.Y < 10) {
					
					if(!(Fig3_1_a.X <= Fig3_1_a.Y))
						Fig3_1_a.error = 1;
					
					if(!(Fig3_1_a.X >= 0))
						Fig3_1_a.error = 1;
					
					if(!(Fig3_1_a.X <= 9))
						Fig3_1_a.error = 1;
					
					if(!(Fig3_1_a.Y <= 9))
						Fig3_1_a.error = 1;
					
					Fig3_1_a.Y = Fig3_1_a.Y + 1;
				}
					
			}
		}
	}
}
public class Fig3_1_a {

	public static int X;
	public static int Y;
	public static Object lock;
	public static int error;
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("\nStarting...");
		Fig3_1_a.X = 0;
		Fig3_1_a.Y = 0;
		Fig3_1_a.lock = new Object();
		Fig3_1_a.error = 0;
		
		W1_3_1 t1 = new W1_3_1();
		W2_3_1 t2 = new W2_3_1();
		
		t1.start();
		t2.start();
		
		try {
			t1.join();
			t2.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("\nEnd of program");

	}

}
