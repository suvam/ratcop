/**
 * Fig 3.6 in Antoine Mine Habilitation Report.
 * Asbtract Consumers/Producers
 */
package experiments.AntoineHabilitation;

/**
 * @author Suvam Mukherjee.
 *
 * Indian Institute of Science, 2017.
 *
 */

class W1_3_6 extends Thread {
	
	public void run() {
		
		while(true) {
			
			synchronized(Fig3_6.lock) {
				
				if(!(Fig3_6.X >= 0))
						Fig3_6.error = 1;
				
				if(!(Fig3_6.X <= 10))
					Fig3_6.error = 1;
				
				if(Fig3_6.X > 0) {
					Fig3_6.X = Fig3_6.X - 1;
				}
			}
		}
	}
}

class W2_3_6 extends Thread {
	
	public void run() {
		
		while(true) {
			
			synchronized(Fig3_6.lock) {
				
				if(!(Fig3_6.X >= 0))
					Fig3_6.error = 1;
				
				Fig3_6.X = Fig3_6.X + 1;
				
				if(Fig3_6.X > 10) {
					Fig3_6.X = 10;
				}
				
				if(!(Fig3_6.X <= 10))
					Fig3_6.error = 1;
			}
		}
	}
}

public class Fig3_6 {

	public static int X;
	public static Object lock;
	public static int error;
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Fig3_6.X = 0;
		Fig3_6.lock = new Object();
		Fig3_6.error = 0;
		
		W1_3_6 t1 = new W1_3_6();
		W2_3_6 t2 = new W2_3_6();
		
		t1.start();
		t2.start();
		
		try {
			t1.join();
			t2.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("\nEnd of program");
	}

}
