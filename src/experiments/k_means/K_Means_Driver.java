/**
 * Implementation of a concurrent k-means algorithm.
 * 
 * For purposes of experiment, we bound k to 4.
 * 
 * Implementation based on slides by Ajay Padoor Chandramohan.
 */
package experiments.k_means;

import java.util.ArrayList;
import java.util.Random;

import experiments.concMatMul.ConcMatMul;

/**
 * @author Suvam Mukherjee.
 *
 * Indian Institute of Science, 2017.
 *
 */
public class K_Means_Driver {
	
	public static int m1_main_x;
	public static int m1_main_y;
	public static int m1_W1_x;
	public static int m1_W1_y;
	public static int m1_W2_x;
	public static int m1_W2_y;
	public static int m1_W3_x;
	public static int m1_W3_y;
	public static int m1_W4_x;
	public static int m1_W4_y;
	
	public static int m2_main_x;
	public static int m2_main_y;
	public static int m2_W1_x;
	public static int m2_W1_y;
	public static int m2_W2_x;
	public static int m2_W2_y;
	public static int m2_W3_x;
	public static int m2_W3_y;
	public static int m2_W4_x;
	public static int m2_W4_y;
	
	public static int m3_main_x;
	public static int m3_main_y;
	public static int m3_W1_x;
	public static int m3_W1_y;
	public static int m3_W2_x;
	public static int m3_W2_y;
	public static int m3_W3_x;
	public static int m3_W3_y;
	public static int m3_W4_x;
	public static int m3_W4_y;
	
	public static int m4_main_x;
	public static int m4_main_y;
	public static int m4_W1_x;
	public static int m4_W1_y;
	public static int m4_W2_x;
	public static int m4_W2_y;
	public static int m4_W3_x;
	public static int m4_W3_y;
	public static int m4_W4_x;
	public static int m4_W4_y;
	
	public static int W1_x_start;
	public static int W1_x_end;
	public static int W1_y_start;
	public static int W1_y_end;
	
	public static int W2_x_start;
	public static int W2_x_end;
	public static int W2_y_start;
	public static int W2_y_end;
	
	public static int W3_x_start;
	public static int W3_x_end;
	public static int W3_y_start;
	public static int W3_y_end;
	
	public static int W4_x_start;
	public static int W4_x_end;
	public static int W4_y_start;
	public static int W4_y_end;
	
	public static int[] points_x;
	public static int[] points_y;
	
	public static ArrayList<Integer> W1_C1_x;
	public static ArrayList<Integer> W1_C1_y;
	public static ArrayList<Integer> W2_C1_x;
	public static ArrayList<Integer> W2_C1_y;
	public static ArrayList<Integer> W3_C1_x;
	public static ArrayList<Integer> W3_C1_y;
	public static ArrayList<Integer> W4_C1_x;
	public static ArrayList<Integer> W4_C1_y;
	
	public static ArrayList<Integer> W1_C2_x;
	public static ArrayList<Integer> W1_C2_y;
	public static ArrayList<Integer> W2_C2_x;
	public static ArrayList<Integer> W2_C2_y;
	public static ArrayList<Integer> W3_C2_x;
	public static ArrayList<Integer> W3_C2_y;
	public static ArrayList<Integer> W4_C2_x;
	public static ArrayList<Integer> W4_C2_y;
	
	public static ArrayList<Integer> W1_C3_x;
	public static ArrayList<Integer> W1_C3_y;
	public static ArrayList<Integer> W2_C3_x;
	public static ArrayList<Integer> W2_C3_y;
	public static ArrayList<Integer> W3_C3_x;
	public static ArrayList<Integer> W3_C3_y;
	public static ArrayList<Integer> W4_C3_x;
	public static ArrayList<Integer> W4_C3_y;
	
	public static ArrayList<Integer> W1_C4_x;
	public static ArrayList<Integer> W1_C4_y;
	public static ArrayList<Integer> W2_C4_x;
	public static ArrayList<Integer> W2_C4_y;
	public static ArrayList<Integer> W3_C4_x;
	public static ArrayList<Integer> W3_C4_y;
	public static ArrayList<Integer> W4_C4_x;
	public static ArrayList<Integer> W4_C4_y;
	
	
	public static int iterations;
	public static int W1_iterations;
	public static int W2_iterations;
	public static int W3_iterations;
	public static int W4_iterations;
	
	public static int n;
	
	public static Object lock;

	/**
	 * @param 
	 * args[0] : number of data points
	 * args[1] : number of iterations
	 */
	public static void main(String[] args) {
		K_Means_Driver.n = Integer.parseInt(args[0]);
		K_Means_Driver.iterations = Integer.parseInt(args[1]);
		
		K_Means_Driver.lock = new Object();
		
		if(K_Means_Driver.n > 0 && K_Means_Driver.iterations > 0) {
			
			// Initialize data points
			Random rand = new Random();
			
			K_Means_Driver.points_x = new int[K_Means_Driver.n];
			K_Means_Driver.points_y = new int[K_Means_Driver.n];
			
			for(int i=0; i < K_Means_Driver.n; i++) {
				K_Means_Driver.points_x[i] = rand.nextInt(100);
				K_Means_Driver.points_y[i] = rand.nextInt(100);
			}
			
			System.out.println("\nPrint points: ");
			for(int i=0, j=0; i<K_Means_Driver.points_x.length && j<K_Means_Driver.points_y.length; i++, j++) {
				System.out.println("<" + K_Means_Driver.points_x[i] + ", " + K_Means_Driver.points_y[j] +">");
			}
			
			// arbitrary initial choices for the means
			// choice_1 is the arbitrary initial mean for cluster 1, and so on...
			int choice_1 = 0, choice_2 = 0, choice_3 = 0, choice_4 = 0;
			
			choice_1 = rand.nextInt(K_Means_Driver.n);
			
			choice_2 = rand.nextInt(K_Means_Driver.n);
			// make another choice for 2 if it equals choice_1
			while(choice_2 == choice_1)
				choice_2 = rand.nextInt(n);
			
			choice_3 = rand.nextInt(K_Means_Driver.n);
			// make another choice for 3 if it equals choice_1 or choice_2
			while((choice_3 == choice_1) || (choice_3 == choice_2))
				choice_3 = rand.nextInt(n);
			
			choice_4 = rand.nextInt(K_Means_Driver.n);
			// make another choice for 4 if it equals choice_1 or choice_2 or choice_3
			while((choice_4 == choice_1) || (choice_4 == choice_2) || (choice_4 == choice_3))
				choice_4 = rand.nextInt(n);
			
			// Initialize the local means of each thread
			// Cluster 1
			K_Means_Driver.m1_W1_x = K_Means_Driver.points_x[choice_1];
			K_Means_Driver.m1_W1_y = K_Means_Driver.points_y[choice_1];
			
			K_Means_Driver.m1_W2_x = K_Means_Driver.points_x[choice_1];
			K_Means_Driver.m1_W2_y = K_Means_Driver.points_y[choice_1];
			
			K_Means_Driver.m1_W3_x = K_Means_Driver.points_x[choice_1];
			K_Means_Driver.m1_W3_y = K_Means_Driver.points_y[choice_1];
			
			K_Means_Driver.m1_W4_x = K_Means_Driver.points_x[choice_1];
			K_Means_Driver.m1_W4_y = K_Means_Driver.points_y[choice_1];
			
			// Cluster 2
			K_Means_Driver.m2_W1_x = K_Means_Driver.points_x[choice_2];
			K_Means_Driver.m2_W1_y = K_Means_Driver.points_y[choice_2];
			
			K_Means_Driver.m2_W2_x = K_Means_Driver.points_x[choice_2];
			K_Means_Driver.m2_W2_y = K_Means_Driver.points_y[choice_2];
			
			K_Means_Driver.m2_W3_x = K_Means_Driver.points_x[choice_2];
			K_Means_Driver.m2_W3_y = K_Means_Driver.points_y[choice_2];
			
			K_Means_Driver.m2_W4_x = K_Means_Driver.points_x[choice_2];
			K_Means_Driver.m2_W4_y = K_Means_Driver.points_y[choice_2];
			
			// Cluster 3
			K_Means_Driver.m3_W1_x = K_Means_Driver.points_x[choice_3];
			K_Means_Driver.m3_W1_y = K_Means_Driver.points_y[choice_3];
			
			K_Means_Driver.m3_W2_x = K_Means_Driver.points_x[choice_3];
			K_Means_Driver.m3_W2_y = K_Means_Driver.points_y[choice_3];
			
			K_Means_Driver.m3_W3_x = K_Means_Driver.points_x[choice_3];
			K_Means_Driver.m3_W3_y = K_Means_Driver.points_y[choice_3];
			
			K_Means_Driver.m3_W4_x = K_Means_Driver.points_x[choice_3];
			K_Means_Driver.m3_W4_y = K_Means_Driver.points_y[choice_3];
			
			// Cluster 4
			K_Means_Driver.m4_W1_x = K_Means_Driver.points_x[choice_4];
			K_Means_Driver.m4_W1_y = K_Means_Driver.points_y[choice_4];
			
			K_Means_Driver.m4_W2_x = K_Means_Driver.points_x[choice_4];
			K_Means_Driver.m4_W2_y = K_Means_Driver.points_y[choice_4];
			
			K_Means_Driver.m4_W3_x = K_Means_Driver.points_x[choice_4];
			K_Means_Driver.m4_W3_y = K_Means_Driver.points_y[choice_4];
			
			K_Means_Driver.m4_W4_x = K_Means_Driver.points_x[choice_4];
			K_Means_Driver.m4_W4_y = K_Means_Driver.points_y[choice_4];
			
			// Partition the data-points for the 4 threads
			K_Means_Driver.W1_x_start = 0;
			K_Means_Driver.W1_x_end = K_Means_Driver.W1_x_start + ((K_Means_Driver.n / 4) - 1);
			K_Means_Driver.W2_x_start = K_Means_Driver.W1_x_end + 1;
			K_Means_Driver.W2_x_end = K_Means_Driver.W2_x_start + ((K_Means_Driver.n / 4) - 1);
			K_Means_Driver.W3_x_start = K_Means_Driver.W2_x_end + 1;
			K_Means_Driver.W3_x_end = K_Means_Driver.W3_x_start + ((K_Means_Driver.n / 4) - 1);
			K_Means_Driver.W4_x_start = K_Means_Driver.W3_x_end + 1;
			K_Means_Driver.W4_x_end = K_Means_Driver.n - 1;
			
			K_Means_Driver.W1_y_start = 0;
			K_Means_Driver.W1_y_end = K_Means_Driver.W1_y_start + ((K_Means_Driver.n / 4) - 1);
			K_Means_Driver.W2_y_start = K_Means_Driver.W1_y_end + 1;
			K_Means_Driver.W2_y_end = K_Means_Driver.W2_y_start + ((K_Means_Driver.n / 4) - 1);
			K_Means_Driver.W3_y_start = K_Means_Driver.W2_y_end + 1;
			K_Means_Driver.W3_y_end = K_Means_Driver.W3_y_start + ((K_Means_Driver.n / 4) - 1);
			K_Means_Driver.W4_y_start = K_Means_Driver.W3_y_end + 1;
			K_Means_Driver.W4_y_end = K_Means_Driver.n - 1;
			
			K_Means_Driver.W1_iterations = 0;
			K_Means_Driver.W2_iterations = 0;
			K_Means_Driver.W3_iterations = 0;
			K_Means_Driver.W4_iterations = 0;
			
			W1 w1 = new W1();
			W2 w2 = new W2();
			W3 w3 = new W3();
			W4 w4 = new W4();
			
			w1.start();
			w2.start();
			w3.start();
			w4.start();
			
			try {
				w1.join();
				w2.join();
				w3.join();
				w4.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			System.out.println("\nEnd of program");
			
		}

	}

}
