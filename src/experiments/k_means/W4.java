/**
 * 
 */
package experiments.k_means;

import java.util.ArrayList;

/**
 * @author Suvam Mukherjee.
 *
 * Indian Institute of Science, 2017.
 *
 */
public class W4 extends Thread{
	
	public void run() {
		
		int loc_iterations;
		int min_dist = 2147483647;
		int cluster = 0;
		
		synchronized(K_Means_Driver.lock) {
			loc_iterations = K_Means_Driver.W4_iterations;
		}
		
		while (loc_iterations < K_Means_Driver.iterations) {
			System.out.println("\nThread 4 iteration: " + loc_iterations);
			// reset clusters
			K_Means_Driver.W4_C1_x = new ArrayList<Integer>();
			K_Means_Driver.W4_C1_y = new ArrayList<Integer>();
			K_Means_Driver.W4_C2_x = new ArrayList<Integer>();
			K_Means_Driver.W4_C2_y = new ArrayList<Integer>();
			K_Means_Driver.W4_C3_x = new ArrayList<Integer>();
			K_Means_Driver.W4_C3_y = new ArrayList<Integer>();
			K_Means_Driver.W4_C4_x = new ArrayList<Integer>();
			K_Means_Driver.W4_C4_y = new ArrayList<Integer>();
			
			K_Means_Driver.W4_C1_x.add(K_Means_Driver.m1_W4_x);
			K_Means_Driver.W4_C1_y.add(K_Means_Driver.m1_W4_y);
			K_Means_Driver.W4_C2_x.add(K_Means_Driver.m2_W4_x);
			K_Means_Driver.W4_C2_y.add(K_Means_Driver.m2_W4_y);
			K_Means_Driver.W4_C3_x.add(K_Means_Driver.m3_W4_x);
			K_Means_Driver.W4_C3_y.add(K_Means_Driver.m3_W4_y);
			K_Means_Driver.W4_C4_x.add(K_Means_Driver.m4_W4_x);
			K_Means_Driver.W4_C4_y.add(K_Means_Driver.m4_W4_y);
			
			// find membership with current approximation
			for(int i=K_Means_Driver.W4_x_start, j=K_Means_Driver.W4_y_start; i<=K_Means_Driver.W4_x_end && j<=K_Means_Driver.W4_y_end; i++, j++) {
				int temp = -1;
				// Get distance from m1
				
				temp = (K_Means_Driver.m1_W4_x - K_Means_Driver.points_x[i]) * (K_Means_Driver.m1_W4_x - K_Means_Driver.points_x[i]) + 
						(K_Means_Driver.m1_W4_y - K_Means_Driver.points_y[j]) * (K_Means_Driver.m1_W4_y - K_Means_Driver.points_y[j]);
				
				if(temp < min_dist) {
					min_dist = temp;
					cluster = 1;
				}

				// Get distance from m2
				temp = (K_Means_Driver.m2_W4_x - K_Means_Driver.points_x[i]) * (K_Means_Driver.m2_W4_x - K_Means_Driver.points_x[i]) + 
						(K_Means_Driver.m2_W4_y - K_Means_Driver.points_y[j]) * (K_Means_Driver.m2_W4_y - K_Means_Driver.points_y[j]);
				
				if(temp < min_dist) {
					min_dist = temp;
					cluster = 2;
				}

				// Get distance from m3
				temp = (K_Means_Driver.m3_W4_x - K_Means_Driver.points_x[i]) * (K_Means_Driver.m3_W4_x - K_Means_Driver.points_x[i]) + 
						(K_Means_Driver.m3_W4_y - K_Means_Driver.points_y[j]) * (K_Means_Driver.m3_W4_y - K_Means_Driver.points_y[j]);
				
				if(temp < min_dist) {
					min_dist = temp;
					cluster = 3;
				}

				// Get distance from m4
				
				temp = (K_Means_Driver.m4_W4_x - K_Means_Driver.points_x[i]) * (K_Means_Driver.m4_W4_x - K_Means_Driver.points_x[i]) + 
						(K_Means_Driver.m4_W4_y - K_Means_Driver.points_y[j]) * (K_Means_Driver.m4_W4_y - K_Means_Driver.points_y[j]);
				
				if(temp < min_dist) {
					min_dist = temp;
					cluster = 4;
				}

				// Put point in the appropriate cluster
				if(cluster == 1) {
					K_Means_Driver.W4_C1_x.add(K_Means_Driver.points_x[i]);
					K_Means_Driver.W4_C1_y.add(K_Means_Driver.points_y[j]);
				}
				if(cluster == 2) {
					K_Means_Driver.W4_C2_x.add(K_Means_Driver.points_x[i]);
					K_Means_Driver.W4_C2_y.add(K_Means_Driver.points_y[j]);
				}
				if(cluster == 3) {
					K_Means_Driver.W4_C3_x.add(K_Means_Driver.points_x[i]);
					K_Means_Driver.W4_C3_y.add(K_Means_Driver.points_y[j]);
				}
				if(cluster == 4) {
					K_Means_Driver.W4_C4_x.add(K_Means_Driver.points_x[i]);
					K_Means_Driver.W4_C4_y.add(K_Means_Driver.points_y[j]);
				}
			}
			
			// Update iterations
			loc_iterations++;
			synchronized(K_Means_Driver.lock) {
				K_Means_Driver.W4_iterations = loc_iterations;
			}
			
			// Update the local mean
			
			int m1_x =0, m1_y =0, m2_x =0, m2_y =0, m3_x =0, m3_y =0, m4_x =0, m4_y =0;
			
			// Cluster 1
			for(int i=0, j=0; i < K_Means_Driver.W4_C1_x.size() && j<K_Means_Driver.W4_C1_y.size(); i++,j++) {
				m1_x += K_Means_Driver.W4_C1_x.get(i);
				m1_y += K_Means_Driver.W4_C1_y.get(j);
			}
			m1_x = m1_x / K_Means_Driver.W4_C1_x.size();
			m1_y = m1_y / K_Means_Driver.W4_C1_y.size();
			
			// Cluster 2
			for(int i=0, j=0; i < K_Means_Driver.W4_C2_x.size() && j<K_Means_Driver.W4_C2_y.size(); i++,j++) {
				m2_x += K_Means_Driver.W4_C2_x.get(i);
				m2_y += K_Means_Driver.W4_C2_y.get(j);
			}
			m2_x = m1_x / K_Means_Driver.W4_C2_x.size();
			m2_y = m1_y / K_Means_Driver.W4_C2_y.size();
			
			// Cluster 3
			for(int i=0, j=0; i < K_Means_Driver.W4_C3_x.size() && j<K_Means_Driver.W4_C3_y.size(); i++,j++) {
				m3_x += K_Means_Driver.W4_C3_x.get(i);
				m3_y += K_Means_Driver.W4_C3_y.get(j);
			}
			m3_x = m1_x / K_Means_Driver.W4_C3_x.size();
			m3_y = m1_y / K_Means_Driver.W4_C3_y.size();
			
			// Cluster 4
			for(int i=0, j=0; i < K_Means_Driver.W4_C4_x.size() && j<K_Means_Driver.W4_C4_y.size(); i++,j++) {
				m4_x += K_Means_Driver.W4_C4_x.get(i);
				m4_y += K_Means_Driver.W4_C4_y.get(j);
			}
			m4_x = m1_x / K_Means_Driver.W4_C4_x.size();
			m4_y = m1_y / K_Means_Driver.W4_C4_y.size();

			synchronized(K_Means_Driver.lock) {
				K_Means_Driver.m1_W4_x = m1_x;
				K_Means_Driver.m1_W4_y = m1_y;
				K_Means_Driver.m2_W4_x = m2_x;
				K_Means_Driver.m2_W4_y = m2_y;
				K_Means_Driver.m3_W4_x = m3_x;
				K_Means_Driver.m3_W4_y = m3_y;
				K_Means_Driver.m4_W4_x = m4_x;
				K_Means_Driver.m4_W4_y = m4_y;
			}
		
			// Pause until all the other threads reach the same iteration count
			
			while(true) {
				synchronized(K_Means_Driver.lock) {
					if((K_Means_Driver.W4_iterations > K_Means_Driver.W1_iterations) && 
							(K_Means_Driver.W4_iterations > K_Means_Driver.W2_iterations) &&
							(K_Means_Driver.W4_iterations > K_Means_Driver.W3_iterations)
							) 
						continue;
					else
						break;
				}
			}
			
			synchronized(K_Means_Driver.lock) {
				K_Means_Driver.m1_W4_x = (K_Means_Driver.m1_W1_x + K_Means_Driver.m1_W2_x + K_Means_Driver.m1_W3_x + K_Means_Driver.m1_W4_x) / 4;
				K_Means_Driver.m1_W4_y = (K_Means_Driver.m1_W1_y + K_Means_Driver.m1_W2_y + K_Means_Driver.m1_W3_y + K_Means_Driver.m1_W4_y) / 4;
				K_Means_Driver.m2_W4_x = (K_Means_Driver.m2_W1_x + K_Means_Driver.m2_W2_x + K_Means_Driver.m2_W3_x + K_Means_Driver.m2_W4_x) / 4;
				K_Means_Driver.m2_W4_y = (K_Means_Driver.m2_W1_y + K_Means_Driver.m2_W2_y + K_Means_Driver.m2_W3_y + K_Means_Driver.m2_W4_y) / 4;
				K_Means_Driver.m3_W4_x = (K_Means_Driver.m3_W1_x + K_Means_Driver.m3_W2_x + K_Means_Driver.m3_W3_x + K_Means_Driver.m3_W4_x) / 4;
				K_Means_Driver.m3_W4_y = (K_Means_Driver.m3_W1_y + K_Means_Driver.m3_W2_y + K_Means_Driver.m3_W3_y + K_Means_Driver.m3_W4_y) / 4;
				K_Means_Driver.m4_W4_x = (K_Means_Driver.m4_W1_x + K_Means_Driver.m4_W2_x + K_Means_Driver.m4_W3_x + K_Means_Driver.m4_W4_x) / 4;
				K_Means_Driver.m4_W4_y = (K_Means_Driver.m4_W1_y + K_Means_Driver.m4_W2_y + K_Means_Driver.m4_W3_y + K_Means_Driver.m4_W4_y) / 4;
			
			}
		}
	}

}
