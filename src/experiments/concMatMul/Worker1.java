/**
 * 
 */
package experiments.concMatMul;

/**
 * @author suvam
 *
 */
public class Worker1 extends Thread {

	
	public void run() {
		System.out.println("\nWorker 1 processing " + ConcMatMul.W1_col_start + " to " + ConcMatMul.W1_col_end);
		for(int col = ConcMatMul.W1_col_start; col <= ConcMatMul.W1_col_end; col++) {
			
			if(!(ConcMatMul.W1_col_end <= ConcMatMul.B_cols))
				ConcMatMul.error = 1;
			if(!(col <= ConcMatMul.B_cols))
				ConcMatMul.error = 1;
			
			for(int rows = 0; rows < ConcMatMul.A_rows; rows++) {
				for(int j = 0; j < ConcMatMul.A_cols; j++) {
					try {
						ConcMatMul.C[rows][col] += ConcMatMul.A[rows][j] * ConcMatMul.B[j][col];
					}
					catch(ArrayIndexOutOfBoundsException e) {
						System.out.println("Worker 1 error");
						System.out.println("col: " + col);
						System.out.println("rows: " + rows);
						System.out.println("j: " + j);
						System.exit(0);
					}
				}
			}
		}
		System.out.println("\nThread 1 done");
	}
}
