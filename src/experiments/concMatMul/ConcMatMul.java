/**
 * Parallelized implementation of matrix multiplication.
 * 
 * Suvam Mukherjee, 2017.
 */
package experiments.concMatMul;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

/**
 * @author suvam
 *
 */
public class ConcMatMul {

	/**
	 * @param args
	 */
	public static int A_rows;
	public static int B_rows;
	public static int A_cols;
	public static int B_cols;
	public static int C_rows;
	public static int C_cols;
	public static int[][] A;
	public static int[][] B;
	public static int[][] C;
	
	public static int error;
	
	public static int W1_col_start, W1_col_end;
	public static int W2_col_start, W2_col_end;
	public static int W3_col_start, W3_col_end;
	public static int W4_col_start, W4_col_end;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		Random rand = new Random();
		
		System.out.println("\nEnter #rows in A: ");
		try {
			ConcMatMul.A_rows = Integer.parseInt(br.readLine());
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("\nEnter #columns in A: ");
		try {
			ConcMatMul.A_cols = Integer.parseInt(br.readLine());
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("\nEnter #rows in B: ");
		try {
			ConcMatMul.B_rows = Integer.parseInt(br.readLine());
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("\nEnter #columns in B: ");
		try {
			ConcMatMul.B_cols = Integer.parseInt(br.readLine());
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// Check validity conditions for matrix multiplication
		if((ConcMatMul.A_rows > 0) && (ConcMatMul.A_cols > 0) && (ConcMatMul.B_rows > 0) && (ConcMatMul.B_cols > 8) && (ConcMatMul.A_cols == ConcMatMul.B_rows)) {
			
			ConcMatMul.C_rows = ConcMatMul.A_rows;
			ConcMatMul.C_cols = ConcMatMul.B_cols;
			
			ConcMatMul.A = new int[ConcMatMul.A_rows][ConcMatMul.A_cols];
			ConcMatMul.B = new int[ConcMatMul.B_rows][ConcMatMul.B_cols];
			ConcMatMul.C = new int[ConcMatMul.C_rows][ConcMatMul.C_cols];
 			
			ConcMatMul.W1_col_start = 0;
			ConcMatMul.W1_col_end = W1_col_start + ((ConcMatMul.B_cols / 4) - 1);
			ConcMatMul.W2_col_start = 1 + ConcMatMul.W1_col_end;
			ConcMatMul.W2_col_end = W2_col_start + ((ConcMatMul.B_cols / 4) - 1);
			ConcMatMul.W3_col_start = 1 + ConcMatMul.W2_col_end;
			ConcMatMul.W3_col_end = W3_col_start + ((ConcMatMul.B_cols / 4) - 1);
			ConcMatMul.W4_col_start = 1 + ConcMatMul.W3_col_end;
			ConcMatMul.W4_col_end = ConcMatMul.B_cols - 1;
			
			if(!(ConcMatMul.W1_col_start == 0))
				ConcMatMul.error = 1;
			
			if(!(ConcMatMul.W1_col_start <= ConcMatMul.W1_col_end))
				ConcMatMul.error = 1;
			
			if(!(ConcMatMul.W1_col_end <= ConcMatMul.B_cols))
				ConcMatMul.error = 1;
			
			// Initializing A
			for(int i=0; i<ConcMatMul.A_rows; i++) {
				for(int j =0; j<ConcMatMul.A_cols; j++) {
					ConcMatMul.A[i][j] = rand.nextInt(100);
				}
			}
			
//			System.out.println("\nPrinting A: ");
//			
//			for(int i=0; i<ConcMatMul.A_rows; i++) {
//				for(int j=0; j<ConcMatMul.A_cols; j++) {
//					System.out.print(A[i][j] + "     ");
//				}
//				System.out.println("");
//			}
			
			// Initializing B
			for(int i=0; i<ConcMatMul.B_rows; i++) {
				for(int j =0; j<ConcMatMul.B_cols; j++) {
					ConcMatMul.B[i][j] = rand.nextInt(100);
				}
			}
			
//			System.out.println("\nPrinting B: ");
//			
//			for(int i=0; i<ConcMatMul.B_rows; i++) {
//				for(int j=0; j<ConcMatMul.B_cols; j++) {
//					System.out.print(B[i][j] + "     ");
//				}
//				System.out.println("");
//			}
			
			// Initialize C
			for(int i=0; i<ConcMatMul.C_rows; i++) {
				for(int j=0; j<ConcMatMul.C_cols; j++) {
					ConcMatMul.C[i][j] = 0;
				}
			}
			
//			System.out.println("\nPrinting C: ");
//			
//			for(int i=0; i<ConcMatMul.C_rows; i++) {
//				for(int j=0; j<ConcMatMul.C_cols; j++) {
//					System.out.print(ConcMatMul.C[i][j] + "     ");
//				}
//				System.out.println("");
//			}
			
			Worker1 t1 = new Worker1();
			Worker2 t2 = new Worker2();
			Worker3 t3 = new Worker3();
			Worker4 t4 = new Worker4();
			
			t1.start();
			t2.start();
			t3.start();
			t4.start();
			
			try {
				t1.join();
				t2.join();
				t3.join();
				t4.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
//			System.out.println("\nPrinting C: ");
//			
//			for(int i=0; i<ConcMatMul.C_rows; i++) {
//				for(int j=0; j<ConcMatMul.C_cols; j++) {
//					System.out.print(ConcMatMul.C[i][j] + "     ");
//				}
//				System.out.println("");
//			}
//			
		}
		try {
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("\nEnd of program");

	}

}
