/**
 * Code to check if the Chord race-detector is working fine.
 */
package regionRace;

/**
 * @author Suvam Mukherjee
 *
 */
class RD_W1 extends Thread {
	
	public void run() {
		CheckRaceDetector.x = 10;
	}
}

class RD_W2 extends Thread {
	
	public void run() {
		int val = CheckRaceDetector.x;
		System.out.println(val);
	}
}


public class CheckRaceDetector {
	public static int x;

	/**
	 * @param args
	 */
	public static void main(String[] args) throws InterruptedException{
		// TODO Auto-generated method stub
		CheckRaceDetector.x = 0;
		RD_W1 t1 = new RD_W1();
		RD_W2 t2 = new RD_W2();
		
		t1.start();
		t2.start();
		
		t1.join();
		t2.join();

	}

}
