/**
 * Class implementing the semantics preserving transformation algorithm given in the paper.
 * 
 * For each class in the given program, extract each line.
 * If the line is an assignment, conditional or loop conditional, extract the variables, check which regions are read, and 
 * insert the appropriate region-based statement after it.
 * 
 * Suvam Mukherjee, 2015.
 * 
 * Input: Program P
 * Output: P' = TRANSFORM(P)
 */



/**
 * @author Suvam Mukherjee, 2017
 *
 */
package regionRace;

import java.io.*;
import java.util.*;


public class TRANSFORM {

	/**
	 * Generic function to extract the variables from a given statement 
	 * 
	 * @param String s: input statement
	 * return variables[] : the set of variables in the statement s
	 */
	
	public static String[] extractVariables(String s)
	{
		String delimiters = "&|<>! =+-*/;\t\n\r\f";
		String sBackUp = s.substring(0, s.length());
		String[] variables; 
		int i = 0;
		
		StringTokenizer st = new StringTokenizer(sBackUp, delimiters);
		variables = new String[st.countTokens()];
		
		while(st.hasMoreTokens())
		{
			variables[i] = st.nextToken();
			i++;
		}
		
		// Get rid of any whitespaces before and after, just in case there is one
		for(i=0; i<variables.length; i++)
			variables[i] = variables[i].trim();
		
		return variables;
		
	}
	
	@SuppressWarnings("unchecked")
	public static void doTransformation(HashSet<String>[] regions, String stmt, BufferedWriter outFile) {
		// TODO Auto-generated method stub
		
		String[] variables;	// Holds the set of variables for each program statement
		StringTokenizer st;	// StringTokenizer for general use
	
		
		// Define the transformer for each kind of statement: assignment, conditional, loop, otherwise.
		
		// Case 1: Conditional or While Loop
		st = new StringTokenizer(stmt,"( \t\n\r\f");
		int conditionalFlag = 0;
		while(st.hasMoreTokens() && conditionalFlag==0)
		{
			String temp = st.nextToken();
			temp = temp.trim();
			if(temp.compareTo("if") == 0 || temp.compareTo("while")==0)
				conditionalFlag = 1;
		}
		if(conditionalFlag == 1)
		{
			// STEP 1: Compute the regions read
			
			// Extract everything beyond the first (
			st = new StringTokenizer(stmt, "(");
			int sizeOfConditional = st.countTokens();
			String s1 ="";
			
			// Drop the first token
			st.nextToken();
			// s1 is everything beyond the first (
			// s1 checked to be working correctly
			for (int i=2; i<=sizeOfConditional; i++)
				s1 += st.nextToken();
			
			// s2 is everything to the left of the last ) of s1
			st = new StringTokenizer(s1, ")");
			sizeOfConditional = st.countTokens();
			String s3 = "";
			
			for(int i=1; i<sizeOfConditional; i++)
				s3 += st.nextToken();
			
			String temp = st.nextToken();
			
			// If temp contains a {, it must be the code after the last )
			if(!(temp.contains("{")))
				s3 += temp;
			
			// s3 tested to be working correctly.
			// Check for the regions read
			
			variables = extractVariables(s3);
			//System.out.println("\nExtracted Variables: ");
			//for(int i=0; i<variables.length; i++)
			//	System.out.println(variables[i] + ", ");
			// Compute the regions read
			HashSet<Integer> conditionalRegions = new HashSet<Integer>();
			
			for(int i=0; i<variables.length; i++)
			{
				for(int j=0; j<regions.length; j++)
				{
					if(regions[j].contains(variables[i]))
					{
						conditionalRegions.add(j);
					}
				}
			}
			
			// STEP 2: Output the original conditional/loop, with the added r==r contraints
			String output = "";
			char[] condChars = stmt.toCharArray();
			int lastIndexOfBracket = 0;
			for(int i=condChars.length-1; i>=0; i--)
				if(condChars[i]==')')
				{
					lastIndexOfBracket = i-1;
					break;
				}
			
			output = stmt.substring(0, lastIndexOfBracket+1);
			
			Iterator it = conditionalRegions.iterator();
			
			while(it.hasNext())
			{
				int itNext = (Integer) it.next();
				output += " && (Region.r" + itNext + "==" + "Region.r" + itNext + ")";
			}
			for(int i=(lastIndexOfBracket+1); i<condChars.length; i++)
				output += condChars[i];
			
			try
			{
				outFile.write(output+"\n");
			}
			catch(IOException e)
			{
				System.out.println("\nTransform.java: Error while writing conditional to file. Please report bug. " + e);
			}
			/********** DEBUGGING CODE **********
			 * 
			 * Ouput Code:
			 * 
			 * System.out.println("\n******************************");
			System.out.println("Original statement: "+stmt);
			System.out.println("Added statement: " + output);
			
			// Check if the regions are computed correctly
			System.out.println("Computed Regions: \n");
			Iterator it = conditionalRegions.iterator();
			while(it.hasNext())
				System.out.println(it.next() + " ");
			********** END DEBUG **********/
				
		}
		else
		{
			// Case 2: Assignments
			if(stmt.contains("="))	
			{
				//System.out.println("Processing Assignment: " + stmt);
				variables = extractVariables(stmt);
				
				//System.out.println("\nVariables: ");
				//for(int i=0; i<variables.length; i++)
				//	System.out.println(variables[i]);
		
				String lhs = variables[0];
				HashSet<Integer> lhsRegions = new HashSet<Integer>();
				HashSet<Integer> rhsRegions = new HashSet<Integer>();
				
				// Check the regions to which the lhs belongs. We need a fresh statement for each region to which lhs belongs.
				for(int i=0; i<regions.length; i++)
				{
					Iterator it = regions[i].iterator();
					
					while(it.hasNext())
					{
						String temp = (String) it.next();
						if(temp.compareTo(lhs)==0)
								lhsRegions.add(i);
					}
				}
				
				
				// Check all the regions that are read in the rhs.
				for(int i=1; i<variables.length; i++)
				{
					for(int j=0; j<regions.length; j++)
					{
						if(regions[j].contains(variables[i]))
						{
							rhsRegions.add(j);
						}
					}
				}
				// System.out.println("\nRegion computations done.");
				
				/*********** DEBUGGING CODE **************	
				
				// Debugging: Has the regions for the lhs been correctly computed?
							System.out.println("\nLHS: " + lhs);
							System.out.println("LHS Regions: ");
							Iterator<Integer> it = lhsRegions.iterator();
							
							while(it.hasNext())
							{
								System.out.println(it.next() + ", ");
							}
							
				
				// Debugging: Has the regions for the rhs been correctly computed?
				System.out.println("\nRHS variables: ");
				
				for(int i=1; i<variables.length; i++)
				{
					System.out.println(variables[i]);
				}
				
				System.out.println("\nRHS Regions ");
				it = rhsRegions.iterator();
				
				while(it.hasNext())
				{
					System.out.println(it.next() + ", ");
				}
				
				// Output statements
				 * System.out.println("\n******************************");
				System.out.println("Original statement: "+stmt);
				System.out.println("Added statements: \n");
				
				********* END OF DEBUGGING CODE ***********/
				
				// Output Original Statement
				try
				{
					outFile.write(stmt + "\n");
				}
				catch(IOException e)
				{
					System.out.println("\nError while writing original assignment statement: "+ e );
				}
				
				// Compute the statement to output
				Iterator<Integer> lhsIterator = lhsRegions.iterator();
				
				while(lhsIterator.hasNext())
				{
					Iterator<Integer> rhsIterator = rhsRegions.iterator();
					String rhsExpression;
					
					if(rhsIterator.hasNext() == false)
					{
						rhsExpression = "0;" ;
					}
					
					else
					{
						rhsExpression = "Region.r" + rhsIterator.next();
					
						while(rhsIterator.hasNext())
						{
							rhsExpression += "+ Region.r" + rhsIterator.next();
						}
						rhsExpression +=";";
					}
					
					String output = "Region.r" + lhsIterator.next() + " = " + rhsExpression;

					try
					{
						outFile.write(output + "\n");
					}
					catch(IOException e)
					{
						System.out.println("\nTransform.java: Error while writing assignments. Please report bug." + e);
					}
				}
				
				
				
			}
			
			else
			{
				// A statment other than conditional or assignment. Output without modifications.
				try
				{
					outFile.write(stmt + "\n");
				}
				catch(IOException e)
				{
					System.out.println("\nTransform.java: Error while writing generic statements. Please report bug." + e);
				}
			}
			
		}
		
	}
	
	public static void transform(String fileList, String regionList, String workingDir)
	{
		String outDirName;	// Directory where the transformed source files would be written
		outDirName = workingDir + "genSrc";
	//	File outDir = new File(outDirName);
	//	boolean result = outDir.mkdirs();
		String[] variables;	// Holds the set of variables for each program statement
		HashSet<String>[] regions = null;
		int numOfRegions = 0;
		
		// System.out.println("Working Dir: " + workingDir);
		
//		if(result == false)
//		{
//			System.out.println("\nError in creating output directory. Please report bug.");
//			System.exit(1);
//		}
//		
		// Create and Populate the Regions
		try
		{
			BufferedReader inRegionList = new BufferedReader(new FileReader(regionList));
			
			numOfRegions = Integer.parseInt(inRegionList.readLine());	// 1st line is number of regions
			
			regions = new HashSet[numOfRegions];
			
			for(int i=0; i<regions.length; i++)
			{
				regions[i] = new HashSet<String>();
			}
			
			
			// Populate each region
			// Region descriptor: Line 1: Number of Variables Line 2...n: Names of variables
			for(int i = 0; i< regions.length; i++)
			{
				int numOfVars = Integer.parseInt(inRegionList.readLine());
				
				for(int j=0; j < numOfVars; j++)
				{
					String varName = inRegionList.readLine();
					varName = varName.trim();
					regions[i].add(varName);
				}
			}
			
			inRegionList.close();
			
			/********** DEBUGGING CODE ********
			// Regions verified to be populated correctly
			System.out.println("Checking if Regions are computed correctly...");
			for(int i=0; i<regions.length; i++)
			{
				Iterator it = regions[i].iterator();
				System.out.println("\nVariables for Region " + i);
				while(it.hasNext())
					System.out.println(it.next());
			}
			********* END DEBUGGING CODE *********/
			
		}
		catch(Exception e)
		{
			System.out.println("\nError in processing region.txt. Please check format, or report bug." + e);
		}
		
		
		// Write out the Region.java file. 
		// Region.java will define all the region variables. Needed because Chord requires bytecode for analysis.
		try
		{
			System.out.println("\nCreating Region.java");
			System.out.println("Path: " + outDirName + "/Region.java");
			BufferedWriter regionJavaOutFile = new BufferedWriter(new FileWriter(outDirName + "/Region.java"));
			String out = "// Ratcop Auto-generated";
			regionJavaOutFile.write(out+ "\n");
			out = "// Suvam Mukherjee";
			regionJavaOutFile.write(out + "\n");
			out = "public class Region {";
			regionJavaOutFile.write(out + "\n");
			
			for(int i=0; i<numOfRegions; i++)
			{
				out = "public static int r" + i + ";";
				regionJavaOutFile.write(out + "\n");
			}
			
			out = "}";
			regionJavaOutFile.write(out + "\n");
			regionJavaOutFile.close();
		}
		catch(Exception e)
		{
			System.out.println("\nError while creating the Region.java file: "+ e);
		}
		
		// Read off the source files and perform transformation
		try
		{
			// System.out.println("Opening: " + fileList);
			BufferedReader inFileList = new BufferedReader(new FileReader(fileList));	// fileList.txt
			String line = null;
			
			while((line = inFileList.readLine()) != null)
			{
				// System.out.println("\nOpening: " + workingDir + "/src/" + line);
				BufferedReader inFile = new BufferedReader(new FileReader(workingDir+line));	// each file in fileList.txt
				// System.out.println("\nOpening: " + outDirName + "/" +line);
				BufferedWriter outFile = new BufferedWriter(new FileWriter(outDirName+"/"+line));
				// System.out.println("\nHello");
				String command = null;
				
				while((command = inFile.readLine()) != null)
				{
					if(command.contains("package"))
					{
						// Ignore package names, all transformed sources go under default package
						continue;
					}
					else
					{
						// System.out.println("Command: " + command);
						doTransformation(regions, command, outFile);
						// System.out.println("\nHello2");
					}
				}
				inFile.close();
				outFile.close();
				
			}
			
			inFileList.close();
			
		}
		catch(Exception e)
		{
			System.out.println("\nAn exception occurred while accessing the source files. Please report issue." + e);
		}
	}
	/**
	 * args[0] : file containing a the list of source files which need to be transformed. This should be a list of .java files.
	 * args[1]: file containing the region definitions
	 * 			Line 1: Number of regions
	 * 			Line 2: Variables in region 1
	 * 			...
	 * 			Line n: Variables in region n
	 * args[2]: working directory (directory containing the Java files in args[0])
	 * 
	 */
	public static void main(String[] args)
	{
		System.out.println("RATCOP Relational Analysis Tool for COncurrent Programs");
		System.out.println("Program Translator to detect Region Races");
		String fileList = args[0];		// List of source files to check for region races
		String regionList = args[1];	// Description of regions
		String workingDir = args[2];

		
		transform(fileList, regionList, workingDir);
		System.out.println("Finished writing augmented file.");
	}

}
