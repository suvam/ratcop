/**
 * Driver to print out the integer variables in each reachable method in every 
 * application class.
 * 
 * Needs rt.jar, jce.jar
 * Ubuntu rt.jar location: /usr/lib/jvm/java-7-openjdk-amd64/jre/lib/rt.jar
 * Ubuntu jce.jar location: /usr/lib/jvm/java-7-openjdk-amd64/jre/lib/jce.jar
 */
package analyses.printVars;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import soot.Body;
import soot.ByteType;
import soot.IntType;
import soot.Pack;
import soot.PackManager;
import soot.PatchingChain;
import soot.Scene;
import soot.SceneTransformer;
import soot.SootClass;
import soot.SootMethod;
import soot.Transform;
import soot.Unit;
import soot.Value;
import soot.ValueBox;
import soot.jimple.AssignStmt;
import soot.jimple.IdentityStmt;
import soot.jimple.IntConstant;
import soot.jimple.NumericConstant;
import soot.jimple.toolkits.callgraph.CallGraph;
import soot.jimple.toolkits.callgraph.ReachableMethods;
import soot.util.Chain;
import stand.driver.SyncNPADriver;

/**
 * @author suvam
 *
 */
public class PrintVarsDriver extends SceneTransformer {
	
	private HashMap<Unit, SootMethod> unitToMeth = new HashMap<Unit, SootMethod>();			// map each unit to the method containing it
	private HashSet<String> varSet = new HashSet<String>();							// set of variables
	
	protected boolean isVariable(String expr) {
		
		char[] chars = expr.toCharArray();
		
		if(chars.length == 0) { 	// buggy string
			return false;
		}
		
		boolean isVar = false;
		
		for(int i=0; i<chars.length; i++) {
			if(Character.isLetter(chars[i])) {
				isVar = true;
				break;
			}
		}
		return isVar;
	}
	
	@SuppressWarnings("unchecked")
	protected void internalTransform(String arg0, Map arg1) {
		System.out.println("\nPerforming Variable Scan...");
		
		Chain<SootClass> appClasses = Scene.v().getApplicationClasses();	// Get the application classes
		// CallGraph cg = Scene.v().getCallGraph();	// Get the callgraph
		ReachableMethods reachingMethods = Scene.v().getReachableMethods();
		
		for(SootClass cl: appClasses) {
			for(SootMethod meth: cl.getMethods()) {
				
				if(reachingMethods.contains(meth)) {	// Check if meth is a reachable method
					//System.out.println("\nNow Processing: " + meth.toString());
					
					Body methBody = meth.getActiveBody();	// Get the method body
					PatchingChain<Unit> units = methBody.getUnits();	// Get the units in the method body
					
					for(Unit stmt: units) {
						unitToMeth.put(stmt, meth);		
						
						// for each statement, get the list of boxes defined
						List<ValueBox> defBoxes = stmt.getDefBoxes();	
						
						// defBox is guaranteed to contain a single Value
						for(ValueBox rhsBox: defBoxes) {
							Value rhsValue = rhsBox.getValue();
							
							// Jimple is typed, get the type of the value
							
							if(rhsValue.getType() instanceof IntType || rhsValue.getType() instanceof ByteType) {
								//System.out.println("\nInteger variable: " + rhsValue);
								
								// Check if the rhsValue is static, indicated by <...>
								// We add a check for r0.<>, which is also local
								if(rhsValue.toString().contains("<") && rhsValue.toString().contains(">") && !rhsValue.toString().contains(".<")) {
									varSet.add(rhsValue.toString());
								}
								else {		// local variable, append method name
									String varName = meth.toString() + "##" + rhsValue.toString();
									varSet.add(varName);
								}
							}
						}
						
						// useBox can further contain valueboxes
						// for example: b0 + b0
						// we need to recurse another step
						List<ValueBox> useBoxes = stmt.getUseBoxes();
						
						// for each value-box, get the list of values
						for(ValueBox ubox: useBoxes) {
							Value lhsValue = ubox.getValue();
							
							List<ValueBox> lhsUseBoxes = lhsValue.getUseBoxes();
							
							for(ValueBox lhsInternalVBox: lhsUseBoxes) {
								Value lhsInternalValue = lhsInternalVBox.getValue();
								//System.out.println("\nTYPE OF " + lhsInternalValue + ": " + lhsInternalValue.getType());
								if(lhsInternalValue instanceof IntConstant) {
									System.out.println("Integer Constant: " + lhsInternalValue);
								}
								if(lhsInternalValue.getType() instanceof IntType || lhsInternalValue.getType() instanceof ByteType) {
									if(isVariable(lhsInternalValue.toString())) {
										// System.out.println("\nInteger variable: " + lhsInternalValue);
										
										// Check if the lhsInternalValue is static, indicated by <...>
										if(lhsInternalValue.toString().contains("<") && lhsInternalValue.toString().contains(">") && !lhsInternalValue.toString().contains(".<")) {
											varSet.add(lhsInternalValue.toString());
										}
										else {		// local variable, append method name
											String varName = meth.toString() + "##" + lhsInternalValue.toString();
											varSet.add(varName);
										}
									}
								}
							}
							
							
						}
					}
					
				}
			}
		}
		
		// Print out the list of variables
		System.out.println("\nScanned set of variables: ");
		for(String s: varSet) {
			System.out.println(s);
		}
		
		// Process Assignment statements
		for(Unit u: unitToMeth.keySet()) {
			
			if(u instanceof IdentityStmt && checkType(((IdentityStmt) u).getLeftOp())) {
				System.out.println("\nIDENTITY STMT: " + u);
			}
			
			if(u instanceof AssignStmt) {
				System.out.println("\nProcessing Assignment statement: " + u);
				List<ValueBox> lhsBoxes = u.getDefBoxes();
				List<ValueBox> rhsBoxes = u.getUseBoxes();
				
				System.out.println("Def boxes: ");
				for(ValueBox lhsVBox: lhsBoxes) {
					Value v = lhsVBox.getValue();
					System.out.println(v);
					
					if(checkType(v)) {
						System.out.println("Valid statement to process");
					}
				}
				
				System.out.println("Use Boxes: ");
				for(ValueBox rhsVBox: rhsBoxes) {
					Value v = rhsVBox.getValue();
					System.out.println(v);
				}
				
				String expr = u.toString();
				if(expr.contains("+")) {
					StringTokenizer st = new StringTokenizer(expr);
					String command = null;
					while(st.hasMoreTokens()) {
						command = st.nextToken("=");
					}
					System.out.println("Addition performed");
					System.out.println("RHS Expression: " + command);
				}
				else if(expr.contains("=")) {
					System.out.println("Equality performed");
				}
			}
		}
	}
	
	/**
	 * Return true if leftOp is of type ByteType, IntType
	 * @param leftOp : Value whose type needs to be checked
	 * @return
	 */
	private boolean checkType(Value leftOp) {
		// TODO Auto-generated method stub
		if((leftOp.getType() instanceof ByteType) || (leftOp.getType() instanceof IntType)) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @param args[0] all external classes (included rt.jar, jce.jar)
	 * @param args[1] fully qualified name of main class
	 */
	public static void main(String[] args) {
		
		String[] soot_args = new String[]{
				//"-v",		// Increase verbosity
				"-w",
				"-p",
				"cg.spark",	// Explicitly run the SPARK phase 
				"enabled:true",
				"-cp",
				args[0], 
				//"-allow-phantom-refs",
				"-app",
				args[1],
				//"-i"
				//"java.",	 
				"-f",
				"jimple",
				//"-x",
				//"jdk",
				//"-no-bodies-for-excluded"
				
		};
		Pack wjtp = PackManager.v().getPack("wjtp");     
        wjtp.add(new Transform("wjtp.printVars", new PrintVarsDriver()));
        
        
        System.out.println("Starting analysis");
        soot.Main.main(soot_args);
	}

}
