/**
 * Driver for the basic sequential analysis.
 */
package analyses.driver;

import java.util.Map;

import analyses.printVars.PrintVarsDriver;
import analyses.sequential.SeqRelAnalysis;
import soot.Pack;
import soot.PackManager;
import soot.Scene;
import soot.SceneTransformer;
import soot.Transform;
import stand.analysis.util.AliasAnalysis;
import stand.graph.SyncCG;
import stand.util.NonSparkPTA;

/**
 * @author suvam
 *
 */
public class SequentialDriver extends SceneTransformer {
	
	protected void internalTransform(String arg0, Map arg1) {
		
		// Build the sync-CFG
		SyncCG scfg = null;
		SeqRelAnalysis seqAn = null;
		
		try {
			scfg = new SyncCG(Scene.v());
			AliasAnalysis aa = new AliasAnalysis(Scene.v().getPointsToAnalysis());
			seqAn = new SeqRelAnalysis(scfg, aa);
		}
		catch (NonSparkPTA e) {			
			e.printStackTrace();
			System.exit(-1);
		}
		
	}

	/**
	 * 
	 * @param args[0] all external classes (included rt.jar, jce.jar)
	 * @param args[1] fully qualified name of main class
	 */
	public static void main(String[] args) {
		
		String[] soot_args = new String[]{
				//"-v",		// Increase verbosity
				"-w",
				"-p",
				"cg.spark",	// Explicitly run the SPARK phase 
				"enabled:true",
				"-cp",
				args[0], 
				//"-allow-phantom-refs",
				"-app",
				args[1],
				//"-i"
				//"java.",	 
				"-f",
				"jimple",
				//"-x",
				//"jdk",
				//"-no-bodies-for-excluded"
				
		};
		Pack wjtp = PackManager.v().getPack("wjtp");     
        wjtp.add(new Transform("wjtp.seqDriver", new SequentialDriver()));
        
        
        System.out.println("Starting analysis");
        soot.Main.main(soot_args);
	}


}
