/**
 * Domain for relational data-flow facts tagged with thread-ids.
 */
package analyses.tid_rel;

import java.util.HashSet;
import java.util.Set;
import apron.Abstract1;
import apron.ApronException;
import apron.Lincons1;
import apron.Manager;

/**
 * @author Suvam Mukherjee.
 *
 * Indian Institute of Science, 2017.
 *
 */
public class TData {
	
	public Abstract1 rel;
	public Set<String> tid;
	
	public TData(Manager man, Lincons1[] cons, HashSet<String> tid) {
		try {
			rel = new Abstract1(man, cons);
		} catch (ApronException e) {
			System.out.println("\nError while creating new TData");
			System.exit(1);
		}
		this.tid = new HashSet<String>(tid);
	}
	
	public TData(Manager man, Lincons1[] cons) {
		try {
			rel = new Abstract1(man, cons);
		} catch (ApronException e) {
			System.out.println("\nError while creating new TData");
			System.exit(1);
		}
		tid = new HashSet<String>();
	}
	
	public TData(Manager man, TData data) {
		try {
			rel = new Abstract1(man, data.getRel());
		} catch (ApronException e) {
			System.err.println("\nError while creating new TData");
		}
		tid = new HashSet<String>(data.getTid());
	}
	
	public Abstract1 getRel() {
		return this.rel;
	}
	
	public void setRel(Manager man, Abstract1 rel) {
		try {
			this.rel = new Abstract1(man, rel);
		} catch (ApronException e) {
			System.err.println("\nAn Apron Exception occurred in setRel()");
		}
	}
	
	public Set<String> getTid() {
		return this.tid;
	}
	
	public void strongUpdateTid(String s) {
		tid = new HashSet<String>();
		tid.add(s);
	}
	
	public HashSet<String> getNewTid() {
		return new HashSet<String>(this.tid);
	}

}
