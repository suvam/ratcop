/**
 * 
 */
package analyses.tid_rel;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import analyses.sequential.SeqRelAnalysis;
import analyses.sequential.SeqRelDriver;
import apron.Abstract1;
import apron.ApronException;
import apron.Lincons1;
import apron.Linexpr1;
import apron.Linterm1;
import apron.MpqScalar;
import soot.Body;
import soot.IntType;
import soot.Pack;
import soot.PackManager;
import soot.Scene;
import soot.SceneTransformer;
import soot.SootMethod;
import soot.Transform;
import soot.Unit;
import soot.Value;
import soot.jimple.BinopExpr;
import soot.jimple.IfStmt;
import soot.jimple.Stmt;
import stand.analysis.npa.SyncNPA;
import stand.analysis.util.AliasAnalysis;
import stand.driver.SyncNPADriver;
import stand.graph.SyncCG;
import stand.graph.SyncEdge;
import stand.util.NonSparkPTA;

/**
 * @author Suvam Mukherjee.
 *
 * Indian Institute of Science, 2017.
 *
 */
public class TidRelAnalysisDriver extends SceneTransformer {

	protected boolean useWidening = false;
	protected int wideningThreshold = 0;
	protected String output = "";
	static BufferedWriter logger = null;
	protected static int domain;

	@Override
	protected void internalTransform(String arg0, Map arg1) {
		// TODO Auto-generated method stub
		System.out.println("\nPerforming Sequential Relational Analysis");
		SyncCG scg = null;
		TidRelAnalysis tidRel = null;
	
		try {
			
			scg = new SyncCG(Scene.v());
			AliasAnalysis aa = new AliasAnalysis(Scene.v().getPointsToAnalysis());
			tidRel = new TidRelAnalysis(scg, aa, this.useWidening, this.wideningThreshold, this.domain);
			
		} catch (NonSparkPTA e) {			
			e.printStackTrace();
			System.exit(-1);
		}
		
		/****** Auto-check Assertions *******/
		long start = System.currentTimeMillis();
		int numAssertions = 0;
		int passedAssertions = 0;
		
		for(SootMethod currsm : scg.getMethToCFG().keySet()) {		
			for(Unit currunit : scg.getMethToCFG().get(currsm).getBody().getUnits()) {
				List<SyncEdge> succs = scg.edgesOutOf(currunit);
				if(succs != null)
					for(SyncEdge succ : succs) {
						
						// Check if condition belongs to an assertion check
						if(succ.getTarget().toString().contains("int error> = 1") && currunit.toString().contains("if ") && currunit.toString().contains("goto")) {
							numAssertions++;
//							System.out.println("\nAssertion found: " + currunit);
//							System.out.println("\nNumber of assertions: " + numAssertions);
//							String command = currunit.toString();
//							StringTokenizer st = new StringTokenizer(command);
//							
//							String ifString = st.nextToken();
//							String op1 = st.nextToken();
//							String relOperator = st.nextToken();
//							String op2 = st.nextToken();
							
//							System.out.println("\nOp1: " + op1 + " operator: " + relOperator + " Op2: " + op2);
							
							// set up the constraint
							// Extract expression
							IfStmt u = (IfStmt) currunit;
							Value cond = u.getCondition();
							Value op1 = ((BinopExpr) cond).getOp1();
							Value op2 = ((BinopExpr) cond).getOp2();
							
							if(!(op1.getType() instanceof IntType || op2.getType() instanceof IntType))
								continue;
							
							String op1var = null, op2var = null, cst = null;
							boolean containsCst = false;	// does the condition contain a boolean constant?
							
							if(tidRel.isNumericConstant(op1.toString().trim())) {	// op1 is a number
								cst = tidRel.rename(op1.toString(), (Unit) u);
								op1var = tidRel.rename(op2.toString(), (Unit) u);
								containsCst= true;
							}
							
							else if(tidRel.isNumericConstant(op2.toString().trim())) {	// op2 is a number
								cst = tidRel.rename(op2.toString(), (Unit) u);
								op1var = tidRel.rename(op1.toString(), (Unit) u);
								containsCst= true;
							}
							
							else {
								op1var = tidRel.rename(op1.toString(), (Unit) u);
								op2var = tidRel.rename(op2.toString(), (Unit) u);
							}
							
							//System.out.println("\nBranching opVar1: " + op1var);
							//System.out.println("\nBranching opVar2: " + op2var);
							
							String relOperator = tidRel.extractRelOperator(u.toString());
							
							//System.out.println("\nOp 1:" + op1);
							//System.out.println("Op 2: " + op2);
							//System.out.println("\nConditional Type is: " + (op1.getType() instanceof IntType || op2.getType() instanceof IntType));
							
							
									
							// Create the conditional constraint
							Lincons1 condConstraint = null;
							
							if(relOperator.contains("==")) {
								if(!containsCst) {
									Linterm1[] condTerms = 
										{
											new Linterm1(op1var.trim(), new MpqScalar(1)),
											new Linterm1(op2var.trim(), new MpqScalar(-1))
										};
		
									Linexpr1 condExpr = new Linexpr1(tidRel.getEnv(), condTerms, new MpqScalar(0));
									condConstraint = new Lincons1(Lincons1.EQ, condExpr);
								}
								else {
									Linterm1[] condTerms = 
										{
											new Linterm1(op1var.trim(), new MpqScalar(1))
										};
									Linterm1[] NotcondTerms = 
										{
											new Linterm1(op1var.trim(), new MpqScalar(1))
										};
									int coeff = -1*Integer.parseInt(cst.trim());
									Linexpr1 condExpr = new Linexpr1(tidRel.getEnv(), condTerms, new MpqScalar(coeff));
									condConstraint = new Lincons1(Lincons1.EQ, condExpr);
								}
							}
							else if(relOperator.contains("!=")) {
								if(!containsCst) {
									Linterm1[] condTerms = 
										{
											new Linterm1(op1var.trim(), new MpqScalar(1)),
											new Linterm1(op2var.trim(), new MpqScalar(-1))
										};
									Linterm1[] NotcondTerms = 
										{
											new Linterm1(op1var.trim(), new MpqScalar(1)),
											new Linterm1(op2var.trim(), new MpqScalar(-1))
										};
									Linexpr1 condExpr = new Linexpr1(tidRel.getEnv(), condTerms, new MpqScalar(0));
									condConstraint = new Lincons1(Lincons1.DISEQ, condExpr);
								}
								else {
									Linterm1[] condTerms = 
										{
											new Linterm1(op1var.trim(), new MpqScalar(1))
										};
									Linterm1[] NotcondTerms = 
										{
											new Linterm1(op1var.trim(), new MpqScalar(1))
										};
									int coeff = -1*Integer.parseInt(cst.trim());
									Linexpr1 condExpr = new Linexpr1(tidRel.getEnv(), condTerms, new MpqScalar(coeff));
									condConstraint = new Lincons1(Lincons1.DISEQ, condExpr);
								}
							}
							else if(relOperator.contains("<=")) {	// op1 <= op2 translates to op2 - op1 >= 0
								if(!containsCst) {
									Linterm1[] condTerms = 
										{
											new Linterm1(op1var.trim(), new MpqScalar(-1)),
											new Linterm1(op2var.trim(), new MpqScalar(1))
										};
									Linterm1[] NotcondTerms = 
										{
											new Linterm1(op1var.trim(), new MpqScalar(1)),
											new Linterm1(op2var.trim(), new MpqScalar(-1))
										};
									Linexpr1 condExpr = new Linexpr1(tidRel.getEnv(), condTerms, new MpqScalar(0));
									condConstraint = new Lincons1(Lincons1.SUPEQ, condExpr);
								}
								else {
									Linterm1[] condTerms = 
										{
											new Linterm1(op1var.trim(), new MpqScalar(-1))
										};
									Linterm1[] NotcondTerms = 
										{
											new Linterm1(op1var.trim(), new MpqScalar(1))
										};
									Linexpr1 condExpr = new Linexpr1(tidRel.getEnv(), condTerms, new MpqScalar(Integer.parseInt(cst.trim())));
									condConstraint = new Lincons1(Lincons1.SUPEQ, condExpr);
								}
							}
							else if(relOperator.contains("<")) {	// op1 < op2 translates to op2 - op1 > 0
								if(!containsCst) {
									Linterm1[] condTerms = 
										{
											new Linterm1(op1var.trim(), new MpqScalar(-1)),
											new Linterm1(op2var.trim(), new MpqScalar(1))
										};
									Linterm1[] NotcondTerms = 
										{
											new Linterm1(op1var.trim(), new MpqScalar(1)),
											new Linterm1(op2var.trim(), new MpqScalar(-1))
										};
									Linexpr1 condExpr = new Linexpr1(tidRel.getEnv(), condTerms, new MpqScalar(0));
									condConstraint = new Lincons1(Lincons1.SUP, condExpr);
								}
								else {
									Linterm1[] condTerms = 
										{
											new Linterm1(op1var.trim(), new MpqScalar(-1))
										};
									Linterm1[] NotcondTerms = 
										{
											new Linterm1(op1var.trim(), new MpqScalar(1))
										};
									Linexpr1 condExpr = new Linexpr1(tidRel.getEnv(), condTerms, new MpqScalar(Integer.parseInt(cst.trim())));
									condConstraint = new Lincons1(Lincons1.SUP, condExpr);
								}
							}
							else if(relOperator.contains(">=")) {	// op1 >= op2 translates to op1 - op2 >= 0
								if(!containsCst) {
									Linterm1[] condTerms = 
										{
											new Linterm1(op1var.trim(), new MpqScalar(1)),
											new Linterm1(op2var.trim(), new MpqScalar(-1))
										};
									Linterm1[] NotcondTerms = 
										{
											new Linterm1(op1var.trim(), new MpqScalar(-1)),
											new Linterm1(op2var.trim(), new MpqScalar(1))
										};
									Linexpr1 condExpr = new Linexpr1(tidRel.getEnv(), condTerms, new MpqScalar(0));
									condConstraint = new Lincons1(Lincons1.SUPEQ, condExpr);
								}
								else {
									Linterm1[] condTerms = 
										{
											new Linterm1(op1var.trim(), new MpqScalar(1))
										};
									Linterm1[] NotcondTerms = 
										{
											new Linterm1(op1var.trim(), new MpqScalar(-1))
										};
									Linexpr1 condExpr = new Linexpr1(tidRel.getEnv(), condTerms, new MpqScalar(-1 * Integer.parseInt(cst.trim())));
									condConstraint = new Lincons1(Lincons1.SUPEQ, condExpr);
								}
							}
							else if(relOperator.contains(">")) {	// op1 > op2 translates to op1 - op2 > 0
								if(!containsCst) {
									Linterm1[] condTerms = 
										{
											new Linterm1(op1var.trim(), new MpqScalar(1)),
											new Linterm1(op2var.trim(), new MpqScalar(-1))
										};
									Linterm1[] NotcondTerms = 
										{
											new Linterm1(op1var.trim(), new MpqScalar(-1)),
											new Linterm1(op2var.trim(), new MpqScalar(1))
										};
									Linexpr1 condExpr = new Linexpr1(tidRel.getEnv(), condTerms, new MpqScalar(0));
									condConstraint = new Lincons1(Lincons1.SUP, condExpr);
								}
								else {
									Linterm1[] condTerms = 
										{
											new Linterm1(op1var.trim(), new MpqScalar(1))
										};
									Linterm1[] NotcondTerms = 
										{
											new Linterm1(op1var.trim(), new MpqScalar(-1))
										};
									Linexpr1 condExpr = new Linexpr1(tidRel.getEnv(), condTerms, new MpqScalar(-1 * Integer.parseInt(cst.trim())));
									condConstraint = new Lincons1(Lincons1.SUP, condExpr);
								}
							}
							
							TData res = tidRel.getFixedPointSol(currunit);
							
							try {
								if(res.getRel().satisfy(tidRel.getManager(), condConstraint)) {
									passedAssertions++;
								}
								else {	// log failed assertion
									String method = scg.getMethodFromUnit(currunit).getSignature().toString();
									String assertion = condConstraint.toString();
									String result = res.getRel().toString();
									String output = "Failed " + assertion + " in method " + method;
									output += "\nFact: " + result + "\n\n";
									try {
										logger.write(output);
									} catch (IOException e) {
										System.err.println("\nAn I/O Error occurred while writing to log file");
									}
								}
							} catch (ApronException e) {
								System.err.println("\nAn Apron Exception occurred while checking for constraint satisfaction");
							}
						}
						
				}
			}
		}
		
		// Compute number of iterations
		HashMap<Unit, Integer> iterations = tidRel.getUnit2CountMap();
		int max_iter = 0;
		for(Unit u: iterations.keySet()) {
			if(iterations.get(u) > max_iter)
				max_iter = iterations.get(u);
		}
		
		/****************** STATISTICS *******************/
		System.out.println("\n**************************************************");
		System.out.println("Number of assertions: " + numAssertions);
		System.out.println("Assertions passed: " + passedAssertions);
		long end = System.currentTimeMillis();
		long time = (end - start) + (tidRel.end - tidRel.start);
		System.out.println("Time taken by analysis: " + time + "ms");
		//System.out.println("Total iterations: " + max_iter);
		System.out.println("Inter-thread propagations: " + (tidRel.interThreadProps ));	// does not include fork/join propagation
		System.out.println("Intra-thread propagations: " + (tidRel.intraThreadProps));
		System.out.println("\n**************************************************\n");
	}
	
	public static void main(String[] args) {
		
		String[] soot_args = new String[]{
				//"-v",		// Increase verbosity
				"-w",
				"-p",
				"cg.spark",	// Explicitly run the SPARK phase 
				"enabled:true",
				"-cp",
				args[0], 
				//"-allow-phantom-refs",
				"-app",
				args[1],
				//"-i"
				//"java.",	 
				"-f",
				"jimple",
				//"-x",
				//"jdk",
				//"-no-bodies-for-excluded"
				
		};
		
        
        System.out.println("\nOutSTAND Static Analyzer for Race-Free Concurrent Programs");
        System.out.println(("Tagged-Domain based Analysis"));
        
        domain = Integer.parseInt(args[3]);
        
        TidRelAnalysisDriver sqreldriver = new TidRelAnalysisDriver();
        
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        
        String input = "";
        
        try{
        	input = args[2].trim();	
        	logger = new BufferedWriter(new FileWriter(new File(input.trim())));
        	
        	System.out.println("Use widening? [y/n]: ");
        	input = br.readLine();
        	
        	if(input.contains("y")) {
        		sqreldriver.useWidening();
        		System.out.println("\nEnter widening threshold: ");
        		sqreldriver.setWideningThreshold(Integer.parseInt(br.readLine().trim()));
        	}
        }
        catch(IOException e) {
        	System.err.println("\nAn I/O Exception occurred while initializing widening");
        }
        
        Pack wjtp = PackManager.v().getPack("wjtp");     
        wjtp.add(new Transform("wjtp.seqRel", sqreldriver));
        soot.Main.main(soot_args);
        try {
			br.close();
			logger.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * If widening is allowed, set the number of iterations allowed prior to widening
	 * @param wideningThreshold
	 */
	private void setWideningThreshold(int wideningThreshold) {
		this.wideningThreshold = wideningThreshold;
	}

	/**
	 * Enables the use of widening
	 */
	private void useWidening() {
		this.useWidening = true;
	}
	
	
}
