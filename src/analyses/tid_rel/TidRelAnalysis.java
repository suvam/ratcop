/**
 * 
 */
package analyses.tid_rel;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import apron.Abstract1;
import apron.ApronException;
import apron.Box;
import apron.Environment;
import apron.Lincons1;
import apron.Linexpr1;
import apron.Linterm1;
import apron.Manager;
import apron.MpqScalar;
import apron.Octagon;
import apron.Polka;
import apron.Texpr1BinNode;
import apron.Texpr1CstNode;
import apron.Texpr1Intern;
import apron.Texpr1VarNode;
import apron.Var;
import soot.Body;
import soot.ByteType;
import soot.IntType;
import soot.PatchingChain;
import soot.Scene;
import soot.SootClass;
import soot.SootMethod;
import soot.Unit;
import soot.Value;
import soot.ValueBox;
import soot.jimple.AssignStmt;
import soot.jimple.BinopExpr;
import soot.jimple.IdentityStmt;
import soot.jimple.IfStmt;
import soot.jimple.toolkits.callgraph.CallGraph;
import soot.jimple.toolkits.callgraph.ReachableMethods;
import soot.util.Chain;
import stand.analysis.SyncAnalysis;
import stand.analysis.util.AliasAnalysis;
import stand.analysis.util.CallString;
import stand.analysis.util.LvalueSet;
import stand.analysis.util.TaggedAbsVal;
import stand.graph.EdgeKind;
import stand.graph.SyncCG;
import stand.graph.SyncEdge;

/**
 * @author Suvam Mukherjee.
 *
 * Indian Institute of Science, 2017.
 *
 */
public class TidRelAnalysis extends SyncAnalysis<TData> {

	private Manager man;	// used to specify the abstract domain
	// private Map<SootMethod, Environment> methToEnv; // map each method to its environment
	private SyncCG scg;		// sync-CFG representation of the program
	private AliasAnalysis aa;	// Flow-insensitive alias-analysis
	private Chain<SootClass> appClasses;	// application classes
	private CallGraph cg;					// call graph
	private Environment env;	// single environment for whole program
	private Environment globalenv;	// environment containing global variables
	private Environment localenv;	// environment containing instance/local variables
	private ReachableMethods rm;	// set of reachable methods
	private HashSet<String> globalVars; 	// set of global variables (static variables)
	private HashSet<String> localVars;		// set of local/instance variables
	
	private HashMap<Unit, SootMethod> unitToMeth;	// map each unit to the method containing it
	
	private HashSet<String> tid;	// set of thread classes
	
	// Region based analysis data structures
	private HashMap<String, ArrayList<String>> regions;
	
	
	
	private boolean useRegions;		// use region based analysis?
	
	public TidRelAnalysis(SyncCG scg, AliasAnalysis aa, boolean useWidening, int wideningThreshold, int domain) {
		super(scg, useWidening);
		this.scg = scg;
		this.aa = aa;
		this.useWidening = useWidening;
		this.wideningThreshold = wideningThreshold;
		
		appClasses = Scene.v().getApplicationClasses();
		cg = Scene.v().getCallGraph();
		rm = Scene.v().getReachableMethods();
		
		// Set the abstract domain (Polka, Octagon, etc)
		if(domain == 1)			// Octagon
			man = new Octagon();
		else if(domain == 2)	// Convex Polyhedra
			man = new Polka(true);
		else if(domain == 3)	// Interval
			man = new Box();
		
		unitToMeth = new HashMap<Unit, SootMethod>();
		globalVars = new HashSet<String>();
		localVars = new HashSet<String>();
		
		this.tid = new HashSet<String>();
		
		computeEnvironments();
		specifyRegionAnalysis();
		doAnalysis();
	}
	
	/**
	 * Check if the user wants a region based analysis.
	 * If yes, setup the regions.
	 */
	private void specifyRegionAnalysis() {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String response ="";
		
		System.out.println("\nUse Regions? [y/n]: ");
		try {
			response = br.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.err.println("\nAn I/O Exception occurred while reading in specifyRegionAnalysis");
			System.exit(1);
		}
		
		if(response.contains("n"))		// region analysis disabled
			return;
		
		// Setup region analysis
		regions = new HashMap<String, ArrayList<String>>();
		useRegions = true;
		int regionCount = 0;		// region count
		
		ArrayList<String> globalVarsCopy = new ArrayList<String>();
		int i = 0;
		for(String v: globalVars) {
			globalVarsCopy.add(i, v);
			i++;
		}
		
		System.out.println("\nSpecify Regions: ");
		while(globalVarsCopy.size() != 0) {
			System.out.println("\n********Setting up Region " + regionCount + "**********");
			String regionID = "r" + regionCount++;
			ArrayList<String> varsForR = new ArrayList<String>();
			
			// Set up a temporary 
			ArrayList<String> temp = new ArrayList<String>(globalVarsCopy);
			
			System.out.println("Available variables: \n");
			
			for(int j=0; j<globalVarsCopy.size(); j++) {
				System.out.println(j + ": " + globalVarsCopy.get(j));
			}
			
			response = "";
			
			while(! response.contains("n")) {
				System.out.println("\nEnter variable index: ");
				int index = 0;
				try {
					index = Integer.parseInt(br.readLine());
				} catch (NumberFormatException e) {
					System.err.println("\nInvalid number entered");
					System.exit(1);
				} catch (IOException e) {
					System.err.println("\nAn I/O Exception occurred while setting up regions");
					System.exit(1);
				}
				varsForR.add(globalVarsCopy.get(index));
				temp.remove(globalVarsCopy.get(index));
				System.out.println("Add another variable? [y/n]: ");
				try {
					response = br.readLine();
				} catch (IOException e) {
					System.err.println("\nAn I/O Exception occurred while setting up regions");
					System.exit(1);
				}
			}
			globalVarsCopy = new ArrayList<String>(temp);
			regions.put(regionID, varsForR);
		}
		
		// Debug: print out region
		for(String v: regions.keySet()) {
			System.out.println("\nRegion " + v);
			System.out.println(regions.get(v));
		}
		System.out.println("\nPress any key and enter to continue...");
		try {
			br.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
		
		// close br
		try {
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	/**
	 * Identify if the input string is a variable (contains at least one letter).
	 * @param expr: expression 
	 * @return true if the expr contains at least one letter
	 */
	protected boolean isVariable(String expr) {
		
		char[] chars = expr.toCharArray();
		
		if(chars.length == 0) { 	// buggy string
			return false;
		}
		
		boolean isVar = false;
		
		for(int i=0; i<chars.length; i++) {
			if(Character.isLetter(chars[i])) {
				isVar = true;
				break;
			}
		}
		return isVar;
	}
	
	/**
	 * This function scans through each reachable method, and discovers the byte/integer variables in them.
	 * It then creates an Apron environment using these, and maps the method to this environment.
	 * As a by-product, a unit is mapped to the method containing it.
	 */
	@SuppressWarnings("unchecked")
	protected void computeEnvironments() {
		HashSet<String> varSet = new HashSet<String>();	// list of integer/byte variables
		String[] intByteVars = null;
		String[] realVars = {"dummy"};
		
		for(SootClass cl: appClasses) {
			tid.add(cl.toString());		// Add the class to tid
			
			for(SootMethod meth: cl.getMethods()) {
				
				if(rm.contains(meth)) {	// Check if meth is a reachable method
					//System.out.println("\nNow Processing: " + meth.toString());
					
					Body methBody = meth.getActiveBody();	// Get the method body
					PatchingChain<Unit> units = methBody.getUnits();	// Get the units in the method body
					
					for(Unit stmt: units) {
						unitToMeth.put(stmt, meth);		
						
						// for each statement, get the list of boxes defined
						List<ValueBox> defBoxes = stmt.getDefBoxes();	
						
						// defBox is guaranteed to contain a single Value
						for(ValueBox rhsBox: defBoxes) {
							Value rhsValue = rhsBox.getValue();
							
							// Jimple is typed, get the type of the value
							
							if(rhsValue.getType() instanceof IntType || rhsValue.getType() instanceof ByteType) {
								//System.out.println("\nInteger variable: " + rhsValue);
								
								// Check if the rhsValue is static, indicated by <...>
								// We add a check for r0.<>, which is also local
								if(rhsValue.toString().contains("<") && rhsValue.toString().contains(">") && !rhsValue.toString().contains(".<")) {
									varSet.add(rhsValue.toString());
									globalVars.add(rhsValue.toString());	// add to the list of global variables
								}
								else {		// local variable, append method name
									String varName = meth.toString() + "##" + rhsValue.toString();
									varSet.add(varName);
									localVars.add(varName);
								}
							}
						}
						
						// useBox can further contain valueboxes
						// for example: b0 + b0
						// we need to recurse another step
						List<ValueBox> useBoxes = stmt.getUseBoxes();
						
						// for each value-box, get the list of values
						for(ValueBox ubox: useBoxes) {
							Value lhsValue = ubox.getValue();
							
							List<ValueBox> lhsUseBoxes = lhsValue.getUseBoxes();
							
							for(ValueBox lhsInternalVBox: lhsUseBoxes) {
								Value lhsInternalValue = lhsInternalVBox.getValue();
								
								if(lhsInternalValue.getType() instanceof IntType || lhsInternalValue.getType() instanceof ByteType) {
									if(isVariable(lhsInternalValue.toString())) {
										// System.out.println("\nInteger variable: " + lhsInternalValue);
										
										// Check if the lhsInternalValue is static, indicated by <...>
										if(lhsInternalValue.toString().contains("<") && lhsInternalValue.toString().contains(">") && !lhsInternalValue.toString().contains(".<")) {
											varSet.add(lhsInternalValue.toString());
											globalVars.add(lhsInternalValue.toString());
										}
										else {		// local variable, append method name
											String varName = meth.toString() + "##" + lhsInternalValue.toString();
											varSet.add(varName);
											localVars.add(varName);
										}
									}
								}
							}
							
							
						}
					}
					
				}
			}
		}
		
		// Create the environment
		intByteVars = new String[varSet.size()];
		int i = 0;
		for(String s: varSet) {
			intByteVars[i] = new String(s);		// Create a copy of the string and store
			intByteVars[i] = intByteVars[i].trim();
			//System.out.println("\nExtracted variable: " + intByteVars[i]);
			i++;
		}
		
		env = new Environment(intByteVars, realVars);
		env.remove(realVars);	// weird idiosyncracy of Apron
		
		String[] globalVarsList = new String[globalVars.size()];
		String[] localVarsList = new String[localVars.size()];
		
		i = 0;
		for(String s: globalVars) {
			globalVarsList[i] = new String(s);
			globalVarsList[i] = globalVarsList[i].trim();
			i++;
		}
		
		i = 0;
		for(String s: localVars) {
			localVarsList[i] = new String(s);
			localVarsList[i] = localVarsList[i].trim();
			i++;
		}
		
		globalenv = new Environment(globalVarsList, realVars);
		localenv = new Environment(localVarsList, realVars);
		
		globalenv.remove(realVars);
		localenv.remove(realVars);
	}
	
	@Override
	/**
	 * All the variables in the initial state are unconstrained.
	 * The tid set is set to all possible thread classes.
	 */
	public TData entryInitialFlow() {
//		System.out.println("\nCreating initial element");
		TData initData = null;
		Lincons1[] validCons = new Lincons1[1];
		validCons[0] = new Lincons1(env, true);
		initData = new TData(man, validCons);
		return initData;
	}
	
	@Override
	/**
	 * Implement the ordering relation. Return true if the oldval is a safe approximation of currval.
	 */
	public boolean isSafeApprox(TData oldval, TData currval) {
	
		// oldval >= currval <=> JOIN(oldval.rel, currval.rel) = oldval.rel && oldval.tidSet >= currval.tidSet
		
		Abstract1 oldCurrJoin = null;
		
		try {

//			if(oldval.isTop(man)) {
//				return true;	// oldval dominates any currval
//			}
//			
//			if(oldval.isBottom(man)) {
//				return false;	// oldval is dominated by any currval
//			}
			oldCurrJoin = oldval.getRel().joinCopy(man, currval.getRel());
			
			if((oldval.getRel().isEqual(man, oldCurrJoin)) && oldval.getTid().containsAll(currval.getTid())) {  // the join equals oldval
				return true;
			}
			else	{
				return false;
			}
			
		} catch (ApronException e) {
			// TODO Auto-generated catch block
			System.out.println("\nAn Apron exception occurred in isSafeApprox");
			System.exit(1);
		}
		
		return false;
	}
	
	@Override
	public boolean readyToProcess(SyncAnalysis<TData>.WorkListElem currwle, TData oldval) {
		// TODO Auto-generated method stub
		return true;
	}
	
	@Override
	/**
	 * Implementation of the transfer functions. 
	 * @param u : Command currently being processed
	 * @param in : Input dataflow fact
	 * @param succlist : list of outgoing edges from this command
	 * @param proplist : dataflow fact propagated to each outgoing edge from this command
	 */
	public void flowthrough(Unit u, TData in, List<SyncEdge> succlist, List<TData> proplist) {
		
		//System.out.println("\n**** Flow Through Invocation ***");
		//System.out.println("\nObtained fact: " + in.toString());
		//System.out.println("Processing statement: " + u);
		super.intraThreadProps++;
		
		// Create a copy of the incoming fact
		TData newtav = null;
		newtav = new TData(man, in);
		
		if(u instanceof IdentityStmt) {
			handleIdentityStmt((IdentityStmt) u, newtav);
		}
		else if(u instanceof AssignStmt) {
			handleAssignStmt((AssignStmt) u, newtav);
		}
		
		this.propValue(newtav, succlist, proplist); // path sensitive part handled here
	}
	
	/**
	 * Handle assignment statements.
	 * Process the assignment only if the type of the lhs is an integer/byte variable.
	 * @param u : The assignment command
	 * @param newtav : A copy of the input element
	 */
	private void handleAssignStmt(AssignStmt u, TData newtav) {
		// TODO Auto-generated method stub
		
		Value leftOp = null;
		List<ValueBox> lhsBoxes = u.getDefBoxes();
		
		for(ValueBox lhsBox: lhsBoxes) {
			leftOp = lhsBox.getValue();
		}
		String lhs = leftOp.toString();
		
		
		lhs = rename(lhs, u);
		
		if(lhs.contains("error"))
			return;
		
		// Scan the environment to check if we are writing to a global var. 
		// If yes, we strongly update the tid set to reflect the id of the latest thread making the update.
		Var[] integerVars = globalenv.getIntVars();
		for(Var v: integerVars) {
			if((v.toString().trim()).compareTo(lhs.trim()) == 0) {
//				System.out.println("\nStatement: " + u.toString());
//				System.out.println("Class of u: " + unitToMeth.get(u).toString());
//				BufferedReader option = new BufferedReader(new InputStreamReader(System.in));
//				try {
//					option.readLine();
//				}
//				catch(IOException e) {
//					System.err.println("\nAn I/O Exception occurred");
//					System.exit(1);
//				}
				newtav.strongUpdateTid(unitToMeth.get(u).toString());
			}
		}
		
		// System.out.println("\nLHS STRING: " + lhs);
		Var lhsOpVar = null;	// lhs Var
		
		// Check if leftOp is of valid type
		if(! checkType(leftOp)) {
			return;
		}
		
		String command = u.toString().trim();
		StringTokenizer st = new StringTokenizer(command);
		String rhs = null;	// stores the rhs expression 
		while(st.hasMoreTokens()) {
			rhs = st.nextToken("=");
		}
		String operator = null;		// stores the operator +,-,*,/,%
		//System.out.println("\nRHS: " + rhs);
		
		
		// Check the operator 
		// Possible Binary operators: +, /, *, %
		// We don't include - as binary op as x-y is converted to x + -y in Jimple
		if(containsBinaryCommand(rhs) && (u.getUseBoxes().size() > 1)) {
			operator = extractBinOperator(command);
			
			// Extract each term in the RHS expression
			ArrayList<String> ops = new ArrayList<String>();
			st = new StringTokenizer(rhs);
			int index = 0;
			while(st.hasMoreTokens()) {
				ops.add(index, st.nextToken(operator).trim());
				index++;
			}
			
//			System.out.println("\nProcessed RHS operands");
//			for(String s: ops) {
//				System.out.println(s);
//			}
			
			boolean rhsContainsNumericConstant = false;
			int numericConstantIndex = 0;
			ArrayList<String> renamedOps = new ArrayList<String>();
			
			// Test the scope of the variables, and rename them accordingly to match the environment
			for(int i=0; i<ops.size(); i++) {
				// ops[i] is a number, no renaming necessary
				if(isNumericConstant(ops.get(i))) {
					rhsContainsNumericConstant = true;
					numericConstantIndex = i;
					renamedOps.add(i, ops.get(i));
					continue;
				}
				// Check if ops[i] is static, indicated by <...>, no renaming necessary
				if(ops.get(i).contains("<") && ops.get(i).toString().contains(">") && !ops.get(i).toString().contains(".<")) {
					renamedOps.add(i, ops.get(i));
					continue;
				}
				// local variable, append method name
				else {		
					renamedOps.add(i, (unitToMeth.get(u).toString() + "##" + ops.get(i)).trim());
				}
			}
//			System.out.println("\nRenaming complete: ");
//			for(String s: renamedOps) {
//				System.out.println(s);
//			}
			
			// Scan the environment to find the variable which matches lhs
			Var[] intVars = env.getIntVars();
			for(Var v: intVars) {
				if((v.toString().trim()).compareTo(lhs.trim()) == 0) {
					//System.out.println("\nLHS operator MATCHED!!!");
					lhsOpVar = v;
				}
			}
			
			
			// Create the RHS expression tree
			Texpr1BinNode ast = null;
			//System.out.println("\nOperator is: " + operator);
			
			if(operator.contains("+")) {
	
				// Build ast according to whether there is a numeric constant in rhs
				if(rhsContainsNumericConstant) {
					
					// Print out the environment
					String opVar1 = null;
					for(Var v: intVars) {
						//System.out.println("Environment var: " + v.toString());
						if((v.toString().trim()).compareTo(renamedOps.get(1-numericConstantIndex).trim()) == 0) {
							//System.out.println("\nMATCHING VARS FOUND!");
							opVar1 = v.toString();
						}
					}
					//System.out.println("\nOperand variable: " + renamedOps.get(1-numericConstantIndex) );
					
					/*
					 * Jimple treats x = x - 10 as x = x + -10, so check the numeric constant value and build ast accordingly
					 */
					int numCst = Integer.parseInt(ops.get(numericConstantIndex).trim());
					
					if(numCst >= 0) {
						ast = new Texpr1BinNode (
							Texpr1BinNode.OP_ADD,
							new Texpr1VarNode(opVar1),
							new Texpr1CstNode(new MpqScalar(numCst))
						);
					}
					else {	// numCst is negative, root will be OP_SUB
						ast = new Texpr1BinNode (
								Texpr1BinNode.OP_SUB,
								new Texpr1VarNode(opVar1),
								new Texpr1CstNode(new MpqScalar(-1 * numCst))
							);
					}
					
				}
				else {
					String opVar1 = null;
					String opVar2 = null;
					//System.out.println("\nProcessing x + y");
					for(Var v: intVars) {
						//System.out.println("Environment var: " + v.toString());
						if((v.toString().trim()).compareTo(renamedOps.get(0).trim()) == 0) {
							//System.out.println("\nMATCHING VARS FOUND!");
							opVar1 = v.toString();
						}
					}
					
					for(Var v: intVars) {
						//System.out.println("Environment var: " + v.toString());
						if((v.toString().trim()).compareTo(renamedOps.get(1).trim()) == 0) {
							//System.out.println("\nMATCHING VARS FOUND!");
							opVar2 = v.toString();
						}
					}
					
					ast = new Texpr1BinNode(
							Texpr1BinNode.OP_ADD,
							new Texpr1VarNode(opVar1),
							new Texpr1VarNode(opVar2)
					);
				}
			}
			
			if(operator.contains("-")) {
				// Build ast according to whether there is a numeric constant in rhs
				if(rhsContainsNumericConstant) {
					String opVar1 = null;
					for(Var v: intVars) {
						//System.out.println("Environment var: " + v.toString());
						if((v.toString().trim()).compareTo(renamedOps.get(1-numericConstantIndex).trim()) == 0) {
							//System.out.println("\nMATCHING VARS FOUND!");
							opVar1 = v.toString();
						}
					}
					//System.out.println("\nOperand variable: " + renamedOps.get(1-numericConstantIndex) );
					ast = new Texpr1BinNode (
						Texpr1BinNode.OP_SUB,
						new Texpr1VarNode(opVar1),
						new Texpr1CstNode(new MpqScalar(Integer.parseInt(ops.get(numericConstantIndex).trim())))
					);
				}
				else {
					String opVar1 = null;
					String opVar2 = null;
					
					for(Var v: intVars) {
						//System.out.println("Environment var: " + v.toString());
						if((v.toString().trim()).compareTo(renamedOps.get(0).trim()) == 0) {
							//System.out.println("\nMATCHING VARS FOUND!");
							opVar1 = v.toString();
						}
					}
					
					for(Var v: intVars) {
						//System.out.println("Environment var: " + v.toString());
						if((v.toString().trim()).compareTo(renamedOps.get(1).trim()) == 0) {
							//System.out.println("\nMATCHING VARS FOUND!");
							opVar2 = v.toString();
						}
					}
					
					ast = new Texpr1BinNode(
							Texpr1BinNode.OP_SUB,
							new Texpr1VarNode(opVar1),
							new Texpr1VarNode(opVar2)
					);
				}
			}
			
			if(operator.contains("*")) {
				//System.out.println("\nProcessing Addition");
				// Build ast according to whether there is a numeric constant in rhs
				if(rhsContainsNumericConstant) {
					String opVar1 = null;
					for(Var v: intVars) {
						//System.out.println("Environment var: " + v.toString());
						if((v.toString().trim()).compareTo(renamedOps.get(1-numericConstantIndex).trim()) == 0) {
							//System.out.println("\nMATCHING VARS FOUND!");
							opVar1 = v.toString();
						}
					}
					//System.out.println("\nOperand variable: " + renamedOps.get(1-numericConstantIndex) );
					ast = new Texpr1BinNode (
						Texpr1BinNode.OP_MUL,
						new Texpr1VarNode(opVar1),
						new Texpr1CstNode(new MpqScalar(Integer.parseInt(ops.get(numericConstantIndex).trim())))
					);
				}
				else {
					String opVar1 = null;
					String opVar2 = null;
					
					for(Var v: intVars) {
						//System.out.println("Environment var: " + v.toString());
						if((v.toString().trim()).compareTo(renamedOps.get(0).trim()) == 0) {
							//System.out.println("\nMATCHING VARS FOUND!");
							opVar1 = v.toString();
						}
					}
					
					for(Var v: intVars) {
						//System.out.println("Environment var: " + v.toString());
						if((v.toString().trim()).compareTo(renamedOps.get(1).trim()) == 0) {
							//System.out.println("\nMATCHING VARS FOUND!");
							opVar2 = v.toString();
						}
					}
					
					ast = new Texpr1BinNode(
							Texpr1BinNode.OP_MUL,
							new Texpr1VarNode(opVar1),
							new Texpr1VarNode(opVar2)
					);
				}
			}
			
			if(operator.contains("/")) {
				//System.out.println("\nProcessing Addition");
				// Build ast according to whether there is a numeric constant in rhs
				if(rhsContainsNumericConstant) {
					String opVar1 = null;
					for(Var v: intVars) {
						//System.out.println("Environment var: " + v.toString());
						if((v.toString().trim()).compareTo(renamedOps.get(1-numericConstantIndex).trim()) == 0) {
							//System.out.println("\nMATCHING VARS FOUND!");
							opVar1 = v.toString();
						}
					}
					//System.out.println("\nOperand variable: " + renamedOps.get(1-numericConstantIndex) );
					ast = new Texpr1BinNode (
						Texpr1BinNode.OP_DIV,
						new Texpr1VarNode(opVar1),
						new Texpr1CstNode(new MpqScalar(Integer.parseInt(ops.get(numericConstantIndex).trim())))
					);
				}
				else {
					String opVar1 = null;
					String opVar2 = null;
					
					for(Var v: intVars) {
						//System.out.println("Environment var: " + v.toString());
						if((v.toString().trim()).compareTo(renamedOps.get(0).trim()) == 0) {
							//System.out.println("\nMATCHING VARS FOUND!");
							opVar1 = v.toString();
						}
					}
					
					for(Var v: intVars) {
						//System.out.println("Environment var: " + v.toString());
						if((v.toString().trim()).compareTo(renamedOps.get(1).trim()) == 0) {
							//System.out.println("\nMATCHING VARS FOUND!");
							opVar2 = v.toString();
						}
					}
					
					ast = new Texpr1BinNode(
							Texpr1BinNode.OP_DIV,
							new Texpr1VarNode(opVar1),
							new Texpr1VarNode(opVar2)
					);
				}
			}
			
			if(operator.contains("%")) {
				//System.out.println("\nProcessing Addition");
				// Build ast according to whether there is a numeric constant in rhs
				if(rhsContainsNumericConstant) {
					String opVar1 = null;
					for(Var v: intVars) {
						//System.out.println("Environment var: " + v.toString());
						if((v.toString().trim()).compareTo(renamedOps.get(1-numericConstantIndex).trim()) == 0) {
							//System.out.println("\nMATCHING VARS FOUND!");
							opVar1 = v.toString();
						}
					}
					//System.out.println("\nOperand variable: " + renamedOps.get(1-numericConstantIndex) );
					ast = new Texpr1BinNode (
						Texpr1BinNode.OP_MOD,
						new Texpr1VarNode(opVar1),
						new Texpr1CstNode(new MpqScalar(Integer.parseInt(ops.get(numericConstantIndex).trim())))
					);
				}
				else {
					String opVar1 = null;
					String opVar2 = null;
					
					for(Var v: intVars) {
						//System.out.println("Environment var: " + v.toString());
						if((v.toString().trim()).compareTo(renamedOps.get(0).trim()) == 0) {
							//System.out.println("\nMATCHING VARS FOUND!");
							opVar1 = v.toString();
						}
					}
					
					for(Var v: intVars) {
						//System.out.println("Environment var: " + v.toString());
						if((v.toString().trim()).compareTo(renamedOps.get(1).trim()) == 0) {
							//System.out.println("\nMATCHING VARS FOUND!");
							opVar2 = v.toString();
						}
					}
					
					ast = new Texpr1BinNode(
							Texpr1BinNode.OP_MOD,
							new Texpr1VarNode(opVar1),
							new Texpr1VarNode(opVar2)
					);
				}
			}
			
			if(ast == null) {
				System.out.println("\nError while creating AST in flowThrough.");
				System.exit(1);
			}
//			
			Texpr1Intern stmt = new Texpr1Intern(env, ast);
			try {
				newtav.getRel().assign(man, lhsOpVar, stmt, null);
			} catch (ApronException e) {
				System.out.println("\nAn Apron exception occurred while performing assignment in flowThrough.");
				System.exit(1);
			}
		}
		
		else {		// Unary command
	//		System.out.println("\nExecuting Unary command");
	//		System.out.println("\nRHS: " + rhs);
			Var[] intVars = env.getIntVars();
			// Appropriately rename the rhs
			rhs = rename(rhs, u);
			boolean varMatched = false;
		
			String temp = new String(rhs);
			
			for(Var v: intVars) {
				//System.out.println("Environment var: " + v.toString());
				if((v.toString().trim()).compareTo(temp.trim()) == 0) {
					//System.out.println("\nMATCHING VARS FOUND!");
					rhs = v.toString();
					varMatched = true;
				}
			}
			
			// RHS is not a variable or a number, then return
			if(!varMatched && !isNumericConstant(rhs))
				return;
			
			//System.out.println("UNARY:: RHS: " + rhs);
			// Create the RHS expression
			Linexpr1 rhsExpr = null;
			if(isNumericConstant(rhs)) {
				// Treat the rhs, which is a constant, as (0*lhs + cst)
				Linterm1[] rhsTerm = 
					{
						new Linterm1(lhs, new MpqScalar(0))
					};
				rhsExpr = new Linexpr1(env, rhsTerm, new MpqScalar(Integer.parseInt(rhs.trim())));
			}
			else {
				Linterm1[] rhsTerm = 
					{
						new Linterm1(rhs, new MpqScalar(1))
					};
				rhsExpr = new Linexpr1(env, rhsTerm, new MpqScalar(0));
			}
			
			try {
				newtav.getRel().assign(man, lhs, rhsExpr, null);
			} catch (ApronException e) {
				// TODO Auto-generated catch block
				System.out.println("\nAn ApronException occurred while processing x = y in flowThrough.");
				System.exit(1);
			}
			
		}
		
	}
	
	/**
	 * Check if the input string is a number
	 * @param string
	 * @return true if input string is a number
	 */
	public boolean isNumericConstant(String string) {
		// TODO Auto-generated method stub
		int cst = 0;
		boolean isANumber = true;
		try {
			cst = Integer.parseInt(string.trim());
		}
		catch(NumberFormatException e) {
			isANumber = false;
		}
		// System.out.println("\nInput string: " + string + " with out: " + isANumber);
		return isANumber;
	}

	/**
	 * Extract the operator
	 * @param command
	 * @return
	 */
	private String extractBinOperator(String command) {
		// TODO Auto-generated method stub
		if(command.contains("+"))
			return "+";
		if(command.contains("-"))
			return "-";
		if(command.contains("*"))
			return "*";
		if(command.contains("/"))
			return "/";
		if(command.contains("%"))
			return "%";
		return null;
	}

	/**
	 * Check if the input RHS expression contains one of the 4 binary operations we analyse
	 * @param command : The RHS expression
	 * @return : true if RHS has one of +, -, /, *, %
	 */
	private boolean containsBinaryCommand(String command) {
		// TODO Auto-generated method stub
		if(command.contains("+") || command.contains("-") || command.contains("/") || command.contains("*") || command.contains("%"))
			return true;
		
		return false;
	}

	/**
	 * Return true if leftOp is of type ByteType, IntType
	 * @param leftOp : Value whose type needs to be checked
	 * @return
	 */
	private boolean checkType(Value leftOp) {
		// TODO Auto-generated method stub
		if((leftOp.getType() instanceof ByteType) || (leftOp.getType() instanceof IntType)) {
			return true;
		}
		return false;
	}
	
	/**
	 * As a first approximation, IdentityStmt is used to initialize local variables. Since we do not consider function calls, we treat this as no-op.
	 * @param u
	 * @param newtav
	 */
	private void handleIdentityStmt(IdentityStmt u, TData newtav) {
		// TODO Auto-generated method stub
		return;
	}
	
	/**
	 * Path sensitive processing.
	 * @param newtav	: processed fact
	 * @param succlist : outgoing edges
	 * @param proplist	: facts for each outgoing edge
	 */
	private void propValue(TData newtav, List<SyncEdge> succlist, List<TData> proplist) {
		// TODO Auto-generated method stub
		for(int i=0; i<succlist.size(); i++) {
			SyncEdge curredge = succlist.get(i);
	//		System.out.println("\nExecuting propValue with edge: " + curredge.getKind());
			if(curredge.getKind() == EdgeKind.FALL_THROUGH) {
				proplist.add(i, newtav);
			}
		
			else if((curredge.getKind() == EdgeKind.BRANCH) || (curredge.getKind() == EdgeKind.BRANCH_FALL_THROUGH)) {
				TData proptav = handleBranchStmt(curredge, newtav);
				proplist.add(i, proptav);
			}
			
			else if(curredge.getKind() == EdgeKind.EXPLICIT) {
				TData proptav = handleMethodCall(curredge, newtav);				
				proplist.add(i, proptav);
			}
			else if(curredge.getKind() == EdgeKind.RETURN) {
				TData proptav = handleReturn(curredge, newtav);								
				proplist.add(i, proptav);
			}
			else if(curredge.getKind() == EdgeKind.MON) {
				TData proptav = handleSync(curredge, newtav);								
				proplist.add(i, proptav);
			}
			else if(curredge.getKind() == EdgeKind.START) {
				TData proptav = handleStart(curredge, newtav);								
				proplist.add(i, proptav);
			}
			else {
				System.out.println("Un-handled edge type: " + curredge.getKind());
				System.exit(-1);
			}
		}
	}
	
	/**
	 * Handle the start edge
	 * @param curredge
	 * @param newtav
	 * @return
	 */
	private TData handleStart(SyncEdge curredge, TData newtav) {
		TData proptav =  null; 
		proptav = new TData(man, newtav);
		return proptav;
	}
	

	private TData handleSync(SyncEdge curredge, TData newtav) {
		// TODO Auto-generated method stub
		super.interThreadProps++;
		TData proptav =  null; 
		proptav = new TData(man, newtav); 
		return proptav;
	}
	
	private TData handleReturn(SyncEdge curredge, TData newtav) {
		// TODO Auto-generated method stub
		return newtav;
	}

	private TData handleMethodCall(SyncEdge curredge, TData newtav) {
		// TODO Auto-generated method stub
		return newtav;
	}
	
	/**
	 * Path sensitive handling of conditionals.
	 * @param curredge 
	 * @param newtav
	 * @return abstract element 
	 */
	private TData handleBranchStmt(SyncEdge curredge, TData newtav) {
		TData propval = null;
		propval = new TData(man, newtav);
		
		// Extract expression
		IfStmt u = (IfStmt)(curredge.getSource());
		Value cond = u.getCondition();
		Value op1 = ((BinopExpr) cond).getOp1();
		Value op2 = ((BinopExpr) cond).getOp2();
		
		if(!(op1.getType() instanceof IntType || op2.getType() instanceof IntType))
			return propval;
		
		String op1var = null, op2var = null, cst = null;
		boolean containsCst = false;	// does the condition contain a boolean constant?
		
		if(isNumericConstant(op1.toString().trim())) {	// op1 is a number
			cst = rename(op1.toString(), (Unit) u);
			Var[] intVars = env.getIntVars();
			op1var = rename(op2.toString(), (Unit) u);
			for(Var v: intVars) {
				//System.out.println("Environment var: " + v.toString());
				if((v.toString().trim()).compareTo(op1var.trim()) == 0) {
					//System.out.println("\nMATCHING VARS FOUND!");
					op1var = v.toString();
					break;
				}
			}
			containsCst= true;
		}
		
		else if(isNumericConstant(op2.toString().trim())) {	// op2 is a number
			cst = rename(op2.toString(), (Unit) u);
			op1var = rename(op1.toString(), (Unit) u);
			Var[] intVars = env.getIntVars();
			for(Var v: intVars) {
				//System.out.println("Environment var: " + v.toString());
				if((v.toString().trim()).compareTo(op1var.trim()) == 0) {
					//System.out.println("\nMATCHING VARS FOUND!");
					op1var = v.toString();
					break;
				}
			}
			containsCst= true;
		}
		
		else {
			Var[] intVars = env.getIntVars();
			op1var = rename(op1.toString(), (Unit) u);
			for(Var v: intVars) {
				//System.out.println("Environment var: " + v.toString());
				if((v.toString().trim()).compareTo(op1var.trim()) == 0) {
					//System.out.println("\nMATCHING VARS FOUND!");
					op1var = v.toString();
					break;
				}
			}
			op2var = rename(op2.toString(), (Unit) u);
			for(Var v: intVars) {
				//System.out.println("Environment var: " + v.toString());
				if((v.toString().trim()).compareTo(op2var.trim()) == 0) {
					//System.out.println("\nMATCHING VARS FOUND!");
					op2var = v.toString();
					break;
				}
			}
		}
		
		//System.out.println("\nBranching opVar1: " + op1var);
		//System.out.println("\nBranching opVar2: " + op2var);
		
		String relOperator = extractRelOperator(u.toString());
		
		//System.out.println("\nOp 1:" + op1);
		//System.out.println("Op 2: " + op2);
		//System.out.println("\nConditional Type is: " + (op1.getType() instanceof IntType || op2.getType() instanceof IntType));
		
		
				
		// Create the conditional constraint
		Lincons1 condConstraint = null;
		Lincons1 NotcondConstraint = null;
		
		if(relOperator.contains("==")) {
			if(!containsCst) {
				Linterm1[] condTerms = 
					{
						new Linterm1(op1var.trim(), new MpqScalar(1)),
						new Linterm1(op2var.trim(), new MpqScalar(-1))
					};
				Linterm1[] NotcondTerms = 
					{
						new Linterm1(op1var.trim(), new MpqScalar(1)),
						new Linterm1(op2var.trim(), new MpqScalar(-1))
					};
				Linexpr1 condExpr = new Linexpr1(env, condTerms, new MpqScalar(0));
				Linexpr1 NotcondExpr = new Linexpr1(env, NotcondTerms, new MpqScalar(0));
				condConstraint = new Lincons1(Lincons1.EQ, condExpr);
				NotcondConstraint = new Lincons1(Lincons1.DISEQ, NotcondExpr);
			}
			else {
				Linterm1[] condTerms = 
					{
						new Linterm1(op1var.trim(), new MpqScalar(1))
					};
				Linterm1[] NotcondTerms = 
					{
						new Linterm1(op1var.trim(), new MpqScalar(1))
					};
				int coeff = -1*Integer.parseInt(cst.trim());
				Linexpr1 condExpr = new Linexpr1(env, condTerms, new MpqScalar(coeff));
				Linexpr1 NotcondExpr = new Linexpr1(env, NotcondTerms, new MpqScalar(coeff));
				condConstraint = new Lincons1(Lincons1.EQ, condExpr);
				NotcondConstraint = new Lincons1(Lincons1.DISEQ, NotcondExpr);
			}
		}
		else if(relOperator.contains("!=")) {
			if(!containsCst) {
				Linterm1[] condTerms = 
					{
						new Linterm1(op1var.trim(), new MpqScalar(1)),
						new Linterm1(op2var.trim(), new MpqScalar(-1))
					};
				Linterm1[] NotcondTerms = 
					{
						new Linterm1(op1var.trim(), new MpqScalar(1)),
						new Linterm1(op2var.trim(), new MpqScalar(-1))
					};
				Linexpr1 condExpr = new Linexpr1(env, condTerms, new MpqScalar(0));
				Linexpr1 NotcondExpr = new Linexpr1(env, NotcondTerms, new MpqScalar(0));
				condConstraint = new Lincons1(Lincons1.DISEQ, condExpr);
				NotcondConstraint = new Lincons1(Lincons1.EQ, NotcondExpr);
			}
			else {
				Linterm1[] condTerms = 
					{
						new Linterm1(op1var.trim(), new MpqScalar(1))
					};
				Linterm1[] NotcondTerms = 
					{
						new Linterm1(op1var.trim(), new MpqScalar(1))
					};
				int coeff = -1*Integer.parseInt(cst.trim());
				Linexpr1 condExpr = new Linexpr1(env, condTerms, new MpqScalar(coeff));
				Linexpr1 NotcondExpr = new Linexpr1(env, NotcondTerms, new MpqScalar(coeff));
				condConstraint = new Lincons1(Lincons1.DISEQ, condExpr);
				NotcondConstraint = new Lincons1(Lincons1.EQ, NotcondExpr);
			}
		}
		else if(relOperator.contains("<=")) {	// op1 <= op2 translates to op2 - op1 >= 0
			if(!containsCst) {
				Linterm1[] condTerms = 
					{
						new Linterm1(op1var.trim(), new MpqScalar(-1)),
						new Linterm1(op2var.trim(), new MpqScalar(1))
					};
				Linterm1[] NotcondTerms = 
					{
						new Linterm1(op1var.trim(), new MpqScalar(1)),
						new Linterm1(op2var.trim(), new MpqScalar(-1))
					};
				Linexpr1 condExpr = new Linexpr1(env, condTerms, new MpqScalar(0));
				Linexpr1 NotcondExpr = new Linexpr1(env, NotcondTerms, new MpqScalar(0));
				condConstraint = new Lincons1(Lincons1.SUPEQ, condExpr);
				NotcondConstraint = new Lincons1(Lincons1.SUP, NotcondExpr);
			}
			else {
				Linterm1[] condTerms = 
					{
						new Linterm1(op1var.trim(), new MpqScalar(-1))
					};
				Linterm1[] NotcondTerms = 
					{
						new Linterm1(op1var.trim(), new MpqScalar(1))
					};
				Linexpr1 condExpr = new Linexpr1(env, condTerms, new MpqScalar(Integer.parseInt(cst.trim())));
				Linexpr1 NotcondExpr = new Linexpr1(env, NotcondTerms, new MpqScalar(-1 * Integer.parseInt(cst.trim())));
				condConstraint = new Lincons1(Lincons1.SUPEQ, condExpr);
				NotcondConstraint = new Lincons1(Lincons1.SUP, NotcondExpr);
			}
		}
		else if(relOperator.contains("<")) {	// op1 < op2 translates to op2 - op1 > 0
			if(!containsCst) {
				Linterm1[] condTerms = 
					{
						new Linterm1(op1var.trim(), new MpqScalar(-1)),
						new Linterm1(op2var.trim(), new MpqScalar(1))
					};
				Linterm1[] NotcondTerms = 
					{
						new Linterm1(op1var.trim(), new MpqScalar(1)),
						new Linterm1(op2var.trim(), new MpqScalar(-1))
					};
				Linexpr1 condExpr = new Linexpr1(env, condTerms, new MpqScalar(0));
				Linexpr1 NotcondExpr = new Linexpr1(env, NotcondTerms, new MpqScalar(0));
				condConstraint = new Lincons1(Lincons1.SUP, condExpr);
				NotcondConstraint = new Lincons1(Lincons1.SUPEQ, NotcondExpr);
			}
			else {
				Linterm1[] condTerms = 
					{
						new Linterm1(op1var.trim(), new MpqScalar(-1))
					};
				Linterm1[] NotcondTerms = 
					{
						new Linterm1(op1var.trim(), new MpqScalar(1))
					};
				Linexpr1 condExpr = new Linexpr1(env, condTerms, new MpqScalar(Integer.parseInt(cst.trim())));
				Linexpr1 NotcondExpr = new Linexpr1(env, NotcondTerms, new MpqScalar(-1 * Integer.parseInt(cst.trim())));
				condConstraint = new Lincons1(Lincons1.SUP, condExpr);
				NotcondConstraint = new Lincons1(Lincons1.SUPEQ, NotcondExpr);
			}
		}
		else if(relOperator.contains(">=")) {	// op1 >= op2 translates to op1 - op2 >= 0
			if(!containsCst) {
				Linterm1[] condTerms = 
					{
						new Linterm1(op1var.trim(), new MpqScalar(1)),
						new Linterm1(op2var.trim(), new MpqScalar(-1))
					};
				Linterm1[] NotcondTerms = 
					{
						new Linterm1(op1var.trim(), new MpqScalar(-1)),
						new Linterm1(op2var.trim(), new MpqScalar(1))
					};
				Linexpr1 condExpr = new Linexpr1(env, condTerms, new MpqScalar(0));
				Linexpr1 NotcondExpr = new Linexpr1(env, NotcondTerms, new MpqScalar(0));
				condConstraint = new Lincons1(Lincons1.SUPEQ, condExpr);
				NotcondConstraint = new Lincons1(Lincons1.SUP, NotcondExpr);
			}
			else {
				Linterm1[] condTerms = 
					{
						new Linterm1(op1var.trim(), new MpqScalar(1))
					};
				Linterm1[] NotcondTerms = 
					{
						new Linterm1(op1var.trim(), new MpqScalar(-1))
					};
				Linexpr1 condExpr = new Linexpr1(env, condTerms, new MpqScalar(-1 * Integer.parseInt(cst.trim())));
				Linexpr1 NotcondExpr = new Linexpr1(env, NotcondTerms, new MpqScalar(Integer.parseInt(cst.trim())));
				condConstraint = new Lincons1(Lincons1.SUPEQ, condExpr);
				NotcondConstraint = new Lincons1(Lincons1.SUP, NotcondExpr);
			}
		}
		else if(relOperator.contains(">")) {	// op1 > op2 translates to op1 - op2 > 0
			if(!containsCst) {
				Linterm1[] condTerms = 
					{
						new Linterm1(op1var.trim(), new MpqScalar(1)),
						new Linterm1(op2var.trim(), new MpqScalar(-1))
					};
				Linterm1[] NotcondTerms = 
					{
						new Linterm1(op1var.trim(), new MpqScalar(-1)),
						new Linterm1(op2var.trim(), new MpqScalar(1))
					};
				Linexpr1 condExpr = new Linexpr1(env, condTerms, new MpqScalar(0));
				Linexpr1 NotcondExpr = new Linexpr1(env, NotcondTerms, new MpqScalar(0));
				condConstraint = new Lincons1(Lincons1.SUP, condExpr);
				NotcondConstraint = new Lincons1(Lincons1.SUPEQ, NotcondExpr);
			}
			else {
				Linterm1[] condTerms = 
					{
						new Linterm1(op1var.trim(), new MpqScalar(1))
					};
				Linterm1[] NotcondTerms = 
					{
						new Linterm1(op1var.trim(), new MpqScalar(-1))
					};
				Linexpr1 condExpr = new Linexpr1(env, condTerms, new MpqScalar(-1 * Integer.parseInt(cst.trim())));
				Linexpr1 NotcondExpr = new Linexpr1(env, NotcondTerms, new MpqScalar(Integer.parseInt(cst.trim())));
				condConstraint = new Lincons1(Lincons1.SUP, condExpr);
				NotcondConstraint = new Lincons1(Lincons1.SUPEQ, NotcondExpr);
			}
		}
		
		if(curredge.getKind().equals(EdgeKind.BRANCH)) {	// True branch
			try {
				propval.getRel().meet(man, condConstraint);
				
		//		System.out.println("\nBRANCH FACT: " + propval);
			} catch (ApronException e) {
				System.err.println("\nAn Apron exception occurred while computing intersection/minimizing in handleBranchStmt");
				System.exit(1);
			}
		}
		else if(curredge.getKind().equals(EdgeKind.BRANCH_FALL_THROUGH)) {	// False branch
			try {
				propval.getRel().meet(man, NotcondConstraint);
				
		//		System.out.println("\nNOT_BRANCH FACT: " + propval);
			} catch (ApronException e) {
				System.err.println("\nAn Apron exception occurred while computing intersection/minimizing in handleBranchStmt");
				System.exit(1);
			}
		}
		return propval;
		
	}
	
	/**
	 * Given an input variable name, rename it by tagging the fully qualified class name (if instance variable).
	 * @param var
	 * @return var renamed to match with the environment
	 */
	public String rename(String var, Unit u) {
		
		if(isNumericConstant(var)) {
			// rhs is a number, no renaming necessary
			return var;
		}
		// Check if rhs is static, indicated by <...>, no renaming necessary
		else if(var.contains("<") && var.toString().contains(">") && !var.toString().contains(".<")) {
			// rhs is static, no renaming necessary
			return var;
		}
		// local variable, append method name
		else {		
			var = unitToMeth.get(u).toString() + "##" + var.trim();
			return var;
		}
	}
	
	/**
	 * Given an input command (as a string), extracts and returns the binary relational operator in the command
	 * @param string : input command
	 * @return : <, >, <=, >=, ==
	 */
	public String extractRelOperator(String string) {

		if(string == null) {
			System.err.println("\nextractRelOperator received a null string!");
			return null;
		}
		// string could be of the form if a != b goto <x >= ...
		// which causes erroneous extraction of the relational operator
		
		StringTokenizer st = new StringTokenizer(string, "goto");
		String condition = st.nextToken();
		// System.out.println(condition);
		
		if(condition.contains("<="))
			return "<=";
		else if(condition.contains(">="))
			return ">=";
		else if(condition.contains("=="))
			return "==";
		else if(condition.contains("<"))
			return "<";
		else if(condition.contains(">") )
			return ">";
		else if(condition.contains("!="))
			return "!=";
		else	return null;
		
		
	}
	
	@Override
	/**
	 * Merge the currval and val, and store in val
	 * The edge is taken as input because monitor edges handles call-string differently.
	 * Since this analyzer does not consider functions, we ignore this parameter.
	 */
	public void merge(SyncEdge edge, TData currval, TData val) {
		
		if(! useRegions)	// standard variable handling
		{
			// inter-thread join
			if(edge.getKind() == EdgeKind.MON) {
				// if the incoming flow fact is tagged with the singleton thread-id, which equals the current thread-id, 
				// then we have a stale fact. So do nothing.
//				System.out.println("\nMerge comparing classes...");
//				System.out.println("Val classes: " + val.getTid().toString());
//				System.out.println("Currval classes: " + currval.getTid().toString());
//				BufferedReader option = new BufferedReader(new InputStreamReader(System.in));
//				try {
//					option.readLine();
//				}
//				catch(IOException e) {
//					System.err.println("\nAn I/O Exception occurred.");
//					System.exit(1);
//				}
				if((val.getTid().size() == 1) && val.getTid().equals(currval.getTid()))
						return;
				try {
					TData val_copy = new TData(man, val);
					val.getRel().changeEnvironment(man, localenv, false);
					
					HashMap<String, ArrayList<Abstract1>> globalsToEnv = new HashMap<String, ArrayList<Abstract1>>();
					
					// project currval and val to each global var
					for(String v: globalVars) {
						String[] v_string = new String[1];
						v_string[0] = v;
						String[] realVars = {"dummy"};
						Environment env_v = new Environment(v_string, realVars);
						env_v.remove(realVars);
						
						Abstract1 currval_v = currval.getRel().changeEnvironmentCopy(man, env_v, false);
						Abstract1 val_v = val_copy.getRel().changeEnvironmentCopy(man, env_v, false);
						
						ArrayList<Abstract1> envListsForV = new ArrayList<Abstract1>();
						envListsForV.add(currval_v);
						envListsForV.add(val_v);
						
						globalsToEnv.put(v, envListsForV);
					}
					
					// join the currval and val projections for each global var v
					HashMap<String, Abstract1> variableWiseJoinedMap = new HashMap<String, Abstract1>();
					for(String v: globalVars) {
						Abstract1 temp = new Abstract1(man,globalsToEnv.get(v).get(0).joinCopy(man, globalsToEnv.get(v).get(1)));
						variableWiseJoinedMap.put(v, temp);
					}
					
					// Extend the environments of each variableWiseJoinedMap
					for(String v: globalVars) {
						variableWiseJoinedMap.get(v).changeEnvironment(man, globalenv, false);
					}
					
					Lincons1[] validCons = new Lincons1[1];
					validCons[0] = new Lincons1(globalenv, true);	// create a valid constraint
					Abstract1 globalMeetEnv = new Abstract1(man, validCons);	// globalMeetEnv is top
					
					for(String v: globalVars) {
						globalMeetEnv.meet(man, variableWiseJoinedMap.get(v));
					}
					
					// Expand globalMeetEnv to include locals and val_local to include globals
					globalMeetEnv.changeEnvironment(man, env, false);
					val.getRel().changeEnvironment(man, env, false);
					
					val.getRel().meet(man, globalMeetEnv);
					val.getTid().addAll(currval.getNewTid());
				}
				catch(ApronException e) {
					System.err.println("\nAn Apron Exception occurred while attempting inter-thread merge.");
					System.exit(1);
				}
			}
			// intra-thread join 
			else {
				try {
					val.getRel().join(man, currval.getRel());
					val.getTid().addAll(currval.getNewTid());
				}
				catch(ApronException e) {
					System.out.println("\nAn Apron Exception occurred while merging.");
					System.exit(1);
				}
			}
		}
		else	// region based merge
		{
			// inter-thread region based join
			if(edge.getKind() == EdgeKind.MON) {
				// if the incoming flow fact is tagged with the singleton thread-id, which equals the current thread-id, 
				// then we have a stale fact. So do nothing.
				if((val.getTid().size() == 1) && val.getTid().equals(currval.getTid()))
						return;
				try {
					TData val_copy = new TData(man, val);
					val.getRel().changeEnvironment(man, localenv, false);

					HashMap<String, ArrayList<Abstract1>> regionsToEnv = new HashMap<String, ArrayList<Abstract1>>();

					// project currval and val to each region
					for(String r: regions.keySet()) {
						String[] r_string = new String[regions.get(r).size()];
						int k = 0;
						for(String v: regions.get(r)) {
							r_string[k] = regions.get(r).get(k);
							k++;
						}
						String[] realVars = {"dummy"};
						Environment env_v = new Environment(r_string, realVars);
						env_v.remove(realVars);

						Abstract1 currval_v = currval.getRel().changeEnvironmentCopy(man, env_v, false);
						Abstract1 val_v = val_copy.getRel().changeEnvironmentCopy(man, env_v, false);

						ArrayList<Abstract1> envListsForR = new ArrayList<Abstract1>();
						envListsForR.add(currval_v);
						envListsForR.add(val_v);

						regionsToEnv.put(r, envListsForR);
					}

					// join the currval and val projections for each region r
					HashMap<String, Abstract1> regionWiseJoinedMap = new HashMap<String, Abstract1>();
					for(String r: regions.keySet()) {
						Abstract1 temp = new Abstract1(man, regionsToEnv.get(r).get(0).joinCopy(man, regionsToEnv.get(r).get(1)));
						regionWiseJoinedMap.put(r, temp);
					}

					// Extend the environments of each regionWiseJoinedMap
					for(String r: regions.keySet()) {
						regionWiseJoinedMap.get(r).changeEnvironment(man, globalenv, false);
					}

					Lincons1[] validCons = new Lincons1[1];
					validCons[0] = new Lincons1(globalenv, true);	// create a valid constraint
					Abstract1 globalMeetEnv = new Abstract1(man, validCons);	// globalMeetEnv is top

					for(String r: regions.keySet()) {
						globalMeetEnv.meet(man, regionWiseJoinedMap.get(r));
					}

					// Expand globalMeetEnv to include locals and val_local to include globals
					globalMeetEnv.changeEnvironment(man, env, false);
					val.getRel().changeEnvironment(man, env, false);

					val.getRel().meet(man, globalMeetEnv);
					val.getTid().addAll(currval.getNewTid());
					
				}
				catch(ApronException e) {
					System.err.println("\nAn Apron Exception occurred while attempting inter-thread merge.");
					System.exit(1);
				}
			}
			// intra-thread join 
			else {
				try {
					val.getRel().join(man, currval.getRel());
					val.getTid().addAll(currval.getNewTid());
					
				}
				catch(ApronException e) {
					System.out.println("\nAn Apron Exception occurred while merging.");
					System.exit(1);
				}
			}
		}
	}
	
	@Override
	/**
	 * Perform standard merge if the number of iterations is less than wideningThreshold.
	 * Otherwise widen val with respect to currval.
	 */
	public void merge(SyncEdge edge, TData currval, TData val, HashMap<Unit, Integer> unit2CountMap,
			Unit currunit) {
			
		if(!unit2CountMap.containsKey(currunit))
		{
			System.err.println("\nERROR! Unit " + currunit + " not processed prior to merge");
			return;
		}
		
		if(unit2CountMap.get(currunit) <= wideningThreshold) {		// less than widening threshold, call standard merge
			merge(edge, currval, val);
		}
		else {		// threshold exceeded, apply widening
			
			if(! useRegions) {	// variable based widening
				// inter-thread variable based widening
				if(edge.getKind() == EdgeKind.MON) {
					
					// if the incoming flow fact is tagged with the singleton thread-id, which equals the current thread-id, 
					// then we have a stale fact. So do nothing.
					if((val.getTid().size() == 1) && val.getTid().equals(currval.getTid()))
							return;
					
					try {
						TData val_copy = new TData(man, val);
						val.getRel().changeEnvironment(man, localenv, false);

						HashMap<String, ArrayList<Abstract1>> globalsToEnv = new HashMap<String, ArrayList<Abstract1>>();

						// project currval and val to each global var
						for(String v: globalVars) {
							String[] v_string = new String[1];
							v_string[0] = v;
							String[] realVars = {"dummy"};
							Environment env_v = new Environment(v_string, realVars);
							env_v.remove(realVars);

							Abstract1 currval_v = currval.getRel().changeEnvironmentCopy(man, env_v, false);
							Abstract1 val_v = val_copy.getRel().changeEnvironmentCopy(man, env_v, false);

							ArrayList<Abstract1> envListsForV = new ArrayList<Abstract1>();
							envListsForV.add(currval_v);
							envListsForV.add(val_v);

							globalsToEnv.put(v, envListsForV);
						}

						// widen the currval and val projections for each global var v
						HashMap<String, Abstract1> variableWiseJoinedMap = new HashMap<String, Abstract1>();
						for(String v: globalVars) {
							Abstract1 temp = new Abstract1(man, globalsToEnv.get(v).get(0).widening(man, globalsToEnv.get(v).get(1)));
							variableWiseJoinedMap.put(v, temp);
						}

						// Extend the environments of each variableWiseJoinedMap
						for(String v: globalVars) {
							variableWiseJoinedMap.get(v).changeEnvironment(man, globalenv, false);
						}

						Lincons1[] validCons = new Lincons1[1];
						validCons[0] = new Lincons1(globalenv, true);	// create a valid constraint
						Abstract1 globalMeetEnv = new Abstract1(man, validCons);	// globalMeetEnv is top

						for(String v: globalVars) {
							globalMeetEnv.meet(man, variableWiseJoinedMap.get(v));
						}

						// Expand globalMeetEnv to include locals and val_local to include globals
						globalMeetEnv.changeEnvironment(man, env, false);
						val.getRel().changeEnvironment(man, env, false);

						val.getRel().meet(man, globalMeetEnv);
						val.getTid().addAll(currval.getNewTid());
					
					}
					catch(ApronException e) {
						System.err.println("\nAn Apron Exception occurred while attempting inter-thread merge.");
						System.exit(1);
					}
				}
				// intra-thread widening
				else {
					try {
						val.getRel().wideningOverwrite(man, currval.getRel());
						val.getTid().addAll(currval.getNewTid());
					
					//	val = val.widening(man, currval);
					} catch (ApronException e) {
						// TODO Auto-generated catch block
						System.err.println("\nAn Apron Exception occurred while attempted to widen in merge");
						System.exit(1);
					}
				}
			}
			else {	// region based
				// inter-thread region based widening
				if(edge.getKind() == EdgeKind.MON) {
					try {
						
						// if the incoming flow fact is tagged with the singleton thread-id, which equals the current thread-id, 
						// then we have a stale fact. So do nothing.
						if((val.getTid().size() == 1) && val.getTid().equals(currval.getTid()))
								return;
						
						TData val_copy = new TData(man, val);
						val.getRel().changeEnvironment(man, localenv, false);

						HashMap<String, ArrayList<Abstract1>> regionsToEnv = new HashMap<String, ArrayList<Abstract1>>();

						// project currval and val to each region
						for(String r: regions.keySet()) {
							String[] r_string = new String[regions.get(r).size()];
							int k = 0;
							for(String v: regions.get(r)) {
								r_string[k] = regions.get(r).get(k);
								k++;
							}
							String[] realVars = {"dummy"};
							Environment env_v = new Environment(r_string, realVars);
							env_v.remove(realVars);

							Abstract1 currval_v = currval.getRel().changeEnvironmentCopy(man, env_v, false);
							Abstract1 val_v = val_copy.getRel().changeEnvironmentCopy(man, env_v, false);

							ArrayList<Abstract1> envListsForR = new ArrayList<Abstract1>();
							envListsForR.add(currval_v);
							envListsForR.add(val_v);

							regionsToEnv.put(r, envListsForR);
						}

						// join the currval and val projections for each region r
						HashMap<String, Abstract1> regionWiseJoinedMap = new HashMap<String, Abstract1>();
						for(String r: regions.keySet()) {
							Abstract1 temp = new Abstract1(man, regionsToEnv.get(r).get(0).widening(man, regionsToEnv.get(r).get(1)));
							regionWiseJoinedMap.put(r, temp);
						}

						// Extend the environments of each regionWiseJoinedMap
						for(String r: regions.keySet()) {
							regionWiseJoinedMap.get(r).changeEnvironment(man, globalenv, false);
						}

						Lincons1[] validCons = new Lincons1[1];
						validCons[0] = new Lincons1(globalenv, true);	// create a valid constraint
						Abstract1 globalMeetEnv = new Abstract1(man, validCons);	// globalMeetEnv is top

						for(String r: regions.keySet()) {
							globalMeetEnv.meet(man, regionWiseJoinedMap.get(r));
						}

						// Expand globalMeetEnv to include locals and val_local to include globals
						globalMeetEnv.changeEnvironment(man, env, false);
						val.getRel().changeEnvironment(man, env, false);

						val.getRel().meet(man, globalMeetEnv);
						val.getTid().addAll(currval.getNewTid());
						
					}
					catch(ApronException e) {
						System.err.println("\nAn Apron Exception occurred while attempting inter-thread merge.");
						System.exit(1);
					}
				}
				// intra-thread widening
				else {
					try {
						val.getRel().wideningOverwrite(man, currval.getRel());
						val.getTid().addAll(currval.getNewTid());
						
						//val = val.widening(man, currval);
					} catch (ApronException e) {
						// TODO Auto-generated catch block
						System.err.println("\nAn Apron Exception occurred while attempted to widen in merge");
						System.exit(1);
					}
				}
			}
		}
	}
	
	@Override
	/**
	 * Returns the state at each program point prior to starting the analysis. 
	 * In this case, it is an unsatisfiable set of constraints, which leads to a bottom abstract element.
	 */
	public TData newInitialFlow() {
		//System.out.println("\nInitializing flow facts at program points");
		
		Lincons1[] unsatCons = new Lincons1[1];
		unsatCons[0] = new Lincons1(env, false);	// create an unsat constraint
		TData initAbs = new TData(man, unsatCons);
		
		return initAbs;
	}
	

	/**
	 * Return the environment
	 */
	public Environment getEnv() {
		return env;
	}
	
	/**
	 * Returns the manager
	 */
	public Manager getManager() {
		return man;
	}
	
	/**
	 * Returns the fixed point solution associated with unit u.
	 */
	public TData getFixedPointSol(Unit u) {
		return (TData) this.getResult(u);
	}

	public TData mergeCopy(SyncEdge edge, TData currval, TData val, HashMap<Unit, Integer> unit2CountMap,
			Unit currunit) {
		if(!unit2CountMap.containsKey(currunit))
		{
			System.err.println("\nERROR! Unit " + currunit + " not processed prior to merge");
			return null;
		}
		
		if(unit2CountMap.get(currunit) <= wideningThreshold) {		// less than widening threshold, call standard merge
			merge(edge, currval, val);
		}
		else {		// threshold exceeded, apply widening
			
			if(! useRegions) {	// variable based widening
				// inter-thread variable based widening
					
				if(edge.getKind() == EdgeKind.MON) {
					if((val.getTid().size()==1) && val.getTid().equals(currval.getTid()))
						return val;
					try {
						TData val_copy = new TData(man, val);
						val.getRel().changeEnvironment(man, localenv, false);

						HashMap<String, ArrayList<Abstract1>> globalsToEnv = new HashMap<String, ArrayList<Abstract1>>();

						// project currval and val to each global var
						for(String v: globalVars) {
							String[] v_string = new String[1];
							v_string[0] = v;
							String[] realVars = {"dummy"};
							Environment env_v = new Environment(v_string, realVars);
							env_v.remove(realVars);

							Abstract1 currval_v = currval.getRel().changeEnvironmentCopy(man, env_v, false);
							Abstract1 val_v = val_copy.getRel().changeEnvironmentCopy(man, env_v, false);

							ArrayList<Abstract1> envListsForV = new ArrayList<Abstract1>();
							envListsForV.add(currval_v);
							envListsForV.add(val_v);

							globalsToEnv.put(v, envListsForV);
						}

						// widen the currval and val projections for each global var v
						HashMap<String, Abstract1> variableWiseJoinedMap = new HashMap<String, Abstract1>();
						for(String v: globalVars) {
							Abstract1 temp = new Abstract1(man, globalsToEnv.get(v).get(0).widening(man, globalsToEnv.get(v).get(1)));
							variableWiseJoinedMap.put(v, temp);
						}

						// Extend the environments of each variableWiseJoinedMap
						for(String v: globalVars) {
							variableWiseJoinedMap.get(v).changeEnvironment(man, globalenv, false);
						}

						Lincons1[] validCons = new Lincons1[1];
						validCons[0] = new Lincons1(globalenv, true);	// create a valid constraint
						Abstract1 globalMeetEnv = new Abstract1(man, validCons);	// globalMeetEnv is top

						for(String v: globalVars) {
							globalMeetEnv.meet(man, variableWiseJoinedMap.get(v));
						}

						// Expand globalMeetEnv to include locals and val_local to include globals
						globalMeetEnv.changeEnvironment(man, env, false);
						val.getRel().changeEnvironment(man, env, false);

						val.getRel().meet(man, globalMeetEnv);
						val.getTid().addAll(currval.getNewTid());
					}
					catch(ApronException e) {
						System.err.println("\nAn Apron Exception occurred while attempting inter-thread merge. Subsequent computations may be incorrect");
						System.exit(1);
					}
				}
				// intra-thread widening
				else {
					try {
						val.setRel(man, val.getRel().widening(man, currval.getRel()));
						val.getTid().addAll(currval.getNewTid());
						
					} catch (ApronException e) {
						// TODO Auto-generated catch block
						System.err.println("\nAn Apron Exception occurred while attempted to widen in merge");
						System.exit(1);
					}
				}
			}
			else {	// region based
				// inter-thread region based widening
				if(edge.getKind() == EdgeKind.MON) {
					if((val.getTid().size()==1) && val.getTid().equals(currval.getTid()))
						return val;
					try {
						TData val_copy = new TData(man, val);
						val.getRel().changeEnvironment(man, localenv, false);

						HashMap<String, ArrayList<Abstract1>> regionsToEnv = new HashMap<String, ArrayList<Abstract1>>();

						// project currval and val to each region
						for(String r: regions.keySet()) {
							String[] r_string = new String[regions.get(r).size()];
							int k = 0;
							for(String v: regions.get(r)) {
								r_string[k] = regions.get(r).get(k);
								k++;
							}
							String[] realVars = {"dummy"};
							Environment env_v = new Environment(r_string, realVars);
							env_v.remove(realVars);

							Abstract1 currval_v = currval.getRel().changeEnvironmentCopy(man, env_v, false);
							Abstract1 val_v = val_copy.getRel().changeEnvironmentCopy(man, env_v, false);

							ArrayList<Abstract1> envListsForR = new ArrayList<Abstract1>();
							envListsForR.add(currval_v);
							envListsForR.add(val_v);

							regionsToEnv.put(r, envListsForR);
						}

						// join the currval and val projections for each region r
						HashMap<String, Abstract1> regionWiseJoinedMap = new HashMap<String, Abstract1>();
						for(String r: regions.keySet()) {
							Abstract1 temp = new Abstract1(man, regionsToEnv.get(r).get(0).widening(man, regionsToEnv.get(r).get(1)));
							regionWiseJoinedMap.put(r, temp);
						}

						// Extend the environments of each regionWiseJoinedMap
						for(String r: regions.keySet()) {
							regionWiseJoinedMap.get(r).changeEnvironment(man, globalenv, false);
						}

						Lincons1[] validCons = new Lincons1[1];
						validCons[0] = new Lincons1(globalenv, true);	// create a valid constraint
						Abstract1 globalMeetEnv = new Abstract1(man, validCons);	// globalMeetEnv is top

						for(String r: regions.keySet()) {
							globalMeetEnv.meet(man, regionWiseJoinedMap.get(r));
						}

						// Expand globalMeetEnv to include locals and val_local to include globals
						globalMeetEnv.changeEnvironment(man, env, false);
						val.getRel().changeEnvironment(man, env, false);

						val.getRel().meet(man, globalMeetEnv);
						val.getTid().addAll(currval.getNewTid());
					}
					catch(ApronException e) {
						System.err.println("\nAn Apron Exception occurred while attempting inter-thread merge. Subsequent computations may be incorrect");
						System.exit(1);
					}
				}
				// intra-thread widening
				else {
					try {
						val.setRel(man, val.getRel().widening(man, currval.getRel()));
						val.getTid().addAll(currval.getNewTid());
					
					} catch (ApronException e) {
						// TODO Auto-generated catch block
						System.err.println("\nAn Apron Exception occurred while attempted to widen in merge");
						System.exit(1);
					}
				}
			}
		}
		return val;
		
	}
	
	public HashMap<Unit, Integer> getUnit2CountMap() {
		return super.unit2CountMap;
	}

}
