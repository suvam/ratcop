
# coding: utf-8

# In[1]:

# Python script to generate large programs to stress-test OutStand and Batman
# Suvam Mukherjee, 2017


# In[2]:

def stressBatman() :
    f = open("BatmanTests/fixedvar/BatmanStress_6_8.in", "w")
    n = 48  # num variables
    threads = 8 # num threads
    line='var '
    
    # Define variables
    for i in range(n-1):
        line += 'x'+str(i)+':int, '
    line += 'x'+str(n-1)+':int;\n\n'
    f.write(line)
    
    # Initialize all the variables
    line = 'initial '
    for i in range(n-1):
        line += 'x'+str(i)+'==0 and '
    line += 'x'+str(n-1)+'==0;\n\n'
    f.write(line);
    
    # Construct and write each thread
    index = 0
    for i in range(threads):
        line = 'thread T'+str(i)+':\n'
        line += 'begin\n'
        line += '\t while (true) do\n'
        line += '\t\t x'+str(index)+'='+str(index)+'; \n'
        for j in range(int(n/threads-1)):
            line += '\t\t x'+str(index + j + 1)+' = x'+str(index+j)+' + 1;\n'
        line += '\t done;\n'
        line += 'end\n\n'
        f.write(line)
        index += int(n/threads)
    
    f.close()


# In[3]:

def stressOutStand():
    n = 48  # num variables
    threads = 8 # num threads
    numvars = int(n/threads)
    fname = 'SOS_'+str(numvars)+'_'+str(threads)
    file_name = "src/stress/fixedvar/"+fname+".java"
    f = open(file_name, "w")
    line = '// Auto-generated stress test \n'
    line += '// Suvam Mukherjee, 2017 \n \n'
    line += 'package stress.fixedthr; \n\n'
    
    # Construct and write each worker thread
    index = 0
    for i in range(threads):
        line += 'class W'+str(i)+'_'+fname+' extends Thread { \n'
        line += '\t public void run() {\n'
        line += '\t \t while(true) { \n'
        line += '\t\t\t '+fname+'.x'+str(index)+'='+str(index)+'; \n'
        for j in range(int(n/threads-1)):
            line += '\t\t\t'+fname+'.x'+str(index + j + 1)+' ='+fname+'.x'+str(index+j)+' + 1;\n'
        line += '\t\t }\n' # end while
        line += '\t }\n'  # end run()
        line += '}\n'
        index += int(n/threads)
    
    # Write the main class and method
    line += 'public class '+fname+' { \n'
    line += 'public static int '    
    for i in range(n-1):
        line += 'x'+str(i)+','
    line += 'x'+str(n-1)+';\n\n'
    line += '\t public static void main(String[] args) throws InterruptedException { \n'
    for i in range(n-1):
        line += '\t\t '+fname+'.x'+str(i)+'=0; \n '
    line += '\t\t '+fname+'.x'+str(n-1)+'=0;\n\n'
    
    for i in range(threads):
        line += '\t\t W'+str(i)+'_'+fname+' t'+str(i)+' = new W'+str(i)+'_'+fname+'(); \n'
    for i in range(threads):
        line += '\t\t t'+str(i)+'.start(); \n'
    for i in range(threads):
        line += '\t\t t'+str(i)+'.join(); \n'
    line += '\t }\n'  # end main
    line += '}\n' # end main class
        
    f.write(line)
    f.close()


# In[4]:

stressOutStand()


# In[ ]:



