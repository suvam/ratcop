# Relational Analysis Tool for COncurrent Programs (RATCOP) #

RATCOP is a static analysis tool, based on abstract interpretation, developed by Suvam Mukherjee at the Department of Computer Science and Automation, Indian Institute of Science, Bangalore, India. The theoretical underpinnings of RATCOP is outlined in this [technical report](http://www.csa.iisc.ernet.in/TR/2016/3/) (and is joint work with Oded Padon, Sharon Shoham and Noam Rinetzky at Tel-Aviv University, Israel, and my advisor Deepak D'Souza at Indian Institute of Science).

RATCOP is a tool which can help you automatically prove relational assertions in your sequential Java programs, and concurrent Java programs which are *datarace-free*. It implements a number of static analyses: with different abstract domains (Octagons, Convex Polyhedra, Intervals), with and without "region" definitions, with and without tracking of thread identifiers in the abstract state. In a concurrent setting, you can provide "regions" (groups of variables) which are handled atomically. With the region definitions, RATCOP computes invariants which are more precise, compared to the results of the analyses which don't use the region definitions. The analyses which track thread ids are, in a similar fashion, more precise than their counterparts without thread-id tracking. Note that, with region definitions, your concurrent Java program must additionally be *region-race* free.

This README is designed to give you a quick introduction to the capabilities of the tool, and help you install it on your machine. 

Consider the following simple concurrent Java program:

```
#!java

/**
 * Simple Example program for demonstration purposes.
 */
package functionalTests;

/**
 * @author Suvam Mukherjee.
 *
 * Indian Institute of Science, 2017.
 *
 */

class Worker1_RM extends Thread {
	public void run() {
		ReadMeExample.x1--;
		ReadMeExample.y1++;
		
		if(!(ReadMeExample.x1 < ReadMeExample.y1))	// assertion 1
			ReadMeExample.error = 1;
		
		synchronized(ReadMeExample.lock) {
			ReadMeExample.x2++;
			ReadMeExample.y2++;
			
			if(!(ReadMeExample.x2 == ReadMeExample.y2))	// assertion 2
				ReadMeExample.error = 1;
		}
	}
}

class Worker2_RM extends Thread {
	public void run() {
		
		synchronized(ReadMeExample.lock) {
			ReadMeExample.x2--;
			ReadMeExample.y2--;
			
			if(!(ReadMeExample.x2 == ReadMeExample.y2))	// assertion 3
				ReadMeExample.error = 1;
		}
	}
}

public class ReadMeExample {

	public static int x1, x2, y1, y2, error;  // global integer variables
	public static Object lock;	// shared lock for synchronization
	
	public static void main(String[] args) throws InterruptedException {
		System.out.println("\nSimple Example for Demo");
		ReadMeExample.x1 = -1;
		ReadMeExample.x2 = 0;
		ReadMeExample.y1 = 0;
		ReadMeExample.y2 = 0;
		ReadMeExample.error = 0;
		ReadMeExample.lock = new Object();
		
		Worker1_RM t1 = new Worker1_RM();
		Worker2_RM t2 = new Worker2_RM();
		t1.start();
		t2.start();
		t1.join();
		t2.join();

	}

}

```
The RATCOP analyzer, without region definitions, would prove the assertion 1 above. However, since the variables x2 and y2 are handled atomically, it makes sense to define them as constituting a single region (which behaves as a meta-variable). With this additional information,RATCOP is able to prove all the 3 assertions.

RATCOP is based on [Soot](https://sable.github.io/soot/) analysis framework. It also employs the numerical abstract domains provided by the [Apron](http://apron.cri.ensmp.fr/library/) library. 

Please remember that RATCOP is a research prototype, and thus has its limitations. Currently, RATCOP can only infer relational properties between integer variables. RATCOP does not perform any Escape Analysis to automatically infer shared variables. Thus, all shared variables must be declared as 

```
#!java

public static int x;
```
Lastly, RATCOP only handles critical sections defined by synchronized keyword.


### What is this repository for? ###

* This repository contains most of the sources and jar files needed to get RATCOP running. RATCOP uses the implementation of the chaotic iteration algorithm (SyncAnalysis.java) and graph data structures (stand.util) of the earlier STAND tool developed by Arnab De. The STAND tool implemented the value-set based analyses outlined [here](http://www.csa.iisc.ernet.in/TR/2010/8/dfadrf-full.pdf). 
* I modified the Apron library in order to fix certain issues. RATCOP will fail to run with the standard distribution of Apron.
* The repository contains the Soot jar.

### How do I get set up? ###

**Update: ** If you want to avoid the hassle of setting up, you can now download a virtual machine, with everything set-up. Simply download the OVA file from [here](https://bitbucket.org/suvam/ratcop-vm), import the appliance with Oracle VirtualBox, and you're all set to go!

* In a nutshell, RATCOP requires Eclipse, Soot and Apron in order to run. Apron, on its own, has several dependencies. I have test RATCOP on Ubuntu Linux and Mac OS X. The following instructions pertain to these two operating systems only. If you are able to run RATCOP on Windows, kindly send me an email and I'll add the instructions here (and acknowledge your help!). 
 
* **Step 0: Clone this repository: **: Clone this repository inside your Eclipse workspace. 

```
#!bash
cd <your Eclipse workspace>
git clone https://suvam@bitbucket.org/suvam/ratcop.git
```

* **Step 1: Set-up Apron: ** For the purposes of RATCOP, you need to have GCC, [GMP](https://gmplib.org/) and [MPFR](http://www.mpfr.org/mpfr-current/#download). The usual steps to install these (after downloading and extracting the files) is the following commands:

```
#!bash

./configure
make
sudo make install
```
For GMP, you should configure with the --enable-cxx option


```
#!bash

./configure --enable-cxx
```
Next, you need to:


```
#!bash

cd ratcop/apron/apron-dist/trunk/apron/
```
and edit the Makefile.config file.
The only changes you should make are the flags JAVA_PREFIX (where the jar files will be generated) and JNIINC (the location of your jni.h file).


```
#!bash

JAVA_PREFIX = <path to where jars will be generated>
JNIINC = -I<path to your jni.h file>
```
Save and exit. Now execute


```
#!bash

./configure
make
sudo make install
```
This should generate the jar files in the japron subdirectory.

* **Step 2: Set up Project in Eclipse: ** In Eclipse, ensure that 
```
#!bash

File -> New -> Java Project
```
Name your project ratcop. This will allow Eclipse to automatically pick up the source files. Now right click on the project ratcop, and select Properties. Now select Java Build Path. Under Source, expand ratcop/src, and set the Native library location to 

```
#!bash

/usr/local/lib
```
Now click on Libraries, then Add External Jars, then add the soot-2.5.0.jar (available under ratcop), and the apron.jar and gmp.jar (both created in the japron subdirectory outlined above). Click OK.
Under Run -> Run Configurations, select Environment. Add a new variable called LD_LIBRARY_PATH and make its value /usr/local/lib.

* **Running your first analysis: ** Now you're all set to analyze your first program! There are a lot of test programs provided. For starters, let's analyze the concurrent Java program presented in this article. The program is available under the functionalTests package. The first step is to run the program in order to create the compiled version. Next, select and then Run Configurations. Let's assume we want to run the analysis which tracks thread-ids, and uses the Octagon Abstract domain. We will run two versions of this analysis: once without regions, and the next time providing the regions defined in the write-up above. The necessary arguments include:


```
#!bash

<path_to_rt.jar>:<path_to_jce.jar>:<path_to_class_files> fully_qualified_main_class <path_to_log> <abstract_domain_id>
```
For example, on my machine, I use the following arguments:

```
#!bash

/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/rt.jar:/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/jce.jar:/home/suvam/workspace/ratcop/bin functionalTests.ReadMeExample /home/suvam/workspace/ratcop/logs/log.txt 1
```
1 indicates we want to use the Octagon abstract domain (2 would use Convex Polyhedra, 3 would use Intervals).

Finally, click Run. The tool would prompt for a widening threshold (which you should set to, say, 5), and then would ask if you want to use regions. The first time, select 'n', and observe the output: the tool is able to prove 1 of the 3 assertions. Repeat the process, while selecting 'y' to defining the regions. Then define the regions following the on-screen prompts. Now, the tool would be able to prove all 3 assertions.

### Contribution guidelines ###

* Writing tests: It would be great if you could create a package with tests, in order to test the different functions of RATCOP
* Code review: Provide feedback about the code.

### Who do I talk to? ###

* Repo owner or admin: Suvam Mukherjee
* Email: suvamm@outlook.com
* Please feel free to contact me for any queries or feedback.