/* -*- C++ -*-
 * apxx_texpr1.hh
 *
 * APRON Library / C++ class wrappers
 *
 * Copyright (C) Antoine Mine' 2007
 *
 */
/* This file is part of the APRON Library, released under LGPL license.  
   Please read the COPYING file packaged in the distribution.
*/

#ifndef __APXX_TEXPR1_HH
#define __APXX_TEXPR1_HH

#include "ap_texpr1.h"
#include "apxx_linexpr1.hh"
#include "apxx_texpr0.hh"


namespace apron {

#include "apxx_texpr1_inline.hh"

}

#endif /* __APXX_TEXPR1_HH */
