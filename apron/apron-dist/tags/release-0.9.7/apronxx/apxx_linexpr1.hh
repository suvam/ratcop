/* -*- C++ -*-
 * apxx_linexpr1.hh
 *
 * APRON Library / C++ class wrappers
 *
 * Copyright (C) Antoine Mine' 2007
 *
 */
/* This file is part of the APRON Library, released under LGPL license.  
   Please read the COPYING file packaged in the distribution.
*/

#ifndef __APXX_LINEXPR1_HH
#define __APXX_LINEXPR1_HH

#include "ap_linexpr1.h"
#include "apxx_environment.hh"
#include "apxx_linexpr0.hh"


namespace apron {

#include "apxx_linexpr1_inline.hh"

}

#endif /* __APXX_LINEXPR1_HH */
