/* -*- C++ -*-
 * apxx_lincons1.hh
 *
 * APRON Library / C++ class wrappers
 *
 * Copyright (C) Antoine Mine' 2007
 *
 */
/* This file is part of the APRON Library, released under LGPL license.  
   Please read the COPYING file packaged in the distribution.
*/

#ifndef __APXX_LINCONS1_HH
#define __APXX_LINCONS1_HH

#include "ap_lincons1.h"
#include "apxx_environment.hh"
#include "apxx_lincons0.hh"
#include "apxx_linexpr1.hh"


namespace apron {

#include "apxx_lincons1_inline.hh"

}

#endif /* __APXX_LINCONS1_HH */
