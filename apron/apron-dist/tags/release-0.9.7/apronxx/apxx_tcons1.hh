/* -*- C++ -*-
 * apxx_tcons1.hh
 *
 * APRON Library / C++ class wrappers
 *
 * Copyright (C) Antoine Mine' 2007
 *
 */
/* This file is part of the APRON Library, released under LGPL license.  
   Please read the COPYING file packaged in the distribution.
*/

#ifndef __APXX_TCONS1_HH
#define __APXX_TCONS1_HH

#include "ap_tcons1.h"
#include "apxx_tcons0.hh"
#include "apxx_texpr1.hh"


namespace apron {

#include "apxx_tcons1_inline.hh"

}

#endif /* __APXX_TCONS1_HH */
