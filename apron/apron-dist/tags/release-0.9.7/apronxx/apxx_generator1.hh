/* -*- C++ -*-
 * apxx_generator1.hh
 *
 * APRON Library / C++ class wrappers
 *
 * Copyright (C) Antoine Mine' 2007
 *
 */
/* This file is part of the APRON Library, released under LGPL license.  
   Please read the COPYING file packaged in the distribution.
*/

#ifndef __APXX_GENERATOR1_HH
#define __APXX_GENERATOR1_HH

#include "ap_generator1.h"
#include "apxx_environment.hh"
#include "apxx_generator0.hh"
#include "apxx_linexpr1.hh"

namespace apron {

#include "apxx_generator1_inline.hh"

}

#endif /* __APXX_GENERATOR1_HH */
