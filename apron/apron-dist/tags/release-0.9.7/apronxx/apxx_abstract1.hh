/* -*- C++ -*-
 * apxx_abstract1.hh
 *
 * APRON Library / C++ class wrappers
 *
 * Copyright (C) Antoine Mine' 2007
 *
 */
/* This file is part of the APRON Library, released under LGPL license.  
   Please read the COPYING file packaged in the distribution.
*/

#ifndef __APXX_ABSTRACT1_HH
#define __APXX_ABSTRACT1_HH

#include "ap_abstract1.h"
#include "apxx_abstract0.hh"
#include "apxx_linexpr1.hh"
#include "apxx_lincons1.hh"
#include "apxx_generator1.hh"


namespace apron {

#include "apxx_abstract1_inline.hh"

}

#endif /* __APXX_ABSTRACT1_HH */
