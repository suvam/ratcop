/* -*- mode: c -*- */

/* This file is part of the APRON Library, released under LGPL license.
   Please read the COPYING file packaged in the distribution  */

quote(C, "\n\
#include <limits.h>\n\
#include \"ap_dimension.h\"\n\
#include \"caml/mlvalues.h\"\n\
#include \"apron_caml.h\"\n\
")

typedef unsigned int ap_dim_t;

typedef [mltype("{\n  dim : int array;\n  intdim : int;\n  realdim : int;\n}"),
	 c2ml(camlidl_apron_dimchange_c2ml),
	 ml2c(camlidl_apron_dimchange_ml2c)]
struct ap_dimchange_t ap_dimchange_t;

struct ap_dimperm_t {
  [size_is(size)]unsigned int* dim;
  unsigned int size;
};

struct ap_dimension_t {
  [mlname(intd)]unsigned int intdim;
  [mlname(reald)]unsigned int realdim;
};

quote(MLMLI,"(** APRON Dimensions and related types *)")
