/* -*- mode: c -*- */

/*
 * ppl.idl
 *
 * OCaml interface specification for camlidl
 *
 * APRON Library / PPL library wrapper
 *
 * Copyright (C) Antoine Mine' 2006
 *
 */

/* This file is part of the APRON Library, released under GPL license.
   Please read the COPYING file packaged in the distribution.
*/

quote(C,"/*\n This file is part of the APRON Library, released under GPL license.\n Please read the COPYING file packaged in the distribution.\n*/")
quote(MLMLI,"(*\n This file is part of the APRON Library, released under GPL license.\n Please read the COPYING file packaged in the distribution.\n*)\n")


quote(C,"#include \"ap_ppl.h\"")
quote(C,"#include \"apron_caml.h\"")

quote(C,"#define I0_CHECK_EXC(man) if (man->result.exn!=AP_EXC_NONE){ value v = camlidl_c2ml_manager_struct_ap_exclog_t(man->result.exclog,_ctx); caml_raise_with_arg(*caml_named_value(\"apron exception\"),v); } ")

import "manager.idl";

quote(MLMLI,"(** Convex Polyhedra and Linear Congruences abstract domains (PPL wrapper) *)\n")

quote(MLMLI,"(** This module is a wrapper around the Parma Polyhedra Library. *)\n\n")


quote(MLMLI,"\
type loose\n\
type strict\n\
  (** Two flavors for convex polyhedra: loose or strict.\n\n\
      Loose polyhedra cannot have strict inequality constraints like [x>0].\n\
      They are algorithmically more efficient\n\
      (less generators, simpler normalization).\n\n\
      Convex polyhedra are defined by the conjunction of a set of linear\n\
      constraints of the form [a_0*x_0 + ... + a_n*x_n + b >= 0] or\n\
      [a_0*x_0 + ... + a_n*x_n + b > 0]\n\
      where [a_0, ..., a_n, b, c] are constants and [x_0, ..., x_n] variables.\n\n\
  *)\n\n\
type grid\n\
  (** Linear congruences.\n\n\

      Linear congruences are defined by the conjunction of equality constraints\n\
      modulo a rational number, of the form [a_0*x_0 + ... + a_n*x_n = b mod c],\n\
      where [a_0, ..., a_n, b, c] are constants and [x_0, ..., x_n] variables.\n\
  *)\n\n\
type 'a t\n\
(** Type of convex polyhedra/linear congruences, where ['a] is [loose], [strict] or [grid].\n\
\n\
    Abstract values which are convex polyhedra have the type\n\
    [loose t Apron.AbstractX.t] or [strict t Apron.AbstractX.t].\n\n\
    Abstract values which are conjunction of linear congruences equalities have the type\n\
    [grid t Apron.AbstractX.t].\n\n\
    Managers allocated by PPL have the type ['a t Apron.Manager.t].\n\
*)\n\
")

quote(MLI,"\n\n(** Allocate a PPL manager for loose convex polyhedra. *)")
ap_manager_ptr ap_ppl_manager_alloc_loose()
quote(call,"_res = ap_ppl_poly_manager_alloc(false);");

quote(MLI,"\n\n(** Allocate a PPL manager for strict convex polyhedra. *)")
ap_manager_ptr ap_ppl_manager_alloc_strict()
quote(call,"_res = ap_ppl_poly_manager_alloc(true);");

quote(MLI,"\n\n(** Allocate a new manager for linear congruences (grids) *)")
ap_manager_ptr ap_ppl_manager_alloc_grid()
quote(call,"_res = ap_ppl_grid_manager_alloc();");

quote(MLI,"\n\n(**
{2 Compilation information}

{3 Bytecode compilation}
To compile to bytecode, you should first generate a custom
interpreter with a command which should look like:

[ocamlc -I $APRON_PREFIX/lib -make-runtime -o myrun -cc \"g++\" bigarray.cma gmp.cma apron.cma ppl.cma]

and then you compile and link your example [X.ml] with

[ocamlc -I $APRON_PREFIX/lib -c X.ml] and

[ocamlc -I $APRON_PREFIX/lib -use-runtime myrun -o X bigarray.cma gmp.cma apron.cma ppl.cma X.cmo]

{b Comments:}

Do not forget the [-cc \"g++\"] option: PPL is a C++ library which requires
a C++ linker.

The C libraries related to [gmp.cma], [apron.cma] and [ppl.cma]
are automatically looked for (thanks to the auto-linking feature
provided by [ocamlc]).  Be aware that PPL requires the C++ wrapper
[libgmpxx.a] library on top of GMP library, which should thus be
installed.  With the [-noautolink] option, the generation of the
custom runtime executable should be done with

[ocamlc -I $APRON_PREFIX/lib -noautolink -make-runtime -o myrun -cc \"g++\" 
bigarray.cma gmp.cma apron.cma ppl.cma -ccopt \"-L$GMP_PREFIX/lib ...\" 
-cclib \"-lap_ppl_caml -lap_ppl -lppl -lgmpxx -lapron_caml -lapron -lgmp_caml 
-lmpfr -lgmp -lbigarray -lcamlidl\"]

{3 Native-code compilation}
You compile and link with

[ocamlopt -I $APRON_PREFIX/lib -c X.ml] and

[ocamlopt -I $APRON_PREFIX/lib -o X -cc \"g++\" bigarray.cmxa gmp.cmxa apron.cmxa ppl.cmxa X.cmx]

{b Comments:} Same as for bytecode compilation. Do not forget the [-cc \"g++\"] option. With the [-noautolink] option, the linking command becomes

[ocamlopt -I $APRON_PREFIX/lib -o X -cc \"g++\" bigarray.cmxa gmp.cmxa apron.cmxa ppl.cmxa -ccopt \"-L$GMP_PREFIX/lib ...\" -cclib \"-lap_ppl_caml -lap_ppl -lppl -lgmpxx -lapron_caml -lapron -lgmp_caml -lmpfr -lgmp -lbigarray -lcamlidl\" X.cmx]
*)")
