% -*- mode: LaTeX -*-

% This file is part of the APRON Library, released under LGPL license.
% Please read the COPYING file packaged in the distribution 

%**********************************************************************
\chapter{Choix g�n�raux}
\label{chap:general}
%**********************************************************************

%======================================================================
\section{S�mantique d'une valeur abstraite}
\label{sec:semantique}
%======================================================================

\point{Concr�tisation d'une valeur abstraite.} Une valeur
abstraite fournie par l'interface a pour concr�tisation un
sous-ensemble $X\subseteq \mathbb{N}^p \times \mathbb{R}^q$.  Les
variables sont donc typ�es, soit enti�res soit r�elles.

\point{}. Les probl�mes de pr�cision et donc d'arithm�tique
modulaire pour les entiers 8, 16, 32 et 64 bits sont laiss�s �
un niveau strictement sup�rieur � 1, voir dans l'analyseur
lui-m�me au niveau de la s�mantique concr�te.

%======================================================================
\section{Niveaux d'interface}
\label{sec:niveaux}
%======================================================================

%----------------------------------------------------------------------
\point{Motivations.}
L'objectif n'est pas d'avoir une liste d'op�rateurs minimales,
mais une liste d'op�rateurs comportant les combinaisons
d'op�rateurs de base pouvant �tre simplifi�es ou fr�quemment
utilis�es par un analyseur.

Il s'agit d'assurer � la fois la performance des implantations et
le confort de l'utilisateur, tout en �vitant la duplication de
code entre les diff�rents librairies.

On propose donc de ne traiter au niveau 0 que les probl�mes de
performance (avantage algorithmique fort) et de reporter au
niveau 1 les probl�mes de confort, sachant que les probl�mes de
performance ne sont pas g�n�riques, mais d�pendent des domaines
abstraits.

%----------------------------------------------------------------------
\point{Principe.}
Il a �t� d�cid� d'�tablir diff�rents niveaux d'interface.
\begin{itemize}
\item Le niveau 0 est en prise directe avec la librairie
  sous-jacente (octogones, poly�dres) et suit les principes
  suivants:
  \begin{itemize}
  \item On y place toutes les fonctions dont l'impl�mentation est
    sp�cifique au domaine abstrait et qui ne peuvent donc pas �tre
    partag�es entre les librairies et .
  \item L'interface est minimale: on en exclut les fonctions qui
    peuvent s'impl�menter.� partir d'autres fonctions de niveau 0,
    \textbf{� moins qu'il n'y ait un avantage algorithmique fort}
    pour le laisser au niveau 0.
  \end{itemize}

\item On r�serve aux niveaux sup�rieurs les fonctions
  factorisables pour tous les domaines abstraits envisag�s. Deux
  exemples envisag�s:

  \begin{enumerate}
  \item L'appel automatique des op�rations de redimensionnement et
    de permutations n�cessaires pour calculer l'intersection
    $P1(x,y)$ avec $P2(y,z)$: ceci ne d�pend pas du domaine
    abstrait consid�r�; au pire, dans le cas de repr�sentation
    interne creuses, il n'y a rien � faire.
  \item L'abstraction d'expressions non-lin�aires par des
    expressions lin�aires d'intervalle.
  \end{enumerate}
\end{itemize}
On peut utiliser deux biblioth�ques de niveau 0 pour les combiner de
mani�re heuristique et obtenir une nouvelle biblioth�que de niveau 0.

\aparte{Ci-dessous, points s'y rapportant:}
\begin{small}
\myfi{Peut-on mettre au niveau 0 des fonctions sp�cifiques au domaine
  abstrait, par exemple une pr�-analyse?}

\myfi{Les produits cart�sions pourraient �tre trait�s au niveau 1.}
\end{small}

%======================================================================
\section{Langage de programmation}
\label{sec:langage}
%======================================================================

%----------------------------------------------------------------------
\point{Interface C} On d�finira au moins une version C de
l'interface, qui servira de r�f�rence.

Le langage C a �t� choisi comme s'interfa�ant ais�ment avec la
plupart des langages (C++, Java, OCaml, Prolog, \ldots). C++ est
moins adapt� de ce point de vue. Par ailleurs, la plupart des
librairies existantes sont en C (NewPolka, C3 (!?), octagones).

%----------------------------------------------------------------------
\point{Interface OCaml} L'IRISA et l'ENS sont int�ress� par la
d�finition d'une version OCaml.

\aparte{Dans la suite, on ne discute que de l'interface C.}


%======================================================================
\section{Formes normales des valeurs abstraites}
\label{sec:normal}
%======================================================================

La notion de forme(s) normale(s) a fait
l'objet de vives discussions. Il s'agit de contr�ler
(abstraitement) la repr�sentation interne des valeurs
abstraites. Les besoins suivants ont �t� d�gag�s:

\point{Repr�sentation minimale en terme d'occupation m�moire.}
Cette notion existe dans les octogones (contraintes redondantes
supprim�es). Dans \textsc{NewPolka} cette notion n'est pas
r�ellement fix�e; quelques possibilit�s:
\begin{enumerate}
\item Ensemble de contraintes non redondantes;
\item Ensemble de contraintes \emph{ou} de g�n�rateurs
  non-redondants, selon laquelle occupe le moins d'espace en
  m�moire; c'est la solution correspondant � la d�finition;
\item double repr�sentation + matrice de saturation: c'est la
  seule notion de forme ``normale'' actuellement implant�e dans
  \textsc{NewPolka}.
\end{enumerate} 

Dans les deux cas (octogones ou \textsc{NewPolka}), la forme
minimale peut ne pas �tre adapt�e aux calculs. Ainsi pour les
octogones, la premi�re chose � faire avant la plupart des
op�rations est de calculer la cl�ture de la forme minimale. Il en
serait de m�me (dans une moindre mesure) pour la solution 2 dans
\textsc{NewPolka}.

\point{Repr�sentation canonique.} Il s'agit ici d'avoir une forme
normale telle que deux valeurs s�mantiquement �gales ont
exactement la m�me repr�sentation. 

La forme minimale pr�c�dente ne fournit pas forc�ment une
repr�sentation canonique. Par exemple, pour les poly�dres, il faut
en outre normaliser la repr�sentation de l'espace des �galit�s (ou
des droites) et trier les contraintes (ou g�n�rateurs).

Probl�mes se posent:
\begin{itemize}
\item Que faire lorsque les entiers ne sont que partiellement
  pris en compte ?
\item Plus g�n�ralement, que faire lorsqu'on ne sait pas (ou on ne
  veut pas, pour des raisons de co�t) la calculer ?
\end{itemize}

\point{Notion de r�duction/approximation.}  Dans certains cas, on
veut contr�ler assez finement la repr�sentation interne, car elle
a un impact sur la pr�cision. Il s'agirait ici soit de simplifier
la repr�sentation, au prix �ventuel d'une perte d'information (ex:
on enl�ve des contraintes trop tarabiscot�es, ou avec des
coefficients de magnitude trop importante), soit d'affiner la
repr�sentation (ex de Fran�ois pour la prise en compte des
entiers, r�duction dans le cas d'un domaine abstrait produit
r�duit de deux domaines abstraits) pour am�liorer la pr�cision
d'op�rations futures.

Une telle fonction sera fournie, param�tr�e par un num�ro
d'algorithme. En outre, chaque op�ration sera param�tr�e par la
r�duction/approximation � effectuer en entr�e et en sortie de
l'op�ration. Exemple(s):
\begin{itemize}
\item Dans \textsc{New Polka} existent des versions strictes et
paresseuses des op�rations. La version stricte travaille toujours
sur des (doubles) repr�sentations minimis�es, tandis que les
versions paresseuses ne recourt � Chernikova que si
indispensable. Le param�trage de chaque op�ration permettra de
maintenir ce type de r�glale, sans polluer l'API par
d'innombrables versions de la m�me op�ration s�mantique.
\end{itemize}


%======================================================================
\section{Fonctionnalit�s et architecture g�n�rale de l'interface}
\label{sec:archi}
%======================================================================

%----------------------------------------------------------------------
\point{Compatibilit� avec les threads.}  L'interface permettra
d'�crire des implantations compatibles avec les threads.

%----------------------------------------------------------------------
\point{Gestionnaire.} 
Un contexte d'appel, objet de type [[manager_t*]], sera explicitement pass� �
chaque fonction afin d'assurer les point suivants.
\begin{description}
\item [La transmission de donn�es globales] sp�cifiques � chaque
  librairie (m�moire de travail, options non fournies via
  l'interface commune, \ldots), ce qui assure en particulier la
  compatibilit� avec les threads.
\item [La transmission des options] (leviers de r�glages des
  algorithmes,etc \ldots). En effet, pour certains op�rateurs ou
  m�me pour chaque op�rateur abstrait, il est possible de
  s�lectionner une impl�mentation particuli�re, l'impl�mentation
  par d�faut, l'impl�mentation donnant le r�sultat le plus pr�cis
  ou l'impl�mentation donnant un r�sultat le plus rapidement
  possible. La perte de g�n�ricit� correspondante semble moins
  g�nante que l'absence de cette flexibilit�, au moins pour
  certains op�rateurs particuli�rement sensibles. Les trois
  impl�mentations de base peuvent �tre identiques: l'association
  num�ro vers algorithme peut �tre surjective.
\item [la gestion des exceptions] (r�cup�ration des exceptions); les
  d�bordements en capacit� (entiers), en temps et en espace sont
  d�tect�s, ou tout au moins, les m�canismes n�cessaires � la
  r�cup�ration des incidents sont d�finis. Le retour d'un r�sultat
  math�matiquement exact peut �tre identifi� � la demande de
  l'utilisateur (projection enti�re, union vs enveloppe convexe,
  test de satisfiabilit� en entier,...).
\end{description}
 
Le contexte n'est pas int�gr� aux objets du domaine abstrait pour
�viter d'avoir � faire une v�rification de compatibilit� des
contextes.

C n'offrant pas de m�canisme d'exceptions (sauf via le compliqu�
[[setjmp]]), et celui-ci �tant sp�cifiques � chaque langage (C++,
OCaml, Java), les informations d'exception sont retourn�es
uniquement via la structure [[manager_t]].

Le contenu du contexte d'appel n'est pas visible directement:
c'est un objet opaque, ferm�, avec des m�thodes. Des primitives de
construction et d'observation seront d�finies et fournies.  \bj{Ce
choix pr�sente aussi l'avantage de faciliter l'interfa�age avec un
langage comme \textsc{OCaml}.}

%----------------------------------------------------------------------
\point{Implantations partielles.}  Les implantations partielles
sont accept�es, mais elles doivent offrir toutes les signatures
pr�vues pour permettre l'�dition de lien et l'�chec �ventuellement
en cas d'appel � une fonction non implant�e.

Lancement d'une exception [[not_implemented]] lorsqu'une fonction
n'est pas implant�e, et retour d'une valeur non sp�cifi�e, le cas
�ch�ant (pointeur nul pour l'interface fonctionnelle ?)

%----------------------------------------------------------------------
\point{Gestion m�moire.} Les fonctions doivent �tre clairement
document�es sur la fa�on dont elles g�rent la m�moire. On
n'implante pas de m�canisme automatique de ramasse-miettes
(compteur de r�f�rence ou autre) pour l'interface C.

L'interface OCaml en revanche utilisera les m�canismes du runtime
OCaml (idem pour Prolog ou autre).

%----------------------------------------------------------------------
\point{Signatures fonctionnelles et imp�ratives.}  
Les signatures fonctionnelles et imp�ratives (ou destructives) sont toutes les deux
support�es.

Questions r�solues:
\begin{enumerate}
\item La repr�sentation interne peut �tre chang�e sans pr�avis si
  la s�mantique est conserv�e.
\item En mode destructif, la premi�re valeur abstraite de la
  fonction est d�truite par la fonction, l'utilisateur n'a plus �
  la g�rer et ne doit pas l'utiliser. Seul le r�sultat (et les
  autres arguments) peuvent l'�tre.
\item Le mode destructif permet d'impl�menter un mode effet de bord comme
  suit: [[a = meet(man,destructive=true,a,b);]]: on peut
  consid�rer que l'on fait un effet de brord sur [[a]].
\item Pour les cha�nes de caract�res, on suit les conventions de C
  [[string.h]].
\end{enumerate}

%----------------------------------------------------------------------
\point{Op�rations n-aires.}  Les op�rations n-aires (ex: borne
sup�rieure) sont support�es en utilisant des tableaux d'arguments.
Elles ne sont offertes qu'en version fonctionnelle.



La possibilit� de passer plus de deux arguments n'est pas un
simple confort. Elle a aussi un impact sur la fonctionalit� dans
le cas o� on souhaite faire une union et o� on a donc besoin de
savoir si l'enveloppe convexe lui est �gale. Idem pour les suites
de projections. Enfin, elle a un impact sur la performance.

Le syst�me [[varargs]] ne sera support� que si un des participants
en a le beosin (aucun pour l'instant). \bj{La version
[[vararg]] est implantable en utilisant la version tableau, donc
cela rel�ve plut�t du niveau 1.}

La d�finition d'une structure de liste propre � cette interface
est exclue.

%----------------------------------------------------------------------
\point{Types des objets et mode de passage des param�tres.} 
Quelques rappels sur les idiomes C:
\begin{enumerate}
\item choix entre passage par valeur et par r�f�rence (pointeur):
<<bidon>>=
void toto(object_t o);  /* appel par valeur sur le type object_t */
void toto(object_t* o); /* appel par r�f�rence sur le type object_t */
@ 

Avantage de l'appel par r�f�rence: plus rapide si
[[sizeof(object_t)]] est sensiblement plus grand que la taille
d'un scalaire (4 ou 8 octets). Inconv�nient: casse-bonbon pour le
programmeur de devoir �crire [[toto(&x)]] au lieu de [[toto(x)]].

\item choix similaire pour la valeur retourn�e:
<<bidon>>=
object_t toto();  /* retour par valeur */
object_t* toto(); /* valeur de retour allou�e par toto et � lib�rer par 
                     l'appelant si n�cessaire */ 
@
L�, le retour par r�f�rence n�cessite une allocation, donc plut�t
moins bien point de vue efficacit� !

\item type abstrait en C = type pointeur.
\end{enumerate}
La derni�re remarque justifie la signature suivante dans
l'interface (\cf \sref{sec:operationfun}), \emph{si l'on choisit
un passage par valeur}:
<<bidon>>=
abstract_t* abstract_meet(manager_t* man, abstract_t* a1, abstract_t* a2);
@ 
\noindent
En effet tous les types impliqu�s sont consid�r�s comme abstraits.
Ici donc, on a un passage par valeur sur des types pointeurs,
\emph{et non un passage par r�f�rence sur les types [[manager_t]]
et [[abstract_t]]}. C'est d'une certaine mani�re une question
d'interpr�tation, bien s�r, mais en pratique si tous les objets
sont d�clar�s, construits, manipul�s avec un type [[object_t*]],
il n'y a jamais de d�r�f�rencement � faire et le programmeur n'a
aucune question � se poser (c'est ce qui se passe dans la
librairie de BDDs \textsc{Cudd}).

Maintenant, si le type [[lincons_t]] des contraintes lin�aires
utilis� dans [[abstract0.h]] est abstrait (g�n�r� � partir d'un
type utilisateur, \cf discussion dans \sref{sec:linexpr}), 
les signatures suivantes sont ``impos�es'' (si passage par valeur):
<<bidon>>=
abstract_t* abstract_meet_lincons(manager_t* man, 
                                  abstract_t* a, lincons_t* c);
abstract_t* abstract_meet_lincons_array(manager_t* man, 
                                        abstract_t* a, 
                                        lincons_t** tlincons, int size);
@ 
\noindent
Une contrainte lin�aire �tant de type [[lincons_t*]], les tableaux
de contraintes lin�aires sont de type [[lincons_t**]].

Mais si le type [[lincons_t]] utilis�s dans ces fonctions est
utilisateur (public), on pourrait \emph{aussi} utiliser (toujours
si passage par valeur)
<<bidon>>=
abstract_t* abstract_meet_lincons(manager_t* man, 
                                  abstract_t* a, lincons_t c);
abstract_t* abstract_meet_lincons_array(manager_t* man, 
                                        abstract_t* a, 
                                        lincons_t* tlincons, int size);
@ 

Dans l'interface propos�e, les conventions suivies sont:
\begin{itemize}
\item Appel et retour par valeur.
\item Les types des objets ([[abstract_t*, linexpr_t*, ray_t*]]) 
  sont en g�n�ral des types pointeurs, sauf pour:
  \begin{itemize}
  \item Les nombres ([[coeff_t]]);
  \item Les intervalles ([[interval_t]]),
  \item Les contraintes ([[lincons_t]])
  \end{itemize}
  Ces trois derniers types sont consid�r�s comme des types
  utilisateurs \cf\sref{chap:coeff}, et leur taille (en terme de
  [[sizeof()]]) reste petite (mais ces structures peuvent bien s�r
  contenir des pointeurs vers des zones de taille importante, \eg
  nombres multi-pr�cision).
\item On utilisera les types [[size_t]] (d�fini dans [[stddef.h]])
  et [[dim_t]] (d�fini par [[unsigned int]]) pour typer les
  tailles (de tableaux, par exemple) et les dimensions
  (demande/suggestion de l'ENS)
\end{itemize}


%----------------------------------------------------------------------
\point{Retour d'arguments multiples.}  Quelle politique lorsque
plusieurs arguments doivent �tre retourn�s ? Par exemple, si une
fonction retourne un tableau d'objets de taille non connue �
l'avance, il faut retourner un pointeur sur un tableau plus la
taille du tableau.
 
Essaye-t-on d'unifier l'interface ? En C, lorsque [[toto(x)]] 
doit retourner deux arguments [[a]] et [[b]], on a 3 solutions;
<<bidon>>=
toto(x,&a,&b); /* param�tres r�sultats, pasq tr�s joli; */

a=toto(x,&b);  /* une valeur retourn�e, l'autre en param�tre r�sultat;
                  gu�re plus joli, et tr�s moche si a et b ont une 
                  signification "symm�triques"
                */

struct titi { a_t a; b_t b; };
titi=toto(x); /* creation d'un nouveau type, parfois 
                juste pour une seule fonction */Ah, ces \verb|*%$�&#| de C, C++, Java !
@ 

Au cours de la r�union de fin juin 2005, on a opt� pour la derni�re solution.

Pour l'interface OCaml, qu'on essayera de g�n�rer un peu
automatiquement � partir de l'interface C, � noter que
\textsc{CamlIDL} sait assez bien traiter les param�tres r�sultats,
en les transformant en valeurs retourn�es dans un n-uplet.


%----------------------------------------------------------------------
\point{Repr�sentation interne des nombres.}
Le type num�rique utilis� en interne dans une librairie sous-jacente
est d�fini � la compilation, e.g. via une
option \verb/-D/, et/ou � l'�dition de lien (comme dans PIPS,
type {\em Value, et dans Polka, type {\em pkint}}). 
 
Un type num�rique utilisateur est fourni par l'interface, \cf
\sref{chap:coeff}.


%----------------------------------------------------------------------
\point{Nommage des fonctions.}  On utilisera des pr�fixes pour
distinguer les diff�rentes biblioth�ques en C? Par exemple, OCT
pour octagone, PPL pour Parma, HQ propos� par Duong.  Pr�fixes
envisag�s: POLKA, C3, PIPS, POLYLIB.

Doit-on ensuite utiliser des macros pour rendre g�n�riques ? Ou
utiliser une technique objet (cf ci-dessous).

\bj{Technique objet: on peut inclure dans la structure
  [[manager_t]] des pointeurs sur les fonctions offertes par
  l'interface, et d�finir des fonctions g�n�riques qui utilisent
  [[manager_t]] pour appeler la fonction effective. C'est ce qui
  me semble le plus souple, car en changeant de manager on change
  appel�e.}

<<bidon>>=
abstract_t* meet(manager_t* man, abstract_t* a1, abstract_t* a2)
{
  return manager->abstract_meet(manager,a1,a2);
}
@



%----------------------------------------------------------------------
\point{S�rialisation/d�s�rialisation.}  La
s�rialisation/d�s�rialisation des objets (valeurs abstraites,
contraintes, \ldots) sera fournie par l'interface.

Si la s�rialisation se fait sur les types utilisateurs
(contraintes notamment), alors possibilit� d'�changer entre
plusieurs librairies. Si elle se fait sur un type abstrait, ce
n'est plus le cas (sans compter le probl�me de la repr�sentation
des coefficients dans les types internes, d�pendant d'options de
compilation de la m�me librairie).

%----------------------------------------------------------------------
\point{Conversions et super-treillis.} 
Afin de faciliter les conversions entre valeurs abstraites de
domaines diff�rents, on peut consid�rer un super-treillis et
n'implanter que les conversions d'un domaine abstrait vers ce
super-treillis.

En l'absence d'informations de congruence, les poly�dres convexes
suffisent pour tous les treillis abstraits envisag�s (intervalles,
�galit�s, octogones, templtate constraints, octa�dres, poly�dres
convexes). Toutefois, si on veut ajouter les �galit�s polynomiales
de degr� born� ([Seidl]), ce n'est plus vrai.

Si on rajoute les congruences, il faut plus. Presburger est un peu
candidat, mais quid des r�els ?

Au sujet d'un super-treillis, remarque anonyme: une syntaxe peut
suffire.

