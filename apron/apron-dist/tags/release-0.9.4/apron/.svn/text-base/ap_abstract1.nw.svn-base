% -*- mode: Noweb; noweb-code-mode: c-mode -*-

% This file is part of the APRON Library, released under LGPL license.
% Please read the COPYING file packaged in the distribution 

%**********************************************************************
\chapter{Interface du domaine abstrait, niveau 1: fichier [[ap_abstract1.h]]}
\label{chap:abstract1}
%**********************************************************************

L'interface de niveau 1 � une librairie sous-jacente est
enti�rement g�n�rique: elle se contente de traduire les op�rations
sur des objets de niveau 1 en objets de niveau 0.

<<*>>=
/* ************************************************************************* */
/* ap_abstract1.h: generic operations on abstract values at level 1 */
/* ************************************************************************* */

/* This file is part of the APRON Library, released under LGPL license.  Please
   read the COPYING file packaged in the distribution */

/* GENERATED FROM ap_abstract1.nw: DO NOT MODIFY ! */

#ifndef _AP_ABSTRACT1_H_
#define _AP_ABSTRACT1_H_

#include "ap_manager.h"
#include "ap_abstract0.h"
#include "ap_expr1.h"

#ifdef __cplusplus
extern "C" {
#endif

@

%======================================================================
\section{Type de donn�e et g�n�ralit�s}
\label{sec:abstract1:management}
%======================================================================

%----------------------------------------------------------------------
\point{Valeur abstraite de niveau 1.}

On proc�de comme dans le module [[expr1]]: on rajoute un
environnement, qui permet d'effectuer la liaison entre noms et
dimensions.

<<*>>=
typedef struct ap_abstract1_t {
  ap_abstract0_t* abstract0;
  ap_environment_t* env;
} ap_abstract1_t;
  /* data structure invariant:
     ap_abstract0_integer_dimension(man,abstract0)== env->intdim &&
     ap_abstract0_real_dimension(man,abstract0)== env->realdim */

typedef struct ap_box1_t {
  ap_interval_t** p;
  ap_environment_t* env;
} ap_box1_t;
void ap_box1_fprint(FILE* stream, const ap_box1_t* box);
void ap_box1_clear(ap_box1_t* box);
@

%----------------------------------------------------------------------
\point{Principes suivis.}

Les principales fonctionnalit�s du niveau 1 sont:
\begin{itemize}
\item D'effectuer la conversion des noms vers les dimensions,
\item De redimensionner les valeurs abstraites et les expressions,
  contraintes, \ldots d�finies avec des environnements diff�rents,
  de telle sorte qu'elles soient d�finies sur un environnement
  commun (le plus petit environnement commun, s'il existe; dans le
  cas contraire, une exception [[INVALID_ARGUMENT]] est lanc�e).
\end{itemize}

La politique de redimensionnement choisie est la suivante:
\begin{itemize}
\item Pour les fonctions prenant en argument une valeur abstraite
  et une expression (ou contrainte, ou g�n�rateur, ou tableau de
  \ldots), on suppose que l'environnement de l'expression est un
  sous-environnement de la valeur abstraite (sinon exception). Si
  c'est un sous-environnement strict, on redimensionne l'expression.

  \textbf{Motivation:} d'une part la simplicit�, d'autre part le peu d'utilit� pratique (c'est BJ qui parle).
  Exemples avec une politique plus laxiste (sur des poly�dres d�finis sur $x$ et $y$):
  \begin{enumerate}
  \item $\textsf{ap\_abstract1\_meet\_lincons1}(\{ 1\leq x \leq y\leq 3 \}, z\geq y)$
    donnerait une nouvele valeur abstraite avec $z$ non contraint.
  \item $\textsf{ap\_abstract1\_assign\_linexpr}(\{ 1\leq x \leq y\leq 3 \}, y, x+z)$
    donnerait une nouvele valeur abstraite avec $y$ et $z$ non contraints.
  \end{enumerate}
  Il faut remarquer qu'on peut traiter ces cas � la main, en
  redimensionant explicitement les poly�dres.

\item Pour les fonctions prenant en argument plusieurs valeurs abstraites, on exige qu'elles soient toutes d�finies sur le m�me environnement.

\end{itemize}

Du point de vue de l'efficacit�, le redimmensionnement a un
certain co�t (calculs sur les environnements, allocation et
d�allocation d'objets temporaires).  C'est de la responsabilit� de
l'utilisateur soit d'�tre pr�cis sur les environnements et de ne
pas en changer tout le temps (efficacit�), soit d'�tre moins
attentif au prix d'un certain co�t.

%======================================================================
\section{Gestion m�moire, Repr�sentations, Entr�es/Sorties}
\label{sec:abstract1:management}
%======================================================================

<<*>>=

/* ********************************************************************** */
/* I. General management */
/* ********************************************************************** */
@

%----------------------------------------------------------------------
\point{Gestion m�moire}

<<*>>=

/* ============================================================ */
/* I.1 Memory */
/* ============================================================ */

ap_abstract1_t ap_abstract1_copy(ap_manager_t* man, const ap_abstract1_t* a);
  /* Return a copy of an abstract value, on
     which destructive update does not affect the initial value. */

void ap_abstract1_clear(ap_manager_t* man, ap_abstract1_t* a);
  /* Free all the memory used by the abstract value */

size_t ap_abstract1_size(ap_manager_t* man, const ap_abstract1_t* a);
  /* Return the abstract size of an abstract value (see ap_manager_t) */

@

%----------------------------------------------------------------------
\point{Control of internal representation}

<<*>>=

/* ============================================================ */
/* I.2 Control of internal representation */
/* ============================================================ */

void ap_abstract1_minimize(ap_manager_t* man, const ap_abstract1_t* a);
  /* Minimize the size of the representation of a.
     This may result in a later recomputation of internal information.
  */

void ap_abstract1_canonicalize(ap_manager_t* man, const ap_abstract1_t* a);
  /* Put the abstract value in canonical form. (not yet clear definition) */

void ap_abstract1_approximate(ap_manager_t* man, ap_abstract1_t* a, int algorithm);
  /* Perform some transformation on the abstract value, guided by the
     field algorithm.

     The transformation may lose information.  The argument "algorithm"
     overrides the field algorithm of the structure of type foption_t
     associated to ap_abstract1_approximate (commodity feature). */

tbool_t ap_abstract1_is_minimal(ap_manager_t* man, const ap_abstract1_t* a);
tbool_t ap_abstract1_is_canonical(ap_manager_t* man, const ap_abstract1_t* a);
@

Les propri�t�s de la forme canonique ne sont pas encore bien
claires (\cf \sref{sec:normal}). On aimerait que par exemple la
s�rialisation en binaire d'un objet canonique soit aussi
canonique.

%----------------------------------------------------------------------
\point{Impression.}

<<*>>=

/* ============================================================ */
/* I.3 Printing */
/* ============================================================ */

void ap_abstract1_fprint(FILE* stream,
                    ap_manager_t* man,
                    const ap_abstract1_t* a);
  /* Print the abstract value in a pretty way */

void ap_abstract1_fprintdiff(FILE* stream,
                        ap_manager_t* man,
                        const ap_abstract1_t* a1, const ap_abstract1_t* a2);
  /* Print the difference between a1 (old value) and a2 (new value).
     The meaning of difference is library dependent. */

void ap_abstract1_fdump(FILE* stream, ap_manager_t* man, const ap_abstract1_t* a);
  /* Dump the internal representation of an abstract value,
     for debugging purposes. */

@

%----------------------------------------------------------------------
\point{Pr�cisions sur les fonctions d'impressions.}  Le format des
fonctions d'impression est propre � chaque librairie: aucune
syntaxe n'est impos�e, ne serait-ce que parce que la
repr�sentation interne des nombres n'est pas unifi�e.

Si l'on d�sire une syntaxe uniforme, il faut d'abord convertir en
contraintes/g�n�rateurs utilisateur.

\ref{super-treillis}

%----------------------------------------------------------------------
\point{S�rialisation}

La s�rialisation/d�s�rialisation en binaire, comme d�j� mentionn�,
ne fonctionne que pour la m�me librairie, compil�e avec les m�mes
options de repr�sentation interne des nombres.

Question: doit-on s�rialiser l'environnement associ� ? (BJ: oui)

<<*>>=

/* ============================================================ */
/* I.4 Serialization */
/* ============================================================ */

ap_membuf_t ap_abstract1_serialize_raw(ap_manager_t* man, const ap_abstract1_t* a);
/* Allocate a memory buffer (with malloc), output the abstract value in raw
   binary format to it and return a pointer on the memory buffer and the size
   of bytes written.  It is the user responsability to free the memory
   afterwards (with free). */

                                                                                ap_abstract1_t ap_abstract1_deserialize_raw(ap_manager_t* man, void* ptr, size_t* size);
/* Return the abstract value read in raw binary format from the input stream
   and store in size the number of bytes read */
@


%======================================================================
\section{Constructeurs, accesseurs, tests et extraction de propri�t�s}
\label{sec:constructor}
%======================================================================

%----------------------------------------------------------------------
\point{Basic constructors}

<<*>>=

/* ********************************************************************** */
/* II. Constructor, accessors, tests and property extraction */
/* ********************************************************************** */

/* ============================================================ */
/* II.1 Basic constructors */
/* ============================================================ */

ap_abstract1_t ap_abstract1_bottom(ap_manager_t* man, ap_environment_t* env);
  /* Create a bottom (empty) value defined on the environment */

ap_abstract1_t ap_abstract1_top(ap_manager_t* man, ap_environment_t* env);
  /* Create a top (universe) value defined on the environment */

ap_abstract1_t ap_abstract1_of_box(ap_manager_t* man,
                             ap_environment_t* env,
                             ap_var_t* tvar,
                             ap_interval_t** tinterval,
                             size_t size);
  /* Abstract an hypercube defined by the arrays tvar and tinterval,
     satisfying: forall i, tvar[i] in tinterval[i].

     If no inclusion is specified for a variable in the environement, its value
     is no constrained in the resulting abstract value.
  */

ap_abstract1_t ap_abstract1_of_lincons_array(ap_manager_t* man,
                                       ap_environment_t* env,
                                       const ap_lincons1_array_t* array);
  /* Abstract a convex polyhedra defined on the environment
     by the array of linear constraints
  */
@

%----------------------------------------------------------------------
\point{Accessors}

<<*>>=

/* ============================================================ */
/* II.2 Accessors */
/* ============================================================ */

const ap_environment_t* ap_abstract1_environment(ap_manager_t* man, const ap_abstract1_t* a);
const ap_abstract0_t* ap_abstract1_abstract0(ap_manager_t* man, const ap_abstract1_t* a);
@


%----------------------------------------------------------------------
\point{Tests}

<<*>>=

/* ============================================================ */
/* II.3 Tests */
/* ============================================================ */

/* If any of the following functions returns tbool_top, this means that
   an exception has occured, or that the exact computation was
   considered too expensive to be performed (according to the options).
   The flag exact and best should be cleared in such a case. */

tbool_t ap_abstract1_is_bottom(ap_manager_t* man, const ap_abstract1_t* a);
tbool_t ap_abstract1_is_top(ap_manager_t* man, const ap_abstract1_t* a);

tbool_t ap_abstract1_is_leq(ap_manager_t* man, const ap_abstract1_t* a1, const ap_abstract1_t* a2);
  /* inclusion check */

tbool_t ap_abstract1_is_eq(ap_manager_t* man, const ap_abstract1_t* a1, const ap_abstract1_t* a2);
  /* equality check */

tbool_t ap_abstract1_sat_lincons(ap_manager_t* man, const ap_abstract1_t* a, const ap_lincons1_t* lincons);
  /* does the abstract value satisfy the linear constraint ? */

tbool_t ap_abstract1_sat_interval(ap_manager_t* man, const ap_abstract1_t* a,
                              ap_var_t var, const ap_interval_t* interval);
  /* is the dimension included in the interval in the abstract value ?
     - Raises an exception if var is unknown in the environment of the abstract value
  */
tbool_t ap_abstract1_is_variable_unconstrained(ap_manager_t* man, const ap_abstract1_t* a,
                                             ap_var_t var);
  /* is the variable unconstrained in the abstract value ?
     - Raises an exception if var is unknown in the environment of the abstract value
  */
@

%----------------------------------------------------------------------
\point{Extraction de propri�t�s}

<<*>>=

/* ============================================================ */
/* II.4 Extraction of properties */
/* ============================================================ */

ap_interval_t* ap_abstract1_bound_linexpr(ap_manager_t* man,
                                    const ap_abstract1_t* a, const ap_linexpr1_t* expr);
  /* Returns the interval taken by a linear expression
     over the abstract value */

ap_interval_t* ap_abstract1_bound_variable(ap_manager_t* man,
                                            const ap_abstract1_t* a, ap_var_t var);
  /* Returns the interval taken by the variable
     over the abstract value
     - Raises an exception if var is unknown in the environment of the abstract value
  */

ap_lincons1_array_t ap_abstract1_to_lincons_array(ap_manager_t* man, const ap_abstract1_t* a);
  /* Converts an abstract value to a polyhedra
     (conjunction of linear constraints).
     - The environment of the result is a copy of the environment of the abstract value.
  */

ap_box1_t ap_abstract1_to_box(ap_manager_t* man, const ap_abstract1_t* a);
  /* Converts an abstract value to an interval/hypercube. */

ap_generator1_array_t ap_abstract1_to_generator_array(ap_manager_t* man, const ap_abstract1_t* a);
  /* Converts an abstract value to a system of generators.
     - The environment of the result is a copy of the environment of the abstract value.
 */
@

%======================================================================
\section{Op�rations}
\label{sec:operationfun}
%======================================================================

%----------------------------------------------------------------------
\point{Bornes sup�rieures et inf�rieures}

<<*>>=

/* ********************************************************************** */
/* III. Operations: functional version */
/* ********************************************************************** */

/* ============================================================ */
/* III.1 Meet and Join */
/* ============================================================ */

ap_abstract1_t ap_abstract1_meet(ap_manager_t* man, bool destructive, ap_abstract1_t* a1, const ap_abstract1_t* a2);
ap_abstract1_t ap_abstract1_join(ap_manager_t* man, bool destructive, ap_abstract1_t* a1, const ap_abstract1_t* a2);
  /* Meet and Join of 2 abstract values
     - The environment of the result is the lce of the arguments
     - Raises an EXC_INVALID_ARGUMENT exception if the lce does not exists
  */

ap_abstract1_t ap_abstract1_meet_array(ap_manager_t* man, const ap_abstract1_t* tab, size_t size);
ap_abstract1_t ap_abstract1_join_array(ap_manager_t* man, const ap_abstract1_t* tab, size_t size);
  /* Meet and Join of an array of abstract values.
     - Raises an [[exc_invalid_argument]] exception if [[size==0]]
       (no way to define the dimensionality of the result in such a case
     - The environment of the result is the lce of the arguments
     - Raises an EXC_INVALID_ARGUMENT exception if the lce does not exists
  */

ap_abstract1_t ap_abstract1_meet_lincons_array(ap_manager_t* man,
                                         bool destructive,
                                         ap_abstract1_t* a,
                                         const ap_lincons1_array_t* array);
  /* Meet of an abstract value with a set of constraints
     (generalize ap_abstract1_of_lincons_array) */

ap_abstract1_t ap_abstract1_add_ray_array(ap_manager_t* man,
                                    bool destructive,
                                    ap_abstract1_t* a,
                                    const ap_generator1_array_t* array);
  /* Generalized time elapse operator */
@

%----------------------------------------------------------------------
\point{Affectations et Substitutions.}

<<*>>=

/* ============================================================ */
/* III.2 Assignement and Substitutions */
/* ============================================================ */

/* Assignement and Substitution of a single
   dimension by a interval linear expression */
ap_abstract1_t ap_abstract1_assign_linexpr(ap_manager_t* man,
                                     bool destructive, ap_abstract1_t* a,
                                     ap_var_t var, const ap_linexpr1_t* expr,
                                     const ap_abstract1_t* dest);
ap_abstract1_t ap_abstract1_substitute_linexpr(ap_manager_t* man,
                                         bool destructive, ap_abstract1_t* a,
                                         ap_var_t var, const ap_linexpr1_t* expr,
                                         const ap_abstract1_t* dest);

/* Parallel Assignement and Substitution of several dimensions by
   linear expressons. */
ap_abstract1_t ap_abstract1_assign_linexpr_array(ap_manager_t* man,
                                           bool destructive, ap_abstract1_t* a,
                                           ap_var_t* tvar,
                                           const ap_linexpr1_t* texpr,
                                           size_t size,
                                           const ap_abstract1_t* dest);
ap_abstract1_t ap_abstract1_substitute_linexpr_array(ap_manager_t* man,
                                               bool destructive, ap_abstract1_t* a,
                                               ap_var_t* tvar,
                                               const ap_linexpr1_t* texpr,
                                               size_t size,
                                               const ap_abstract1_t* dest);
@

%----------------------------------------------------------------------
\point{Projections et Quantifications existentielles.}

<<*>>=

/* ============================================================ */
/* III.3 Projections */
/* ============================================================ */

ap_abstract1_t ap_abstract1_forget_array(ap_manager_t* man,
                                   bool destructive, ap_abstract1_t* a, 
                                   ap_var_t* tvar, size_t size,
                                   bool project);
@

%----------------------------------------------------------------------
\point{Changement d'environnement}

<<*>>=

/* ============================================================ */
/* III.4 Change of environnement */
/* ============================================================ */

ap_abstract1_t ap_abstract1_change_environment(ap_manager_t* man,
                                         bool destructive, ap_abstract1_t* a,
                                         ap_environment_t* nenv,
                                         bool project);

ap_abstract1_t ap_abstract1_minimize_environment(ap_manager_t* man,
                                           bool destructive, ap_abstract1_t* a);
  /* Remove from the environment of the abstract value
     variables that are unconstrained in it. */

ap_abstract1_t ap_abstract1_rename_array(ap_manager_t* man,
                                   bool destructive, ap_abstract1_t* a,
                                   ap_var_t* var, ap_var_t* nvar, size_t size);
  /* Parallel renaming. The new variables should not interfere with the variables that are not renamed. */
@

%----------------------------------------------------------------------
\point{Expansion et pliage}

<<*>>=

/* ============================================================ */
/* III.5 Expansion and folding of dimensions */
/* ============================================================ */

ap_abstract1_t ap_abstract1_expand(ap_manager_t* man,
                             bool destructive, ap_abstract1_t* a,
                             ap_var_t var,
                             ap_var_t* tvar, size_t size);
  /* Expand the variable var into itself + the size additional variables 
     of the array tvar, which are given the same type as var.

     It results in (size+1) unrelated variables having same
     relations with other variables.

     The additional variables are added to the environment
     of the argument for making the environment of the result.
  */

ap_abstract1_t ap_abstract1_fold(ap_manager_t* man,
                           bool destructive, ap_abstract1_t* a,
                           ap_var_t* tvar, size_t size);
  /* Fold the dimensions in the array tvar of size n>=1 and put the result
     in the first variable in the array.

     The other variables of the array are then forgot
     and removed from the environment. */

@

%----------------------------------------------------------------------
\point{�largissement.}

<<*>>=

/* ============================================================ */
/* III.6 Widening */
/* ============================================================ */

/* Widening */
ap_abstract1_t ap_abstract1_widening(ap_manager_t* man,
                               const ap_abstract1_t* a1, const ap_abstract1_t* a2);
/* Widening with threshold */
ap_abstract1_t ap_abstract1_widening_threshold(ap_manager_t* man,
                               const ap_abstract1_t* a1, const ap_abstract1_t* a2,
                               const ap_lincons1_array_t* array);

@

%----------------------------------------------------------------------
\point{Cl�ture topologique.}

<<*>>=

/* ============================================================ */
/* III.7 Closure operation */
/* ============================================================ */

/* Returns the topological closure of a possibly opened abstract value */

ap_abstract1_t ap_abstract1_closure(ap_manager_t* man, bool destructive,  ap_abstract1_t* a);
@

<<*>>=
#ifdef __cplusplus
}
#endif

#endif
@
