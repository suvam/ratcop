# $Id$

# This file is part of the APRON Library, released under LGPL
# license.

# Please read the COPYING file packaged in the distribution

This package is an OCAML interface for the APRON library/interface.

The interface is accessed via the Apron module, which is decomposed into 15
submodules, corresponding to C modules:

Scalar     : scalars (numbers)     
Interval   : intervals on scalars 
Coeff      : coefficients (either scalars or intervals)
Dimension  : dimensions and related operations
Linexpr0   : (interval) linear expressions, level 0
Lincons0   : (interval) linear constraints, level 0
Generator0 : generators, level 0
Manager    : managers
Abstract0: : abstract values, level 0
Var        : variables 
Environment: environment binding variables to dimensions
Linexpr1   : (interval) linear expressions, level 1
Lincons1   : interval) linear constraints, level 1
Generator1 : generators, level 1
Abstract1  : abstract values, level 1
Parser     : parsing of expressions, constraints and generators

REQUIREMENTS
============
M4 preprocessor (standard on any UNIX system)
APRON library
GMP library (tested with version 4.0 and up)
mlgmpidl package (included)
OCaml 3.0 or up (tested with 3.09)
Camlidl (tested with 1.05)

INSTALLATION
============

1. Library
----------
Set the file Makefile.config to your own setting.
You might also have to modify the Makefile for executables

type 'make', and then 'make install'

The OCaml part of the library is named apron.cma (.cmxa, .a)
The C part of the library is named libapron_caml.a (libapron_caml_debug.a)

'make install' installs not only .mli, .cmi, but also .idl files.

2. Documentation
----------------

The documentation (currently very sketchy) is generated with ocamldoc.

'make mlgmpidl.dvi'
'make html' (put the HTML files in the html subdirectoy)

3. Miscellaneous
----------------

'make clean' and 'make distclean' have the usual behaviour.

COMPILATION AND LINKING
=======================

To make things clearer, we assume an example file 'example.ml' which uses both
NewPolka (convex polyhedra) and Itv (intervals) libraries, in their versions
where integers (resp. rationals) are GMP integers (resp.  rationals). We assume
that C and OCaml interface and library files are located in directory
$APRON/lib.

The native-code compilation command looks like

ocamlopt -I $APRON/lib -o exampleg.opt bigarray.cmxa gmp.cmxa apron.cmxa itv.cmxa polka.cmxa -cclib "-lpolkag -litvmpq"

Comments:

1. You need at least the libraries 'bigarray' (standard OCaml
  distribution), 'gmp', and 'apron' (standard APRON distribution),
  plus the one implementing an effective abstract domains: here,
  'itv', and 'polka'.

2. The C libraries associated to those OCaml libraries
  ('gmp_caml', 'itv_caml', ...)  are automatically looked for,
  with the exception of the libraries implementing abstract
  domains ('polkag', 'itvmpq'). The reason is that most of these
  libraries may be compiled with different internal
  representations and/or options, while their interface remains
  the same.

The byte-code compilation process looks like

ocamlc -I $APRON/lib -make-runtime -o myrun bigarray.cma gmp.cma apron.cma itv.cma polka.cma -cclib "-lpolkag -litvmpq"
 
ocamlc -I $APRON/lib -use-runtime myrun -o exampleg bigarray.cma gmp.cma apron.cma itv.cma polka.cma -cclib "-lpolkag -litvmpq" example.ml

Comments:
1. One first build a custom bytecode interpreter that includes the
   new native-code needed;
2. One then compile the 'example.ml' file.

The automatic search for C libraries associated to these OCaml
libraries can be disabled by the option '-noautolink' supported by
both 'ocamlc' and 'ocamlopt' commands. For instance, the command
for native-code compilation can alternatively looks like:

ocamlopt -I $APRON/lib -noautolink -o exampleg.opt bigarray.cmxa gmp.cmxa apron.cmxa itv.cmxa polka.cmxa -cclib "-lpolkag -polka_caml -litvmpq -litv_caml -lapron_caml -lapron -lgmp_caml -LGMP/lib -lgmp" 

The option '-verbose' helps to understand what is happening in
case of problem.
