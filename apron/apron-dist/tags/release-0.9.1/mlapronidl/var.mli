
type t = string
val compare : t -> t -> int
val to_string : string -> string
val hash : t -> int
