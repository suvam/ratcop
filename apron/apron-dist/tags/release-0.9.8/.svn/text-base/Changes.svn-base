Version 0.9.8

- Several bugs corrections
- New C++ interface, still experimental 
- Clarification of license issues related to the PPL (under GPL license,
  whereas APRON is LGPL)

Version 0.9.7

Many changes !

General:

- Addition of the reduced product of linear inequalities (from
  Polka library) and linear congruences (from PPL library).

- Addition of tree expressions and constraints, generalizing
  linear expressions and constraints with
  * non-linear operations (multiplication, division, square root, ...)
  * integer, rational/real and floating-point semantics

C interface:

- API:
  * Addition of ap_abstract1_unify function
  * Modification of the signature of ap_environment_remove

- some (redundant) functions removed from the API requested from
  underlying domains, but still provided through the APRON
  interface (level 0 and 1) for compatibility reasons:
  * of_lincons_array
  * assign_linexpr, substitute_linexpr (superseded by
    assign_linexpr_array, substitute_linexpr_array).

- Suffixes of C libraries names indicating internal number
  representation made more systematic and uniform among various
  underlying libraries:

  * Il, Ill, MPZ: denotes resp. long int, long long int, mpz_t (GMP)
  * Rl, Rll, MPQ: denotes resp. rationals using long int, long
                  long int, and mpq_t (GMP)
  * D, Dl: denotes resp. double and long double

  C library name without suffix (eg, libpolka.a) corresponds to
  the default suffix MPQ.

- Other changes in library organisation:

  * libitv.a integrated in libapron.a 
  * libapron_ppl.a renamed in libap_ppl.a,
    libppl_caml.a  renamed in libap_ppl_caml.a 

OCaml interface
- Linking simplified thanks to the change in C libraries (see above).
- API:
  * Addition of Environment.lce, Abstract1.unify (Abstract1.unify_with) functions
  * Modification of the signature of Environment.remove

Version 0.9.6

OCaml interface
- PPL module
- Parsing extended for congruence equalities and congruence generators.

General:
- Addition of PPL *convex polyhedra* and *linear congruences*
  (grids) abstract domains
- Some internal reorganisation
  apron/ap_linearize module added
- Full support for interval linear expressions and linearisation to
  (quasi-)linear expressions in NewPolka and PPL libraries.  Well
  tested for quasilinear expressions, not yet for full interval
  linear expressions.
- Many bugs corrected.

Version 0.9.5

OCaml interface
- Better type systems, similar to the system used by the [Bigarray] 
  library of the OCaml standard distribution.
- Generation and installation in $(APRON_PREFIX)/bin of apronrun
  and aprontop executables in the main Makefile.

C interface:
- Use of const qualifier removed in the API, except for strings
  (const char *)

General:
- mpfr library now required in addition to gmp library.
- conversions between mpq_t and double are now safer, thanks to the use of
  mpfr.
- itv becomes box.
- package num extended with a new package itv, used currently by box.
- support for linear congruences added in constraints and generators.
- NewPolka offers a layer for linear equalities only, in addition to more
  general linear inequalities.
- Improved Makefiles and Makefile.config.model
- Bugs corrected, in particular in Box and Newpolka.

Version 0.9.4

- Modification of Makefile and Makefile.config.model

- Updated documentation

- OCaml linking: apron.cmo, polka.cmo, oct.cmo... become libraries
  apron.cma, polka.cma,... that reference needed C libraries, with
  the exception of libpolkaXX, liboctXX, ... This is documented in
  READMEs and doc.

- mlapronidl: Added string parsing (module Parser)

- newpolka: 

  * Stronger normalization in widening for strict constraints

  * Bug fixed (some of them were severe)

