# This file is part of the APRON Library, released under LGPL license, except
# files requiring the PPL library (in ppl and products subdirectories)

# Please read the COPYING file packaged in the distribution

This main directory contain the following subpackages of the APRON project:

num:
	manipulating "generic" numbers (used by intervals,
        newpolka and possibly octagons).

itv:
	extension of num, manipulating also linear expressions.

apron:
	apron interface, C version

box:
	interval library

octagons:
	octagon library

newpolka:
	convex polyhedra and linear equalities library

ppl:
	wrapper to call the Parma Polyhedra Library from APRON (C & OCaml)
	requires PPL 0.9 and GMPXX
	not compiled unless HAS_PPL is defined in Makefile.config
	under GPL (instead of LGPL) license

products:
	- reduced product of NewPolka convex polyhedra and PPL
	  linear congruences.

mlgmpidl:
	OCaml interface for the GMP and MPFR libraries, used by mlapronidl.
	Requires GMP version 4.2 or up, MPFR 2.2 or up.
	not compiled unless HAS_OCAML is defined in Makefile.config

mlapronidl:
	apron interface, OCaml version (require apron)
	not compiled unless HAS_OCAML is defined in Makefile.config

apronxx:
	apron interface, C++ version
	not compiled unless HAS_CPP is defined in Makefile.config

Makefile.config.model:
	common configuration file model (to be copied to Makefile.config)
	(installation directories mainly)

Makefile:
	compiles as many things as it can

	make all (C + OCaml version if HAS_OCAML)
	make c (C version)
	make ml (OCaml stuff)
	make doc (not useful for a normal user)
	make clean (clean things, with the exception of generated files)
	make mostlyclean (clean all it can clean, implies clean)
	make rebuild (build generated files, mainly for OCaml interface)
	make distclean (clean the distribution directories)
	make install (installs in directories specified in Makefile.config)

Minimal Requirements:
	c: GMP, MPFR
	ml: OCaml, CamlIDL
	c++: GCC 4.1.2
	doc: latex, dvipdf, makeinfo, texi2html, texi2dvi
	rebuild: GNU sed, GNU m4
