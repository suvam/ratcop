#ifndef _T1P_JOINSTD_H_
#define _T1P_JOINSTD_H_

/*
#include "t1p_representation.h"
#include "ap_manager.h"
#include "ap_tcons0.h"
#include "ap_lincons0.h"
#include "ap_generator0.h"
#include "ap_generic.h"
*/


/* 2.Join */
t1p_t* t1p_join_std(ap_manager_t* man, bool destructive, t1p_t* a1, t1p_t* a2);


#endif
