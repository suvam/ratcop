\documentclass[a4paper,10pt]{article}
\usepackage[cyr]{aeguill}
\usepackage[frenchb, english]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{listings}
\usepackage[colorlinks=true,linkcolor=blue,pdfstartview=FitH]{hyperref}
\usepackage[gen]{eurosym}

\title{Multiplication on affine forms using Semidefinite Programming}
\author{MeASI}
\date{\today}

\begin{document}
\maketitle

We use affine forms [\cite{affineArithmetic}] to represent variables. 
Let $x$ be a variable within an interval $\textbf{I}$ (assumed to have finite bounds), its related affine form, $\hat{x}$, is as follow.
$$
\hat{x} = \alpha_0^x + \sum_{i=1}^n{\alpha_i^x\epsilon_i},
$$
where noise symbols, $\epsilon_i$, are assumed to have their unknown values within $[-1,1]$.
Properties below are always satisfied.
$$
\forall x \in \textbf{I}, \exists (\varepsilon_1,\varepsilon_2,\dots{},\varepsilon_n) \in [-1,1]^n | x = \alpha_0^x + \sum_{i=1}^n{\alpha_i^x\varepsilon_i}
$$
$$
\forall (\varepsilon_1,\varepsilon_2,\dots{},\varepsilon_n) \in [-1,1]^n, \alpha_0^x + \sum_{i=1}^n{\alpha_i^x\varepsilon_i} \in \textbf{I}.
$$
%The decomposition is unique if all $\alpha_i$, $i = 1..n $, are non negative.

\paragraph{Multiplication operation}. 
We denote by $\mathcal{A}$ the set of affine forms which can be seen as a direct sum of $\mathbb{R}$ and $\mathbb{R}^n$, that is, $\mathcal{A} = \mathbb{R} \oplus \mathbb{R}^n$.
A variable $x$ in $\mathcal{A}$ is defined by a constant, $\alpha_0^x$, in $\mathbb{R}$ plus a vector, $V^x := (\alpha_1^x,..,\alpha_n^x)^t$ in $\mathbb{R}^n$.

%The multiplication of two elements of $\mathcal{A}$, say $x$ and $y$, is defined by the bilinear form $\phi: \mathcal{A}\times\mathcal{A} \to \mathbb{R}$, $\phi(x,y) \mapsto \alpha_0^x\alpha_0^y + \alpha_0^xV^y + \alpha_0^yV^x + V^x\cdot V^y$, where $V^x\cdot V^y$ is the classical scalar product.
%$$
%\forall \hat{x} \in \mathcal{A} \textrm{ generated by the finite set} (\varepsilon_1,\varepsilon_2,\dots{},\varepsilon_n) \in [-1,1]^n, n \textrm{ is finite}
%$$
Let $x$ and $y$ be two variables lying on \textbf{$I_x$} and \textbf{$I_y$} respectively, let $\hat{x}$ and $\hat{y}$ be their respective affine forms.
Multiplying two affine forms produces a non linear term.
$$
\begin{array}{l l l}
\hat{z} &= \hat{x} * \hat{y} &= (\alpha_0^x + \sum_{i=1}^n{\alpha_i^x\epsilon_i}) * (\alpha_0^y + \sum_{i=1}^n{\alpha_i^y\epsilon_i}) \\
   &    &= \alpha_0^x\alpha_0^y + \sum_{i=1}^n{(\alpha_i^x\alpha_0^y + \alpha_i^y\alpha_0^x)\epsilon_i} + \sum_{i=1}^n\sum_{j=1}^n{\alpha_i^x\alpha_j^y\epsilon_i\epsilon_j} \\
%   &	&= \alpha_0^z + \sum_{i=1}^n{\alpha_i^z\epsilon_i} + \alpha_{n+1}^z\epsilon_{n+1}
\end{array}
$$

The affine form, $\hat{z}$, can be seen as a function of $\epsilon_i$, parameterized by coefficients of $\hat{x}$ and $\hat{y}$.
It is in fact a quadratic form and its related matrix is the kronecker product of $(\alpha_0^x,V^x)$ and $(\alpha_0^y,V^y)$:
$$
q(t) = t. Q_{x,y}. t^t,
$$
$$
t = (1,\epsilon_1,\epsilon_2,\ldots,\epsilon_n)
$$
and,
$$
Q_{x,y} = (\alpha_0^x,\alpha_1^x,\ldots,\alpha_n^x) \otimes \left( \begin{array}{c} \alpha_0^y \\ \alpha_1^y \\ \vdots \\ \alpha_n^y \end{array} \right) = \left( \begin{array}{cccc}
\alpha_0^x\alpha_0^y & \alpha_1^x\alpha_0^y & \ldots & \alpha_n^x\alpha_0^y \\
\alpha_0^x\alpha_1^y & \alpha_1^x\alpha_1^y & \ldots & \alpha_n^x\alpha_1^y \\
\vdots & \vdots & \vdots & \vdots \\ 
\alpha_0^x\alpha_n^y & \alpha_1^x\alpha_n^y & \ldots & \alpha_n^x\alpha_n^y \\
\end{array} \right)
$$ 

We need to define 

\appendix
\begin{description}
\item{\href{http://www.netlib.org/blas}{BLAS}: } The BLAS (Basic Linear Algebra Subprograms) are routines that provide standard building blocks for performing basic vector and matrix operations. The Level 1 BLAS perform scalar, vector and vector-vector operations, the Level 2 BLAS perform matrix-vector operations, and the Level 3 BLAS perform matrix-matrix operations. Because the BLAS are efficient, portable, and widely available, they are commonly used in the development of high quality linear algebra software, LAPACK for example.
\item{\href{http://www.netlib.org/lapack}{LAPACK}: } LAPACK is written in Fortran77 and provides routines for solving systems of simultaneous linear equations, least-squares solutions of linear systems of equations, eigenvalue problems, and singular value problems. The associated matrix factorizations (LU, Cholesky, QR, SVD, Schur, generalized Schur) are also provided, as are related computations such as reordering of the Schur factorizations and estimating condition numbers. Dense and banded matrices are handled, but not general sparse matrices. In all areas, similar functionality is provided for real and complex matrices, in both single and double precision.
\item{\href{http://www.netlib.org/clapack/}{CLAPACK}: } The CLAPACK library was built using a Fortran to C conversion utility called f2c.
The entire Fortran 77 LAPACK library is run through f2c to obtain C code, and then modified to improve readability.  
CLAPACK's goal is to provide LAPACK for someone who does not have access to a Fortran compiler.
\item{\href{http://math-atlas.sourceforge.net}{ATLAS}: } The ATLAS (Automatically Tuned Linear Algebra Software) project is an ongoing research effort focusing on applying empirical techniques in order to provide portable performance. At present, it provides C and Fortran77 interfaces to a portably efficient BLAS implementation, as well as a few routines from LAPACK.
\item{\href{http://www.ti3.tu-harburg.de/Software/PROFILEnglisch.html}{Profil}: } PROFIL (Programmer's Runtime Optimized Fast Interval Library) is a C++ class library supporting the most commonly needed interval and real operations in a user friendly way. PROFIL is based on BIAS (Basic Interval Arithmetic Subroutines). The developement of BIAS was guided by the ideas of BLAS, i.e. to provide an interface for basic vector and matrix operations with specific and fast implementations on various machines, the latter frequently provided by the manufacturers.
\item{SDP solvers: } SDPA (C/C++), SDPT3(Matlab), CSDP(C/C++), etc.
\item{\href{http://infohost.nmt.edu/~sdplib}{SDPLIB}: } SDPLIB is a collection of semidefinite programming test problems. All problems are stored in the SDPA sparse format.
\end{description}
\end{document}

