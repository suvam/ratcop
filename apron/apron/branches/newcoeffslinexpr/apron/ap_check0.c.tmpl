/* -*- mode: c -*- */

/* ************************************************************************* */
/* ap_check0.c.tmpl: checks for level 0 */
/* ************************************************************************* */

/* This file is part of the APRON Library, released under LGPL license.  Please
   read the COPYING file packaged in the distribution */

#include "ap_check0.h"

/* ********************************************************************** */
/* 2 Checking typing w.r.t. manager */
/* ********************************************************************** */

/*
  These functions return true if everything is OK, otherwise they raise an
  exception in the manager and return false.
*/

/* One abstract value */

void ap_check0_man1_raise(ap_funid_t funid, ap_manager_t* man, ap_abstract0_t* a)
{
  char str[160];
  snprintf(
      str,159,
      "The abstract value of type %s is not of the type %s expected by the manager",
      a->man->library,man->library
  );
  ap_manager_raise_exception(man,
			     AP_EXC_INVALID_ARGUMENT,
			     funid,
			     str);
}

/* Two abstract values */
bool ap_check0_man2(ap_funid_t funid,
		    ap_manager_t* man, ap_abstract0_t* a1, ap_abstract0_t* a2)
{
  bool res;
  char str[160];

  res = true;
  if (man->library != a1->man->library){
    snprintf(
	str,159,
	"The first abstract value of type %s is not of the type %s expected by the manager",
	a1->man->library,man->library
    );
    res = false;
  }
  else if (man->library != a2->man->library){
    snprintf(
	str,159,
	"The second abstract value of type %s is not of the type %s expected by the manager",
	a2->man->library,man->library);
    res = false;
  }
  if (!res){
    ap_manager_raise_exception(man,
			       AP_EXC_INVALID_ARGUMENT,
			       funid,
			       str);
  }
  return res;
}
/* Array of abstract values */
bool ap_check0_man_array(ap_funid_t funid,
			 ap_manager_t* man, ap_abstract0_t** tab, size_t size)
{
  size_t i;
  for (i=0;i<size;i++){
    if (man->library != tab[i]->man->library){
      char str[160];
      snprintf(
	  str,159,
	  "The %luth abstract value of the array is of type %s and not of the type %s expected by the manager",
	  (unsigned long)i,tab[i]->man->library,man->library
      );
      ap_manager_raise_exception(man,
				 AP_EXC_INVALID_ARGUMENT,
				 funid,
				 str);
      return false;
    }
  }
  return true;
}

/* ********************************************************************** */
/* 3. Checking compatibility of arguments: abstract values */
/* ********************************************************************** */

/* Check that the 2 abstract values have the same dimensionality */
bool ap_check0_abstract2(ap_funid_t funid, ap_manager_t* man,
			 ap_abstract0_t* a1, ap_abstract0_t* a2)
{
  ap_dimension_t dim1 = _ap_abstract0_dimension(a1);
  ap_dimension_t dim2 = _ap_abstract0_dimension(a2);
  if ( (dim1.intd != dim2.intd) || (dim1.reald != dim2.reald) ){
    char str[160];

    snprintf(str,159,"\
incompatible dimensions for the two arguments:\n        \
first abstract0:  (%3lu,%3lu)\n                         \
second abstract0: (%3lu,%3lu)",
	     (unsigned long)dim1.intd,
	     (unsigned long)dim1.reald,
	     (unsigned long)dim2.intd,
	     (unsigned long)dim2.reald);
    ap_manager_raise_exception(man,
			       AP_EXC_INVALID_ARGUMENT,
			       funid,str);
    return false;
  } else {
    return true;
  }
}

/* Check that the array of abstract values have the same dimensionality.*/
bool ap_check0_abstract_array(ap_funid_t funid, ap_manager_t* man,
			      ap_abstract0_t** tab, size_t size)
{
  size_t i=0;
  ap_dimension_t dim;
  ap_dimension_t dim0;
  bool res;

  res = true;

  if (size==0){
    ap_manager_raise_exception(man,
			       AP_EXC_INVALID_ARGUMENT,
			       funid,"array of abstract values of size 0");
    res = false;
  }
  else {
    dim0 = _ap_abstract0_dimension(tab[0]);
    for (i=1; i<size; i++){
      dim = _ap_abstract0_dimension(tab[i]);
      if ( (dim.intd != dim0.intd) || (dim.reald != dim0.reald) ){
	res = false;
	break;
      }
    }
  }
  if (!res){
    char str[160];
    if (size==0){
      snprintf(str,159,"array of size 0");
    }
    else {
      snprintf(str,159,"\
incompatible dimensions for the array of polyhedra:\n   \
abstract0 0: (%3lu,%3lu)\n				\
abstract0 %lu: (%3lu,%3lu)				\
",
	       (unsigned long)dim0.intd,(unsigned long)dim0.reald,
	       (unsigned long)i,
	       (unsigned long)dim.intd,(unsigned long)dim.reald
      );
    }
    ap_manager_raise_exception(man,
			       AP_EXC_INVALID_ARGUMENT,
			       funid,str);
  }
  return res;
}

/* ********************************************************************** */
/* 4. Checking compatibility of arguments: dimensions */
/* ********************************************************************** */

/* Check that the dimension makes sense in the given dimensionality */
void ap_check0_dim_raise(ap_funid_t funid, ap_manager_t* man,
			 ap_dimension_t dimension, ap_dim_t dim,
			 char* prefix)
{
  char str[160];

  snprintf(str,159,"\
%s:\n						\
abstract0:  (%3lu,%3lu)\n			\
dimension:  %3lu\n",
	   prefix,
	   (unsigned long)dimension.intd,
	   (unsigned long)dimension.reald,
	   (unsigned long)dim);
  ap_manager_raise_exception(man,
			     AP_EXC_INVALID_ARGUMENT,
			     funid,str);
}

/* Check that the array of dimensions make sense in the given dimensionality */
bool ap_check0_dim_array(ap_funid_t funid, ap_manager_t* man,
			 ap_dimension_t dimension, ap_dim_t* tdim, size_t size)
{
  size_t i;
  for (i=0;i<size;i++){
    ap_dim_t dim = tdim[i];
    if (dim>=dimension.intd+dimension.reald){
      char str[80];
      sprintf(str,"incompatible %luth dimension in the array for the abstract value",(unsigned long)i);
      ap_check0_dim_raise(funid,man,dimension,dim,str);
      return false;
    }
  }
  return true;
}

/* ********************************************************************** */
/* 5. Checking compatibility of arguments: expressions */
/* ********************************************************************** */

void ap_check0_expr_raise(ap_funid_t funid, ap_manager_t* man,
			  ap_dimension_t dimension,
			  ap_dim_t dim,
			  char* prefix)
{
  char str[160];
  snprintf(str,159,"\
%s:\n						\
abstract0: (%3lu,%3lu)\n			\
dimension: %3lu					\
",
	   prefix,
	   (unsigned long)dimension.intd,(unsigned long)dimension.reald,
	   (unsigned long)dim);
  ap_manager_raise_exception(man,
			     AP_EXC_INVALID_ARGUMENT,
			     funid,str);
}

/* Check that the linear expression makes sense in the given dimensionality */
MACRO_FOREACH ZZZ @MainNum
ap_dim_t ap_check0_linexprZZZ_dim(ap_dimension_t dimension,
				  ap_linexprZZZ_t expr)
{
  size_t nbdims;
  ap_dim_t dim;

  nbdims = dimension.intd+dimension.reald;
  /* Assumed to be sorted (this is not checked) */
  if (expr->effsize==0)
    dim = AP_DIM_MAX;
  else {
    dim = expr->linterm[expr->effsize-1]->dim;
    if (dim < nbdims)
      dim = AP_DIM_MAX;
  }
  return dim;
}
ENDMACRO

MACRO_FOREACH TTT ("expr","cons");
ap_dim_t ap_check0_linTTT_dim(ap_dimension_t dimension, ap_linTTT0_t a)
{
  MACRO_SWITCH(a->discr) XXX
    return ap_check0_linTTTXXX_dim(dimension,a->linTTT.XXX);
  ENDMACRO;
}
bool ap_check0_linTTT(ap_funid_t funid, ap_manager_t* man,
		      ap_dimension_t dimension,
		      ap_linTTT0_t a)
{
  ap_dim_t dim = ap_check0_linTTT_dim(dimension,a);
  if (dim!=AP_DIM_MAX){
    ap_check0_expr_raise(funid,man,dimension,dim,"incompatible dimension in linear expression/constraint");
    return false;
  }
  return true;
}
ap_dim_t ap_check0_tTTT_dim(ap_dimension_t dimension,
			     ap_tTTT0_t* expr)
{
  ap_dim_t dim;
  dim = ap_tTTT0_max_dim(expr);
  if (dim <= dimension.intd+dimension.reald)
    return AP_DIM_MAX;
  else
    return dim;
}
bool ap_check0_tTTT(ap_funid_t funid, ap_manager_t* man,
		    ap_dimension_t dimension,
		    ap_tTTT0_t* expr)
{
  ap_dim_t dim = ap_check0_tTTT_dim(dimension,expr);
  if (dim!=AP_DIM_MAX){
    ap_check0_expr_raise(funid,man,dimension,dim,"incompatible dimension in the tree expression/constraint");
    return false;
  }
  return true;
}
ENDMACRO

/* ********************************************************************** */
/* 6. Checking compatibility of arguments: array of expressions/constraints/generators */
/* ********************************************************************** */


/* Check that array of linear expr/cons/gen makes sense in the given dimensionality */
MACRO_FOREACH TTT ("expr","cons","gen");
bool ap_check0_linTTT_array(ap_funid_t funid, ap_manager_t* man,
			     ap_dimension_t dimension,
			     ap_linTTT0_array_t array)
{
  size_t i;
  ap_dim_t dim = AP_DIM_MAX;
  MACRO_SWITCH(array->discr) XXX {
    for (i=0;i<array->linTTT_array.XXX->size; i++){
      dim = ap_check0_linTTTXXX_dim(dimension,array->linTTT_array.XXX->p[i]);
      if (dim!=AP_DIM_MAX){
	break;
      }
    }
  }
  ENDMACRO;
  if (dim!=AP_DIM_MAX){
    char str[80];
    sprintf(str,"incompatible dimension in the %luth expression/constraint/generator of the array",(unsigned long)i);
    ap_check0_expr_raise(funid,man,dimension,dim,str);
    return false;
  }
  return true;
}
ENDMACRO

/* Check that array of tree expr/cons makes sense in the given dimensionality */
MACRO_FOREACH TTT ("expr","cons");
bool ap_check0_tTTT_array(ap_funid_t funid, ap_manager_t* man,
			   ap_dimension_t dimension,
			   ap_tTTT0_array_t* array)
{
  size_t i;

  for (i=0;i<array->size; i++){
    if (array->p[i]==NULL){
      char str[80];
      sprintf(str,"null pointer in the %luth expression/constraint of the array",(unsigned long)i);
      ap_manager_raise_exception(man,
				 AP_EXC_INVALID_ARGUMENT,
				 funid,str);
      return false;
    }
    ap_dim_t dim = ap_check0_tTTT_dim(dimension,array->p[i]);
    if (dim!=AP_DIM_MAX){
      char str[80];
      sprintf(str,"incompatible dimension in the %luth expression/constraint of the array",(unsigned long)i);
      ap_check0_expr_raise(funid,man,dimension,dim,str);
      return false;
    }
  }
  return true;
}
ENDMACRO

/* ********************************************************************** */
/* 7. Checking compatibility of arguments: dimchange */
/* ********************************************************************** */

bool ap_check0_dimchange_add(ap_funid_t funid, ap_manager_t* man,
			     ap_dimension_t dimension, ap_dimchange_t* dimchange)
{
  size_t i,size;
  bool res = true;
  /* Check consistency between intdim and the dimensions in the array */
  for (i=0; i<dimchange->dim.intd; i++){
    if (dimchange->p[i]>dimension.intd){
      res = false;
      break;
    }
  }
  size = dimchange->dim.intd+dimchange->dim.reald;
  if (res && size>0){
    /* Check sortedness */
    ap_dim_t dim = 0;
    for (i=1; i<size; i++){
      if (dim>dimchange->p[i]){
	res = false;
	break;
      } else {
	dim = dimchange->p[i];
      }
    }
    res = res && dimchange->p[size-1]<=dimension.intd+dimension.reald;
  }
  if (!res){
    ap_manager_raise_exception(man,
			       AP_EXC_INVALID_ARGUMENT,
			       funid,
			       "dimchange->dim is not sorted or is inconsistent wrt dimchange->dim.intd or abstract0");
  }
  return res;
}

bool ap_check0_dimchange_remove(ap_funid_t funid, ap_manager_t* man,
				ap_dimension_t dimension, ap_dimchange_t* dimchange)
{
  size_t i,size;
  bool res = true;
  /* Check consistency between intdim and the dimensions in the array */
  for (i=0; i<dimchange->dim.intd; i++){
    if (dimchange->p[i]>=dimension.intd){
      res = false;
      break;
    }
  }
  size = dimchange->dim.intd+dimchange->dim.reald;
  if (res && size>0){
    /* Check sortedness */
    ap_dim_t dim = 0;
    for (i=1; i<dimchange->dim.intd+dimchange->dim.reald; i++){
      if (dim>=dimchange->p[i]){
	res = false;
	break;
      } else {
	dim = dimchange->p[i];
      }
    }
    res = res && dimchange->p[size-1]<dimension.intd+dimension.reald;
  }
  if (!res){
    ap_manager_raise_exception(man,
			       AP_EXC_INVALID_ARGUMENT,
			       funid,
			       "dimchange->dim is not sorted, contains duplicatas, or is inconsistent wrt dimchange->dim.intd or abstract0");
  }
  return res;
}
bool ap_check0_dimperm(ap_funid_t funid, ap_manager_t* man,
		       ap_dimension_t dimension, ap_dimperm_t* perm)
{
  size_t i;
  size_t size = dimension.intd+dimension.reald;
  bool res = (dimension.intd+dimension.reald==perm->size);
  if (res){
    for (i=0; i<perm->size; i++){
      if (perm->p[i]>=size){
	res = false;
	break;
      }
    }
  }
  if (!res){
    ap_manager_raise_exception(man,
			       AP_EXC_INVALID_ARGUMENT,
			       funid,
			       "permutation is not valid");
  }
  return res;
}
